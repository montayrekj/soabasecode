$(document).ready(function() {	
 	var training_id;
 	var plan_id = $('#plan_id').text().trim();
 	var text = $('#head').text();
 	var year = text.substring(0,4);
 	var date_created = new Date($('#dateCreated').text());
 	//Get the current date
 	var today = new Date();
 	var dd = today.getDate();
 	var mm = today.getMonth()+1; //January is 0!
 	var yyyy = today.getFullYear();
 		
 	if(dd<10) {
 	    dd='0'+dd
 	} 

 	if(mm<10) {
 	    mm='0'+mm
 	} 

 	today = year +'-'+ mm + '-' + dd;
 	var restriction = (yyyy == date_created.getFullYear())?true:false;
	var calendar = $('#calendar').fullCalendar({
		header: {
			left: 'prev,next',
			center: 'title',
			right: 'listWeek, month'
		},
		viewRender: function(view,element) {
			var start = new Date(year + '-01-01');
		    var end = new Date(year + '-12-31');
            if ( view.end >= end) {
                $("#calendar .fc-next-button").hide();
                return false;
            }
            else {
                $("#calendar .fc-next-button").show();
            }

            if ( view.start <= start) {
                $("#calendar .fc-prev-button").hide();
                return false;
            }
            else {
                $("#calendar .fc-prev-button").show();
            }
        },
		defaultDate: today,
		selectable: restriction,
		views: {
			listWeek: { buttonText: 'List View' }
		},
		fixedWeekCount: false,
		select: function(start, end) {
			var eventData;
			$('#training_form')[0].reset();
			$('.label').hide();
			$('#date_start').html(new Date(start));
			$('#training_modal').modal('show');
			$.ajax({
				url: "/SoaBaseCode/admin/generateTrainingId",
				success: function(responseData){
					training_id = responseData;
				},	
				error: function(){
					alert("error1")
				}
			});
			$('#add_training').unbind('click').click(function(){
				var t_title = $('#title').val();
				var color = $("#showPaletteOnly").spectrum('get');
				if(validate(t_title, start)){
					var endDate = new Date($('#end').val());
					var color = $('#showPaletteOnly').spectrum('get').toName();
					switch(color){
						case 'red':training_id+=(1*10000);break;
						case 'green':training_id+=(2*10000);break;
						case 'blue':training_id+=(3*10000);break;
						case 'yellow':training_id+=(4*10000);break;
						case 'orange':training_id+=(5*10000);break;
					}
					eventData = {
						id: training_id,
						title: t_title,
						start: start,
						color: color,
						end: moment(endDate).add(1 ,'days'),
						allDay: true,
						description: 'No course outline specified.',
						url: "/SoaBaseCode/trainings/trainingInfo?training_id=" + training_id

					};
					$.ajax({
						url: "/SoaBaseCode/admin/newtraining",
						type: 'POST',
						data: {
							"trainingId" : training_id,
							"trainingTitle" : eventData.title,
							"dateStart" : convertDate(new Date(start)),
							"dateEnd" : convertDate(endDate),
							"annualPlan" : plan_id
						},
						success: function(){
							$('#training_form')[0].reset();
							$('#calendar').fullCalendar('renderEvent', eventData, true);
							$('#training_modal').modal('hide');
						},
						error: function(){
							alert("error2")
						}
					});
					$('#calendar').fullCalendar('unselect');
				}
			});
			$('#calendar').fullCalendar('unselect');
		},
		editable: restriction,
		eventLimit: true, // allow "more" link when too many events
		eventClick:  function(event, jsEvent, view) {
			$('#modalId').html(event.id);
            $('#modalTitle').html(event.title);
            $('#modalBody').html(event.description);
            $('#eventUrl').attr('href',event.url);
            $('#fullCalModal').modal();
            return false;
        },
        eventRender: function(event, element) {
            $(element).tooltip({title: "Click me!"});             
        },
		events: function(start, end, timezone, callback){
			var events = [];
			$.ajax({
				url: '/SoaBaseCode/admin/trainingevents',
				type: 'get',
				data: {
					"planId" : plan_id
				},
				success: function(responseData){
					object = responseData;
					var ecolor;
					for(var x = 0;x < object.length;x++){
						var f = Math.floor((parseInt(object[x].trainingId)%1000000)/10000);
						var desc = null!=object[x].outline?object[x].outline:'No course outline specified.';
						switch(f){
							case 1:ecolor='red';break;
							case 2:ecolor='green';break;
							case 3:ecolor='blue';break;
							case 4:ecolor='yellow';break;
							case 5:ecolor='orange';break;
						}
						events.push({
							id: object[x].trainingId,
							title: object[x].trainingTitle,
							start: object[x].dateStart,
							end: moment(object[x].dateEnd).add(1 , 'days'),
							color: ecolor,
							allDay: true,
							description: desc,
							url: "/SoaBaseCode/trainings/trainingInfo?training_id=" + object[x].trainingId
						});
					}
					callback(events);
				},
				error: function(){
					alert('error3');
				}
			});
		},
		eventDrop: function(event, delta, revertFunc){
			var newEnd = moment(event.end.format()).subtract(1,'days').format('YYYY-MM-DD');
	        if (!confirm("Are you sure you want to move " + event.title + " to " + event.start.format() + " ?")) {
	            revertFunc();
	        }else{
	        	$.ajax({
	        		url: '/SoaBaseCode/admin/updatetraining',
	        		type: 'post',
	        		data: {
	        			'trainingId' : event.id,
	        			'trainingTitle' : event.title,
	        			'dateStart' : event.start.format('YYYY-MM-DD'),
	        			'dateEnd' : newEnd
	        		},
	        		error: function(){
	        			revertFunc();
	        		}
	        	})
	        	.done(function(){
	        		$.ajax({
	        			url: '/SoaBaseCode/trainings/populateAttendance',
	        			type: 'post',
	        			data: {
	        				trainingId : event.id
	        			}
	        		});
	        	});
	        }
		},
		eventResize: function(event, delta, revertFunc){
			var newEnd = moment(event.end.format()).subtract(1,'days').format('YYYY-MM-DD');
	        if (!confirm("Are you sure you want to extend " + event.title + " to " + newEnd + " ?")) {
	            revertFunc();
	        }else{
	        	$.ajax({
	        		url: '/SoaBaseCode/admin/updatetraining',
	        		type: 'post',
	        		data: {
	        			'trainingId' : event.id,
	        			'trainingTitle' : event.title,
	        			'dateStart' : event.start.format('YYYY-MM-DD'),
	        			'dateEnd' : newEnd
	        		},
	        		error: function(){
	        			revertFunc();
	        		}
	        	})
	        	.done(function(){
	        		$.ajax({
	        			url: '/SoaBaseCode/trainings/populateAttendance',
	        			type: 'post',
	        			data: {
	        				trainingId : event.id
	        			}
	        		});
	        	});
	        }
		}
	});
	$('#delete-event').unbind('click').click(function(){
		var id = $('#modalId').html();
		if(confirm('Deleting this event might result to deletion of other records related to this event.Do you wish to continue?')){
        	$.ajax({
            	url: '/SoaBaseCode/trainings/deleteTraining',
            	type: 'post',
            	data: {
            		'trainingId': id
            	},
            	success: function(responseData){
            		if(true == responseData){
            			$('#calendar').fullCalendar('removeEvents', id);
            		}
            		else{
            			alert('Failed to delete event.');
            		}
            	},
            	error: function(){
            		alert('An error occured while processing your request.');
            	},
            	complete: function(){
            		$('#fullCalModal').modal('hide');
            	}
            });
        }
	});
	$("#showPaletteOnly").spectrum({
	    showPaletteOnly: true,
	    showPalette:true,
	    color: 'red',
	    palette: [
	              ['red','blue','green','yellow','orange']
	          ]
	});
	$('input').focus(function(){
		$('.label').fadeOut(300);
	});
	function validate(title, start){
		var ret = true;
		var end;
		if(title.trim().length == 0){
			$('#title_err').fadeIn(300);
			ret = false;
		}
		var end = $('#end').val();
		if(end.trim().length > 0){
			if(new Date(end) < new Date(start)){
				$('#date_err').fadeIn(300);
				ret = false
			}
		}else{
			$('#date_err').fadeIn(300);
			ret = false;
		}
		
		return ret;
	}
	function convertDate(date){
		var year = date.getFullYear();
		var month = date.getMonth() + 1;
		var date = date.getDate();
		return year + '-' + month + '-' + date;
	}
});
