$(document).ready(function() {
	var dest = 1;
	var trainingId = $('#det_id').text();
	var frm_id;
	var uId;
	var prompt = $('#prompt-tab').DataTable({
		'dom': 'tip'
	});
	var master = $('#master_tab').DataTable({
		"ajax": {
			'type' : 'get',
			'data' : {
				'training_id' : trainingId
			},
			'url' : '/SoaBaseCode/trainings/getAvailUsers',
			'dataSrc' : function(responseData){
				return responseData;
			}
		},
		"createdRow": function ( row, data, index ) {
            $(row).addClass('user_row');
        },
		"columns" : [ { data:   "userId",
					    render: function ( data, type, row ) {
					  		if ( type === 'display' ) {
					        	return '<input type="checkbox" class="user" value="'+ data + '">';
					    	}
					    	return data;
					  	} 
					 },
		             {"data" : "firstName"},
		             {"data" : "lastName"},
		             {"data" : "userType"} ],
		"columnDefs" : [ {
			"targets" : "no-sort",
			"orderable" : false
		} ]
	});
	master.on( 'xhr', function ( e, settings, json, xhr ) {
		if ( json === null ) {
			master.clear().draw();
	  		return true
	  	}
	});
	var faci = $('#facilitators').DataTable({
		"ajax": {
			'type' : 'get',
			'data' : {
				'training_id' : trainingId
			},
			'url' : '/SoaBaseCode/trainings/getAllFacilitators',
			'dataSrc' : function(responseData){
				return responseData;
			}
		},
		"createdRow": function ( row, data, index ) {
            $(row).addClass('user_row');
        },
		"columns" : [ { data:   "userId",
		                render: function ( data, type, row ) {
	                    	if ( type === 'display' ) {
	                        	return '<input type="checkbox" class="faci" value="' + data +'">';
	                     	}
	                    return data;
		                }		             
		             },
		             {data : "firstName",
		              createdCell: function (td, cellData, rowData, row, col){
		            	  			$(td).attr('id','fname');
		              			}
					 },
		             {data : "lastName",
		              createdCell: function (td, cellData, rowData, row, col){
		            	  			$(td).attr('id','lname');
		              			}
					 },
		             {"data" : "userType"} ],
		"columnDefs" : [ {
			"targets" : "no-sort",
			"orderable" : false
		} ]
	});
	faci.on( 'xhr', function ( e, settings, json, xhr ) {
	 	if ( json === null ) {
			faci.clear().draw();
	  		return true
	  	}
	});
	var parti = $('#participants').DataTable({
		"ajax": {
			'type' : 'get',
			'data' : {
				'training_id' : trainingId
			},
			'url' : '/SoaBaseCode/trainings/getAllParticipants',
			'dataSrc' : function(responseData){
				return responseData;
			}
		},
		"createdRow": function ( row, data, index ) {
            $(row).addClass('user_row');
        },
		"columns" : [ { data:   "userId",
					    render: function ( data, type, row ) {
					        if ( type === 'display' ) {
					            return '<input type="checkbox" class="parti" value="'+ data + '">';
					        }
					        return data;
					    }
		             },
		             {data : "firstName",
		              createdCell: function (td, cellData, rowData, row, col){
		            	  			$(td).attr('id','fname');
		              			}
					 },
		             {data : "lastName",
		              createdCell: function (td, cellData, rowData, row, col){
		            	  			$(td).attr('id','lname');
		              			}
					 },
		             {"data" : "userType"}
		             ],
		"columnDefs" : [ {
			"targets" : "no-sort",
			"orderable" : false
		} ]
	});
	parti.on( 'xhr', function ( e, settings, json, xhr ) {
		if ( json === null ) {
			parti.clear().draw();
	    	return true;
		}
	});
	var frm = $('#frm_tab').DataTable({
		"columnDefs" : [ {
			"targets" : "no-sort",
			"orderable" : false
		} ]
	});
	var asgn = $('#assign_tab').DataTable({
		"ajax": {
			'type' : 'get',
			'data' : {
				'training_id' : trainingId
			},
			'url' : '/SoaBaseCode/trainings/getAllParticipants',
			'dataSrc' : function(responseData){
				return responseData;
			}
		},
		"createdRow": function ( row, data, index ) {
            $(row).addClass('user_row');
        },
		"columns" : [ { data:   "userId",
					    render: function ( data, type, row ) {
					  		if ( type === 'display' ) {
					        	return '<input type="checkbox" class="user" value="'+ data + '">';
					    	}
					    	return data;
					  	} 
					 },
		             {"data" : "firstName"},
		             {"data" : "lastName"},
		             {"data" : "userType"} ],
		"columnDefs" : [ {
			"targets" : "no-sort",
			"orderable" : false
		} ]
	});
	asgn.on( 'xhr', function ( e, settings, json, xhr ) {
		if ( json === null ) {
			parti.clear().draw();
	    	return true;
		}
	});
	
	var asgn2 = $('#assign_tab2').DataTable({
		"ajax": {
			'type' : 'get',
			'data' : {
				'training_id' : trainingId
			},
			'url' : '/SoaBaseCode/trainings/getAllFacilitators',
			'dataSrc' : function(responseData){
				return responseData;
			}
		},
		"createdRow": function ( row, data, index ) {
            $(row).addClass('user_row');
        },
		"columns" : [ { data:   "userId",
					    render: function ( data, type, row ) {
					  		if ( type === 'display' ) {
					        	return '<input type="checkbox" class="user" value="'+ data + '">';
					    	}
					    	return data;
					  	} 
					 },
		             {"data" : "firstName"},
		             {"data" : "lastName"},
		             {"data" : "userType"} ],
		"columnDefs" : [ {
			"targets" : "no-sort",
			"orderable" : false
		} ]
	});
	asgn2.on( 'xhr', function ( e, settings, json, xhr ) {
		if ( json === null ) {
			parti.clear().draw();
	    	return true;
		}
	});
	
	$('.sorting').click();
	$('#edit').click(function() {
		$('#edit_form')[0].reset();
		$('.label').hide();
		$('.feedback').hide();
		if(undefined != $('#det_obj').find('li').html()){
			$('#objective_div').html($('#objective_div').html());
		}else{
			$('#objective_div').html('');
		}
	});
	$('#add_faci').click(function(){
		if(faci.data().count()){
			alert('You have already assigned a facilitator for this training.');
		}else{
			dest = 1;
			$('#master_list').modal('show');
			$('.user').click(function(){
				if( dest == 1 && $(this).is(':checked') ){
					$('.user').attr('disabled',true);
					$(this).attr('disabled',false);
				}else{
					$('.user').attr('disabled',false);
				}
			});
		}
	});
	$('#add_parti').click(function(){
		dest = 0;
		$('#master_list').modal('show');
	});
	$('#add_user').unbind('click').click(function(){
		var  uids = [];
		$('.user:checkbox:checked').each(function(){
			uids.push($(this).val());
		})
		if(dest == 1){
			$.ajax({
				url: '/SoaBaseCode/trainings/addFacilitator',
				type: 'post',
				data: {
					'faciId' : uids[0],
					'trainingId': trainingId
				},
				success: function(responseData){
					faci.ajax.reload( null, false );
					master.ajax.reload( null, false );
					frm.ajax.reload();
					$('#master_list').modal('hide');
					$('#faci-feedback').html('<b>New Facilitator assigned.</b>');
					$('#faci-feedback').fadeIn(200).delay(1000).fadeOut(300);
					alert("${training.skAssessFrm}");
				},
				error: function(){
					alert('error');
				}
			});
		}else{
			$('#att_list').attr('disabled',true);
			$.ajax({
				url: '/SoaBaseCode/trainings/addParticipant',
				type: 'post',
				data: {
					'uids' : uids,
					'trainingId': trainingId,
				},
				success: function(responseData){
					parti.ajax.reload( null, false );
					asgn.ajax.reload(null, false);
					master.ajax.reload( null, false );
					$('#parti-feedback').html('<b>'+ responseData + ' new participant(s) added</b>');
					$('#master_list').modal('hide');
					$('#parti-feedback').fadeIn(200).delay(1000).fadeOut(300);
				},
				error: function(){
					alert('error');
				}
			})
			.done(function(responseData){
				if(responseData > 0){
					$.ajax({
						url: '/SoaBaseCode/trainings/populateAttendance',
						type: 'post',
						data: {
							'trainingId' : trainingId
						}
					})
					.done(function(){
						$('#att_list').attr('disabled',false);
					});
				}
			});
		}
	});
	$('#del-training').click(function(){
		var id = $(this).val();
		$('.panel-tr').html('');
		$('#tr-feedback').html('');
		$('#tr-affiliated').hide();
		$('#buttons').show();
		var dataCount = faci.data().count() + parti.data().count();
		faci.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
			var fname = this.data().firstName;
			var lname = this.data().lastName;
			var span = '<a id="item" class="list-group-item" style="border:none">' +
			'<p class="list-group-item-text">Facilitator: '+ 
			fname + '&nbsp;'+lname+'</p></a>';
			$('.panel-tr').append(span);
		});
		parti.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
			var fname = this.data().firstName;
			var lname = this.data().lastName;
			var span = '<a id="item" class="list-group-item" style="border:none">' +
			'<p class="list-group-item-text">Participant: '+ 
			fname + '&nbsp;'+lname+'</p></a>';
			$('.panel-tr').append(span);
		});
		if(dataCount > 0){
			$('.tr-err').show();
			$('#tr-affiliated').show();
		}else{
			$('.tr-err').hide();
			$('#tr-affiliated').hide();
		}
		$('#prompt-delete-tr').modal('show');
		$('#tr-proceed').click(function(){
			console.log(id)
			$('#buttons').hide();
			$.ajax({
				url: '/SoaBaseCode/trainings/deleteTraining',
				type: 'post',
				data: {
					'trainingId': trainingId
				},
				success: function(responseData){
					if(responseData){
						window.history.back();
					}else{
						$('#tr-feedback').html('<span style="color:#ff8b8e"><b>Failed to delete record</b></span>');
					}
				},
				error: function(){
					$('#tr-feedback').html('<span style="color:#ff8b8e"><b>Failed to delete record</b></span>');
				}
			});
		});
		$('#tr-cancel').click(function(){
			$('#prompt-delete-tr').modal('hide');
		});
	});
	$('#remove_faci').unbind('click').click(function(){
		var fcount = 0;
		var faciId;
		prompt.clear().draw();
		$('.yes').removeClass('parti-yes');
		$('.yes').addClass('faci-yes');
		$('.panel-body').html('');
		$('#affiliated').hide();
		$('.faci:checkbox:checked').each(function(){
			++fcount;
			faciId = $(this).val();
			var fname = $(this).closest('tr').find('#fname').text();
			var lname = $(this).closest('tr').find('#lname').text();
			prompt.row.add([fname,lname]).draw();
		});
		if(fcount > 0){
			$('#buttons').show();
			$('#feedback').html('');
			$('#prompt-tab-container').show();
			$('#prompt_delete').modal('show');
			$('.faci-yes').unbind('click').click(function(){
				$('#buttons').hide();
				$.ajax({
					url: '/SoaBaseCode/trainings/deleteFacilitator',
					type: 'post',
					data: {
						'faciId': faciId,
						'trainingId': trainingId
					},
					success: function(responseData){
						faci.ajax.reload(null, false);
						master.ajax.reload( null, false );
						var success = responseData;
						var fail = fcount - responseData;
						var sResponse = '<span style="color:#228B22"><b>Facilitator record deleted.<b></span><br>';
						var fResponse = '<span style="color:#ff8b8e"><b>'+ fail +' failed.</b></span>'
						$('#feedback').html(sResponse + fResponse);
					}
				})
			});
			$('#cancel').click(function(){
				$('.faci:checkbox:checked').each(function(){
					$(this).attr('checked', false);
					$('#prompt_delete').modal('hide');
				});
			});
		}else{
			alert('Please choose a record to be deleted.');
		}
	});
	$('#remove_parti').unbind('click').click(function(){
		var partiIds = [];
		var pcount = 0;
		prompt.clear().draw();
		$('.yes').removeClass('faci-yes');
		$('.yes').addClass('parti-yes');
		$('.parti:checkbox:checked').each(function(){
			++pcount;
			partiIds.push($(this).val());
			var fname = $(this).closest('tr').find('#fname').html();
			var lname = $(this).closest('tr').find('#lname').html();
			prompt.row.add([fname,lname]).draw();
		});
		if(pcount > 0){
			$('#affiliated').hide();
			$('#buttons').show();
			$('#feedback').html('');
			$('#prompt-tab-container').show();
			$('#prompt_delete').modal('show');
			$.ajax({
				url: '/SoaBaseCode/trainings/getParticipantAttendance',
				type: 'post',
				data: {
					'partiIds': partiIds,
					'trainingId': trainingId
				},
				success: function(responseData){
					var object = responseData
					if(object.length > 0){
						$('.panel-body').html('');
						for(var x=0;x<object.length;x++){
							var span = '<a id="item" class="list-group-item" style="border:none">' +
										'<h4 class="list-group-item-text">Attendance: ' + object[x].attendanceId +
										'</h4><p class="list-group-item-text">Date: '+ object[x].attendanceDate+'&nbsp;'+
										'Status: '+object[x].participantStatus +'</p></a>';
							$('.panel-body').append(span);
						}
						$('#affiliated').show();
					}
				}
			})
			$('.parti-yes').unbind('click').click(function(){
				$('#buttons').hide();
				$.ajax({
					url:'/SoaBaseCode/trainings/deleteParticipant',
					type: 'post',
					data: {
						'partiIds': partiIds,
						'trainingId': trainingId
					},
					success: function(responseData){
						parti.ajax.reload(null, false);
						asgn.ajax.reload(null, false);
						master.ajax.reload( null, false );
						var success = responseData;
						var fail = pcount - responseData;
						var sResponse = '<span style="color:#228B22"><b>'+ success +' participant record(s) deleted.<b></span><br>';
						var fResponse = '<span style="color:#ff8b8e"><b>'+ fail +' failed.</b></span>'
						$('#feedback').html(sResponse + fResponse);
					}
				});
			});
			$('#cancel').click(function(){
				$('.parti:checkbox:checked').each(function(){
					$(this).attr('checked', false);
					$('#prompt_delete').modal('hide');
				});
			});
		}else{
			alert('Please choose a record to be deleted.');
		}
	});
	$('#add').click(function() {
		var content = $('#objective').val();
		var node = '<div class=\'node\'><div class=\'o_content\'>'
				+ content
				+ '</div><a id=\'remove_obj\' class=\'close\'> &times;</a></div>';
		if (content.trim().length > 0) {
			$('#objective_div').append(node);
			$('#objective').val('');
		} else {
			$('#obj_err').fadeIn(300);
		}
	});
	$('#objective_div').on('click', '#remove_obj',function() {
		$(this).parent().remove();
	});
	$('#edit_submit').click(function() {
		if (validate()) {
			var objective_string = '';
			$('#objective_div > div').each(function() {
				objective_string += $(this).find('.o_content').html() + '|';
			});
			$.ajax({
				url : '/SoaBaseCode/admin/updatetraining',
				type : 'POST',
				data : {
					'trainingId' : $('#training_id').text(),
					'trainingTitle' : $('#training_title').val(),
					'dateStart' : $('#date_start').val(),
					'dateEnd' : $('#date_end').val(),
					'objectives' : objective_string.trim(),
					'outline' : $('#edit_outline').val(),
					'annualPlan' : $('#det_plan').text().trim()
				},
				success : function(responseData) {
					var training = responseData;
					$('#det_id').text(training.trainingId);
					$('#det_title').text(training.trainingTitle);
					$('#det_start').text(training.dateStart);
					$('#det_end').text(training.dateEnd);
					if(null != training.objectives){
						var raw = training.objectives.trim();
						$('#det_obj').html('');
						if (raw.length > 0) {
							var obj = "";
							for (var x = 0; x < raw.length; x++) {
								if (raw.charAt(x) == '|') {
									$('#det_obj').append('<li>' + obj + '</li>');
									obj = "";
								} else {
									obj += raw.charAt(x);
								}
							}
						} else {
							$('#det_obj').html('No objectives specified.');
						}
					}
					if (null != training.outline && training.outline.trim().length > 0) {
						var outline = '<textarea id=\'outline_content\' readonly>'
								+ training.outline
								+ '</textarea>';
						$('#det_outline').html(outline);
						$('#edit_outline').text(training.outline);
					} else {
						$('#det_outline').html('<textarea id=\'outline_content\' readonly>No outline specified.</textarea>');
					}
					$('#success').fadeIn(300);

				},
				error : function() {
					$('#fail').fadeIn(300);
				}
			});
		}
	});
	$('input').focus(function() {
		$('.label').fadeOut(300);
		$('.feedback').fadeOut(300);
	});
	function validate() {
		var ret = true;
		if ($('#training_title').val().trim().length == 0) {
			$('#title_err').fadeIn(300);
			ret = false;
		}
		if ($('#date_start').val() > $('#date_end').val()) {
			$('#date_err').fadeIn(300);
			ret = false;
		}
		return ret;
	}
	$('button[name=frmAssign]').click(function(){
		frm_id = this.id;
		alert(frm_id);	
	});
	//Training Participants' Form Handles
	$('#assign_frm').click(function(){
		var training_id = $('#det_id').text();
		//alert(frm_id);
		
		var  uids = [];
		$('.user:checkbox:checked').each(function(){
			uids.push($(this).val());
		})
		if(frm_id == 1){
			skillsFrm = "assigned";
			
			$.ajax({
				url: '/SoaBaseCode/trainings/assignForm',
				type: 'post',
				data: {
					//'userId': user_id,
					'uids': uids,
					'trainingId': training_id,
					'skillsAssessfrm': skillsFrm,
					'courseFdbackfrm': "none",
					'trainingEffectiveness_frm': "none"
				},
				success: function(responseData){
					alert("sudskills");
					//row.remove();
					//parti.row.add(rowNode).draw();	
					$('#assign_list').modal('hide');
				},
				error: function(){
					alert('error');
				}
			});
		}else if (frm_id == 2){
			
		}else if (frm_id == 3){
			crsFrm = "assigned";
			
			$.ajax({
				url: '/SoaBaseCode/trainings/assignForm',
				type: 'post',
				data: {
					'uids': uids,
					'trainingId': training_id,
					'courseFdbackfrm': crsFrm,
					'skillsAssessfrm': "none",
					'trainingEffectiveness_frm': "none",
					'faciFdbckfrm': "none"
				},
				success: function(responseData){
					//row.remove();
					//parti.row.add(rowNode).draw();	
					$('#assign_list').modal('hide');
				},
				error: function(){
					alert('error');
				}
			});
		}else if (frm_id == 4){
			tEffectiveFrm = "assigned";
			alert("assigneffective")
			$.ajax({
				url: '/SoaBaseCode/trainings/assignForm',
				type: 'post',
				data: {
					'uids': uids,
					'trainingId': training_id,
					'courseFdbackfrm': "none",
					'skillsAssessfrm': "none",
					'trainingEffectiveness_frm': tEffectiveFrm,
					'faciFdbckfrm': "none"
				},
				success: function(responseData){
					//row.remove();
					//parti.row.add(rowNode).draw();	
					$('#assign_list').modal('hide');
				},
				error: function(){
					alert('error');
				}
			});
		} else if (frm_id == 5){
			tEffectiveFrm = "assigned";
			
			$.ajax({
				url: '/SoaBaseCode/trainings/assignForm',
				type: 'post',
				data: {
					'uids': uids,
					'trainingId': training_id,
					'courseFdbackfrm': "none",
					'skillsAssessfrm': "none",
					'trainingEffectiveness_frm': "none",
					'faciFdbckfrm': tEffectiveFrm
				},
				success: function(responseData){
					//row.remove();
					//parti.row.add(rowNode).draw();	
					$('#assign_list').modal('hide');
				},
				error: function(){
					alert('error');
				}
			});
		}
	});
	$('button[name=frmRelease]').click(function(){
		var training_id = $('#det_id').text();
		frm_id = this.id;
		alert(frm_id);	
		if(frm_id == 1){
			skillsFrm = "released";
			
			$.ajax({
				url: '/SoaBaseCode/trainings/releaseForm',
				type: 'post',
				data: {
					'userId': uId,
					'trainingId': training_id,
					'skillsAssessfrm': skillsFrm,
					'courseFdbackfrm': "none",
					'trainingEffectiveness_frm': "none",
					'faciFdbckfrm': "none"
				},	
				success: function(responseData){
					//row.remove();
					//parti.row.add(rowNode).draw();	
					$('#frm').modal('hide');
				},
				error: function(){
					alert('error');
				}
			});
		}else if (frm_id == 2){
			
		}else if (frm_id == 3){
			crsFrm = "released";
			
			$.ajax({
				url: '/SoaBaseCode/trainings/releaseForm',
				type: 'post',
				data: {
					'userId': uId,
					'trainingId': training_id,
					'courseFdbackfrm': crsFrm,
					'skillsAssessfrm': "none",
					'trainingEffectiveness_frm': "none",
					'faciFdbckfrm': "none"
				},
				success: function(responseData){
					//row.remove();
					//parti.row.add(rowNode).draw();	
					$('#frm').modal('hide');
				},
				error: function(){
					alert('error');
				}
			});
		}else if (frm_id == 4){
			skillsFrm = "released";
			alert("sudeffectiveness");
			$.ajax({
				url: '/SoaBaseCode/trainings/releaseForm',
				type: 'post',
				data: {
					'userId': uId,
					'trainingId': training_id,
					'courseFdbackfrm': "none",
					'skillsAssessfrm': "none",
					'trainingEffectiveness_frm': skillsFrm,
					'faciFdbckfrm': "none"
				},
				success: function(responseData){
					//row.remove();
					//parti.row.add(rowNode).draw();	
					$('#frm').modal('hide');
				},
				error: function(){
					alert('error');
				}
			});
		}
		
		else if (frm_id == 5){
			skillsFrm = "released";
			alert("sudfacifd");
			$.ajax({
				url: '/SoaBaseCode/trainings/releaseForm',
				type: 'post',
				data: {
					'userId': uId,
					'trainingId': training_id,
					'courseFdbackfrm': "none",
					'skillsAssessfrm': "none",
					'trainingEffectiveness_frm': "none",
					'faciFdbckfrm': skillsFrm
				},
				success: function(responseData){
					//row.remove();
					//parti.row.add(rowNode).draw();	
					$('#frm').modal('hide');
				},
				error: function(){
					alert('error');
				}
			});
		}
	});
	//Training Facilitators' Form Handles
	$('#assign_frm2').click(function(){
		var training_id = $('#det_id').text();
		//alert(frm_id);
		
		var  uids = [];
		$('.user:checkbox:checked').each(function(){
			uids.push($(this).val());
		})
		if (frm_id == 2){
			tnaForm = "assigned";
			
			$.ajax({
				url: '/SoaBaseCode/trainings/assignForm2',
				type: 'post',
				data: {
					//'userId': user_id,
					'uids': uids,
					'trainingId': training_id,
					'faciFdbfrm': "none",
					'tnaForm': tnaForm
				},
				success: function(responseData){
					alert("sudskills");
					//row.remove();
					//parti.row.add(rowNode).draw();	
					$('#assign_list2').modal('hide');
				},
				error: function(){
					alert('error');
				}
			});
		}else if (frm_id == 4){
			faciForm = "assigned";
			
			$.ajax({
				url: '/SoaBaseCode/trainings/assignForm2',
				type: 'post',
				data: {
					//'userId': user_id,
					'uids': uids,
					'trainingId': training_id,
					'faciFdbfrm': faciForm,
					'tnaForm': "none"
				},
				success: function(responseData){
					alert("sudskills");
					//row.remove();
					//parti.row.add(rowNode).draw();	
					$('#assign_list2').modal('hide');
				},
				error: function(){
					alert('error');
				}
			});
		}
	});
	$('button[name=frmRelease2]').click(function(){
		var training_id = $('#det_id').text();
		frm_id = this.id;
		alert(frm_id + "asd");	
		if(frm_id == 2){
			tnaForm = "released";
			
			$.ajax({
				url: '/SoaBaseCode/trainings/releaseForm2',
				type: 'post',
				data: {
					'userId': uId,
					'trainingId': training_id,
					'faciFdbfrm': "none",
					'tnaForm': tnaForm
				},	
				success: function(responseData){
					//row.remove();
					//parti.row.add(rowNode).draw();	
					$('#frm').modal('hide');
				},
				error: function(){
					alert('error');
				}
			});
		}
	});
	$('button[name=frmEdit]').click(function(){
		var training_id = $('#det_id').text();
		frm_id = this.id;
			
		if(frm_id == 2){
			window.location = "http://localhost:8080/SoaBaseCode/tna/edit";
		} else if(frm_id == 4){
			window.location = "http://localhost:8080/SoaBaseCode/tee/edit";
		}
	});
});