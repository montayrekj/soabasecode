function appendquestion(URLPath) {
	alert("Adding Question");

	var data = $("#tnaForm").serialize();
	console.log("Data == " + data);

	$.ajax({
		type : "POST",
		data : data,
		url : URLPath,
		success : function(responseData) {
			alert("DONE");
			console.log(responseData);
			$('#essays').html($(responseData).find('#essays').html());
		}
	});
}

function executeSubmit(URLPath){
	var answer = $('#tnaForm').serializeArray();
	
	console.log(answer);
	$.ajax({
		type : "POST",
		contentType : 'application/json; charset=utf-8',

		data : JSON.stringify(answer),
		url : URLPath,
		success : function(responseData) {
			location.href="home";
			alert("Successfully submitted form!");
		}
	});
}
