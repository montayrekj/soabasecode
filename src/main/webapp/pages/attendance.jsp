<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="templates/import.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Administrator</title>
</head>
<body class="hold-transition skin-red sidebar-mini sidebar-collapse">
	<div class="wrapper">
		<%@ include file="templates/header.jsp"%>
		<%@ include file="templates/sidenav.jsp"%>
		<div class="content-wrapper">
			<section class="content-header">
			<h1>
				Training Attendance <small>Check daily attendance.</small>
			</h1>
			<button class="add btn btn-info" type="button" onclick="window.history.back()">Back</button><br><br>
			</section>
			<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box box-success">
						<div class="box-header">
							<small><i>*Click on the participant's id to view their profile.*</i></small>
						</div>
						<div class="box-body">
							<table id="att_tab">
								<thead>
									<th>Participant ID</th>
									<c:set var="tempId" value="${null}" />
									<c:set var="flag" value="${true}" />
									<c:forEach items="${attendanceList}" var="head">
										<c:if test="${true == flag}">
											<c:choose>
												<c:when test="${tempId == null}">
													<c:set var="tempId" value="${head.participantId}" />
													<th>${head.attendanceDate}</th>
												</c:when>
												<c:otherwise>
													<c:choose>
														<c:when
															test="${null != tempId && head.participantId != tempId}">
															<c:set var="flag" value="${false}" />
														</c:when>
														<c:otherwise>
															<th>${head.attendanceDate}</th>
														</c:otherwise>
													</c:choose>
												</c:otherwise>
											</c:choose>
										</c:if>
									</c:forEach>
								</thead>
								<c:set var="currId" value="${null}" />
								<tbody>
									<c:forEach items="${attendanceList}" var="row">
										<c:choose>
											<c:when test="${currId !=row.participantId}">
												<c:set var="currId" value="${row.participantId}" />
												<tr class="att_tab_row">
													<td class="prof"><a>${row.participantId}</a></td>
											</c:when>
										</c:choose>
										<td>
										<select value="${row.attendanceId}"
											class="att_status" <c:if test="${row.attendanceDate.compareTo(today) > 0}"><c:out value="disabled"/></c:if>>
												<option
													<c:if test="${row.participantStatus == \"N/A\"}">
												<c:out value="selected"/>
											</c:if>>N/A</option>
												<option
													<c:if test="${row.participantStatus == \"Present\"}">
												<c:out value="selected"/>
											</c:if>>Present</option>
												<option
													<c:if test="${row.participantStatus == \"Absent\"}">
												<c:out value="selected"/>
											</c:if>>Absent</option>
												<option
													<c:if test="${row.participantStatus == \"Tardy\"}">
												<c:out value="selected"/>
											</c:if>>Tardy</option>
										</select></td>
										<c:choose>
											<c:when test="${currId !=null && currId !=row.participantId}">
												</tr>
											</c:when>
										</c:choose>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			</section>
		</div>
	</div>
	<div id="parti_prof" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header" style="background-color:#dd4b39;color:#fff">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Participant's Profile</h4>
				</div>
				<div class="modal-body">
					<div id="body-content" style="display:flex;flex-wrap:wrap">
						<div style="margin:10px">
							<img class="profile-user-img img-responsive img-circle"
								src="/SoaBaseCode/images/thumbnail.png" alt="User profile picture">
						</div>
						<div style="margin:10px">
							<div>
								<label>Name: </label>&nbsp;<a id="name"></a>
							</div>
							<div>
								<label>Age: </label>&nbsp;<a id="age"></a>
							</div>
							<div>
								<label>Gender: </label>&nbsp;<a id="gender"></a>
							</div>
							<div>
								<label>Address: </label>&nbsp;<a id="add"></a>
							</div>
							<div>
								<label>Email: </label>&nbsp;<a id="email"></a>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer" style="background-color: #C0C0C0">
				</div>
			</div>
		</div>
	</div>
</body>
<%@ include file="templates/footer.jsp"%>
<script>
	$(document).ready(function() {
		var prevStatus;
		$('#att_tab').DataTable({
			'dom' : 'fltip'
		});
		$('.prof').on('click',function(){
			$.ajax({
				url: '/SoaBaseCode/admin/getParticipantProfile',
				type: 'get',
				data: {
					'participantId' : $(this).text().trim()
				},
				success: function(responseData){
					var middleName = null != responseData.middleName?responseData.middleName + " ":"";
					$('#name').text(responseData.firstName + " " + middleName + responseData.lastName);
					$('#age').text(responseData.age);
					$('#gender').text(responseData.gender);
					$('#add').text(responseData.address);
					$('#email').text(responseData.email);
					$('#parti_prof').modal('show');
				},
				error: function(){
					alert('Unable to retrieve participant\'s profile!');
				}
			});
			$('#parti_prof').modal('show');
		});
		$('.att_status').on('focus', function() {
			prevStatus = $(this).val();
		});
		$('.att_status').on('change', function() {
			var att_id = $(this).attr('value');
			var stat = $(this).val();
			console.log(stat + " " + att_id);
			$.ajax({
				url : '/SoaBaseCode/trainings/updateAttendance',
				type : 'post',
				data : {
					'attendanceId' : att_id,
					'participantStatus' : stat
				},
				success : function(responseData) {
					console.log(responseData);
				},
				error : function() {
					alert('An error occured while updating!');
					$(this).val(prevStatus);
				}
			});
		});
		function convertDate(date) {
			var year = date.getFullYear();
			var month = date.getMonth() + 1;
			var date = date.getDate();
			return year + '-' + month + '-' + date;
		}
	});
</script>
<style>
.att_tab_row:hover{
	background-color: #f2f2f2;
	cursor: pointer;
}
</style>
</html>