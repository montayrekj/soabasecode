<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Create Dynamic Form Using jQuery </title>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet"
	href="http://localhost:8086/SoaBaseCode/lib/css/style123.css"
	type="text/css" />
<script src="http://localhost:8086/SoaBaseCode/lib/jQuery/script.js" type="text/javascript"></script>
</head>
<body>
<div class="maindiv">
<div id="header"></div>
<div class="menu">
<button id="namebutton"><img src="images/name-img.png">Name</button>
<button id="emailbutton"><img src="images/email.png">Email</button>
<button id="addressbutton"><img src="images/contact-img.png">Address</button>
<button id="checkboxbutton"><img src="images/check-img.png">CheckBox</button>
<button id="radioaddbutton"><img src="images/radio-img.png">Radio</button>
<button id="reset">Reset</button>
</div>
<div class="InputsWrapper1">
<div id="yourhead">
<div id="your">
<h2 id="yourtitle">Your Form Title<img src="images/edit-form.png"></h2>
<h4 id="justclickid">Just Click on Fields on left to start building your form. It's fast, easy & fun.</h4>
</div>
</div>
<div id="InputsWrapper"></div>
</div>
</div>
</body>
</html>