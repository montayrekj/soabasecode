<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="templates/import.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Calendar</title>
</head>
<body class="hold-transition skin-red sidebar-mini sidebar-collapse">
	<div class="wrapper">
		<%@ include file="templates/header.jsp"%>
		<%@ include file="templates/sidenav.jsp"%>
		<div class="content-wrapper">
			<section class="content-header">
			<h1 id="head">${year} Training Plan <small>Check upcoming trainings.</small>
			</h1>
			</section>
			<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box box-success">
						<div class="box-header" style="display: flex; flex-wrap: wrap">
						</div>
						<div class="box-body">
							<div id='viewOnlyCalendar'></div>
							<div id="fullCalModal" class="modal fade">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header"
											style="background-color: #dd4b39; color: #fff">
											<button type="button" class="close" data-dismiss="modal">
												<span aria-hidden="true">�</span> <span class="sr-only">close</span>
											</button>
											<span id="modalId" style="display:none"></span>
											<h4 id="modalTitle" class="modal-title"></h4>
										</div>
										<div id="modalBody" class="modal-body"></div>
										<div class="modal-footer">
											<a id="eventUrl" class="btn btn-primary">More</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</section>
		</div>
	</div>
</body>
<%@ include file="templates/footer.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	var today = new Date();
	var year = today.getFullYear();
	var viewOnlyCalendar = $('#viewOnlyCalendar').fullCalendar({
		header: {
			left: 'prev,next',
			center: 'title',
			right: 'listWeek, month'
		},
		viewRender: function(view,element) {
			var start = new Date(year + '-01-01');
		    var end = new Date(year + '-12-31');
            if ( view.end >= end) {
                $("#viewOnlyCalendar .fc-next-button").hide();
                return false;
            }
            else {
                $("#viewOnlyCalendar .fc-next-button").show();
            }

            if ( view.start <= start) {
                $("#viewOnlyCalendar .fc-prev-button").hide();
                return false;
            }
            else {
                $("#viewOnlyCalendar .fc-prev-button").show();
            }
        },
		defaultDate: today,
		selectable: false,
		views: {
			listWeek: { buttonText: 'List View' }
		},
		fixedWeekCount: false,
		editable: false,
		eventLimit: true,
		eventClick:  function(event, jsEvent, view) {
			$('#modalId').html(event.id);
            $('#modalTitle').html(event.title);
            $('#modalBody').html(event.description);
            $('#eventUrl').attr('href',event.url);
            $('#fullCalModal').modal();
            return false;
        },
		eventRender: function(event, element) {
	    	$(element).tooltip({title: "Click me!"});             
	    },
		events: function(start, end, timezone, callback){
			var events = [];
			$.ajax({
				url: '/SoaBaseCode/calendar/trainingevents',
				type: 'get',
				data: {
					"planId" : year.toString()
				},
				success: function(responseData){
					object = responseData;
					for(var x = 0;x < object.length;x++){
						var desc = null!=object[x].outline?object[x].outline:'No course outline specified.';
						events.push({
							id: object[x].trainingId,
							title: object[x].trainingTitle,
							start: object[x].dateStart,
							end: moment(object[x].dateEnd).add(1 , 'days'),
							description: desc,
							allDay: true,
							url: "/SoaBaseCode/trainings/viewTraining?training_id=" + object[x].trainingId
						});
					}
					callback(events);
				},
				error: function(){
					alert('error3');
				}
			});
		}
	});

});
</script>
</html>