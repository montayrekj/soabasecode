<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta
	content="width = device-width, initial-scale = 1, maximum-scale = 1, user-scalable = no"
	name="viewport">
<link rel="stylesheet" href="/SoaBaseCode/lib/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/SoaBaseCode/lib/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="/SoaBaseCode/lib/ion-icons/css/ionicons.min.css">

<link rel="stylesheet" href="/SoaBaseCode/lib/fullcalendar-3.4.0/fullcalendar.min.css">
<link rel="stylesheet" href="/SoaBaseCode/lib/fullcalendar-3.4.0/fullcalendar.print.css" media="print">
<link rel="stylesheet" href="/SoaBaseCode/lib/css/dataTables.css">
<link rel="stylesheet" href="/SoaBaseCode/lib/select2/select2.min.css">
<link rel="stylesheet"
	href="/SoaBaseCode/lib/timepicker/bootstrap-timepicker.min.css">
<link rel="stylesheet" href="/SoaBaseCode/lib/wysihtml5/bootstrap3-wysihtml5.min.css">
<link rel="stylesheet" href="/SoaBaseCode/lib/adminlte/AdminLTE.min.css">
<link rel="stylesheet" href="/SoaBaseCode/lib/adminlte/skins/skin-red.min.css">
<link rel="stylesheet" href="/SoaBaseCode/lib/css/style.css">
<link rel="stylesheet" href="/SoaBaseCode/lib/css/hover-min.css">
<link rel="stylesheet" href="/SoaBaseCode/lib/css/balloon.min.css">
<link rel="stylesheet" href="/SoaBaseCode/lib/css/spectrum.css">
<link rel="stylesheet" href="/SoaBaseCode/css/training-info.css">