<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<aside class="main-sidebar">
	<section class="sidebar">
		<!-- <div class="user-panel">
			<div class="pull-left image">
				<img src="images/thumbnail.png" class="img-circle" alt="User Image"
					href="#">
			</div>
			<div class="pull-left info">
				<p></p>
				<span><i class="fa fa-circle text-success"></i> Online<span>
			</div>
		</div>-->
		<br>
		<ul class="sidebar-menu">
			<!--<li class = "header">TO DO</li>-->
			<li id="li-home"><a href="/SoaBaseCode/home"> <i
					class="fa fa-home"></i> <span>Home</span>
			</a></li>
			<li id="li-trainings"><a href="/SoaBaseCode/trainings"> <i
					class="fa fa-sitemap"></i> <span>Trainings</span>
			</a></li>
			<li id="li-calendar"><a href="/SoaBaseCode/calendar"> <i
					class="fa fa-calendar"></i> <span>Calendar</span>
			</a></li>
			<li id="li-facilitator"><a href="/SoaBaseCode/facilitator">
					<i class="fa fa-mortar-board"></i> <span>Facilitator</span>
			</a></li>

			<c:if
				test="${user.getUserType().equals('employee') || user.getUserType().equals('admin')}">
				<li id="li-supervisor"><a href="/SoaBaseCode/supervisor"> <i
						class="fa fa-group"></i> <span>Supervisor</span>
				</a></li>
			</c:if>
			<c:if test="${user.getUserType().equals('admin')}">
				<li class="treeview" id="li-administrator"><a href="#"> <i
						class="fa fa-table"></i> <span>Administrator</span> <span
						class="pull-right-container"> <i
							class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
					<ul class="treeview-menu">
						<li><a href="/SoaBaseCode/admin/training-plan"> <i
								class="fa fa-calendar"></i> <span>Training Plan</span>
						</a></li>
						<li><a href="/SoaBaseCode/admin/systemusers"> <i
								class="fa fa-group"></i> <span>System Users</span>
						</a></li>
						<li><a href="/SoaBaseCode/admin/reports"> <i
								class="fa fa-bar-chart"></i> <span>Reports</span>
						</a></li>
					</ul></li>
			</c:if>

		</ul>
	</section>
</aside>