<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<header class="main-header">
	<a href="/SoaBaseCode/home" class="logo"> <span class="logo-mini"> <b><small>ASI</small></b>
	</span> <span class="logo-lg"> <b>Alliance</b>
	</span>
	</a>
	<nav class="navbar navbar-static-top" role="navigation">
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
			role="button"> <span class="sr-only">Toggle navigation</span>
		</a>
		<div class="navbar-custom-menu" style="height: 100%">
			<ul class="nav navbar-nav">
				<li class="dropdown notifications-menu"><a href="#"
					class="dropdown-toggle" data-toggle="dropdown"> <i
						class="fa fa-bell-o"></i> <!-- <span class="label label-warning">1</span> -->
				</a>
					<ul class="dropdown-menu">
						<li class="header">You have 0 notification</li>
						<li>
							<ul class="menu">
								<!-- <li><a href="#"> <i class="fa fa-user text-aqua"></i>
										User scheduled an appointment with you.
								</a></li> -->
							</ul>
						</li>

						<listyle="padding: 0;"><a href="/home"
							class="col-xs-12 btn btn-default btn-flat"
							style="border: none; padding: 15px;"> View All </a></li>
					</ul></li>
				<li class="dropdown user user-menu"><a href="#"
					class="dropdown-toggle" data-toggle="dropdown"
					aria-expanded="false" aria-haspopup="true"> <img
						src="/SoaBaseCode/images/thumbnail.png" class="user-image" alt="User Image">
						<span class="hidden-xs">${user.getFirstName()} ${user.getLastName()}</span>
				</a>
					<ul class="dropdown-menu">
						<li class="user-header"><img src="/SoaBaseCode/images/thumbnail.png"
							class="img-circle" alt="User Image">
							<p  class="text-muted" >${user.getFirstName()} ${user.getMiddleName()} ${user.getLastName()}</p>
							<p class="text-muted text-capitalize" style="font-size: 1em">${user.getUserType()}</p></li>
						<li style="padding: 0;"><a href="/SoaBaseCode/home"
							class="col-xs-12 btn btn-default btn-flat"
							style="border: none; padding: 15px;">Home</a></li>
						<li style="padding: 0;"><a href="#"
							class="col-xs-12 btn btn-default btn-flat"
							style="border: none; padding: 15px;" data-toggle="modal" data-target="#myModal">Edit User</a></li>
						<li style="padding: 0;"><a href="/SoaBaseCode/logout"
							class="col-xs-12 btn btn-default btn-flat"
							style="border: none; padding: 15px;"> Sign out </a></li>
					</ul></li>

			</ul>
		</div>

		

	</nav>
	
	<!-- Modal -->
		<div class="modal fade" id="myModal" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 style="color: red;">
							<span class="glyphicon glyphicon-lock"></span> Edit User
						</h4>
					</div>
					<div class="modal-body">
						<form role="form" id="editForm" action="" method="post">
							<div class="form-group">
								<label for="usrname"><span
									class="glyphicon glyphicon-user"></span> Username</label> <input
									type="text" class="form-control" id="usrname"
									value="${username}" name="uname">
							</div>
							<div class="form-group">
								<label for="psw"><span
									class="glyphicon glyphicon-eye-open"></span> Current Password</label> <input
									type="password" class="form-control" id="psw"
									placeholder="Enter password" name="pass">
							</div>
							<div class="form-group">
								<label for="newpsw"><span
									class="glyphicon"></span> New Password</label> <input
									type="password" class="form-control" id="newpsw"
									placeholder="Enter new password" name="pword">
							</div>
							<div class="form-group">
								<label for="cfrmpsw"><span
									class="glyphicon"></span> Confirm Password</label> <input
									type="password" class="form-control" id="cfrmpsw"
									placeholder="Confirm password" name="cpword">
							</div>
							<div id='alert-container'></div>
							<button type="button"
								class="btn btn-default btn-success btn-block" onclick="editUser('${pageContext.request.contextPath}/home/editUser')">
								<span class="glyphicon glyphicon-edit"></span> Edit
							</button>
						</form>
					</div>
					<div class="modal-footer">
						<button type="submit"
							class="btn btn-default btn-default pull-left"
							data-dismiss="modal">
							<span class="glyphicon glyphicon-remove"></span> Cancel
						</button>
						
					</div>
				</div>
			</div>
		</div>
</header>

<script>

function editUser(URLPath){
	var data = $("#editForm").serialize();
	
	$.ajax({
		url: URLPath,
        data: data,
        method : "post",
        datatype: "text",
        success : function(response){
        	if($(response).find('#alert-container').html() == ""){
        		
        		//Hides the modal
        		$('#myModal').modal('toggle');
        		
        		//Hides the modal before alerting that the user has been edited
        		setTimeout(function() {
        			alert("Successfully edited user!");
        		},15)
        		
        	} else {
        		 $('#alert-container').html($(response)[1].innerHTML);
        	}
        	
        },
        error : function(error){
        	alert("error: " + error);
        }
	});
}

</script>