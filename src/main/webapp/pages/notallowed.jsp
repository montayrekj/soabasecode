<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Not Allowed</title>
<%@ include file="templates/import.jsp"%>
</head>
<body class="hold-transition skin-red sidebar-mini">
	<div class="wrapper">
		<header class="main-header"> <a href="" class="logo"> <span
			class="logo-lg"> <b>Alliance Software</b>
		</span>
		</a> <nav class="navbar navbar-static-top" role="navigation"> </nav></header>
		<div id="content-wrapper" style="background-color: white">
		<div class="content">
			<div class="jumbotron text-center text-danger">
				<h2>Not Allowed</h2>
				<h2><small>You are not allowed to access this page</small></h2>
			</div>
			</div>
		</div>
	</div>
</body>
</html>