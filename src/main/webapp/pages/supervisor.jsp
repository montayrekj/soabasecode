<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="templates/import.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Supervisor</title>
</head>
<body class="hold-transition skin-red sidebar-mini sidebar-collapse">
	<div class="wrapper">
		<%@ include file="templates/header.jsp"%>
		<%@ include file="templates/sidenav.jsp"%>
		<div class="content-wrapper">
			<section class="content-header">
			<h1>
				Supervisor <small>Assess subordinates in training.</small>
			</h1>
			</section>
			<section class="content">
			<div class="row">
				<div class="col-xs-4">
					<div class="box box-success">
						<div class="overlay" id="tr-details-load" style="display: none">
							<i class="fa fa-refresh fa-spin"></i>
						</div>
						<div class="box-header">
							<center>
								<h4>Details</h4>
							</center>
						</div>
						<div id="tr-details-info">
							<div class="box-body box-profile">
								<img class="profile-user-img img-responsive img-circle"
									src="images/thumbnail.png" alt="User profile picture">
								<h5 class="text-center">
									<strong><c:out value="${p.getFirstName().concat(' '.concat(p.getLastName()))}" default="Participant Name"/></strong>
								</h5>
								<hr>
								<strong><i class="fa fa-id-card-o margin-r-5"></i> Training ID</strong>
								<p class="text-muted">&nbsp;${t.getTrainingId()}</p>
								<hr>
								<strong><i class="fa fa-file-text-o margin-r-5"></i> Training Title</strong>
								<p class="text-muted">&nbsp;${t.getTrainingTitle()}</p>
								<hr>
								<strong><i class="fa fa-calendar-check-o margin-r-5"></i>
									Start Date</strong>
								<p class="text-muted">&nbsp;${dateStart}</p>
								<hr>
								<strong><i class="fa fa-calendar-minus-o margin-r-5"></i>
									End Date</strong>
								<p class="text-muted">&nbsp;${dateEnd}</p>
							</div>
							<!--div class="box-footer">
								<center>
									<h4>Forms</h4>
								</center>
							</div>	
							<div class="box-body">
								<center>
									<button class="btn btn-default btn-form" type="button" disabled
										id="skillsassessment_button">Skills Assessment Form</button>
								</center>
								<hr>
								<center>
									<button class="btn btn-default btn-form" type="button" disabled
										id="trainingeffect_button">Training Effectiveness
										Assessment</button>
								</center>
								<hr>
							</div>-->
						</div>
					</div>
				</div>
				<div class="col-xs-8">
					<div class="box box-warning">
						<div class="box-header">
							<center>
								<h4>Trainings Joined by Subordinates</h4>
							</center>
						</div>
						<div class="box-body">
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<th rowspan=2>Employee Name</th>
										<th rowspan=2>Training Title</th>
										<th rowspan=2>Status</th>
										<th colspan=2>Forms</th>
									</tr>
									<tr>
										<th>Skills Assessment</th>
										<th>Trainining Effectiveness</th>
									</tr>
								</thead>
								<tbody>
									<c:if test="${stList.size() == 0 }">
										<tr>
											<td colspan=3>You don't have subordinates under training</td>
										</tr>
									</c:if>
									<c:forEach items="${stList}" var="item">
										<tr
											onclick="showDetails(${item[0].getTrainingId()}, ${item[1].getUserId()})">
											<td>${item[1].getLastName()}, ${item[1].getFirstName()}</td>
											<td>${item[0].getTrainingTitle()}</td>
											<td>
												<center>
													<c:if test="${item[0].getDateEnd().compareTo(today) < 0}">
														<span class="label label-default status">Finished</span>
													</c:if>
													<c:if test="${item[0].getDateStart().compareTo(today) > 0}">
														<span class="label label-primary status">Not Started</span>
													</c:if>
													<c:if
														test="${item[0].getDateEnd().compareTo(today) >= 0 && item[0].getDateStart().compareTo(today) <= 0}">
														<span class="label label-success status">Ongoing</span>
													</c:if>
												</center>
											</td>
											<c:if test="${item[2].getSkillsAssessfrm() == 'released'}">
													<td>
														<form action="" method="POST" id="skform">
															<center><button type="button" class="btn btn-primary" enabled
															id="${item[0].getTrainingId()}" name="skf" onclick="toSAForm(${item[0].getTrainingId()},'saf',${item[1].getUserId()})">Answer</button></center>
														</form>
													</td>
											</c:if>
											<c:if test="${item[2].getSkillsAssessfrm() == 'assigned'}">
												<td>
													<center><button type="button" class="btn btn-primary" disabled>Answer</button></center>
												</td>
											</c:if>
											<c:if test="${item[2].getSkillsAssessfrm() == 'none'}">
												<td>
													<center><button type="button" class="btn btn-danger" disabled>Locked</button></center>
												</td>
											</c:if>
											
											<c:if test="${item[2].getTrainingEffectiveness_frm() == 'released'}">
													<td>
														<form action="" method="POST" id="teform">
															<center><button type="button" class="btn btn-primary" enabled
															id="${item[0].getTrainingId()}" name="tef" onclick="toTEForm(${item[0].getTrainingId()},'tef',${item[1].getUserId()})">Answer</button></center>
														</form>
													</td>
											</c:if>
											<c:if test="${item[2].getTrainingEffectiveness_frm() == 'assigned'}">
												<td>
													<center><button type="button" class="btn btn-primary" disabled>Answer</button></center>
												</td>
											</c:if>
											<c:if test="${item[2].getTrainingEffectiveness_frm() == 'none'}">
												<td>
													<center><button type="button" class="btn btn-danger" disabled>Locked</button></center>
												</td>
											</c:if>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			</section>
		</div>
	</div>
</body>
<%@ include file="templates/footer.jsp"%>
<script>
	function showDetails(trID, prtID) {
		var data = "trID=" + trID + "&prtID=" + prtID;
		$.ajax({
			url : "/SoaBaseCode/supervisor/get-details",
			data : data,
			method : "post",
			datatype : "text",
			beforeSend : function() {
				$("#tr-details-load").show();
			},
			success : function(response) {
				$("#tr-details-info").html(
						$(response).find("#tr-details-info").html());
				// changeDate();
			},
			error : function() {
				alert("Error");
			},
			complete : function() {
				$("#tr-details-load").hide();
			}
		});
	}

	function toSAForm(trID,form, uId){
		var data = "trID="+trID;
		//var form = "form="+form;
		console.log(data);
		
		if(form == "saf"){
			$.ajax({
		        url: "/SoaBaseCode/trainings/forms",
		        data: {
		        	tId: trID,
		        	ftype: form,
		        	job: "supervisor:" + uId
		        },
		        method: "POST",
		        datatype: "text",
		        success:function(response){
		        	alert("Success!");
		        	location.href = "assessment";
		        },
		        error: function(){
		        	alert("Error");
		        }
		    });
		}
	}
	
	function toTEForm(trID,form, uId){
		var data = "trID="+trID;
		console.log(data);
		alert(uId);
		if(form == "tef"){
			$.ajax({
		        url: "/SoaBaseCode/trainings/forms",
		        data: {
		        	tId: trID,
		        	ftype: form,
		        	job: "supervisor:" + uId
		        },
		        method: "POST",
		        datatype: "text",
		        success:function(response){
		        	alert("Success!");
		        	location.href = "tee";
		        },
		        error: function(){
		        	alert("Error");
		        }
		    });
		}
	}
	// function changeDate() {
	// 	$("#date-start").html(convertDateFormat($("#date-start").html()));
	// 	$("#date-end").html(convertDateFormat($("#date-end").html()));
	// }
</script>
</html>