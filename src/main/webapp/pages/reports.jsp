<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="templates/import.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Reports</title>
</head>
<body class="hold-transition skin-red sidebar-mini sidebar-collapse">
	<div class="wrapper">
		<%@ include file="templates/header.jsp"%>
		<%@ include file="templates/sidenav.jsp"%>
		<div class="content-wrapper">
			<section class="content-header">
			<h1>
				Reports <small>View reports.</small>
			</h1>
			</section>
			<section class="content">
			<div class="row">
				<div class="col-xs-4">
					<div class="box box-success">
						<div class="overlay" id="tr-details-load" style="display: none">
							<i class="fa fa-refresh fa-spin"></i>
						</div>
						<div class="box-header">
							<center>
								<h4>Details</h4>
							</center>
						</div>
						<div id="tr-details-info">
							<div class="box-body box-profile">
								<strong><i class="fa fa-id-card-o margin-r-5"></i> Training ID</strong>
								<p class="text-muted">&nbsp;${t.getTrainingId()}</p>
								<hr>
								<strong><i class="fa fa-file-text-o margin-r-5"></i> Training Title</strong>
								<p class="text-muted">&nbsp;${t.getTrainingTitle()}</p>
								<hr>
								<strong><i class="fa fa-calendar-check-o margin-r-5"></i>
									Start Date</strong>
								<p class="text-muted">&nbsp;${t.getDateStart()}</p>
								<hr>
								<strong><i class="fa fa-calendar-minus-o margin-r-5"></i>
									End Date</strong>
								<p class="text-muted">&nbsp;${t.getDateEnd()}</p>
							</div>
							<!--div class="box-footer">
								<center>
									<h4>Forms</h4>
								</center>
							</div>	
							<div class="box-body">
								<center>
									<button class="btn btn-default btn-form" type="button" disabled
										id="skillsassessment_button">Skills Assessment Form</button>
								</center>
								<hr>
								<center>
									<button class="btn btn-default btn-form" type="button" disabled
										id="trainingeffect_button">Training Effectiveness
										Assessment</button>
								</center>
								<hr>
							</div>-->
						</div>
					</div>
				</div>
				<div class="col-xs-8">
					<div class="box box-warning">
						<div class="box-header">
							<center>
								<h4>Trainings</h4>
							</center>
						</div>
						<div class="box-body">
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<th rowspan=2>Training ID</th>
										<th rowspan=2>Training Title</th>
										<th rowspan=2>Status</th>
										<th colspan=3>Reports</th>
									</tr>
									<tr>
										<th>Course Feedback</th>
										<th>Trainining Effectiveness</th>
										<th>Participant Skills</th>
									</tr>
								</thead>
								<tbody>
									<c:if test="${stList.size() == 0 }">
										<tr>
											<td colspan=3>No Trainings.</td>
										</tr>
									</c:if>
									<c:forEach items="${trainingList}" var="item">
										<tr
											onclick="showDetails(${item.getTrainingId()})">
											<td>${item.getTrainingId()}</td>
											<td>${item.getTrainingTitle()}</td>
											<td>
												<center>
													<c:if test="${item.getDateEnd().compareTo(today) < 0}">
														<span class="label label-default status">Finished</span>
													</c:if>
													<c:if test="${item.getDateStart().compareTo(today) > 0}">
														<span class="label label-primary status">Not Started</span>
													</c:if>
													<c:if
														test="${item.getDateEnd().compareTo(today) >= 0 && item.getDateStart().compareTo(today) <= 0}">
														<span class="label label-success status">Ongoing</span>
													</c:if>
												</center>
											</td>
											<td><center><button onclick="redirect('course',${item.getTrainingId()})" type="button" class="btn btn-primary" enabled>View</button></a></center></td>
											<td><center><button onclick="redirect('effectiveness',${item.getTrainingId()})" type="button" class="btn btn-primary" enabled>View</button></center></td>
											<td><center><button onclick="redirect('facilitator',${item.getTrainingId()})" type="button" class="btn btn-primary" enabled>View</button></center></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			</section>
		</div>
	</div>
</body>
<%@ include file="templates/footer.jsp"%>
<script>
	function redirect(destination, trainingId){
		var uri;
		if(destination == 'course'){
			uri = '/SoaBaseCode/admin/reports'
			$(location).attr('href',url);
		}else if(destination == 'effectiveness'){
			uri = '/SoaBaseCode/admin/effectiveness?trainingId='+trainingId;
			$(location).attr('href',uri);
		}else{
			uri = '/SoaBaseCode/admin/participant-skills?trainingId='+trainingId;
			$(location).attr('href',uri)
		}
	}
	function showDetails(trID) {
		var data = "trainingId=" + trID;
		$.ajax({
			url : "/SoaBaseCode/admin/reports",
			data : data,
			type: 'get',
			datatype : "text",
			beforeSend : function() {
				$("#tr-details-load").show();
			},
			success : function(response) {
				$("#tr-details-info").html(
						$(response).find("#tr-details-info").html());
				// changeDate();
			},
			error : function() {
				alert("Error");
			},
			complete : function() {
				$("#tr-details-load").hide();
			}
		});
	}
</script>
</html>