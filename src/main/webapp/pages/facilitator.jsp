<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Facilitator</title>
<%@ include file="templates/import.jsp"%>
</head>
<body class="hold-transition skin-red sidebar-mini sidebar-collapse">
	<div class="wrapper">
		<%@ include file="templates/header.jsp"%>
		<%@ include file="templates/sidenav.jsp"%>
		<div class="content-wrapper">
			<section class="content-header">
			<h1>
				Facilitator <small>View the trainings you handle.</small>
			</h1>
			</section>
			<section class="content">
			<div class="row">
				<div class="col-xs-4">
					<div class="box box-success">
						<div class="overlay" id="tr-details-load" style="display: none">
							<i class="fa fa-refresh fa-spin"></i>
						</div>
						<div class="box-header">
							<center>
								<h4>Training Details</h4>
							</center>
						</div>

						<div id="tr-details-info">
							<div class="box-body">
								<strong><i class="fa fa-id-card margin-r-5"></i> ID</strong>
								<p class="text-muted">&nbsp;${training.getTrainingId() }</p>
								<hr>
								<strong><i class="fa fa-id-card margin-r-5"></i> Title</strong>
								<p class="text-muted">&nbsp;${training.getTrainingTitle() }</p>
								<hr>
								<strong><i class="fa fa-calendar-check-o margin-r-5"></i>
									Start Date</strong>
								<p class="text-muted">&nbsp;${dateStart}</p>
								<hr>
								<strong><i class="fa fa-calendar-minus-o margin-r-5"></i>
									End Date</strong>
								<p class="text-muted">&nbsp;${dateEnd}</p>

								<!--  <center><a id="full-details-button">
									<button class="btn btn-primary" type="button" disabled>Full Details</button>
								</a></center>-->
							</div>
							<div class="box-footer">
								<center>
									<strong>Forms</strong>
								</center>
								<hr>
								<center>
									<button class="btn btn-default btn-form" type="button"
										${outline} id="${training.getTrainingId()}" name="editO"
										data-toggle="modal" data-target="#edit_info" onclick="edit()">Edit
										Course Outline and Objectives</button>
								</center>
								<hr>
								<center>
									<button class="btn btn-default btn-form" type="button"
										${tnaReleased} id="trainingneeds_button"
										id="${training.getTrainingId()}" name="tna"
										onclick="toTNA(${training.getTrainingId()},'tna')">Input
										Training Needs Analysis</button>
								</center>
							</div>

							<div id="edit_info" class="modal fade" role="dialog">
								<div class="modal-dialog">

									<!-- Modal content-->
									<div class="modal-content">
										<div class="modal-header"
											style="background-color: #dd4b39; color: #fff">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title">
												Edit training: <a id="training_id" style="color: #fff">${training.trainingId}</a>
											</h4>
										</div>
										<div class="modal-body">
											<div id="fail" class="feedback">
												<i class="fa fa-exclamation-circle" aria-hidden="true"></i>
												Failed to edit training!
											</div>
											<div id="success" class="feedback">
												<i class="fa fa-check-circle" aria-hidden="true"></i>
												Training information updated!
											</div>
											<form id="edit_form">
												<div>
													<small>Add objective:</small><br>
													<div>
														<div style="display: flex">
															<input id="objective" type="text" name="objective"
																class="form-control form-input" required> <input
																id="adde" type="button" class="bttn btn-primary"
																value="Add" onclick="add()">
														</div>
														<span id="obj_err" class="label label-danger">Can't
															add a blank objective.</span>
													</div>
												</div>
											</form>
											<div>
												<label>Objective(s):</label><br>
												<div id="objective_div" class="form-control form-input">
													<c:forEach items="${objectives}" var="objective">
														<div class="node">
															<div class="o_content">${objective}</div>
															<a id="remove_obj" class="close"> &times;</a>
														</div>
													</c:forEach>
												</div>
											</div>
											<div>
												<label>Training Outline:</label><br>
												<textarea id="edit_outline" name="outline"
													style="resize: none" form="edit_form"
													class="form-control form-input">${training.outline}</textarea>
												<span id="" class="err" hidden>Training title must
													not be blank.</span>
											</div>
										</div>
										<div class="modal-footer">
											<button form="edit_form" type="button" id="edit_submit1"
												class="btn btn-success" style="width: 100%"
												onclick="submitEditTraining(${training.trainingId})">Submit</button>
										</div>
									</div>

								</div>
							</div>


						</div>
					</div>
				</div>
				<div class="col-xs-8">
					<div class="box box-success">
						<div class="box-header">
							<center>
								<h4>Trainings Handled</h4>
							</center>
						</div>
						<div class="box-body">
							<table class="table table-hover table-bordered">
								<thead>
									<tr>
										<th>ID</th>
										<th>Title</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
									<c:if test="${trainingList.size() == 0 }">
										<tr>
											<td colspan=3>You have not handled a single training
												yet.</td>
										</tr>
									</c:if>
									<c:forEach items="${trainingList}" var="item">
										<tr onclick="showDetails('${item.getTrainingId()}')">
											<td>${item.getTrainingId() }</td>
											<td>${item.getTrainingTitle() }</td>
											<td>
												<center>
													<c:if test="${item.getDateEnd().compareTo(today) < 0}">
														<span class="label label-default">Finished</span>
													</c:if>
													<c:if test="${item.getDateStart().compareTo(today) > 0}">
														<span class="label label-primary">Not Started</span>
													</c:if>
													<c:if
														test="${item.getDateEnd().compareTo(today) >= 0 && item.getDateStart().compareTo(today) <= 0}">
														<span class="label label-success">Ongoing</span>
													</c:if>
												</center>
											</td>
										</tr>





									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			</section>


		</div>
	</div>

</body>
<%@ include file="templates/footer.jsp"%>
<script>
	function showDetails(trID) {
		var data = "trID=" + trID;
		$.ajax({
			url : "/SoaBaseCode/facilitator/get-details",
			data : data,
			method : "post",
			datatype : "text",
			beforeSend : function() {
				$("#tr-details-load").show();
			},
			success : function(response) {
				$("#tr-details-info").html($(response).find("#tr-details-info").html());
	        	$("#full-details-button").attr("href", "/SoaBaseCode/trainings/viewTraining?training_id="+trID);
	        	$('#full-details-button').children().attr("disabled",false);
	        },
	        error: function(){
	        	alert("Error");
	        	$("#full-details-button").attr("href", "");
	        	$('#full-details-button').children().attr("disabled",true);
			},
			complete : function() {
				$("#tr-details-load").hide();
			}
		});
	}
	
	function toTNA(trID,form){
		var data = "trID="+trID;
		//var form = "form="+form;
		console.log(data);
		
		if(form == "tna"){
			$.ajax({
		        url: "/SoaBaseCode/trainings/forms",
		        data: {
		        	tId: trID,
		        	ftype: form,
		        	job: "employee"
		        },
		        method: "POST",
		        datatype: "text",
		        success:function(response){
		        	alert("Success!");
		        	location.href = "tna";
		        },
		        error: function(){
		        	alert("Error");
		        }
		    });
		}
	}
	
	function submitEditTraining(trID){
		alert(trID);
		var objective_string = '';
		$('#objective_div > div').each(function() {
			objective_string += $(this).find('.o_content').html() + '|';
		});
		$.ajax({
			url : '/SoaBaseCode/facilitator/updatetraining',
			type : 'POST',
			data : {
				'trainingId' : trID,
				'objectives' : objective_string.trim(),
				'outline' : $('#edit_outline').val(),
			},
			success : function(responseData) {
				var training = responseData;
				if (null != training.outline && training.outline.trim().length > 0) {
					$('#edit_outline').text(training.outline);
				}
				$('#success').fadeIn(300);

			},
			error : function() {
				$('#fail').fadeIn(300);
			}
		});
	}
	
	function edit(){
		$('#edit_form')[0].reset();
		$('.label-danger').hide();
		$('.feedback').hide();
	}
	
	function add(){
		alert("sudadd!");
		var content = $('#objective').val();
		var node = '<div class=\'node\'><div class=\'o_content\'>'
				+ content
				+ '</div><a id=\'remove_obj\' class=\'close\'> &times;</a></div>';
		if (content.trim().length > 0) {
			$('#objective_div').append(node);
			$('#objective').val('');
		} else {
			$('#obj_err').fadeIn(300);
		}
	}
	
	
	$(document).ready(function() {
		
		$('.sorting').click();
		$('button[name=editO]').click(function() {
			alert("sudedit");
			$('#edit_form')[0].reset();
			$('.label').hide();
			$('.feedback').hide();
			if(undefined != $('#det_obj').find('li').html()){
				$('#objective_div').html($('#objective_div').html());
			}else{
				$('#objective_div').html('');
			}
		});
		$('#objective_div').on('click', '#remove_obj',function() {
			$(this).parent().remove();
		});
	});
</script>
</html>