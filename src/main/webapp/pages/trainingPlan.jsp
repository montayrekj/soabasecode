<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="templates/import.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Administrator</title>
</head>
<body class="hold-transition skin-red sidebar-mini sidebar-collapse">
	<div class="wrapper">
		<%@ include file="templates/header.jsp"%>
		<%@ include file="templates/sidenav.jsp"%>
		<div class="content-wrapper">
			<section class="content-header">
			<h1 id="head">${fn:substring(planInfo.dateCreated, 0, 4)}
				Training Plan <small>Schedule trainings.</small>
			</h1>
			<button class="add btn btn-info" type="button"
				onclick="window.history.back()">Back</button>
			<br>
			<br>
			</section>
			<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box box-success">
						<div class="box-header" style="display: flex; flex-wrap: wrap">
							<div style="width: 1190px;display:none">
								ID : <a id="plan_id">${planInfo.id}</a><br> 
								<br>Date Create: <a id="dateCreated">${planInfo.dateCreated}</a>
							</div>
						</div>
						<div class="box-body">
							<div id='calendar'></div>
							<div id="fullCalModal" class="modal fade">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header"
											style="background-color: #dd4b39; color: #fff">
											<button type="button" class="close" data-dismiss="modal">
												<span aria-hidden="true">�</span> <span class="sr-only">close</span>
											</button>
											<span id="modalId" style="display:none"></span>
											<h4 id="modalTitle" class="modal-title"></h4>
										</div>
										<div id="modalBody" class="modal-body"></div>
										<div class="modal-footer">
											<a id="eventUrl" class="btn btn-primary">Event Page</a>
											<button id="delete-event" class="btn btn-danger">Delete</button>
										</div>
									</div>
								</div>
							</div>
							<div id="training_modal" class="modal fade" role="dialog">
								<div class="modal-dialog">

									<!-- Modal content-->
									<div class="modal-content">
										<div class="modal-header"
											style="background-color: #dd4b39; color: #fff">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title">New Training</h4>
										</div>
										<div class="modal-body">
											<div>
												<input type="text" name="showPaletteOnly"
													id="showPaletteOnly" style="display: none;">
											</div>
											<div>
												<h3>Start Date:</h3>
												<span id="date_start"></span>
											</div>
											<form id="training_form">
												<div style="margin-top: 8px;">
													<label>Training Title:</label><br> <input id="title"
														type="text" name="trainingTitle"
														class="form-control form-input" required> <span
														id="title_err" class="label label-danger" hidden>Training
														title must not be blank.</span>
												</div>
												<div style="display: flex">
													<div style="margin-top: 8px;">
														<label>Date End:</label><br> <input id="end"
															type="date" name="dateEnd"
															class="form-control form-input" required> <span
															id="date_err" class="label label-danger" hidden>Date
															end must be later or same as date start.</span>
													</div>
												</div>
											</form>
										</div>
										<div class="modal-footer">
											<button type="button" id="add_training"
												class="btn btn-success">Add</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</section>
		</div>
	</div>
</body>
<%@ include file="templates/footer.jsp"%>
<style>
.label {
	display: none;
}
</style>
</html>