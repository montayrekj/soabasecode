<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet"
	href="http://localhost:8080/SoaBaseCode/css/styles.css" />
<link rel="stylesheet"
	href="http://localhost:8080/SoaBaseCode/lib/css/bootstrap.min.css"
	type="text/css" />
<link rel="stylesheet"
	href="http://localhost:8080/SoaBaseCode/css/media-queries.css" />
<script type="text/javascript"
	src="http://localhost:8080/SoaBaseCode/js/assignment.js"></script>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html" charset="UTF-8" />
</head>
<body style="background: #DDDDEE">
	<div class="container" id="main-container">
		<div class="page-header">
			<h3 style="background: #DDDDEE;">
				Search and Delete
				</h4>
		</div>

		<div class="page-body" style="width: 750px;">
			<form id="asda">
				<div class="form-group">
					<select class="form-control" name="filter" id="filter">
						<option>Select All</option>
						<option>ID</option>
						<option>Username</option>
						<option>Type</option>
					</select><br>
					<div class="page-body" style="width: 600px;">
						<form id="asda">
							<div class="form-group">
								<input class="form-control table col" type="text"
									name="inputValue" placeholder="Input value to search"
									id="myInput" onkeyup="myFunction()">
							</div>

						</form>
						<form
							action="${pageContext.request.contextPath}/assignment/delete"
							method="POST" id="phoneform">
							<div id="phoneTable">
								<table class="table table-striped table-bordered" id="myTable">
									<thead>
										<tr>
											<th>ID</th>
											<th>Username</th>
											<th>Type</th>

											<th><input type="checkbox"
												onclick="checkAll(this.checked);" /></th>
										</tr>
									</thead>
									<c:forEach items="${myTable}" var="myPhone">
										<tbody>
											<tr>
												<th>${myPhone.getId()}</th>
												<td>${myPhone.getUsername()}</td>
												<td>${myPhone.getType()}</td>
												<td><input type="checkbox" class="chck" name="phoneID"
													value="${myPhone.getId()}" /></td>
											</tr>
										</tbody>
									</c:forEach>
								</table>
							</div>
						</form>
					</div>
					<div id='alert-container'></div>
					<div class="form-group" >
						
						<button class='btn btn-default' type="button"
							formaction="${pageContext.request.contextPath}/home/upgrade"
							onclick="executeUpgrade('${pageContext.request.contextPath}/home/upgrade')">Upgrade
						</button>

					</div>


					<div class="form-group">
						<form
							action="${pageContext.request.contextPath}/assignment/delete"
							method="POST" id="errorform">
							<div id="errorDiv"></div>
							<input class='btn btn-default' type="button" name="delete"
								value="Delete"
								onclick="executeError('${pageContext.request.contextPath}/assignment/delete')" />
						</form>
					</div>
				</div>
</body>
<script
	src="http://localhost:8080/SoaBaseCode/lib/js/jquery-1.10.1.min.js"></script>
<script src="http://localhost:8080/SoaBaseCode/lib/js/bootstrap.min.js"></script>
<script src="http://localhost:8080/SoaBaseCode/lib/js/jquery.blockUI.js"></script>

</html>