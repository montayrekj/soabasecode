<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Course Feedback Form Edit</title>
<%@ include file="templates/import.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
</head>
<body>
	<center>
		<form method="POST" id="sectionform1">
			<h2>Course Feedback Form</h2>
			<div class="section" id="cffTable">
				 <h4><input type="checkbox" onclick="checkAll(this.checked);"/> Select All</h4>
				<c:forEach items="${questions}" var="myqList" varStatus="loop">
					<c:choose>
						<c:when test="${not empty myqList.question}">
							<h3><input type="checkbox" class="chck" name="qID" value="${myqList.idQuestions}" /> ${loop.count}.${myqList.question}</h3>
							<c:choose>
								<c:when test="${empty myqList.assessType}">
									<p>
										<input type="text">
									</p>
								</c:when>
								<c:when test="${myqList.assessType == 'essay'}">
									<p>
										<textarea placeholder="Form Description" name="formdesc"
											style="width: 700px;"></textarea>
									</p>
								</c:when>
								<c:when test="${myqList.assessType == 'multiple'}">
									<c:forEach items="${questionsChoice}" var="qcList"
										varStatus="loop">
										<c:choose>
											<c:when test="${qcList.qId == myqList.idQuestions}">
												<c:forEach items="${choice}" var="cList" varStatus="loop">
													<c:choose>
														<c:when test="${qcList.choiceId == cList.id}">
															<input type="radio" name="${qcList.qId}">${cList.name}
															<br>
														</c:when>
													</c:choose>
												</c:forEach>
											</c:when>
										</c:choose>
									</c:forEach>
								</c:when>
								<c:when test="${myqList.assessType == 'checkbox'}">
									<c:forEach items="${questionsChoice}" var="qcList"
										varStatus="loop">
										<c:choose>
											<c:when test="${qcList.qId == myqList.idQuestions}">
												<c:forEach items="${choice}" var="cList" varStatus="loop">
													<c:choose>
														<c:when test="${qcList.choiceId == cList.id}">
															<input type="checkbox" name="${cList.id}">${cList.name}
															<br>
														</c:when>
													</c:choose>
												</c:forEach>
											</c:when>
										</c:choose>
									</c:forEach>
								</c:when>
							</c:choose>
							<hr width="800px">
						</c:when>
					</c:choose>
				</c:forEach>
				<button type="button" data-toggle="modal" class='btn btn-default'
					data-target="#modalQuestion">Add Question</button>
				<button type="button" class='btn btn-default'onclick="removequestion('${pageContext.request.contextPath}/cff/removequestion')">Delete Question</button>
			</div>
		</form>

		<div class="modal fade" id="modalQuestion" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 style="color: red;">Add Question</h4>
					</div>
					<div class="modal-body">
						<form role="form" id="editForm" action="" method="post">
							<div class="form-group">
								<label for="question">Input Question</label>
								<textarea class="form-control" id="questionid"
									name="questionname"></textarea>
							</div>
							<div class="form-group">
								<div id="styled-select">
									<label for="newpsw"> Answer Type </label> <br /> <select
										id="answerid" name="answername" class="dropdown">
										<option value="input" name="input">Short Answer Type</option>
										<option value="essay" name="essay">Paragraph</option>
										<option value="multiple" name="multiple">Multiple
											Choice</option>
										<option value="checkbox" name="checkbox">Checkboxes</option>
									</select>
								</div>
							</div>
							<div class="optional multiple" style="display: none">
								<label for="instruction">Input choices separated by
									comma</label> <input name="mchoices" type="text"
									placeholder="ex. Potato,Patatas,Pototo" size="80">
							</div>
							<div class="optional checkbox" style="display: none">
								<label for="instruction">Input choices separated by
									comma</label> <input name="cchoices" type="text"
									placeholder="ex. Potato,Patatas,Pototo" size="80">
							</div>
							<div class="checkbox"></div>
							<div id='alert-container'></div>
							<button type="button"
								class="btn btn-default btn-success btn-block"
								onclick="addquestion('${pageContext.request.contextPath}/cff/addquestion')">
								<span class="glyphicon glyphicon-edit"></span> Add
							</button>
						</form>
					</div>
					<div class="modal-footer">
						<button type="submit"
							class="btn btn-default btn-default pull-left"
							data-dismiss="modal">
							<span class="glyphicon glyphicon-remove"></span> Cancel
						</button>
					</div>
				</div>
			</div>
		</div>
		<script src="/SoaBaseCode/lib/js/jquery-1.10.1.min.js"></script>
		<%@ include file="templates/footer.jsp"%>
		<script>
			function addquestion(URLPath) {
				alert("Adding Question");

				var data = $("#editForm").serialize();
				alert("DATA" + data);
				$.ajax({
					type : "POST",
					data : data,
					url : URLPath,
					success : function(responseData) {
						alert("DONE");
						console.log(responseData);
						$('#cffTable').html(
								$(responseData).find('#cffTable').html());
					}
				});
			}
			function checkAll(checked) {
				$(".chck").prop("checked", checked);
			}
			$("select").change(function() {
				// hide all optional elements
				$('.optional').css('display', 'none');

				$("select option:selected").each(function() {
					if ($(this).val() == "multiple") {
						$('.multiple').css('display', 'block');
					} else if ($(this).val() == "checkbox") {
						$('.checkbox').css('display', 'block');
					} else if ($(this).val() == "Other") {
						$('.other').css('display', 'block');
					}
				});
			});
			function removequestion(URLPath) {
				alert("Removing Question");
				var data = $('#sectionform1').serialize();
				alert("DATA" + data);
				$.ajax({
					type : "POST",
					data : data,
					url : URLPath,
					success : function(responseData) {
						alert("Done");
						$("#cffTable").html(
								$(responseData).find("#cffTable").html());
					}
				});
			}
		</script>
	</center>
</body>
</html>