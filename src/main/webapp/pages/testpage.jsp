<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<title>JAVA BASE CODE BETA 1.0</title>
<link rel="stylesheet"
	href="http://localhost:8080/SoaBaseCode/css/styles.css" />
<link rel="stylesheet"
	href="http://localhost:8080/SoaBaseCode/lib/css/bootstrap.min.css"
	type="text/css" />
<link rel="stylesheet"
	href="http://localhost:8080/SoaBaseCode/css/media-queries.css" />
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>

<body style="background: #DDDDDF">
	<div id="main-container" class="container"  style="width: 800px;">
		<div class="page-header">
			<h3>Insert and Update</h3>
		</div>

		<form
			
			method="GET">
			<div class="form group">
			<input type="text" name="id" class="form-control" placeholder="Enter phone id..." value="${tempid}"></input><br /> 
			<input type="text" name="brand" class="form-control" placeholder="Enter phone brand..."></input><br /> 
			<input type="text" name="name" class="form-control" placeholder="Enter phone name..."></input><br /> 
			<input type="text" name="color" class="form-control" placeholder="Enter phone color..."></input><br />
			<input type="text" name="OS_version" class="form-control" placeholder="Enter phone OS version..."></input><br />
			<input type="text" name="price" class="form-control" placeholder="Enter phone price..."></input><br />
			<button type="submit" class="btn btn-default" style="margin-left:auto;margin-right:auto;" formaction="http://localhost:8080/SoaBaseCode/testpage/insert">Insert</button><br/><br/>
			<button type="submit" class="btn btn-default" formaction="http://localhost:8080/SoaBaseCode/testpage/update">Update</button><br/>
			<br/>
			</div>
			</form>
			
		<br/>
		<c:forEach var="myListss" items="${myTable}">
        ${myListss}<br />
		</c:forEach>
		<br/>
		<br>
	</div>
</body>
</html>