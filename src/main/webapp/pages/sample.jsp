<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet"
	href="http://localhost:8080/SoaBaseCode/css/styles.css" />
<link rel="stylesheet"
	href="http://localhost:8080/SoaBaseCode/lib/css/bootstrap.min.css"
	type="text/css" />
<link rel="stylesheet"
	href="http://localhost:8080/SoaBaseCode/css/media-queries.css" />
<script type="text/javascript"
	src="http://localhost:8080/SoaBaseCode/js/assignment.js"></script>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html" charset="UTF-8" />
</head>
<body style="background: #DDDDEE">
	<div class="container" id="main-container">
		<div class="page-header">
			<h3 style="background: #DDDDEE;">
				Search and Delete
				</h4>
		</div>

		<div class="page-body" style="width: 750px;">
			<form id="asda">
				<div class="form-group">
					<select class="form-control" name="filter" id="filter">
						<option>Select All</option>
						<option>ID</option>
						<option>Name</option>
						<option>Brand</option>
						<option>Color</option>
						<option>OS Version</option>
						<option>Price</option>
					</select><br> <input class="form-control table col" type="text"
						name="inputValue" placeholder="Input value to search" id="myInput"
						onkeyup="myFunction()">
				</div>

			</form>
			<form action="${pageContext.request.contextPath}/assignment/delete"
				method="POST" id="phoneform">
				<div id="phoneTable">
					<table class="table table-striped table-bordered" id="myTable">
						<thead>
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Brand</th>
								<th>Color</th>
								<th>OS Version</th>
								<th>Price</th>
								<th><input type="checkbox"
									onclick="checkAll(this.checked);" /></th>
							</tr>
						</thead>
						<c:forEach items="${myTable}" var="myPhone">
							<tbody>
								<tr>
									<th id="ids">${myPhone.getId()}</th>
									<td>${myPhone.getName()}</td>
									<td>${myPhone.getBrand()}</td>
									<td>${myPhone.getColor()}</td>
									<td>${myPhone.getOS_version()}</td>
									<td>${myPhone.getPrice()}</td>
									<td><input type="checkbox" class="chck" name="phoneID"
										value="${myPhone.getId()}" /></td>
								</tr>
							</tbody>
						</c:forEach>
					</table>
				</div>
			</form>
		</div>
		<div class="form-group">
			<input class='btn btn-default' type="button" name="delete"
				value="Delete"
				onclick="executeDelete('${pageContext.request.contextPath}/assignment/delete')" />
	
		</div>
		<div class="form-group">
			<input class='btn btn-default' type="button" name="delete"
				value="Delete"
				onclick="executeDelete('SoaBaseCode/assignment/delete')" />
	
		</div>
	</div>
</body>
<script
	src="http://localhost:8080/SoaBaseCode/lib/js/jquery-1.10.1.min.js"></script>
<script src="http://localhost:8080/SoaBaseCode/lib/js/bootstrap.min.js"></script>

</html>