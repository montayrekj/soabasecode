<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Home Page</title>
<%@ include file="templates/import.jsp"%>
</head>
<body class="hold-transition skin-red sidebar-mini">
	<div class="wrapper">
		<header class="main-header"> <a href="#" class="logo"> <span
			class="logo-lg"> <b>Alliance Software</b>
		</span>
		</a> <nav class="navbar navbar-static-top" role="navigation"> </nav></header>
		<div id="content-wrapper" style="background-color: white">
			<div class="row">
				<div class="col-xs-12">
					<div class="jumbotron text-center">
						<h2>Alliance Training Effectiveness System</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-1"></div>
				<div class="col-xs-4">
					<h3>Hi There!</h3>
					<h4>Please Log-in to continue.</h4>
				</div>
				<div class="col-xs-2"></div>
				<div class="col-xs-4">
					<div class="panel panel-danger">
						<div class="panel-heading"><h5><label>Log-In</label></h5></div>
						<form action="" method="post" role="form" id="loginForm">
							<div class="panel-body">
								<div class="form-group">
									<label>Username</label> <input name="login_id" type="text"
										class="form-control" placeholder="Enter ID..." id="id" /><span
										id="id-err-msg" class="label label-warning"></span>
								</div>
								<div class="form-group">
									<label>Password</label> <input name="login_password"
										type="password" class="form-control"
										placeholder="Enter Password..." id="password" />
								</div>
								<div id='alert-container'></div>
							</div>
							<div class="panel-footer float-right">
								<div class="form-group">
									<button type="reset" class="btn btn-default">Clear</button>
									<button type="button" class="btn btn-primary" id="submitbtn"
										onclick="executeLogin('${pageContext.request.contextPath}/login/submit')">Submit</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="col-xs-1"></div>
			</div>
			<div class="col-xs-1"></div>
		</div>
	</div>
	<!--<div class="jumbotron text-center">
			<h1>Hi There!</h1>
			<p>Welcome to the Web App</p>
		</div>
		<div class="text-center">
			<h2>To get started, Kindly log in.</h2>
			<button class="btn btn-primary btn-lg" data-toggle="modal"
				data-target="#loginModal">Log In</button>
		</div>
		<div class="modal fade" tabindex="-1" role="dialog" id="loginModal"
			aria-labelledby="loginModalLabel">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button class="close" type="button" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title">Log in</h4>
					</div>

					<form action="" method="post" role="form" id="loginForm">
						<div class="modal-body" id="loginModalBody">
							<div class="form-group">
								<label>ID Number</label> <input name="login_id" type="number"
									class="form-control" placeholder="Enter ID..." id="id" /><span
									id="id-err-msg" class="label label-warning"></span>
							</div>
							<div class="form-group">
								<label>Password</label> <input name="login_password"
									type="password" class="form-control"
									placeholder="Enter Password..." id="password" />
							</div>
							<div id='alert-container'></div>
						</div>
						<div class="modal-footer">
							<div class="form-group">
								<button type="reset" class="btn btn-default">Clear</button>
								<button type="button" class="btn btn-primary"
									onclick="executeLogin('${pageContext.request.contextPath}/login')">Submit</button>
							</div>
						</div>
					</form>

				</div>
			</div>
		</div>
		-->
	</div>
	</div>
</body>
<script src="lib/js/jquery-1.10.1.min.js"></script>
<script src="lib/js/bootstrap.min.js"></script>
<script src="js/common.js"></script>
<script src="lib/js/jquery.blockUI.js"></script>
<script>
//TODO:
//	Move this script tag to a js file
	function executeLogin(URLPath){
	    var data = $("#loginForm").serialize();
	    
	    $.ajax({
	        url: URLPath,
	        data: data,
	        method: "post",
	        datatype: "text"
	    }).done(function(response) {
	        if(($(response).text().indexOf("password is incorrect") == -1) && ($(response).text().indexOf("User doesn't exist") == -1)){
	        	location.href = "home";	//redirect
	        }
	        else{
	            $('#alert-container').html($(response)[1].innerHTML);
	        }
	    });
	};
	$("#id").keypress(function (e) {
		if ((e.which < 65 || e.which > 90) && (e.which < 97 || e.which > 122) && (e.which < 48 || e.which > 57) && e.which != 8 && e.which != 0) {
			//display error message
			$("#id-err-msg").html("Alphanumeric characters only.").show().fadeOut(1800);
			return false;
		}
		if ($("#id").val().length == 24 && e.which != 8){
			$("#id-err-msg").html("Usernames are maximum of 24 character.").show().fadeOut(1800);
			return false;
		}
	});
	$("#password").keyup(function(event){
	    if(event.keyCode == 13){
	        $("#submitbtn").click();
	    }
	});
</script>
</html>