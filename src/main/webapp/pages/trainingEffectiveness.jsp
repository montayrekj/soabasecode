<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Training Effectiveness Evaluation</title>
<%@ include file="templates/import.jsp"%>
<link rel="stylesheet" href="/SoaBaseCode/lib/css/main.css">
</head>
<body class="hold-transition skin-red">
	<header class="main-header"> <a href="/SoaBaseCode/home"
		class="logo"> <span class="logo-mini"> <b><small>ASI</small></b>
	</span> <span class="logo-lg"> <b>Alliance</b>
	</span>
	</a> <nav class="navbar navbar-static-top" role="navigation"> <!-- <a href="#" class="sidebar-toggle" data-toggle="offcanvas"
			role="button"> <span class="sr-only">Toggle navigation</span>
		</a> -->
	<div class="navbar-custom-menu" style="height: 100%">
		<ul class="nav navbar-nav">
			<li class="dropdown user user-menu"><a href="#"
				class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"
				aria-haspopup="true"> <img
					src="/SoaBaseCode/images/thumbnail.png" class="user-image"
					alt="User Image"> <span class="hidden-xs">${user.getFirstName()}
						${user.getLastName()}</span>
			</a>
				<ul class="dropdown-menu">
					<li class="user-header"><img
						src="/SoaBaseCode/images/thumbnail.png" class="img-circle"
						alt="User Image">
						<p class="text-muted">${user.getFirstName()}
							${user.getMiddleName()} ${user.getLastName()}</p>
						<p class="text-muted text-capitalize" style="font-size: 1em">${user.getUserType()}</p></li>
					<li style="padding: 0;"><a href="/SoaBaseCode/home"
						class="col-xs-12 btn btn-default btn-flat"
						style="border: none; padding: 15px;">Home</a></li>
					<li style="padding: 0;"><a href="/SoaBaseCode/logout"
						class="col-xs-12 btn btn-default btn-flat"
						style="border: none; padding: 15px;"> Sign out </a></li>
				</ul></li>

		</ul>
	</div>
	</nav> </header>
		<center>
			<h2>Training Effectiveness Evaluation Form</h2>
			<p>Create/Edit Form</p>
		</center>
	<center>
		<form method="POST" id="teeFrm">
			<div id="teeTable">
				<table class="table table-condensed" style="width: 90%">
					<thead>
						<tr>
							<th>Question</th>
							<th width="50%" colspan="5">Choice</th>
							<th align="center" width="5%"><div class="checkbox">
									<label><input type="checkbox"
										onclick="checkAll(this.checked);" /></label>
								</div></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${questions}" var="myqList" varStatus="loop">
							<tr>
								<td
									style="text-align: justify; border-right: 1px solid #e3e3e3;"><p>${loop.count}.
										${myqList.question}</p></td>
								<c:choose>
									<c:when test="${myqList.assessType == 'multiple'}">
										<td><div class="radio">
												<label><input type="radio"
													name="${myqList.idQuestions}" value="Strongly Agree">Strongly
													Agree</label>
											</div></td>
										<td><div class="radio">
												<label><input type="radio"
													name="${myqList.idQuestions}" value="Agree">Agree</label>
											</div></td>
										<td><div class="radio">
												<label><input type="radio"
													name="${myqList.idQuestions}" value="Neutral">Neutral</label>
											</div></td>
										<td><div class="radio">
												<label><input type="radio"
													name="${myqList.idQuestions}" value="Disagree">Disagree</label>
											</div></td>
										<td><div class="radio">
												<label><input type="radio"
													name="${myqList.idQuestions}" value="Strongly Disagree">Strongly
													Disagree</label>
											</div></td>
									</c:when>
									<c:when test="${myqList.assessType == 'multiple2'}">
										<td><div class="radio">
												<label><input type="radio"
													name="${myqList.idQuestions}" value="EE">EE</label>
											</div></td>
										<td><div class="radio">
												<label><input type="radio"
													name="${myqList.idQuestions}" value="SS">SS</label>
											</div></td>
										<td><div class="radio">
												<label><input type="radio"
													name="${myqList.idQuestions}" value="ME">ME</label>
											</div></td>
										<td><div class="radio">
												<label><input type="radio"
													name="${myqList.idQuestions}" value="DN">DN</label>
											</div></td>
										<td><div class="radio">
												<label><input type="radio"
													name="${myqList.idQuestions}" value="NI">NI</label>
											</div></td>
									</c:when>
									<c:otherwise>
										<td colspan="5"><center>
												<textarea class="form-control" rows="5" cols="69"
													placeholder="Enter text..." name="${myqList.idQuestions}"></textarea>
											</center></td>
									</c:otherwise>
								</c:choose>

								<td style="border-left: 1px solid #e3e3e3;"><div
										class='checkbox'>
										<label><input type="checkbox" class="chck" name="qID"
											value="${myqList.idQuestions}" /></label>
									</div></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<br> <br>
				<button type="button" data-toggle="modal" class='btn btn-primary'
					data-target="#myModal">Add Question</button>
				<button type="button" class='btn btn-danger'
					onclick="removequestion('${pageContext.request.contextPath}/tee/removequestion')">Remove
					Question</button>
			</div>
		</form>
		<br>
		<form method="GET" id="tfrom">
			<button type="submit" class='btn btn-success'
				formaction="/SoaBaseCode/home">Done</button>
		</form>
		<br>
	</center>
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 style="color: red;">Add Question</h4>
				</div>
				<div class="modal-body">
					<form role="form" id="editForm" action="" method="post">
						<div class="form-group">
							<label for="usrname"> Question</label> <input
								class="form-control" type="text" id="questionid"
								name="questionname">
						</div>
						<div class="form-group">
							<label for="newpsw"> Answer Type </label> <br /> <select
								id="answerid" name="answername">
								<option value="essay" name="essay">Essay</option>
								<option value="multiple" name="multiple">Multiple
									Choice(ex: Strongly Agree..)</option>
								<option value="multiple2" name="multiple2">Multiple
									Choice 2 (ex: SE)</option>
							</select>
						</div>
						<div id='alert-container'></div>
						<button type="button"
							class="btn btn-default btn-success btn-block"
							onclick="addquestion('${pageContext.request.contextPath}/tee/addquestion')">
							<span class="glyphicon glyphicon-edit"></span> Add
						</button>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-default btn-default pull-left"
						data-dismiss="modal">
						<span class="glyphicon glyphicon-remove"></span> Cancel
					</button>

				</div>
			</div>
		</div>
	</div>
	<script src="/SoaBaseCode/lib/js/jquery-1.10.1.min.js"></script>
	<%@ include file="templates/footer.jsp"%>
	
	<script>
		function executeSubmit(URLPath) {
			var teeanswer = $('#teeFrm').serializeArray();

			console.log(teeanswer);
			$.ajax({
				type : "POST",
				contentType : 'application/json; charset=utf-8',

				data : JSON.stringify(teeanswer),
				url : URLPath,
				success : function(responseData) {
					location.href = "home";
					alert("Successfully submitted form!");
				}
			});

		}
		function addquestion(URLPath) {
			alert("Adding Question");

			var data = $("#editForm").serialize();

			$.ajax({
				type : "POST",
				data : data,
				url : URLPath,
				success : function(responseData) {
					alert("DONE");
					console.log(responseData);
					$('#teeTable').html(
							$(responseData).find('#teeTable').html());
				}
			});
		}
		function checkAll(checked) {
			$(".chck").prop("checked", checked);
		}
		function removequestion(URLPath) {
			alert("Removing Question");
			var data = $('#teeFrm').serialize();
			$.ajax({
				type : "POST",
				data : data,
				url : URLPath,
				success : function(responseData) {
					alert("Done");
					$("#teeTable").html(
							$(responseData).find("#teeTable").html());
				}
			});
		}
	</script>
</body>


</html>