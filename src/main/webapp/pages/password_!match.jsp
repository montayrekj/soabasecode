<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head></head>
<body>
	<div id='alert-container'>
		<div class="alert alert-danger alert-dismissable" role="body"
			id="alertDiv">
			<button class="close" data-dismiss="alert" aria-label="close"
				type="button">
				<span aria-hidden="true">×</span>
			</button>
			<strong>Password</strong> <i>doesn't match with </i> <strong>Confirm Password.</strong>
		</div>
	</div>
</body>
</html>