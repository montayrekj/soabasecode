<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="templates/import.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Reports</title>
</head>
<body class="hold-transition skin-red sidebar-mini sidebar-collapse">
	<div class="wrapper">
		<%@ include file="templates/header.jsp"%>
		<%@ include file="templates/sidenav.jsp"%>
		<div class="content-wrapper">
			<section class="content-header">
			<h1>
				Reports <small>Training Effectiveness.</small>
			</h1>
			</section>
			<section class="content">
			<div class="row">
				<div class="col-xs-4">
					<div class="box box-success">
						<div class="overlay" id="tr-details-load" style="display: none">
							<i class="fa fa-refresh fa-spin"></i>
						</div>
						<div class="box-header">
							<center>
								<h4>Details</h4>
							</center>
						</div>
						<div id="tr-details-info">
							<div class="box-body box-profile">
								<strong><i class="fa fa-id-card-o margin-r-5"></i> Training ID</strong>
								<p class="text-muted">&nbsp;${t.getTrainingId()}</p>
								<hr>
								<strong><i class="fa fa-file-text-o margin-r-5"></i> Training Title</strong>
								<p class="text-muted">&nbsp;${t.getTrainingTitle()}</p>
								<hr>
								<strong><i class="fa fa-calendar-check-o margin-r-5"></i>
									Start Date</strong>
								<p class="text-muted">&nbsp;${t.getDateStart()}</p>
								<hr>
								<strong><i class="fa fa-calendar-minus-o margin-r-5"></i>
									End Date</strong>
								<p class="text-muted">&nbsp;${t.getDateEnd()}</p>
							</div>
							<div class="box-footer">
								<center>
									<h4>What aspects of the training could be improved?</h4>
								</center>
							</div>	
							<div class="box-body">
								<table style="max-height: 200px;overflow-y:auto" class="table table-hover table-bordered">
									<c:forEach items="${others}" var="item">
										<tr>
											<c:if test="${item.getQId()== 58}">
												<td colspan="3">${item.getAnswer()}</td>
											</c:if>
										</tr>
									</c:forEach>
								</table>								
								<hr>
								<center>
									<h4>Other comments</h4>
								</center>
								<table style="max-height: 200px;overflow-y:auto" class="table table-hover table-bordered">
									<c:forEach items="${others}" var="item">
										<tr>
											<c:if test="${item.getQId()== 59}">
												<td colspan="3">${item.getAnswer()}</td>
											</c:if>
										</tr>
									</c:forEach>
								</table>			
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-8">
					<div class="box box-warning">
						<div class="box-header">
							<center>
								<h4>Consolidated Answers</h4>
							</center>
						</div>
						<c:forEach items="${questions}" var="item" varStatus = "loop">
							<c:if test="${item.getAssessType() == 'multiple' || item.getAssessType() == 'multiple2'}">
								<div class="box-body">
									<div id="container${item.getIdQuestions()}" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
								</div>
							</c:if>
						</c:forEach>
						<!--  <div class="box-body">
							<div id="container1" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
							<div id="container2" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
							<div id="container3" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
							<div id="container4" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
							<div id="container5" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
							<div id="container6" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
							<div id="container7" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
						</div> -->
					</div>
				</div>
			</div>
			</section>
		</div>
	</div>
</body>
<%@ include file="templates/footer.jsp"%>
<script>
$(document).ready(function(){
	var names = ['Strongly Agree','Agree','Neutral','Disagree','Strongly Disagree'];
	var names2 = ['EE','SS', 'ME','DN','MI'];
	var containers = [];
	var multipleId = [];
	var multiple2Id = [];
	var multiple = 0;
	var multiple2 = 0;
	var ctr = 0;
	//var qs = ['${questions[0].getQuestion()}','${questions[1].getQuestion()}','${questions[2].getQuestion()}','${questions[3].getQuestion()}','${questions[4].getQuestion()}','${questions[5].getQuestion()}']
	var qs = [];
	$.ajax({
		url: '/SoaBaseCode/admin/eff-questions',
		type: 'get',
		data: {
			'formId': '4'
		},
		success: function(responseData){
			object = responseData;
			for(var x=0; x<object.length; x++){
				if(object[x].assessType == "multiple"){
					containers.push('container'+object[x].idQuestions);
					multipleId.push(ctr);
					console.log("XM: " + ctr);
					//multiple++;
					ctr++;
				} else if(object[x].assessType == "multiple2"){
					containers.push('container'+object[x].idQuestions);
					multiple2Id.push(ctr);
					console.log("XM2: " + ctr);
					//multiple2++;
					ctr++;
				}
			}
		}
	});
	$.ajax({
		url: '/SoaBaseCode/admin/eff-questionNames',
		type: 'get',
		data: {
			'formId': '4'
		},
		success: function(responseData){
			object = responseData;
			for(var x = 0; x < object.length; x++){
				var temp=object[x];
				console.log(containers[x] + " : " +temp);
				qs.push(temp);
			}
		}
	});
	$.ajax({
		url: '/SoaBaseCode/admin/eff-feedback',
		type: 'get',
		data: {
			'trainingId': '${t.getTrainingId()}'
		},
		success: function(responseData){
			object = responseData;
			console.log("Length: " + object[1]);
			if(object.length == 0){
				for(var x=0;x<multipleId.length;x++){
					var a = multipleId[x];
					var chartArray = [];
					chartArray.push({name:"No Reponses",y:1,color:'#b3b3b3'});
					loadChart(chartArray, containers[a], qs[a]);
				}
			}else{
				if(object[x] != null){
					for(var x=0;x<multipleId.length;x++){
						var a = multipleId[x];
						var chartArray = [];
						for(var z=2;z<object[x].length;z++){
							chartArray.push({name:names[z-2],y:object[x][z]});
						}
						loadChart(chartArray, containers[a], qs[a]);
					}
				} else {
					for(var x=0;x<multipleId.length;x++){
						var a = multipleId[x];
						var chartArray = [];
						chartArray.push({name:"No Reponses",y:1,color:'#b3b3b3'});
						loadChart(chartArray, containers[a], qs[a]);
					}
				}
			}
		}
		
	});
	$.ajax({
		url: '/SoaBaseCode/admin/eff-rating',
		type: 'get',
		data: {
			'trainingId': '${t.getTrainingId()}'
		},
		success: function(responseData){
			object = responseData;
			console.log("Length: " + object[0]);
			if(object.length == 0){
				for(var x=0;x<multiple2Id.length;x++){
					var a = multiple2Id[x];
					var chartArray = [];
					chartArray.push({name:"No Response",y:1,color:'#b3b3b3'});
					loadChart(chartArray, containers[a], qs[a]);
				}
			}else{
				if(object[x] != null){
					for(var x=0;x<multiple2Id.length;x++){
						var a = multiple2Id[x];
						console.log(a);
						var chartArray = [];
						for(var z=2;z<object[x].length;z++){
							chartArray.push({name:names2[z-2],y:object[x][z]});
						}
						loadChart(chartArray, containers[a], qs[a]);
					}
				} else {
					for(var x=0;x<multiple2Id.length;x++){
						var a = multiple2Id[x];
						var chartArray = [];
						chartArray.push({name:"No Response",y:1,color:'#b3b3b3'});
						loadChart(chartArray, containers[a], qs[a]);
					}
				}
			}
		}
		
	});
	function loadChart(arr,container,q){
	 Highcharts.chart(container, {
	        chart: {
	            plotBackgroundColor: null,
	            plotBorderWidth: null,
	            plotShadow: false,
	            type: 'pie'
	        },
	        title: {
	            text: q
	        },
	        tooltip: {
	            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	        },
	        plotOptions: {
	            pie: {
	                allowPointSelect: true,
	                cursor: 'pointer',
	                dataLabels: {
	                    enabled: false
	                },
	                showInLegend: true
	            }
	        },
	        series: [{
	            name: 'Responses',
	            colorByPoint: true,
	            data: arr
	        }]
	    });
	}
	
});	
</script>
</html>