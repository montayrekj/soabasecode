<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="css/styles.css" />
<link rel="stylesheet" href="lib/css/bootstrap.min.css" type="text/css" />
<link rel="stylesheet" href="css/media-queries.css" />
<script type="text/javascript" src="js/assignment.js"></script>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html" charset="UTF-8" />
</head>
<body>
	<div class="container" id="main-container">
		<div class="page-header">
			<h4 style="background: white;">Page Header</h4>
		</div>
		<div class="page-body" style="width: 600px;">
			<form id="asda">
				<div class="form-group">
					<input class="form-control table col" type="text" name="inputValue"
						placeholder="Input value to search" id="myInput" onkeyup="myFunction()"> 
					<select
						class="form-control" name="column">
						<option value="column" disabled selected>Column</option>
						<option value="id">Name</option>
						<option value="name">Name</option>
						<option value="brand">Brand</option>
						<option value="color">Color</option>
						<option value="os_version">OS Version</option>
						<option value="price">Price</option>
					</select>
				</div>
				<div class="form-group">
					<input class='btn btn-default' type="submit" name="displayAll"
						value="Display All" /> <input class='btn btn-default'
						type="submit" name="search" value="Search" /> <input
						class='btn btn-default' type="button" name="delete" value="Delete"
						onclick="executeDelete('${pageContext.request.contextPath}/assignment/delete')" />

				</div>
			</form>
			<form action="${pageContext.request.contextPath}/assignment/delete"
				method="POST" id="phoneform">
				<div id="phoneTable">
					<table class="table table-striped table-bordered" id="myTable">
						<thead>
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Brand</th>
								<th>Color</th>
								<th>OS Version</th>
								<th>Price</th>
								<th><input type="checkbox"
									onclick="checkAll(this.checked);" /></th>
							</tr>
						</thead>
						<c:forEach items="${myTable}" var="myPhone">
							<tr>
								<th>${myPhone.getId()}</th>
								<td>${myPhone.getName()}</td>
								<td>${myPhone.getBrand()}</td>
								<td>${myPhone.getColor()}</td>
								<td>${myPhone.getOS_version()}</td>
								<td>${myPhone.getPrice()}</td>
								<td><input type="checkbox" class="chck" name="phoneID"
									value="${myPhone.getId()}" /></td>
							</tr>
						</c:forEach>
					</table>
				</div>
			</form>
		</div>
	</div>
</body>
<script src="lib/js/jquery-1.10.1.min.js"></script>
<script src="lib/js/bootstrap.min.js"></script>

</html>