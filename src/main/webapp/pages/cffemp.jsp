<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Course Feedback Form Edit</title>
<%@ include file="templates/import.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
</head>
<body>
	<center>
		<form method="POST" id="sectionform1">
			<h2>Course Feedback Form</h2>
			<div class="section" id="cffTable">
				<c:forEach items="${questions}" var="myqList" varStatus="loop">
					<c:choose>
						<c:when test="${not empty myqList.question}">
							<h3>${loop.count}.${myqList.question}</h3>
							<c:choose>
								<c:when test="${empty myqList.assessType}">
									<p>
										<input type="text">
									</p>
								</c:when>
								<c:when test="${myqList.assessType == 'essay'}">
									<p>
										<textarea placeholder="Form Description" name="${myqList.idQuestions}"
											style="width: 700px;"></textarea>
									</p>
								</c:when>
								<c:when test="${myqList.assessType == 'multiple'}">
									<c:forEach items="${questionsChoice}" var="qcList"
										varStatus="loop">
										<c:choose>
											<c:when test="${qcList.qId == myqList.idQuestions}">
												<c:forEach items="${choice}" var="cList" varStatus="loop">
													<c:choose>
														<c:when test="${qcList.choiceId == cList.id}">
															<input type="radio" name="${qcList.qId}" value="${cList.name}">${cList.name}
															<br>
														</c:when>
													</c:choose>
												</c:forEach>
											</c:when>
										</c:choose>
									</c:forEach>
								</c:when>
								<c:when test="${myqList.assessType == 'checkbox'}">
									<c:forEach items="${questionsChoice}" var="qcList"
										varStatus="loop">
										<c:choose>
											<c:when test="${qcList.qId == myqList.idQuestions}">
												<c:forEach items="${choice}" var="cList" varStatus="loop">
													<c:choose>
														<c:when test="${qcList.choiceId == cList.id}">
															<input type="checkbox" name="${cList.id}">${cList.name}
															<br>
														</c:when>
													</c:choose>
												</c:forEach>
											</c:when>
										</c:choose>
									</c:forEach>
								</c:when>
							</c:choose>
							<hr width="800px">
						</c:when>
					</c:choose>
				</c:forEach>
				<button type="button" onclick="executeSubmit('${pageContext.request.contextPath}/cff/submit')">Submit</button>
			</div>
		</form>
		<script src="/SoaBaseCode/lib/js/jquery-1.10.1.min.js"></script>
		<%@ include file="templates/footer.jsp"%>
		
		<script>
		function executeSubmit(URLPath){
			var answer = $('#sectionform1').serializeArray();
			
			console.log(answer);
			$.ajax({
				type : "POST",
				contentType : 'application/json; charset=utf-8',

				data : JSON.stringify(answer),
				url : URLPath,
				success : function(responseData) {
					location.href="home";
					alert("Successfully submitted form!");
				}
			});
		}
		</script>
	</center>
</body>
</html>