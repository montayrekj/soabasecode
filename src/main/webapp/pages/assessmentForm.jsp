<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Assessment Form</title>
<%@ include file="templates/import.jsp" %>
<link rel="stylesheet"
	href="/SoaBaseCode/lib/bootstrap/css/bootstrap.min.css" type="text/css" />
<link rel="stylesheet"
	href="/SoaBaseCode/lib/css/main.css"
	type="text/css" />
</head>
<body class="hold-transition skin-red">
<header class="main-header">
	<a href="/SoaBaseCode/home" class="logo"> <span class="logo-mini"> <b><small>ASI</small></b>
	</span> <span class="logo-lg"> <b>Alliance</b>
	</span>
	</a>
	<nav class="navbar navbar-static-top" role="navigation">
		<!-- <a href="#" class="sidebar-toggle" data-toggle="offcanvas"
			role="button"> <span class="sr-only">Toggle navigation</span>
		</a> -->
		<div class="navbar-custom-menu" style="height: 100%">
			<ul class="nav navbar-nav">
				<li class="dropdown user user-menu"><a href="#"
					class="dropdown-toggle" data-toggle="dropdown"
					aria-expanded="false" aria-haspopup="true"> <img
						src="/SoaBaseCode/images/thumbnail.png" class="user-image" alt="User Image">
						<span class="hidden-xs">${user.getFirstName()} ${user.getLastName()}</span>
				</a>
					<ul class="dropdown-menu">
						<li class="user-header"><img src="/SoaBaseCode/images/thumbnail.png"
							class="img-circle" alt="User Image">
							<p  class="text-muted" >${user.getFirstName()} ${user.getMiddleName()} ${user.getLastName()}</p>
							<p class="text-muted text-capitalize" style="font-size: 1em">${user.getUserType()}</p></li>
						<li style="padding: 0;"><a href="/SoaBaseCode/home"
							class="col-xs-12 btn btn-default btn-flat"
							style="border: none; padding: 15px;">Home</a></li>
						<li style="padding: 0;"><a href="/SoaBaseCode/logout"
							class="col-xs-12 btn btn-default btn-flat"
							style="border: none; padding: 15px;"> Sign out </a></li>
					</ul></li>

			</ul>
		</div>
	</nav>
</header>
<div class="jumbotron"><center><h2>Assessment Form</h2><p>Assess your skills before training</p></center></div>
	<div>
		<center>
			<form id="frm1" action="" method="POST">
				<div id="assessmentTable">

				<table class="table table-condensed" style="width: 60%" id="assessTable">
					<c:forEach items="${assessmentFormSC}" var="myFormC">
							<c:choose>
								<c:when
									test="${not empty myFormC.id && not empty myFormC.form.id}">
									<fmt:parseNumber var="id1" integerOnly="true" type="number"
										value="${myFormC.id}" />
									<thead><tr><th colspan="2" class="subtopic"><h3>${myFormC.name}</h3></th></thead>
									<fmt:parseNumber var="idz" integerOnly="true" type="number"
										value="${myFormC.id}" />

									<c:forEach items="${assessmentFormSC}" var="myFormSC">
										<c:choose>
											<c:when test="${not empty myFormSC.id}">
												<c:choose>
													<c:when test="${not empty myFormSC.formSc.form.id}">

														<fmt:parseNumber var="idd" integerOnly="true"
															type="number" value="${myFormSC.formSc.id}" />
														<fmt:parseNumber var="id2" integerOnly="true"
															type="number" value="${myFormSC.id}" />



														<c:choose>

															<c:when test="${myFormSC.formSc.id == idz}">


																<fmt:parseNumber var="id3" integerOnly="true"
																	type="number" value="${myFormC.id}" />
																<thead>
																	<th>${myFormSC.name}</th>
																	<th>${userType}</th>
																</thead>

																<c:forEach items="${questions}" var="myQuestions">


																	<c:choose>

																		<c:when test="${myQuestions.fkFscid == id2}">
																			
																			<tr>
																				<td>${myQuestions.question}</td>
																				<td id="${myQuestions.idQuestions}"><c:choose>

																						<c:when
																							test="${myQuestions.assessType == 'skills'}">

																							<select class="form-control" name="${myQuestions.idQuestions}">
																								<option selected></option>
																								<option value="low">Low</option>
																								<option value="beginner">Beginner</option>
																								<option value="mid/practicing">Mid/Practicing</option>
																								<option value="high">High Performance</option>
																							</select>

																						</c:when>
																						<c:otherwise>
																							<select class="form-control" name="${myQuestions.idQuestions}">
																								<option selected></option>
																								<option value="N/a">n/a</option>
																								<option value="0">0</option>
																								<option value=">1yr">> 1yr</option>
																								<option value="2yrs">2 years</option>
																								<option value="3yrs">3 years</option>
																								<option value="4yrs">4 years</option>
																								<option value="<5yrs">< 5yrs</option>
																							</select>
																						</c:otherwise>
																					</c:choose></td>

																			</tr>
																		</c:when>
																		<c:when test="${myQuestions.fkFscid == idz}">

																		</c:when>
																	</c:choose>
																</c:forEach>

															</c:when>



														</c:choose>
													</c:when>
												</c:choose>
											</c:when>
										</c:choose>
									</c:forEach>

									<td></td>
									<td></td> 
									<c:forEach items="${questions}" var="myQuestions">
										<c:choose>
											<c:when test="${myQuestions.fkFscid == idz}">

												<tr>
													<td>${myQuestions.question}</td>
													<td id="${myQuestions.idQuestions}"><c:choose>
															<c:when test="${myQuestions.assessType == 'skills'}">
																<select class="form-control" name="${myQuestions.idQuestions}">
																	<option selected></option>
																	<option value="low">Low</option>
																	<option value="beginner">Beginner</option>
																	<option value="mid/practicing">Mid/Practicing</option>
																	<option value="high">High Performance</option>
																</select>
															</c:when>
															<c:otherwise>
																<select class="form-control" name="${myQuestions.idQuestions}">
																	<option selected></option>
																	<option value="n/a">N/A</option>
																	<option value="0">0</option>
																	<option value=">1yr">> 1yr</option>
																	<option value="2yrs">2 years</option>
																	<option value="3yrs">3 years</option>
																	<option value="4yrs">4 years</option>
																	<option value="<5yrs">< 5yrs</option>
																</select>
															</c:otherwise>
														</c:choose></td>
												</tr>

											</c:when>
										</c:choose>
									</c:forEach>

								</c:when>
							</c:choose>

					</c:forEach>
				</table>
			</div>
			</form>
			<br>
			<button type="button" id="submitFrm" class="btn btn-primary"
				onclick="executeSubmit('${pageContext.request.contextPath}/assessment/submit')">Submit
				Form</button>

		</center>
		<br>

	</div>
</body>
<script src="lib/js/jquery-1.10.1.min.js"></script>
<script src="lib/js/bootstrap.min.js"></script>
<script src="lib/js/jquery.blockUI.js"></script>
<script>
	function executeSubmit(URLPath) {
		var answer = $('#frm1').serializeArray();

		console.log("Answer: " + JSON.stringify(answer));

		$.ajax({
			type : "POST",
			contentType : 'application/json; charset=utf-8',

			data : JSON.stringify(answer),
			url : URLPath,
			success : function(responseData) {
				location.href="home";
				alert("Successfully submitted form!");
			}
		});

		console.log(answer);
	}
</script>
</html>