<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="templates/import.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Training Information</title>
</head>
<body class="hold-transition skin-red sidebar-mini sidebar-collapse">
	<div class="wrapper">
		<%@ include file="templates/header.jsp"%>
		<%@ include file="templates/sidenav.jsp"%>
		<div class="content-wrapper">
			<section class="content-header">
			<h1>
				Training information <small>Check training information.</small>
			</h1>
			<a href="/SoaBaseCode/calendar"><button class="add btn btn-info" type="button">Back</button></a><br><br>
			</section>
			<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box box-success">
						<div class="overlay" id="tr-details-load" style="display: none">
							<i class="fa fa-refresh fa-spin"></i>
						</div>
						<div class="box-header">
							<h3>Training details</h3>
						</div>
						<div class="box-body" style="display: flex; flex-wrap: wrap;">
							<div class="info_sections">
								<div class="details">
									<b>ID:</b> <a id="det_id">${training.trainingId}</a>
								</div>
								<div class="details">
									<b>Plan ID:</b> <a id="det_plan">${training.annualPlan}</a>
								</div>
								<div class="details">
									<b>Title:</b> <a id="det_title">${training.trainingTitle}</a>
								</div>
								<div class="details">
									<b>Start Date:</b> <a id="det_start">${training.dateStart}</a>
								</div>
								<div class="details">
									<b>End Date:</b> <a id="det_end">${training.dateEnd}</a>
								</div>
							</div>
							<div id="objectives" class="info_sections">
								<b>Objective(s):</b><br>
								<div id="objectives_list">
									<ul id="det_obj">
										<c:choose>
											<c:when test="${not empty objectives}">
												<c:forEach items="${objectives}" var="objective">
													<li>${objective}</li>
												</c:forEach>
											</c:when>
											<c:when test="${empty objectives}">
											No objectives specified.
										</c:when>
										</c:choose>
									</ul>
								</div>
							</div>
							<div id="outline" class="info_sections">
								<b>Outline:</b>
								<div style="margin-top: 2px;" id="det_outline">
									<c:choose>
										<c:when test="${not empty training.outline}">
											<textarea id="outline_content" readonly>${training.outline}</textarea>
										</c:when>
										<c:when test="${empty training.outline}">
											<textarea id="outline_content" readonly>No outline specified.</textarea>
										</c:when>
									</c:choose>
								</div>
							</div>
						</div>
					</div>
					<div class="box box-danger">
						<div class="box-header">
							<h3>Training Facilitator(s)</h3>
						</div>
						<div class="box-body">
							<table id="v-facilitators" class="display">
								<thead>
									<tr>
										<th>First Name</th>
										<th>Last Name</th>
										<th>Type</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
					<div class="box box-warning">
						<div class="box-header">
							<h3>Training Participant(s)</h3>
						</div>
						<div class="box-body">
							<table id="v-participants" class="display">
								<thead>
									<tr>
										<th>First Name</th>
										<th>Last Name</th>
										<th>Type</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
			</section>
		</div>
	</div>
</body>
<%@ include file="templates/footer.jsp"%>

<script type="text/javascript">
$(document).ready(function() {
	var trainingId = $('#det_id').text();
	var faci = $('#v-facilitators').DataTable({
		"ajax": {
			'type' : 'get',
			'data' : {
				'training_id' : trainingId
			},
			'url' : '/SoaBaseCode/trainings/getAllFacilitators',
			'dataSrc' : function(responseData){
				return responseData;
			}
		},
		"createdRow": function ( row, data, index ) {
            $(row).addClass('user_row');
        },
		"columns" : [{data : "firstName",
		              createdCell: function (td, cellData, rowData, row, col){
		            	  			$(td).attr('id','fname');
		              			}
					 },
		             {data : "lastName",
		              createdCell: function (td, cellData, rowData, row, col){
		            	  			$(td).attr('id','lname');
		              			}
					 },
		             {"data" : "userType"} ],
		"columnDefs" : [ {
			"targets" : "no-sort",
			"orderable" : false
		} ]
	});
	faci.on( 'xhr', function ( e, settings, json, xhr ) {
	 	if ( json === null ) {
			faci.clear().draw();
	  		return true
	  	}
	});
	var parti = $('#v-participants').DataTable({
		"ajax": {
			'type' : 'get',
			'data' : {
				'training_id' : trainingId
			},
			'url' : '/SoaBaseCode/trainings/getAllParticipants',
			'dataSrc' : function(responseData){
				return responseData;
			}
		},
		"createdRow": function ( row, data, index ) {
            $(row).addClass('user_row');
        },
		"columns" : [{data : "firstName",
		              createdCell: function (td, cellData, rowData, row, col){
		            	  			$(td).attr('id','fname');
		              			}
					 },
		             {data : "lastName",
		              createdCell: function (td, cellData, rowData, row, col){
		            	  			$(td).attr('id','lname');
		              			}
					 },
		             {"data" : "userType"}
		             ],
		"columnDefs" : [ {
			"targets" : "no-sort",
			"orderable" : false
		} ]
	});
	parti.on( 'xhr', function ( e, settings, json, xhr ) {
		if ( json === null ) {
			parti.clear().draw();
	    	return true;
		}
	});
	setInterval( function () {
	    faci.ajax.reload();
	}, 60000 );
	setInterval( function () {
	    parti.ajax.reload();
	}, 60000 );
});
</script>
</html>