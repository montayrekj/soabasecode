<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
 <title>JAVA BASE CODE BETA 1.0</title>
	<link rel="stylesheet" href="css/styles.css" />
	<link rel="stylesheet" href="lib/css/bootstrap.min.css" />
	<link rel="stylesheet" href="css/media-queries.css" />
  	<meta name="viewport" content="width=device-width" /> 	
  	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>

<body style="background: #DDDDDF">
<div id="main-container" class="container">
	<div class="page-header"> 
		<h3> CALCULATOR </h3>
	</div>
	
	<div class="container">
		<form action="http://localhost:8080/SoaBaseCode/calculator" method="GET">
			<div class="table col form-group">
				<label for="1stnum">1st number</label> 
				<input type="text" name="num1" id="1stnum" class="form-control" placeholder="Enter a number"></input><br/>
			</div>
			
			<div class="form-group "> 
			
				<label for="2ndnum">2nd number</label>
				
				<input type="text" name="num2" id="2ndnum" class="form-control" placeholder="Enter a number"></input><br/>
				
			</div>
			
			<div class="table col"> 
			
				<label for="opss">Operator:</label>
				<input type="text" name="op" id="opss" class="form-control" placeholder="Enter operator (eg: + - / *)"></input><br/>
			
			</div>
			
			<div class="${classDiv}"><br/>Result through the Servlet Request: ${result} <br></div>
			<div class="${classDiv}">Result through Mapping: ${result2}</div>
			
			<div class="table col"><button type="submit" class="btn btn-default">Submit</button><br/></div>
			
			<div class="page-header"> 
			</div>
		</form>
	</div>
	
</div>
</body>