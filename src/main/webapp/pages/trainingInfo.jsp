<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="templates/import.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Administrator</title>
</head>
<body class="hold-transition skin-red sidebar-mini sidebar-collapse">
	<c:if test="${empty training}">
		<c:redirect url="/home" />
	</c:if>
	<div class="wrapper">
		<%@ include file="templates/header.jsp"%>
		<%@ include file="templates/sidenav.jsp"%>
		<div class="content-wrapper">
			<section class="content-header">
			<h1>
				Training information <small>Edit, update training's
					information.</small>
			</h1>
			<a
				href="/SoaBaseCode/admin/trainingsched?plan_id=${training.annualPlan}"><button
					class="add btn btn-info" type="button">Back</button></a> </section>
			<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box box-success">
						<div class="overlay" id="tr-details-load" style="display: none">
							<i class="fa fa-refresh fa-spin"></i>
						</div>
						<div class="box-header">
							<h3>Training details</h3>
							<button type="button" id="del-training"
								class="add btn btn-danger">
								<span class="glyphicon glyphicon-trash"></span> Delete
							</button>
							<button type="button" id="edit" class="add btn btn-success"
								data-toggle="modal" data-target="#edit_info">
								<span class="glyphicon glyphicon-edit"></span> Edit
							</button>
							<button type="button" id="edit" class="add btn btn-warning"
								data-toggle="modal" data-target="#frm">
								<span class="glyphicon glyphicon-list-alt"></span> Forms
							</button>
							<a id="att_list"
								href="/SoaBaseCode/trainings/getAttendance?trainingId=${training.trainingId}"><button
									class="add btn btn-info">
									<i class="fa fa-calendar" aria-hidden="true"></i> Attendance
								</button></a>
						</div>
						<div class="box-body" style="display: flex; flex-wrap: wrap;">
							<div class="info_sections">
								<div class="details">
									<b>ID:</b> <a id="det_id">${training.trainingId}</a>
								</div>
								<div class="details">
									<b>Plan ID:</b> <a id="det_plan">${training.annualPlan}</a>
								</div>
								<div class="details">
									<b>Title:</b> <a id="det_title">${training.trainingTitle}</a>
								</div>
								<div class="details">
									<b>Start Date:</b> <a id="det_start">${training.dateStart}</a>
								</div>
								<div class="details">
									<b>End Date:</b> <a id="det_end">${training.dateEnd}</a>
								</div>
							</div>
							<div id="objectives" class="info_sections">
								<b>Objective(s):</b><br>
								<div id="objectives_list">
									<ul id="det_obj">
										<c:choose>
											<c:when test="${not empty objectives}">
												<c:forEach items="${objectives}" var="objective">
													<li>${objective}</li>
												</c:forEach>
											</c:when>
											<c:when test="${empty objectives}">
											No objectives specified.
										</c:when>
										</c:choose>
									</ul>
								</div>
							</div>
							<div id="outline" class="info_sections">
								<b>Outline:</b>
								<div style="margin-top: 2px;" id="det_outline">
									<c:choose>
										<c:when test="${not empty training.outline}">
											<textarea id="outline_content" readonly>${training.outline}</textarea>
										</c:when>
										<c:when test="${empty training.outline}">
											<textarea id="outline_content" readonly>No outline specified.</textarea>
										</c:when>
									</c:choose>
								</div>
							</div>
						</div>
					</div>
					<div class="box box-danger">
						<div class="box-header">
							<h3>Training Facilitator(s)</h3>
							<span id="faci-feedback" style="color: #228B22"></span>
							<button id="add_faci" class="add btn btn-success"
								<c:if test="${training.getDateEnd().compareTo(today) < 0}"><c:out value="disabled"/></c:if>>
								<i class="fa fa-user-plus" aria-hidden="true"></i> Add
							</button>
							<button id="remove_faci" class="add btn btn-danger"
								<c:if test="${training.getDateEnd().compareTo(today) < 0}"><c:out value="disabled"/></c:if>>
								<i class="fa fa-user-times" aria-hidden="true"></i> Remove
							</button>
						</div>
						<div class="box-body">
							<table id="facilitators" class="display">
								<thead>
									<tr>
										<th class="no-sort"></th>
										<th>First Name</th>
										<th>Last Name</th>
										<th>Type</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
					<div class="box box-warning">
						<div class="box-header">
							<h3>Training Participant(s)</h3>
							<span id="parti-feedback" style="color: #228B22"></span>
							<button id="add_parti" class="add btn btn-success"
								<c:if test="${training.getDateEnd().compareTo(today) < 0}"><c:out value="disabled"/></c:if>>
								<i class="fa fa-user-plus" aria-hidden="true"></i> Add
							</button>
							<button id="remove_parti" class="add btn btn-danger"
								<c:if test="${training.getDateEnd().compareTo(today) < 0}"><c:out value="disabled"/></c:if>>
								<i class="fa fa-user-times" aria-hidden="true"></i> Remove
							</button>
						</div>
						<div class="box-body">
							<table id="participants" class="display">
								<thead>
									<tr>
										<th class="no-sort"></th>
										<th>First Name</th>
										<th>Last Name</th>
										<th>Type</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
			</section>
			<div id="edit_info" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header"
							style="background-color: #dd4b39; color: #fff">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">
								Edit training: <a id="training_id" style="color: #fff">${training.trainingId}</a>
							</h4>
						</div>
						<div class="modal-body">
							<div id="fail" class="feedback">
								<i class="fa fa-exclamation-circle" aria-hidden="true"></i>
								Failed to edit training!
							</div>
							<div id="success" class="feedback">
								<i class="fa fa-check-circle" aria-hidden="true"></i> Training
								information updated!
							</div>
							<form id="edit_form">
								<div>
									<label>Training Title:</label><br> <input
										id="training_title" type="text" name="trainingTitle"
										class="form-control form-input"
										value="${training.trainingTitle}" required> <span
										id="title_err" class="label label-danger" hidden>Training
										title must not be blank.</span>
								</div>
								<div>
									<label>Training Date Start:</label><br> <input
										id="date_start" type="date" name="dateStart"
										class="form-control form-input" value="${training.dateStart}"
										required>
								</div>
								<div>
									<label>Training Date End:</label><br> <input id="date_end"
										type="date" name="dateEnd" class="form-control form-input"
										value="${training.dateEnd}" required> <span
										id="date_err" class="label label-danger" hidden>Date
										end must be later or same as date start.</span>
								</div>
								<div>
									<small>Add objective:</small><br>
									<div>
										<div style="display: flex">
											<input id="objective" type="text" name="objective"
												class="form-control form-input" required> <input
												id="add" type="button" class="bttn btn-primary" value="Add">
										</div>
										<span id="obj_err" class="label label-danger">Can't add
											a blank objective.</span>
									</div>
								</div>
							</form>
							<div>
								<label>Objective(s):</label><br>
								<div id="objective_div" class="form-control form-input">
									<c:forEach items="${objectives}" var="objective">
										<div class="node">
											<div class="o_content">${objective}</div>
											<a id="remove_obj" class="close"> &times;</a>
										</div>
									</c:forEach>
								</div>
							</div>
							<div>
								<label>Training Outline:</label><br>
								<textarea id="edit_outline" name="outline" style="resize: none"
									form="edit_form" class="form-control form-input">${training.outline}</textarea>
								<span id="" class="err" hidden>Training title must not be
									blank.</span>
							</div>
						</div>
						<div class="modal-footer">
							<button form="edit_form" type="button" id="edit_submit"
								class="btn btn-success" style="width: 100%">Submit</button>
						</div>
					</div>

				</div>
			</div>
			<div id="master_list" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header"
							style="background-color: #dd4b39; color: #fff">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Users' Master List</h4>
						</div>
						<div class="modal-body">
							<table id="master_tab" style="width: 100%">
								<thead>
									<tr>
										<th class="no-sort"></th>
										<th>First Name</th>
										<th>Last Name</th>
										<th>Type</th>
									</tr>
								</thead>
							</table>
						</div>
						<div class="modal-footer">
							<button type="button" id="add_user" class="btn btn-success"
								<c:if test="${training.getDateEnd().compareTo(today) < 0}"><c:out value="disabled"/></c:if>>Add</button>
						</div>
					</div>
				</div>
			</div>
			<div id="assign_list" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header"
							style="background-color: #dd4b39; color: #fff">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Participants' Master List (Assign)</h4>
						</div>
						<div class="modal-body">
							<table id="assign_tab" style="width: 100%">
								<thead>
									<tr>
										<th class="no-sort"></th>
										<th>First Name</th>
										<th>Last Name</th>
										<th>Type</th>
									</tr>
								</thead>
							</table>
						</div>
						<div class="modal-footer">
							<button type="button" id="assign_frm" class="btn btn-default">Add</button>
						</div>
					</div>
				</div>
			</div>
			
			<div id="assign_list2" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header"
							style="background-color: #dd4b39; color: #fff">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Facilitators' Master List (Assign)</h4>
						</div>
						<div class="modal-body">
							<table id="assign_tab2" style="width: 100%">
								<thead>
									<tr>
										<th class="no-sort"></th>
										<th>First Name</th>
										<th>Last Name</th>
										<th>Type</th>
									</tr>
								</thead>
							</table>
						</div>
						<div class="modal-footer">
							<button type="button" id="assign_frm2" class="btn btn-default">Add</button>
						</div>
					</div>
				</div>
			</div>
			
			<div id="frm" class="modal fade" role="dialog">
				<div class="modal-dialog modal-lg">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header"
							style="background-color: #dd4b39; color: #fff">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Forms</h4>
						</div>
						<div class="modal-body">
							<table id="frm_tab" class="display">
								<thead>
									<tr>

										<th>Name</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${forms}" var="form">
									<c:if test="${form.id == 3 || form.id == 4 || form.id == 5}">
										<tr>
											<td>${form.name}</td>
											<td>
												<c:choose>
												<c:when test="${form.id == 3 || form.id == 4 || form.id == 5}">
													<c:choose>
													<c:when test="${form.id == 2}">
													<button class="add btn btn-info" data-dismiss="modal"
														data-toggle="modal" data-target="#assign_list2"
														name="frmAssign" id="${form.id}">Assign</button>
													</c:when>
													<c:otherwise>
													<button class="add btn btn-info" data-dismiss="modal"
														data-toggle="modal" data-target="#assign_list"
														name="frmAssign" id="${form.id}">Assign</button>
													</c:otherwise>
													</c:choose>
													<c:choose>
													<c:when test="${form.id == 2}">
													<button class="add btn btn-danger" name="frmRelease2"
														id="${form.id}">Release</button>
													</c:when>
													<c:otherwise>
													<button class="add btn btn-danger" name="frmRelease"
														id="${form.id}">Release</button>
													</c:otherwise>
													</c:choose>
													
													<button class="add btn btn-warning">Generate</button>
													<button class="add btn btn-success" id="${form.id}" name="frmEdit">Edit</button>
													<!--  <button class="add btn btn-primary">Add</button> -->
												</c:when>
												</c:choose>
											</td>
										</tr>
										</c:if>
										<c:if test="${form.id == 1 || form.id == 2}">
											<c:if test="${form.id == 1 && training.skAssessFrm != 'released'}">
													<tr>
													<td>${form.name}</td>
													<td><button class="add btn btn-success" id="${form.id}" name="frmEdit">Edit</button>
													<!--  <button class="add btn btn-primary">Add</button> --></td>
													</tr>
											</c:if>
											<c:if test="${form.id == 2 && training.tnaFrm != 'released'}">
													<tr>
													<td>${form.name}</td>
													<td><button class="add btn btn-success" id="${form.id}" name="frmEdit">Edit</button>
													<!--  <button class="add btn btn-primary">Add</button> --></td>
													</tr>
											</c:if>
										</c:if>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<div class="modal-footer">
							<button type="button" id="add_user" class="btn btn-default">Add</button>
						</div>
					</div>
				</div>
			</div>
			<div id="prompt_delete" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header"
							style="background-color: #dd4b39; color: #fff">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Are you sure you want to remove the
								following?</h4>
						</div>
						<div class="modal-body">
							<div id="prompt-tab-container" hidden>
								<table id="prompt-tab" style="width: 100%">
									<thead>
										<th>First Name</th>
										<th>Last Name</th>
									</thead>
								</table>
							</div>
							<div id="affiliated" class="panel panel-warning"
								style="margin-top: 2%;" hidden>
								<div class="panel-heading">
									<h3 class="panel-title">Affected records in this delete.</h3>
									<span class="pull-right clickable panel-collapsed"><i
										class="glyphicon glyphicon-chevron-down"></i></span>
								</div>
								<div class="panel-body" style="display: none; padding: 0"></div>
							</div>
						</div>
						<div class="modal-footer" style="display: flex; flex-wrap: wrap;">
							<div id="feedback" style="width: 78%; text-align: left"></div>
							<div id="buttons">
								<button type="button" id="proceed" class="yes btn btn-success">Yes</button>
								<button type="button" id="cancel" class="btn btn-danger">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="prompt-delete-tr" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header"
							style="background-color: #dd4b39; color: #fff">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Delete</h4>
						</div>
						<div class="modal-body">
							<div class="tr-err" style="display: none">
								<i class="fa fa-exclamation-triangle" aria-hidden="true">
									Deleting this record might result to deletion of the following:</i>
							</div>
							<div id="tr-affiliated" class="panel panel-warning"
								style="margin-top: 2%;" hidden>
								<div class="panel-heading">
									<h3 class="panel-title">Affected records in this delete.</h3>
									<span class="pull-right clickable panel-collapsed"><i
										class="glyphicon glyphicon-chevron-down"></i></span>
								</div>
								<div class="panel-tr panel-body"
									style="display: none; padding: 0"></div>
							</div>
							<div>
								<br>
								<h4>
									<b>Are you sure you want to delete this training?</b>
								</h4>
							</div>
						</div>
						<div class="modal-footer" style="display: flex; flex-wrap: wrap;">
							<div id="tr-feedback" style="width: 78%; text-align: left"></div>
							<div id="buttons">
								<button type="button" id="tr-proceed" class="btn btn-success">Yes</button>
								<button type="button" id="tr-cancel" class="btn btn-danger">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<%@ include file="templates/footer.jsp"%>
<style>
.label {
	display: none;
}

.panel-heading span {
	margin-top: -20px;
	font-size: 15px;
}

.panel-body {
	max-height: 205px;
	overflow-y: auto;
}

.row {
	margin-top: 40px;
	padding: 0 10px;
}

.clickable {
	cursor: pointer;
}

#item:hover {
	background-color: #dd4b39;
	color: #f2f2f2;
	cursor: pointer;
}
</style>
<script type="text/javascript">
	$(document).ready(
			function() {
				$('.panel-heading span.clickable').on(
						"click",
						function(e) {
							if ($(this).hasClass('panel-collapsed')) {
								// expand the panel
								$(this).parents('.panel').find('.panel-body')
										.slideDown();
								$(this).removeClass('panel-collapsed');
								$(this).find('i').removeClass(
										'glyphicon-chevron-down').addClass(
										'glyphicon-chevron-up');
							} else {
								// collapse the panel
								$(this).parents('.panel').find('.panel-body')
										.slideUp();
								$(this).addClass('panel-collapsed');
								$(this).find('i').removeClass(
										'glyphicon-chevron-up').addClass(
										'glyphicon-chevron-down');
							}
						});
			});
</script>
</html>