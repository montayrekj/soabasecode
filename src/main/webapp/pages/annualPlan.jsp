<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Annual Plan</title>
<%@ include file="templates/import.jsp"%>
</head>
<body class="hold-transition skin-red sidebar-mini sidebar-collapse">
	<div class="wrapper">
		<%@ include file="templates/header.jsp"%>
		<%@ include file="templates/sidenav.jsp"%>
		<div class="content-wrapper">
			<section class="content-header">
			<h1>
				Annual Plan <small>Create, update annual plans.</small>
			</h1>
			</section>
			<section class="content">
			<div class="row">
				<div class="col-xs-4">
					<div class="box box-success">
						<div class="overlay" id="tr-details-load" style="display: none">
							<i class="fa fa-refresh fa-spin"></i>
						</div>
						<div class="box-header">
							<center>
								<h4>Plan Details</h4>
							</center>
						</div>
						<div id="tr-details-info">
							<div class="box-body">
								<strong><i class="fa fa-id-card margin-r-5"></i> ID</strong>
								<p class="text-muted">&nbsp;${plan.getId()}</p>
								<hr>
								<strong><i class="fa fa-indent margin-r-5"></i> Title </strong>
								<p class="text-muted">&nbsp;${fn:substring(plan.getDateCreated(), 0, 4)}
									<c:if test="${not empty plan.getId()}">
										<c:out value=" Training Plan"/>
									</c:if></p>
								<hr>
								<strong><i class="fa fa-calendar-check-o margin-r-5"></i>
									Date Created</strong>
								<p class="text-muted">&nbsp;${plan.getDateCreated()}</p>
								<hr>
								<strong><i class="fa fa-spinner margin-r-5"></i>
									Pending Events</strong>
								<p class="text-muted">&nbsp;${plan.getPendingEvents() }</p>
								<hr>
								<strong><i class="fa fa-play-circle-o margin-r-5"></i>
									Ongoing Events</strong>
								<p class="text-muted">&nbsp;${plan.getOngoingEvents() }</p>
								<hr>
								<strong><i class="fa fa-flag-checkered margin-r-5"></i>
									Finished Events</strong>
								<p class="text-muted">&nbsp;${plan.getFinishedEvents()}</p>
								<hr>
								<center>
									<a id="full-details-button">
										<button class="btn btn-primary" type="button" disabled>View</button>
									</a>
								</center>
								<div class="box-header"></div>
								<div class="box-body"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-8">
					<div class="box box-danger">
						<div class="box-header">
							<button id="new-plan" class="add btn btn-success"
								data-toggle="modal" data-target="#create-plan">
								<i class="fa fa-plus-square"></i>
							</button>
							<center>
								<h4>Annual Plans</h4>
							</center>
						</div>
						<div class="box-body">
							<table class="table table-hover table-bordered">
								<thead>
									<tr>
										<th>ID</th>
										<th>Title</th>
										<th>Date Created</th>
									</tr>
								</thead>
								<tbody id="plan-tbody">
									<c:if test="${planList.size() == 0 }">
										<tr>
											<td class="empty" colspan=3>There are no created plans yet.</td>
										</tr>
									</c:if>
									<c:forEach items="${planList}" var="item">
										<tr onclick="showDetails('${item.getId()}')">
											<td><center>${item.getId() }</center></td>
											<td>${fn:substring(item.getDateCreated(), 0, 4)}
												Training Plan</td>
											<td><center>${item.getDateCreated()}</center></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			</section>
			<!-- Modal -->
			<div id="create-plan" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header"
							style="background-color: #dd4b39; color: #fff">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">New Annual Plan</h4>
						</div>
						<div class="modal-body">
							<div id="fail" class="feedback">
								<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Uh
								oh..an annual plan might have been created already for this
								year!
							</div>
							<div id="success" class="feedback">
								<i class="fa fa-check-circle" aria-hidden="true"></i> Successfully added a new plan.
							</div>
							<div class="overlay" id="pl-details-load"
								style="display: none; text-align: center; padding-top: 15px; padding-bottom: 15px;">
								<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<%@ include file="templates/footer.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	$('#new-plan').click(function(){
		$('.feedback').hide();
		var today = new Date();
		var id = today.getFullYear();
		var created = new Date().toISOString().slice(0,10);
		$.ajax({
			url : '/SoaBaseCode/admin/createplan',
			type : 'post',
			data : {
				'id' : id,
				'dateCreated' : convertDate(today)
			},
			beforeSend: function(){
				$("#pl-details-load").show();
			},
			success : function(responseData) {
				var success = responseData;
				var tr = '<tr onclick="showDetails(\''+id+'\')">';
				tr += '<td><center>'+id+'</center></td><td>'+id+' Annual Training Plan.</td>';
				tr += '<td><center>'+created+'</center></td></tr>';
				setTimeout(function() {
					$("#pl-details-load").hide();
					if(success){
						$('.empty').hide();
						$('#plan-tbody').append(tr);
						$('#success').fadeIn(300).delay(1000);
					}else{
						$('#fail').fadeIn(300).delay(1000);
					}
			   }, 2000);
			},
			error : function() {
				alert('error');
			}
		})
	});
	function convertDate(date) {
		var year = date.getFullYear();
		var month = date.getMonth() + 1;
		var date = date.getDate();
		return year + '-' + month + '-' + date;
	}
});
	function showDetails(planId) {
		var data = "planId=" + planId;
		$.ajax({
			url : "/SoaBaseCode/admin/training-plan",
			method : 'get',
			data : data,
			beforeSend : function() {
				$("#tr-details-load").show();
			},
			success : function(response) {
				$("#tr-details-info").html(
						$(response).find("#tr-details-info").html());
				$("#full-details-button").attr("href",
						"/SoaBaseCode/admin/trainingsched?plan_id=" + planId);
				$('#full-details-button').children().attr("disabled", false);
				$('#edit-title').show();
			},
			error : function() {
				alert("Error");
				$("#full-details-button").attr("href", "");
				$('#full-details-button').children().attr("disabled", true);
				$('#edit-title').hide();
			},
			complete : function() {
				$("#tr-details-load").hide();
			}
		});
	}
</script>
</html>