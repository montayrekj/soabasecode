<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home</title>
<%@ include file="templates/import.jsp"%>
</head>
<body class="hold-transition skin-red sidebar-mini sidebar-collapse">
	<div class="wrapper">
		<%@ include file="templates/header.jsp"%>
		<%@ include file="templates/sidenav.jsp"%>
		<div class="content-wrapper">
			<section class="content-header">
			<h1>Home</h1>
			</section>
			<section class="content">
			<div class="row">
				<div class="col-xs-4">
					<div class="box box-success">
						<div class="box-header">
							<center>
								<h4>Your Profile</h4>
							</center>
						</div>
						<div class="box-body box-profile">
							<img class="profile-user-img img-responsive img-circle"
								src="images/thumbnail.png" alt="User profile picture">
							<h3 class="profile-username text-center">${name}</h3>
							<p class="text-muted text-center text-capitalize">${type}</p>
							<hr>
							<strong><i class="fa fa-id-card margin-r-5"></i> ID
								Number</strong>
							<p class="text-muted">${idnum}</p>
							<hr>
							<strong><i class="fa fa-intersex margin-r-5"></i> Sex</strong>
							<p class="text-muted">
								<c:if test="${sex == 'M'}">
									Male
								</c:if>
								<c:if test="${sex == 'F'}">
									Female
								</c:if>
							</p>
							<hr>
							<!-- <strong><i class="fa fa-birthday-cake margin-r-5"></i>
								Birth Date</strong>
							<p class="text-muted">${bday}</p>
							<hr> -->
							<strong><i class="fa fa-map-marker margin-r-5"></i>
								Address</strong>
							<p class="text-muted">${address}</p>
							<hr>
							<strong><i class="fa fa-envelope margin-r-5"></i> Email</strong>
							<p class="text-muted">${email}</p>
							<hr>
						</div>
					</div>
				</div>
				<div class="col-xs-8">
					<div class="box box-warning">
						<div class="box-header">
							<center>
								<h4>Notifications</h4>
							</center>
						</div>
						<div class="box-body">
							<table class="table table-bordered table-hover">
								<tbody>
								<c:if test="${notifList == null}">
									<tr><td>You have no notifications as for the moment</td></tr>
								</c:if>
								<c:forEach items="${notifList}" var="item">
									<tr><td>${item.getMessage() }</td></tr>
								</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			</section>
		</div>
	</div>
</body>
<%@ include file="templates/footer.jsp"%>
</html>