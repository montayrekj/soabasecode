<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>System Users</title>
<%@ include file="templates/import.jsp"%>
<link rel="stylesheet" href="/SoaBaseCode/lib/css/jquery-ui-1.10.3.custom.css">
<style>
	.bodycontainer {
		max-height: 450px;
		width: 100%;
		margin: 0;
		overflow-y: auto;
	}
	.table-scrollable {
		margin: 0;
		padding: 0;
	}
	.col1 {width: 14%;}
	.col2 {width: 25%;}
	.col3 {width: 25%;}
	.col4 {width: 20%;}
	.col5 {width: 8%;}
</style>
</head>
<body class="hold-transition skin-red sidebar-mini sidebar-collapse">
	<div class="wrapper">
		<%@ include file="templates/header.jsp"%>
		<%@ include file="templates/sidenav.jsp"%>
		<div class="content-wrapper">
			<section class="content-header">
			<h1>
				System Users <small> Add/edit system users</small>
			</h1>
			</section>
			<section class="content">
			<divstyle="height: 50%">
				<div class="row">
					<div class="col-xs-12">
						<center>
							<button style="width: 50%" type="button" id="new-user-btn" class="btn btn-success"
								data-toggle="modal" onclick="openNewUser();">
								<span class="glyphicon glyphicon-plus"></span> New User
							</button>
						</center>
					</div>
				</div><br>
				<div class="col-xs-12">
					<div class="box box-success">
						<div class="box-body">
							<div class="table-responsive">
								<table class="table table-hover table-condensed table-striped table-bordered">
									<thead>
										<tr>
											<th class="col1">User ID</th>
											<th class="col2">Last Name</th>
											<th class="col3">First Name</th>
											<th class="col4">Type</th>
											<th class="col5"></th>
											<th class="col5"></th>
										</tr>
									</thead>
								</table>
								<div id="table-body" class="bodycontainer scrollable"> 
									<table class="table table-hover table-condensed table-striped table-scrollable table-bordered">
										<tbody>
											<c:choose>
												<c:when test="${not empty userList}">
													<c:forEach items="${userList}" var="item">
														<tr>
															<td class="col1">${item.getUserId()}</td>
															<td class="col2">${item.getLastName()}</td>
															<td class="col3">${item.getFirstName()}</td>
															<td class="col4 text-capitalize">${item.getUserType()}</td>
															<td class="col5"><center><button type="button" onclick="openUpdateUser(${item.getUserId()})""><i class="fa fa-cog"></i></button></center></td>
															<td class="col5"><center><button type="button" onclick="openDeleteModal(${item.getUserId()})"><i class="fa fa-times"></i></button></center></td>
														</tr>
													</c:forEach>
												</c:when>
												<c:otherwise>
													<tr>
														<td colspan="5">Something went wrong!!</td>
													</tr>
												</c:otherwise>
											</c:choose>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<!-- modal -->
		<div id="form-modal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header"
						style="background-color: #dd4b39; color: #fff">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="modal-title">New User</h4>
					</div>
					<div class="modal-body">
						<form id="user-form">
							<div class="form-group">
								<div class="col-xs-4">
									<label for="lname">Last Name</label>
									<input class="form-control" type="text" name="last_name" id="lname" onkeypress="employeeNames(event)"/>
								</div>
								<div class="col-xs-4">
									<label for="fname">First Name</label>
									<input class="form-control" type="text" name="first_name" id="fname"/>
								</div>
								<div class="col-xs-4">
									<label for="mname">Middle Name</label>
									<input class="form-control" type="text" name="middle_name" id="mname"/>
								</div>
							</div><br><div class="form-group">
								<div class="col-xs-3">
									<label for="uType">User Type</label>
									<select class="form-control" id="uType" name="user_type" >
										<option value="employee">Employee</option>
										<option value="professional">Professional</option>
										<option value="admin">Admin</option>
									</select>
								</div>
								<div class="col-xs-3">
									<label for="email">E-mail</label>
									<input class="form-control" type="email" name="email" id="email"/>
								</div>
								<div class="col-xs-3">
									<label for="age">Age</label>
									<input class="form-control" type="number" name="age" id="age"/>
								</div>
								<div class="col-xs-3">
									<label for="gender">Gender</label>
									<select class="form-control" id="gender" name="gender"/>
										<option value="M">Male</option>
										<option value="F">Female</option>
									</select>
								</div>
							</div><br>
							<div class="form-group">
								<div class="col-xs-12">
									<label for="addr">Address</label>
									<input class="form-control" type="text" name="address" id="addr"/>
									<br>
								</div>
							</div>
							<input type="hidden" id="userid" name="user_id"/>
							<input type="hidden" id="empid" name="emp_id"/>
						</form>
						<div>

						<div>
					</div>
					<div class="modal-footer" style="display: flex; flex-wrap: wrap;">
					<div id="tr-feedback" style="width: 78%; text-align: left"></div>
						<div id="buttons">
							<button type="button" id="tr-proceed" class="btn btn-success" onclick="submitForm()">Add User</button>
							<button type="button" id="tr-cancel" class="btn btn-danger" data-dissmiss="modal" onclick="$('#form-modal').modal('hide')">Cancel</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="delete-modal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header"
						style="background-color: #dd4b39; color: #fff">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="modal-title">Delete User</h4>
					</div>
					<div class="modal-body">
						<div>
							<p class="fa fa-exclamation-triangle" aria-hidden="true"> Deleting User...</p>
						</div>
						<div>
							<P class="fa">Do you wish to continue?</p>
						</div>
						<input type="hidden" name="delete-user-id" id="deleteUserId"/>
					</div>
					<div class="modal-footer" style="display: flex; flex-wrap: wrap;"> 
					<div id="tr-feedback" style="width: 78%; text-align: left"></div>
						<div id="buttons">
							<button type="button" class="btn btn-success" onclick="deleteUser($('#deleteUserId').val())">Yes</button>
							<button type="button" class="btn btn-danger" data-dissmiss="modal" onclick="$('#delete-modal').modal('hide')">Cancel</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</body>
<%@ include file="templates/footer.jsp"%>
<script src="/SoaBaseCode/lib/jQueryUI/jquery-ui.min.js"></script>
<script>
	function deleteUser(userId) {
		$.ajax({
			url: "/SoaBaseCode/admin/systemusers/delete",
			type: "post",
			data: {"userId": userId},
			success : function(responseData) {
				if(responseData == true)
					alert("Successfully deleted user!");
				else
					alert("Something went wrong!");
			},
			complete: function() {
				$("#form-modal").modal("toggle");
				$.ajax({
					url: "/SoaBaseCode/admin/systemusers",
					method: "get",
					success: function(response) {
						$("#table-body").html($(response).find("#table-body").html());
					}
				})
			}
		});
	}
	function openDeleteModal(userId) {
		$("#deleteUserId").val(userId);
		$("#delete-modal").modal("show");
		$("#form-modal").modal("hide");

	}
	function submitForm() {
		if($("#empId").val() == "") {
			$("#empId").val(0);
		}
		var answer = $('#user-form').serialize();	
		var urlPath = "";

		if($("#tr-proceed").html().indexOf("Add") != -1) { //for add
			urlPath = "/SoaBaseCode/admin/systemusers/create";
		} else {	//for update
			urlPath = "/SoaBaseCode/admin/systemusers/update";
		}

		$.ajax({
			method : "post",
			datatype : 'text',
			data : answer,
			url : urlPath,
			success : function(responseData) {
				if(responseData == true) alert("Successfully submitted form!");
				else alert("Something went wrong!");
				console.log(responseData);
			},
			complete: function() {
				$("#form-modal").modal("hide");
				$.ajax({
					url: "/SoaBaseCode/admin/systemusers",
					method: "get",
					success: function(response) {
						$("#table-body").html($(response).find("#table-body").html());
					}
				})
			}
		});
	}
	function openNewUser() {
		$("#user-form").trigger("reset");	
		$("#tr-proceed").html("Add User");
		$("#modal-title").html("New User");
		$("#form-modal").modal("show");

		$.ajax({
			url: "/SoaBaseCode/admin/systemusers/generateID",
			type: "post",
			success: function(responseData) {
				$("#userid").val(responseData);
			},
			error: function() {
				alert("error");
				console.log("Error: Cannot generate user ID");
			}
		})
	}
	function openUpdateUser(userId) {

		$.ajax({
			url: "/SoaBaseCode/admin/systemusers/select",
			type: "post",
			data: {"userId": userId},
			beforeSend: function() {
				$("#user-form").trigger("reset");
			},
			success: function(responseData) {
				var user = responseData;
				$("#userid").val(user.userId);
				$("#empid").val(user.empId);
				$("#lname").val(user.lastName);
				$("#mname").val(user.middleName);
				$("#fname").val(user.firstName);
				$("#uType").val(user.userType);
				$("#age").val(user.age);
				$("#gender").val(user.gender);
				$("#addr").val(user.address);
				$("#email").val(user.email);
			},
			error: function() {
				alert("error");
				console.log("Error: Cannot retrieve user details");
			}, 
			complete: function() {
				$("#tr-proceed").html("Update User");
				$("#modal-title").html("Update User");
				$("#form-modal").modal("show");
			}
		})
	}
	function employeeNames(event) {
		if(event.keyCode == 13) {
			$.ajax({  
				url: "/SoaBaseCode/admin/systemusers/selectEmployee",
				type: "post",
				data: {'lastName': $("#lname").val()},
				success: function(responseData) {
					console.log(responseData);
					var emp = responseData;
					if(emp.length == 1) {
						$('#empid').val(emp[0][0]);
						$('#fname').val(emp[0][1]);
						$('#mname').val(emp[0][2]);
						$('#lname').val(emp[0][3]);
						$('#age').val(emp[0][4]);
						$('#gender').val(emp[0][5]);
					} 
				},
				error: function() {
					alert("error");
				}
			});
		}
		else {
			$('#empid').val("");
			$('#fname').val("");
			$('#mname').val("");
			$('#age').val("");
			$('#gender').val("");
			$('#uType').val("");
		}
	}
	$("#age").keypress(function (e) {
		if ((e.which < 48 || e.which > 57) && e.which != 8 && e.which != 0) {
			//display error message
			return false;
		}
	});
	// $("#lname").keypress(function (e) {
	// 		$('#empid').val("");
	// 		$('#fname').val("");
	// 		$('#mname').val("");
	// 		$('#age').val("");
	// 		$('#gender').val("");
	// });
</script>
</html>