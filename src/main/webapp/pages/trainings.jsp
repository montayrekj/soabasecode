<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Trainings</title>
<%@ include file="templates/import.jsp"%>
</head>
<body class="hold-transition skin-red sidebar-mini sidebar-collapse">
	<div class="wrapper">
		<%@ include file="templates/header.jsp"%>
		<%@ include file="templates/sidenav.jsp"%>
		<div class="content-wrapper">
			<section class="content-header">
			<h1>
				Trainings <small>View attended trainings and answer forms</small>
			</h1>
			</section>
			<section class="content">
			<div class="row">
				<div class="col-xs-4">
					<div class="box box-success">
						<div class="overlay" id="tr-details-load" style="display: none">
							<i class="fa fa-refresh fa-spin"></i>
						</div>
						<div class="box-header">
							<center>
								<h4>Training Details</h4>
							</center>
						</div>
						<div id="tr-details-info">
							<div class="box-body">
								<strong><i class="fa fa-id-card margin-r-5"></i> ID</strong>
								<p class="text-muted">&nbsp;${training.getTrainingId() }</p>
								<hr>
								<strong><i class="fa fa-id-card margin-r-5"></i> Title</strong>
								<p class="text-muted">&nbsp;${training.getTrainingTitle() }
								</p>
								<hr>
								<strong><i class="fa fa-calendar-check-o margin-r-5"></i>
									Start Date</strong>
								<p class="text-muted">&nbsp;${dateStart }</p>
								<hr>
								<strong><i class="fa fa-calendar-minus-o margin-r-5"></i>
									End Date</strong>
								<p class="text-muted">&nbsp;${dateEnd }</p>
								<hr>
								<!-- center><a id="full-details-button">
									<button class="btn btn-primary" type="button" disabled>Full Details</button>
								</a></center>-->
								<div class="box-header">
									<hr>
									<center>
										<h4>Forms</h4>
									</center>
								</div>
								<div class="box-body">
									<center>
										<form action="" method="POST" id="skform">
											<button class="btn btn-default btn-form" type="button"
												${saReleased} id="${training.getTrainingId()}" name="skf"
												onclick="toSAForm(${training.getTrainingId()},'saf')">Skills
												Assessment Form</button>
										</form>
									</center>
									<hr>
									<center>
										<button class="btn btn-default btn-form" type="button"
											${cfReleased} id="${training.getTrainingId()}" name ="cff"
											onclick="toSAForm(${training.getTrainingId()},'cff')">Course Feedback Form</button>
									</center>
									<hr>
									<center>
										<button class="btn btn-default btn-form" type="button"
											${fdRleased} id="${training.getTrainingId()}" name ="fcfd"
											onclick="toSAForm(${training.getTrainingId()},'fcfd')">Facilitator's Feedback Form</button>
									</center>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-8">
					<div class="box box-danger">
						<div class="box-header">
							<center>
								<h4>Trainings Attended</h4>
							</center>
						</div>
						<div class="box-body">
							<table class="table table-hover table-bordered">
								<thead>
									<tr>
										<th>ID</th>
										<th>Title</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
									<c:if test="${trainingList.size() == 0 }">
										<tr>
											<td colspan=3>You have not joined a single training yet.</td>
										</tr>
									</c:if>
									<c:forEach items="${trainingList}" var="item">
										<tr onclick="showDetails('${item.getTrainingId()}')">
											<td><center>${item.getTrainingId() }</center></td>
											<td>${item.getTrainingTitle() }</td>
											<td>
												<center>
													<c:if test="${item.getDateEnd().compareTo(today) < 0}">
														<span class="label label-default status">Finished</span>
													</c:if>
													<c:if test="${item.getDateStart().compareTo(today) > 0}">
														<span class="label label-primary status">Not Started</span>
													</c:if>
													<c:if
														test="${item.getDateEnd().compareTo(today) >= 0 && item.getDateStart().compareTo(today) <= 0}">
														<span class="label label-success status">Ongoing</span>
													</c:if>
												</center>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			</section>
		</div>
	</div>
</body>
<%@ include file="templates/footer.jsp"%>
<script>
	function showDetails(trID) {
		var data = "trID="+trID;
	    $.ajax({
	        url: "/SoaBaseCode/trainings/get-details",
	        data: data,
	        method: "post",
	        datatype: "text",
	        beforeSend: function(){
	        	$("#tr-details-load").show();
	        },
	        success:function(response){
	        	$("#tr-details-info").html($(response).find("#tr-details-info").html());
	        	$("#full-details-button").attr("href", "/SoaBaseCode/trainings/viewTraining?training_id="+trID);
	        	$('#full-details-button').children().attr("disabled",false);
	        },
	        error: function(){
	        	alert("Error");
	        	$("#full-details-button").attr("href", "");
	        	$('#full-details-button').children().attr("disabled",true);
	        },
	        complete: function(){
	        	$("#tr-details-load").hide();
	        }
	    });
	}
	
	function toSAForm(trID,form){
		var data = "trID="+trID;
		//var form = "form="+form;
		console.log(data);
		
		if(form == "saf"){
			$.ajax({
		        url: "/SoaBaseCode/trainings/forms",
		        data: {
		        	tId: trID,
		        	ftype: form,
		        	job: "employee"
		        },
		        method: "POST",
		        datatype: "text",
		        success:function(response){
		        	alert("Success!");
		        	location.href = "assessment";
		        },
		        error: function(){
		        	alert("Error");
		        }
		    });
		} else if(form == "fcfd"){
			$.ajax({
		        url: "/SoaBaseCode/trainings/forms",
		        data: {
		        	tId: trID,
		        	ftype: form,
		        	job: "employee"
		        },
		        method: "POST",
		        datatype: "text",
		        success:function(response){
		        	alert("Success!");
		        	location.href = "facifdbck";
		        },
		        error: function(){
		        	alert("Error");
		        }
		    });
		} else if(form == "cff"){
			$.ajax({
		        url: "/SoaBaseCode/trainings/forms",
		        data: {
		        	tId: trID,
		        	ftype: form,
		        	job: "employee"
		        },
		        method: "POST",
		        datatype: "text",
		        success:function(response){
		        	alert("Success!");
		        	location.href = "cff";
		        },
		        error: function(){
		        	alert("Error");
		        }
		    });
		}
	}
	
	
</script>
</html>