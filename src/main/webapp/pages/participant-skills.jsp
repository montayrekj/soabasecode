<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="templates/import.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Reports</title>
</head>
<body class="hold-transition skin-red sidebar-mini sidebar-collapse">
	<div class="wrapper">
		<%@ include file="templates/header.jsp"%>
		<%@ include file="templates/sidenav.jsp"%>
		<div class="content-wrapper">
			<section class="content-header">
			<h1>
				Reports <small>Check participant's skill.</small>
			</h1>
			</section>
			<section class="content">
			<div class="row">
				<div class="col-xs-5">
					<div class="box box-success">
						<div class="overlay" id="tr-details-load" style="display: none">
							<i class="fa fa-refresh fa-spin"></i>
						</div>
						<div class="box-header">
							<center>
								<h4>Details</h4>
							</center>
						</div>
						<div id="tr-details-info">
							<div class="box-body box-profile">
								<h5 class="text-center">
									<strong id="partiId"><c:out value="${participant.getParticipantId()}" default="Participant Id"/></strong>
								</h5>
								<hr>
								<div id="radar" style="min-width:300px;max-width:500px;height:400px;margin: 0 auto"></div>
							</div>
							<!--div class="box-footer">
								<center>
									<h4>Forms</h4>
								</center>
							</div>	
							<div class="box-body">
								<center>
									<button class="btn btn-default btn-form" type="button" disabled
										id="skillsassessment_button">Skills Assessment Form</button>
								</center>
								<hr>
								<center>
									<button class="btn btn-default btn-form" type="button" disabled
										id="trainingeffect_button">Training Effectiveness
										Assessment</button>
								</center>
								<hr>
							</div>-->
						</div>
					</div>
				</div>
				<div class="col-xs-7">
					<div class="box box-warning">
						<div class="box-header">
							<center>
								<h4>Participants</h4>
							</center>
						</div>
						<div class="box-body">
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<th rowspan=2>First Name</th>
										<th rowspan=2>Last Name</th>
										<th rowspan=2>Type</th>
									</tr>
								</thead>
								<tbody>
									<c:if test="${empty userList}">
										<tr>
											<td colspan=3>This training has no participants.</td>
										</tr>
									</c:if>
									<c:forEach items="${userList}" var="item">
										<tr
											onclick="showDetails(${item.getUserId()})">
											<td>${item.getFirstName()}</td>
											<td>${item.getLastName()}</td>
											<td>${item.getUserType()}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			</section>
		</div>
	</div>
</body>
<%@ include file="templates/footer.jsp"%>
<script>
	function showDetails(userId) {
		$.ajax({
			url : "/SoaBaseCode/admin/participant-skills",
			data : {
				'trainingId':'${trainingId}',
				'userId': userId
			},
			method : "get",
			beforeSend : function() {
				$("#tr-details-load").show();
			},
			success : function(response) {
				$("#tr-details-info").html(
						$(response).find("#tr-details-info").html());
				$.ajax({
					url: '/SoaBaseCode/admin/getPreSkills',
					type: 'get',
					data: {
						'participantId' : $(response).find("#partiId").text()
					},
					success: function(responseData){
						var o = responseData;
						$.ajax({
							url: '/SoaBaseCode/admin/getPostSkills',
							type: 'get',
							data: {
								'participantId' : $(response).find("#partiId").text()
							},
							success: function(responseData){
								loadChart(o,responseData)
							}
						});
					}
				});
			},
			error : function() {
				alert("Error");
			},
			complete : function() {
				$("#tr-details-load").hide();
			}
		});
	}
	function loadChart(chartData1, chartData2){
		Highcharts.chart('radar', {

		    chart: {
		        polar: true,
		        type: 'line'
		    },

		    title: {
		        text: 'Participant Skills',
		        x: -80
		    },

		    pane: {
		        size: '75%'
		    },

		    xAxis: {
		        categories: ['Generic','SE','Technical'],
		        tickmarkPlacement: 'on',
		        lineWidth: 0
		    },

		    yAxis: {
		        gridLineInterpolation: 'polygon',
		        lineWidth: 0,
		        min: 0
		    },

		    tooltip: {
		        shared: true,
		        pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f}</b><br/>'
		    },

		    legend: {
		        align: 'left',
		        verticalAlign: 'top',
		        y: 70,
		        layout: 'vertical'
		    },

		    series: [{
		        name: 'Pre-Training',
		        data: chartData1,
		        pointPlacement: 'on',
		        color: '#ff0000'
		    }, {
		        name: 'Post-Training',
		        data: chartData2,
		        pointPlacement: 'on',
		        color: '#228B22'
		    }]

		});
	}
</script>
</html>