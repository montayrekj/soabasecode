<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="http://localhost:8086/SoaBaseCode/lib/css/table.css"
	type="text/css" />
<title>TEE</title>
</head>
<body>
	<center>
		<form method="POST" action="" id="teeFrm">
			<div id="teeTable">
				<table style="width: 90%">
					<thead>
						<tr>
							<th height="50">Question</th>
							<th width="45%" colspan="5">Choice</th>
						</tr>
					</thead>
					<c:forEach items="${questions}" var="myqList">
						<tr>
							<td align="justify">${myqList.id}.${myqList.questionName}</td>
							<c:choose>
								<c:when test="${myqList.questionType == 'multiple'}">
									<td height="30" colspan="5"><label><input
											type="radio" name="${myqList.id}" value="Strongly Agree">Strongly
											Agree</label> <label><input type="radio" name="${myqList.id}"
											value="Agree">Agree</label> <label><input
											type="radio" name="${myqList.id}" value="Neutral">Neutral</label>
										<label><input type="radio" name="${myqList.id}"
											value="Disagree">Disagree</label> <label><input
											type="radio" name="${myqList.id}" value="Strongly Disagree">Strongly
											Disagree</label></td>
								</c:when>
								<c:when test="${myqList.questionType == 'multiple2'}">
									<td height="30"><center>
											<label><input type="radio" name="${myqList.id}"
												value="EE">EE </label> <label><input type="radio"
												name="${myqList.id}" value="SS">SS</label> <label><input
												type="radio" name="${myqList.id}" value="ME">ME</label> <label><input
												type="radio" name="${myqList.id}" value="DN">DN</label> <label><input
												type="radio" name="${myqList.id}" value="NI">NI</label>
										</center></td>
								</c:when>
								<c:otherwise>
									<td colspan="5"><center>
											<textarea rows="5" cols="69" placeholder="Enter text..." name="${myqList.id}"></textarea>
										</center></td>
								</c:otherwise>
							</c:choose>
						</tr>
					</c:forEach>
				</table>
				<br><br>
				<input class='btn btn-default' type="button" name="delete"
								value="Submit"
								onclick="executeSubmit('${pageContext.request.contextPath}/tee/submit')" />
			</div>
		</form>
	</center>
</body>
<
<script src="/SoaBaseCode/lib/js/jquery-1.10.1.min.js"></script>
<script>
function executeSubmit(URLPath){
	var answer = $('#teeFrm').serializeArray();

	console.log(answer);
	$.ajax({
		type : "POST",
		contentType : 'application/json; charset=utf-8',

		data : JSON.stringify(answer),
		url : URLPath,
		success : function(responseData) {
		//	location.href="home";
			alert("Successfully submitted form!");
		}
	});
	
}
</script>
</html>