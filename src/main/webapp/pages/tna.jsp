<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
function removequestion(clicked_id) {
	alert("Removing Question");
	var data123 = clicked_id;

	$.ajax({
		type : "POST",
		data : {
			data12 : data123
		},
		url : '/SoaBaseCode/tna/removequestion',
		success : function(responseData) {
			alert("DONE");
			console.log(responseData);
			$('#essays').html($(responseData).find('#essays').html());
		}
	});
}
</script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
	href="/SoaBaseCode/lib/css/table.css"
	type="text/css" />
<title>Training Needs Analysis</title>
<%@ include file="templates/import.jsp" %>
<link rel="stylesheet" href="/SoaBaseCode/lib/css/main.css">
</head>
<body class="hold-transition skin-red">
	<header class="main-header"> <a href="/SoaBaseCode/home"
		class="logo"> <span class="logo-mini"> <b><small>ASI</small></b>
	</span> <span class="logo-lg"> <b>Alliance</b>
	</span>
	</a> <nav class="navbar navbar-static-top" role="navigation"> <!-- <a href="#" class="sidebar-toggle" data-toggle="offcanvas"
			role="button"> <span class="sr-only">Toggle navigation</span>
		</a> -->
	<div class="navbar-custom-menu" style="height: 100%">
		<ul class="nav navbar-nav">
			<li class="dropdown user user-menu"><a href="#"
				class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"
				aria-haspopup="true"> <img
					src="/SoaBaseCode/images/thumbnail.png" class="user-image"
					alt="User Image"> <span class="hidden-xs">${user.getFirstName()}
						${user.getLastName()}</span>
			</a>
				<ul class="dropdown-menu">
					<li class="user-header"><img
						src="/SoaBaseCode/images/thumbnail.png" class="img-circle"
						alt="User Image">
						<p class="text-muted">${user.getFirstName()}
							${user.getMiddleName()} ${user.getLastName()}</p>
						<p class="text-muted text-capitalize" style="font-size: 1em">${user.getUserType()}</p></li>
					<li style="padding: 0;"><a href="/SoaBaseCode/home"
						class="col-xs-12 btn btn-default btn-flat"
						style="border: none; padding: 15px;">Home</a></li>
					<li style="padding: 0;"><a href="/SoaBaseCode/logout"
						class="col-xs-12 btn btn-default btn-flat"
						style="border: none; padding: 15px;"> Sign out </a></li>
				</ul></li>

		</ul>
	</div>
	</nav> </header>
	<center>
		<h2>Training Needs Analysis</h2>
		<p>Description</p>
	</center>
	<form action="" id="tnaForm" method="POST">
		<center>
		<div id="tableDiv">
				<table style="width: 90%" id="tnaTable" class="table">
					<tr>
						<th height="35" colspan="2" align="justify">Name: <input class="form-control" style="display: inline; width: 200px;"
							type="text"></input>
						</th>
						<th height="35" colspan="2" align="justify">Position: <input class="form-control" style="display: inline; width: 200px;"
							type="text"></input>
						</th>
						<th height="35" colspan="2" align="justify">TNA Done by: <input class="form-control" style="display: inline; width: 200px;" type="text"></input>
						</th>
					</tr>
					<tr>
						<c:forEach items="${questions}" var="question">
							<c:choose>
								<c:when
									test="${not empty question.idQuestions && not empty question.fkId && empty question.assessType}">
									<td align="center">${question.question}</td>
								</c:when>
							</c:choose>
						</c:forEach>
					</tr>
					<tr>
						<td></td>
						<td align="center">
							Y&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;N</td>
						<td colspan="5"></td>
					</tr>
					<tr>
					<c:forEach items="${questions}" var="question" varStatus="loop">
							<c:choose>
								<c:when
									test="${not empty question.idQuestions && not empty question.fkId && empty question.assessType}">
									<c:if test="${loop.index == 4}">
									<td align="center"><input type="text"
										placeholder="insert tasks" name="${question.idQuestions}"></td>
									</c:if>
									<c:if test="${loop.index == 5}">
									<td align="center"><input type="radio" id="${loop.index}" name="${question.idQuestions}"
										value="yes"></input>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input
										type="radio" id="${loop.index}" name="${question.idQuestions}" value="no"></input></td>
									</c:if>
									<c:if test="${loop.index == 6}">
									<td align="center"><input type="text"
										placeholder="insert training needs" name="${question.idQuestions}""></input></td>
									</c:if>
									<c:if test="${loop.index == 7}">
									<td align="center"><textarea name="${question.idQuestions}" cols="30" rows="4"
											placeholder="insert how this will be achieved"></textarea></td>
									</c:if>
									<c:if test="${loop.index == 8}">
									<td align="center"><input type="date" name="${question.idQuestions}""></td>
									</c:if>
									<c:if test="${loop.index == 9}">
									<td align="center"><input type="text"
										placeholder="insert training provider" name="${question.idQuestions}""></input></td>
									</c:if>
								</c:when>
							</c:choose>

					</c:forEach>
					</tr>
				</table>
				
				<div id="essays">
					<c:forEach items="${questions}" var="question">
						<c:choose>
							<c:when
								test="${not empty question.idQuestions && not empty question.fkId && question.assessType == 'essay'}">
								<p>
									${question.question} <br />
									<textarea class="form-control" name="${question.idQuestions}" cols="40" rows="5"
										placeholder="insert comments"></textarea>
									<br />
									<button type="button" onClick="removequestion(this.id)" id="${question.idQuestions}" name="remove" class="btn btn-danger">Remove Question</button>
								</p>
							</c:when>
						</c:choose>
					</c:forEach>
				</div>
			</div>
		</center>
		<br />
		<center>
			<div class="form-group">
				<input class="form-control" type="text" placeholder="Add Question" id="question" style="display: inline; width: 30%"
					name="div1"></input>
				<button class="btn btn-primary" style="display: inline;" type="button" onclick="appendquestion('${pageContext.request.contextPath}/tna/appendquestion')">Append Question</button>
			</div>
		</center>
		<br>
	</form>
</body>
<script src="/SoaBaseCode/lib/js/jquery-1.10.1.min.js"></script>
<script src="/SoaBaseCode/lib/js/bootstrap.min.js"></script>
<script src="/SoaBaseCode/lib/js/jquery.blockUI.js"></script>
<script src="/SoaBaseCode/js/tna.js"></script>
</html>
