<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Training Effectiveness Evaluation</title>
<%@ include file="templates/import.jsp"%>
<link rel="stylesheet" href="/SoaBaseCode/lib/css/main.css">
</head>
<body class="hold-transition skin-red">
	<header class="main-header"> <a href="/SoaBaseCode/home"
		class="logo"> <span class="logo-mini"> <b><small>ASI</small></b>
	</span> <span class="logo-lg"> <b>Alliance</b>
	</span>
	</a> <nav class="navbar navbar-static-top" role="navigation"> <!-- <a href="#" class="sidebar-toggle" data-toggle="offcanvas"
			role="button"> <span class="sr-only">Toggle navigation</span>
		</a> -->
	<div class="navbar-custom-menu" style="height: 100%">
		<ul class="nav navbar-nav">
			<li class="dropdown user user-menu"><a href="#"
				class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"
				aria-haspopup="true"> <img
					src="/SoaBaseCode/images/thumbnail.png" class="user-image"
					alt="User Image"> <span class="hidden-xs">${user.getFirstName()}
						${user.getLastName()}</span>
			</a>
				<ul class="dropdown-menu">
					<li class="user-header"><img
						src="/SoaBaseCode/images/thumbnail.png" class="img-circle"
						alt="User Image">
						<p class="text-muted">${user.getFirstName()}
							${user.getMiddleName()} ${user.getLastName()}</p>
						<p class="text-muted text-capitalize" style="font-size: 1em">${user.getUserType()}</p></li>
					<li style="padding: 0;"><a href="/SoaBaseCode/home"
						class="col-xs-12 btn btn-default btn-flat"
						style="border: none; padding: 15px;">Home</a></li>
					<li style="padding: 0;"><a href="/SoaBaseCode/logout"
						class="col-xs-12 btn btn-default btn-flat"
						style="border: none; padding: 15px;"> Sign out </a></li>
				</ul></li>

		</ul>
	</div>
	</nav> </header>
	<center>
		<h2>Training Effectiveness Evaluation Form</h2>
		<p>Answer Form</p>
	</center>
	<center>
		<form method="POST" id="teeFrm">
			<div id="teeTable">
				<table class="table table" style="width: 90%">
					<thead>
						<tr>
							<th>Question</th>
							<th width="50%" colspan=5>Choice</th>

						</tr>
					</thead>
					<tbody>
						<c:forEach items="${questions}" var="myqList" varStatus="loop">
							<tr>
								<td
									style="text-align: justify; border-right: 1px solid #e3e3e3;"><p>${loop.count}.
										${myqList.question}</p></td>
								<c:choose>
									<c:when test="${myqList.assessType == 'multiple'}">
										<td><div class="radio">
												<label><input type="radio"
													name="${myqList.idQuestions}" value="Strongly Agree">Strongly
													Agree</label>
											</div></td>
										<td><div class="radio">
												<label><input type="radio"
													name="${myqList.idQuestions}" value="Agree">Agree</label>
											</div></td>
										<td><div class="radio">
												<label><input type="radio"
													name="${myqList.idQuestions}" value="Neutral">Neutral</label>
											</div></td>
										<td><div class="radio">
												<label><input type="radio"
													name="${myqList.idQuestions}" value="Disagree">Disagree</label>
											</div></td>
										<td><div class="radio">
												<label><input type="radio" class="inline-radio"
													name="${myqList.idQuestions}" value="Strongly Disagree">Strongly
													Disagree</label>
											</div></td>

									</c:when>
									<c:when test="${myqList.assessType == 'multiple2'}">
										<td>
											<div class="radio">
												<label><input type="radio"
													name="${myqList.idQuestions}" value="EE">EE</label>
											</div>
										</td>
										<td>
											<div class="radio">
												<label><input type="radio"
													name="${myqList.idQuestions}" value="SS">SS</label>
											</div>
										</td>
										<td>
											<div class="radio">
												<label><input type="radio"
													name="${myqList.idQuestions}" value="ME">ME</label>
											</div>
										</td>
										<td>
											<div class="radio">
												<label><input type="radio"
													name="${myqList.idQuestions}" value="DN">DN</label>
											</div>
										</td>
										<td>
											<div class="radio">
												<label><input type="radio"
													name="${myqList.idQuestions}" value="NI">NI</label>
											</div>
										</td>
									</c:when>
									<c:otherwise>
										<td colspan=5><center>
												<textarea class="form-control" rows="5" cols="69"
													placeholder="Enter text..." name="${myqList.idQuestions}"></textarea>
											</center></td>

									</c:otherwise>

								</c:choose>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<br> <br> <input class='btn btn-primary' type="button"
					name="delete" value="Submit"
					onclick="executeSubmit('${pageContext.request.contextPath}/tee/submit')" />
			</div>
		</form>
	</center>
	<script src="/SoaBaseCode/lib/js/jquery-1.10.1.min.js"></script>
	<%@ include file="templates/footer.jsp"%>
	
	<script>
		function executeSubmit(URLPath) {
			var teeanswer = $('#teeFrm').serializeArray();

			console.log(teeanswer);
			$.ajax({
				type : "POST",
				contentType : 'application/json; charset=utf-8',

				data : JSON.stringify(teeanswer),
				url : URLPath,
				success : function(responseData) {
					location.href = "home";
					alert("Successfully submitted form!");
				}
			});

		}
	</script>
</body>


</html>