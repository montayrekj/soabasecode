<?php session_start();  ?>
<?php
if($_SESSION['type'] != 'student'){
  header("location:calendarCounselor.php");
}
require("includes/header.php");
?>
<div class = "content-wrapper">
  <section class = "content-header">
    <h1>
      Calendar
      <small>Check out what's in store for you today.</small>
    </h1>
  </section>
  <section class = "content">
    <div class = "row">
      <div class="modal" id = "eventModal">
         <div class="modal-dialog">
           <div class="modal-content">
             <form id = "eventForm" method = "post" action = "acceptEvent.php" class = "form-horizontal">
               <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true">&times;</span></button>
                 <h4 class="modal-title" id = "eventModalTitle">No title available</h4><i id = "eventModalStatus">No status available</i>
               </div>
               <div class="modal-body">
                 <input type = "text" id = "eventModalId" name = "eventId" hidden = "hidden">
                 <div class = "form-group">
                   <label for = "eventTitle" class = "col-sm-2 control-label">Title</label>
                   <div class = "col-sm-10">
                     <input type = "text" id = "eventTitle" class = "form-control" name = "eventTitle">
                   </div>
                 </div>
                 <div class = "form-group">
                   <label for = "eventDescription" class = "col-sm-2 control-label">Description</label>
                   <div class = "col-sm-10">
                     <textarea id = "eventDescription" class = "form-control" name = "eventDescription"></textarea>
                   </div>
                 </div>
                 <!-- Date -->
                 <div class = "form-group col-sm-12">
                   <label for = "eventDate" class = "col-sm-2 control-label">Date</label>
                   <div class = "col-sm-10 input-group date">
                     <div class = "input-group-addon">
                       <i class = "fa fa-calendar"></i>
                     </div>
                     <input type = "text" class = "form-control datepicker" id = "eventDate" name = "eventDate">
                   </div>
                   <!-- /.input group -->
                 </div>
                 <!-- time Picker -->
                 <div class="bootstrap-timepicker col-sm-12">
                   <div class="form-group">
                     <label class = "col-sm-2 control-label" for = "eventStartTime">Start</label>
                     <div class = "col-sm-10 input-group">
                       <input type = "text" id = "eventStartTime" class = "form-control timepicker" name = "eventStartTime" placeholder = "Start">
                       <div class="input-group-addon">
                         <i class="fa fa-clock-o"></i>
                       </div>
                     </div>
                     <!-- /.input group -->
                   </div>
                   <!-- /.form group -->
                 </div>
                 <!-- /.time picker -->
                 <!-- time Picker -->
                 <div class="bootstrap-timepicker col-sm-12">
                   <div class="form-group">
                     <label class = "col-sm-2 control-label" for = "eventEndTime">End</label>
                     <div class = "col-sm-10 input-group">
                       <input type = "text" id = "eventEndTime" class = "form-control timepicker" name = "eventEndTime" placeholder = "End">
                       <div class="input-group-addon">
                         <i class="fa fa-clock-o"></i>
                       </div>
                     </div>
                     <!-- /.input group -->
                   </div>
                   <!-- /.form group -->
                 </div>
                 <!-- /.time picker -->
               </div>
               <div class="modal-footer">
                 <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                 <div id = "eventModalFooter"></div>
               </div>
             </form>
           </div>
           <!-- /.modal-content -->

         </div>
         <!-- /.modal-dialog -->
       </div>
       <!-- /.modal -->
       <div class="modal" id = "addEventModal">
          <div class="modal-dialog">
            <div class="modal-content">
              <form id = "addEventForm" method = "post" action = "addEvent.php" class = "form-horizontal">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id = "addEventModalTitle">No title available</h4>
                </div>
                <div class="modal-body">
                  <div class = "form-group">
                    <label for = "addEventTitle" class = "col-sm-2 control-label">Title</label>
                    <div class = "col-sm-10">
                      <input type = "text" id = "addEventTitle" class = "form-control" name = "eventTitle" required>
                    </div>
                  </div>
                  <div class = "form-group">
                    <label for = "selectEventType" class = "col-sm-2 control-label">Type</label>
                    <div class = "col-sm-10">
                      <select id = "selectAddType" class = "form-control" style = "width:100%;" name = "type">
                        <option value = "Educational">Educational</option>
                        <option value = "Personal / Social">Personal / Social</option>
                        <option value = "Career / Vocation">Career / Vocation</option>
                        <option value = "Others">Others</option>
                      </select>
                    </div>
                  </div>
                  <div class = "form-group">
                    <label for = "addEventDescription" class = "col-sm-2 control-label">Description</label>
                    <div class = "col-sm-10">
                      <textarea id = "addEventDescription" class = "form-control" name = "eventDescription" required></textarea>
                    </div>
                  </div>
                  <!-- Date -->
                  <div class = "form-group col-sm-12" hidden = "true">
                    <label for = "addEventDate" class = "col-sm-2 control-label">Date</label>
                    <div class = "col-sm-10 input-group date">
                      <div class = "input-group-addon">
                        <i class = "fa fa-calendar"></i>
                      </div>
                      <input type = "text" class = "form-control datepicker" id = "addEventDate" name = "eventDate" required>
                    </div>
                    <!-- /.input group -->
                  </div>
                  <div class = "form-group">
                    <label for = "selectAddPerson" class = "col-sm-2 control-label">Student/s</label>
                    <div class = "col-sm-10">
                      <select id = "selectAddPerson" class = "form-control" style = "width:100%;" name = "studentId">
                        <?php
                          require("sql_connect.php");
                          $result = mysqli_query($mysqli, "SELECT student_id, stud_lname, stud_fname, stud_mname FROM student");

                          if($result){
                            while($row = mysqli_fetch_assoc($result)){
                              if($row['student_id'] == $_SESSION['currentUser']){
                        ?>
                        <option value = <?php echo $row["student_id"]; ?>><?php echo($row["student_id"] . " - " . $row["stud_lname"] . ", " . $row["stud_fname"] . " " . $row["stud_mname"]); ?></option>
                        <?php
                              }
                            }
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                  <!-- time Picker -->
                  <div class="bootstrap-timepicker col-sm-12">
                    <div class="form-group">
                      <label class = "col-sm-2 control-label" for = "addEventStartTime">Start</label>
                      <div class = "col-sm-10 input-group">
                        <input type = "text" id = "addEventStartTime" class = "form-control timepicker" name = "eventStartTime" placeholder = "Start" required>
                        <div class="input-group-addon">
                          <i class="fa fa-clock-o"></i>
                        </div>
                      </div>
                      <!-- /.input group -->
                    </div>
                    <!-- /.form group -->
                  </div>
                  <!-- /.time picker -->
                  <!-- time Picker -->
                  <div class="bootstrap-timepicker col-sm-12">
                    <div class="form-group">
                      <label class = "col-sm-2 control-label" for = "addEventEndTime">End</label>
                      <div class = "col-sm-10 input-group">
                        <input type = "text" id = "addEventEndTime" class = "form-control timepicker" name = "eventEndTime" placeholder = "End" required>
                        <div class="input-group-addon">
                          <i class="fa fa-clock-o"></i>
                        </div>
                      </div>
                      <!-- /.input group -->
                    </div>
                    <!-- /.form group -->
                  </div>
                  <!-- /.time picker -->
                </div>
                <?php
                  if($_SESSION['type'] == 'student'){
                ?>
                  <input type = "text" id = "addEventRequestedBy" name = "requested_by" value = "student" hidden = "hidden" required>
                <?php
                  }else{
                ?>
                    <input type = "text" id = "addEventRequestedBy" name = "requested_by" value = "counselor" hidden = "hidden" required>
                <?php
                  }
                ?>

                <div class="modal-footer">
                  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                  <button type="submit" class="btn btn-primary" name = "btnSetAppointment">Set Appointment</button>
                </div>
              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
      <div class = "col-md-3">
        <div class = "box box-danger">
          <div class = "box-header with-border">
            <h4 class = "box-title">Delete an event</h4>
          </div>
          <div class = "box-body" id = "trash" style = "height: 250px">
          </div>
        </div>
      </div>
      <div class = "col-md-9">
        <div class = "box box-success">
          <div class = "box-body no-padding">
            <div id = "calendar"></div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<?php require("includes/footer.php"); ?>
<script>
	$(document).ready(function() {
  $("#li-calendar").addClass("active");

  /* initialize the select tag
  -----------------------------------------------------------------*/
  $('#selectAddPerson').select2();
  $('#selectAddType').select2();

  //Timepicker
  $(".timepicker").timepicker({
    showInputs: false
  });

  //Date picker
  $(".datepicker").datepicker({
    autoclose: true
  });

	var zone = "08:00";  //Change this to your timezone

	$.ajax({
		url: 'process.php',
        type: 'POST', // Send post data
        data: 'type=fetchStudent',
        async: false,
        success: function(s){
        	json_events = s;
        }
	});


	var currentMousePos = {
	    x: -1,
	    y: -1
	};
		jQuery(document).on("mousemove", function (event) {
        currentMousePos.x = event.pageX;
        currentMousePos.y = event.pageY;
    });

		/* initialize the external events
		-----------------------------------------------------------------*/

		$('#external-events .fc-event').each(function() {

			// store data so the calendar knows to render an event upon drop
			$(this).data('event', {
				title: $.trim($(this).text()), // use the element's text as the event title
				stick: true, // maintain when user navigates (see docs on the renderEvent method)
        is_accepted: 'U'
			});

			// make the event draggable using jQuery UI
			$(this).draggable({
				zIndex: 999,
				revert: true,      // will cause the event to go back to its
				revertDuration: 0  //  original position after the drag
			});

		});

		/* initialize the calendar
		-----------------------------------------------------------------*/

		$('#calendar').fullCalendar({
			events: JSON.parse(json_events),
			//events: [{"id":"14","title":"New Event","start":"2015-01-24T16:00:00+04:00","allDay":false}],
			utc: true,
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			editable: true,
			droppable: true,
			slotDuration: '00:30:00',
      eventRender: function(event, element) {
        if(event.description != null){
          element.find('.fc-title').html("<b>" + event.title + "</b>");
          element.append("Description: " +  event.description)
        }

        if(event.is_accepted == 'Y'){
          element.css('background-color', '#00A65A');
          element.css('border-color', '#00A65A');
        }else if(event.is_accepted == 'P'){
          element.css('background-color', '#F39C12');
          element.css('border-color', '#F39C12');
        }else if(event.is_accepted == 'N'){
          element.css('background-color', '#DD4B39');
          element.css('border-color', '#DD4B39');
        }

        // event.start = event.start.format('YYYY-MM-DD');
        // alert(event.start.format('YYYY-MM-DD') + 'T' + event.startTime);
        // var start = moment(event.start).format('YYYY-MM-DD');
        // var start = moment(event.start).format('YYYY-MM-DD');
        // alert(date);

        if(event.is_accepted != 'N'){
          event.editable = false;
        }

        element.css('padding', '5px');
      },
			eventReceive: function(event){
				var title = event.title;
				var start = event.start.format("YYYY-MM-DD");

				$.ajax({
		    		url: 'process.php',
		    		data: 'type=new&title='+title+'&startdate='+start+'&zone='+zone+'&is_accepted='+event.is_accepted,
		    		type: 'POST',
		    		dataType: 'json',
		    		success: function(response){
		    			event.id = response.eventid;
		    			$('#calendar').fullCalendar('updateEvent',event);
		    		},
		    		error: function(e){
		    			console.log(e.responseText);

		    		}
		    	});
				$('#calendar').fullCalendar('updateEvent',event);
				console.log(event);
			},
			eventDrop: function(event, delta, revertFunc) {
		        var title = event.title;
		        var start = event.start.format();
		        var end = (event.end == null) ? start : event.end.format();
		        $.ajax({
					url: 'process.php',
					data: 'type=resetdate&title='+title+'&start='+start+'&end='+end+'&eventid='+event.id+'&date='+event.start.format("YYYY-MM-DD"),
					type: 'POST',
					dataType: 'json',
					success: function(response){
						if(response.status != 'success')
						revertFunc();
					},
					error: function(e){
						revertFunc();
						alert('Error processing your request: '+e.responseText);
					}
				});
		    },
		    eventClick: function(event, jsEvent, view) {
          $('#eventModalTitle').text(event.title);

          if(event.is_accepted == 'Y'){
            $('#eventModalStatus').text("(Approved)");
          }else if(event.is_accepted == 'N'){
            $('#eventModalStatus').text("(Rejected)");
          }if(event.is_accepted == 'P'){
            $('#eventModalStatus').text("(Pending)");
          }

          $('#eventModalId').val(event.id);
          $('#eventTitle').val(event.title);
          $('#eventDescription').val(event.description);
          $('#eventDate').val(moment(event.start).format('MM/DD/YYYY'));
          $('#eventStartTime').val(moment(event.start).format('hh:mm A'));
          $('#eventEndTime').val(moment(event.end).format('hh:mm A'));


          if(event.is_accepted == 'Y'){
            $('#eventModalFooter').html('<button type="submit" class="btn btn-danger" name = "btnReject">Cancel Schedule</button>');
          }else if(event.is_accepted == 'N'){
            $('#eventModalFooter').html('<button type="submit" class="btn btn-primary" name = "btnReschedule">Reschedule</button>');
          }else if(event.is_accepted == 'P'){
            $('#eventModalFooter').html('<button type="submit" class="btn btn-success" name = "btnAccept">Accept Schedule</button><button type="submit" class="btn btn-danger" name = "btnReject">Reject Schedule</button>');
          }

          $('#eventModal').modal();
			},
			eventResize: function(event, delta, revertFunc) {
				console.log(event);
				var title = event.title;
				var end = event.end.format();
				var start = event.start.format();
		        $.ajax({
					url: 'process.php',
					data: 'type=resetdate&title='+title+'&start='+start+'&end='+end+'&eventid='+event.id+'&date='+event.start.format("YYYY-MM-DD"),
					type: 'POST',
					dataType: 'json',
					success: function(response){
						if(response.status != 'success')
						revertFunc();
					},
					error: function(e){
						revertFunc();
						alert('Error processing your request: '+e.responseText);
					}
				});
		    },
			eventDragStop: function (event, jsEvent, ui, view) {
			    if (isElemOverDiv()) {
			    	var con = confirm('Are you sure to delete this event permanently?');
			    	if(con == true) {
						$.ajax({
				    		url: 'process.php',
				    		data: 'type=remove&eventid='+event.id,
				    		type: 'POST',
				    		dataType: 'json',
				    		success: function(response){
				    			console.log(response);
				    			if(response.status == 'success'){
				    				$('#calendar').fullCalendar('removeEvents');
            						getFreshEvents();
            					}
				    		},
				    		error: function(e){
				    			alert('Error processing your request: '+e.responseText);
				    		}
			    		});
					}
				}
			},
      dayClick: function(date, allDay, jsEvent, view){
        var today = new Date();

        if(date >= today){
          $('#addEventModalTitle').text(date.format("dddd - MMMM DD, YYYY"));
          $('#addEventDate').val(date.format("YYYY-MM-DD"));
          $('#addEventModal').modal();
        }
      },
      /* This constrains it to today or later */
        eventConstraint: {
            start: moment().format('YYYY-MM-DD'),
            end: '2100-01-01' // hard coded goodness unfortunately
        },

        dayRender: function (date, cell) {
            var today = new Date();

            if(date < today){
              cell.css("background-color", "#EEEEEE");
            }
        },

        businessHours:
        {
                start: '9:00',
                end:   '17:00',
                dow: [ 1, 2, 3, 4, 5]
        },
		});

	function getFreshEvents(){
		$.ajax({
			url: 'process.php',
	        type: 'POST', // Send post data
	        data: 'type=fetchStudent',
	        async: false,
	        success: function(s){
	        	freshevents = s;
	        }
		});
		$('#calendar').fullCalendar('addEventSource', JSON.parse(freshevents));
	}


	function isElemOverDiv() {
      var trashEl = jQuery('#trash');

      var ofs = trashEl.offset();

      var x1 = ofs.left;
      var x2 = ofs.left + trashEl.outerWidth(true);
      var y1 = ofs.top;
      var y2 = ofs.top + trashEl.outerHeight(true);

      if (currentMousePos.x >= x1 && currentMousePos.x <= x2 &&
          currentMousePos.y >= y1 && currentMousePos.y <= y2) {
          return true;
      }
      return false;
  }
});

</script>
