<!DOCTYPE>
<html>
  <head>
    <title>USC Guidance Center</title>
    <meta charset = "utf-8">
    <meta http-equiv = "X-UA-Compatible" content = "IE=edge">
    <meta content = "width = device-width, initial-scale = 1, maximum-scale = 1, user-scalable = no" name = "viewport">
    <link rel = "stylesheet" href = "bootstrap/css/bootstrap.min.css">
    <link rel = "stylesheet" href = "font-awesome/css/font-awesome.min.css">
    <link rel = "stylesheet" href = "ion-icons/css/ionicons.min.css">
    <link rel = "stylesheet" href = "fullcalendar/fullcalendar.min.css">
    <link rel = "stylesheet" href = "fullcalendar/fullcalendar.print.css" media = "print">
    <link rel = "stylesheet" href = "datatables/dataTables.bootstrap.css">
    <link rel = "stylesheet" href = "adminlte/AdminLTE.min.css">
    <link rel = "stylesheet" href = "adminlte/skins/skin-green.min.css">
    <link rel = "stylesheet" href = "css/style.css">
  </head>
  <body class = "hold-transition skin-green sidebar-mini">
    <div class = "wrapper">
      <header class = "main-header">
        <a href = "index.php" class = "logo">
          <span class = "logo-lg"><b>USC</b> Guidance Center</span>
        </a>
        <nav class = "navbar navbar-static-top" role = "navigation">
        </nav>
      </header>
