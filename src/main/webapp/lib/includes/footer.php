<footer class = "main-footer">
  <div class = "pull-right hidden-xs">
    Credits to Ted125
  </div>
  <strong>Copyright &copy; 2017 <a href = "https://usc.edu.ph">University of San Carlos</a>.</strong>
  All rights reserved.
</footer>
</div>
</body>
<script src = "jQuery/jquery-2.2.3.min.js"></script>
<script src = "jQueryUI/jquery-ui.min.js"></script>
<script src = "bootstrap/js/bootstrap.min.js"></script>
<script src = "datatables/jquery.dataTables.min.js"></script>
<script src = "datatables/dataTables.bootstrap.min.js"></script>
<script src = "slimScroll/jquery.slimscroll.min.js"></script>
<script src = "ChartJS/Chart-2.1.4.min.js"></script>
<script src = "fastclick/fastclick.min.js"></script>
<script src = "adminlte/app.min.js"></script>
<script src = "fullcalendar/moment.min.js"></script>
<script src = "fullcalendar/fullcalendar.min.js"></script>
<script src = "select2/select2.min.js"></script>
<script src = "datepicker/bootstrap-datepicker.js"></script>
<script src = "timepicker/bootstrap-timepicker.min.js"></script>
<script src = "wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
</html>
