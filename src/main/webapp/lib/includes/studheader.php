<!DOCTYPE>
<html>
  <head>
    <title>USC Guidance Center</title>
    <meta charset = "utf-8">
    <meta http-equiv = "X-UA-Compatible" content = "IE=edge">
    <meta content = "width = device-width, initial-scale = 1, maximum-scale = 1, user-scalable = no" name = "viewport">
    <link rel = "stylesheet" href = "bootstrap/css/bootstrap.min.css">
    <link rel = "stylesheet" href = "font-awesome/css/font-awesome.min.css">
    <link rel = "stylesheet" href = "ion-icons/css/ionicons.min.css">
    <link rel = "stylesheet" href = "fullcalendar/fullcalendar.min.css">
    <link rel = "stylesheet" href = "fullcalendar/fullcalendar.print.css" media = "print">
    <link rel = "stylesheet" href = "datatables/dataTables.bootstrap.css">
    <link rel = "stylesheet" href = "adminlte/AdminLTE.min.css">
    <link rel = "stylesheet" href = "adminlte/skins/skin-green.min.css">
    <link rel = "stylesheet" href = "css/style.css">
  </head>
  <body class = "hold-transition skin-green sidebar-mini">
    <div class = "wrapper">
      <header class = "main-header">
        <a href = "index.php" class = "logo">
          <span class = "logo-mini"><b>GC</b></span>
          <span class = "logo-lg"><b>USC</b> Guidance Center</span>
        </a>
        <nav class = "navbar navbar-static-top" role = "navigation">
          <a href = "#" class = "sidebar-toggle" data-toggle = "offcanvas" role = "button">
            <span class = "sr-only">Toggle navigation</span>
          </a>
          <div class = "navbar-custom-menu">
            <ul class = "nav navbar-nav">
              <li class = "dropdown notifications-menu">
                <a href = "#" class = "dropdown-toggle" data-toggle = "dropdown">
                  <i class = "fa fa-bell-o"></i>
                  <span class = "label label-warning">1</span>
                </a>
                <ul class = "dropdown-menu">
                  <li class = "header">You have 1 notification</li>
                  <li>
                    <ul class = "menu">
                      <li>
                        <a href = "#">
                          <i class = "fa fa-user text-aqua"></i>
                          User scheduled an appointment with you.
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li class = "footer">
                    <a href = "#">View All</a>
                  </li>
                </ul>
              </li>
              <li class = "dropdown user user-menu">
                <a href = "#" class = "dropdown-toggle" data-toggle = "dropdown">
                  <img src = "images/thumbnail.png" class = "user-image" alt = "User Image">
                  <span class = "hidden-xs"><?php
						echo $_SESSION['currentUser'];
					?></span>
                </a>
                <ul class = "dropdown-menu">
                  <li class = "user-header">
                    <img src = "images/thumbnail.png" class = "img-circle" alt = "User Image">
                    <p>
                      <?php
					  echo $_SESSION['fn']." ".$_SESSION['mi'].' . '.$_SESSION['ln']." - ".$_SESSION['type'];
                      
                      //<small>Member since January 1, 2017</small>
					  ?>
                    </p>
                  </li>
                  <li class = "user-body">
                    <div class = "row">
                      <a href = "profile.php" class = "col-xs-12 text-center">Profile</a>
                    </div>
                  </li>
                  <li class = "user-footer" style = "padding: 0;">
                    <a href = "logout.php" class = "col-xs-12 btn btn-default btn-flat" style = "border: none; padding: 15px; id = 'logout' name = 'logout'">Sign out</a>
                  </li>
                </ul>
              </li>
			  
            </ul>
          </div>
        </nav>
      </header>
      <aside class = "main-sidebar">
        <section class = "sidebar">
          <div class = "user-panel">
            <div class = "pull-left image">
              <img src = "images/thumbnail.png" class = "img-circle" alt = "User Image">
            </div>
            <div class = "pull-left info">
              <p><?php
					  echo $_SESSION['fn']." ".$_SESSION['mi'].' . '.$_SESSION['ln'];
                 ?>
			  </p>
              <span><i class = "fa fa-circle text-success"></i> Online<span>
            </div>
          </div>
          <br>
          <ul class = "sidebar-menu">
            <li class = "header">TO DO</li>
            <li id = "li-timeline">
              <a href = "index.php">
                <i class = "fa fa-arrows-h"></i>
                <span>Timeline</span>
              </a>
            </li>
            <li id = "li-calendar">
              <a href = "calendar.php">
                <i class = "fa fa-calendar"></i>
                <span>Calendar</span>
              </a>
            </li>
            <li id = "li-history">
              <a href = "history.php">
                <i class = "fa fa-history"></i>
                <span>History</span>
              </a>
            </li>
            <li class = "treeview" id = "li-reports">
              <ul class = "treeview-menu">
                <li>
                </li>
              </ul>
            </li>
          </ul>
        </section>
      </aside>
