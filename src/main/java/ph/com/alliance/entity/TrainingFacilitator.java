package ph.com.alliance.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the training_facilitators database table.
 * 
 */
@Entity
@Table(name="training_facilitators")
@NamedQuery(name="TrainingFacilitator.findAll", query="SELECT t FROM TrainingFacilitator t")
public class TrainingFacilitator implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="facilitator_id")
	private int facilitatorId;

	@Column(name="faci_fdbfrm")
	private String faciFdbfrm;

	@Column(name="tna_form")
	private String tnaForm;

	@Column(name="training_id")
	private int trainingId;

	@Column(name="user_id")
	private int userId;

	public TrainingFacilitator() {
	}

	public int getFacilitatorId() {
		return this.facilitatorId;
	}

	public void setFacilitatorId(int facilitatorId) {
		this.facilitatorId = facilitatorId;
	}

	public String getFaciFdbfrm() {
		return this.faciFdbfrm;
	}

	public void setFaciFdbfrm(String faciFdbfrm) {
		this.faciFdbfrm = faciFdbfrm;
	}

	public String getTnaForm() {
		return this.tnaForm;
	}

	public void setTnaForm(String tnaForm) {
		this.tnaForm = tnaForm;
	}

	public int getTrainingId() {
		return this.trainingId;
	}

	public void setTrainingId(int trainingId) {
		this.trainingId = trainingId;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "TrainingFacilitator [facilitatorId=" + facilitatorId
				+ ", faciFdbfrm=" + faciFdbfrm + ", tnaForm=" + tnaForm
				+ ", trainingId=" + trainingId + ", userId=" + userId + "]";
	}

}