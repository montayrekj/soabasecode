package ph.com.alliance.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * The persistent class for the annual_plan database table.
 * 
 */
@Entity
@Table(name="annual_plan")
@NamedQuery(name="AnnualPlan.findAll", query="SELECT a FROM AnnualPlan a")
public class AnnualPlan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="date_created")
	private String dateCreated;
	
	@Transient
	private int ongoingEvents;
	
	@Transient
	private int pendingEvents;
	
	@Transient
	private int finishedEvents;
	public AnnualPlan() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}

	public int getOngoingEvents() {
		return ongoingEvents;
	}

	public void setOngoingEvents(int ongoingEvents) {
		this.ongoingEvents = ongoingEvents;
	}

	public int getPendingEvents() {
		return pendingEvents;
	}

	public void setPendingEvents(int pendingEvents) {
		this.pendingEvents = pendingEvents;
	}

	public int getFinishedEvents() {
		return finishedEvents;
	}

	public void setFinishedEvents(int finishedEvents) {
		this.finishedEvents = finishedEvents;
	}

}