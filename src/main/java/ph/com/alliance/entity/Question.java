package ph.com.alliance.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the questions database table.
 * 
 */
@Entity
@Table(name="questions")
@NamedQuery(name="Question.findAll", query="SELECT q FROM Question q")
public class Question implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_questions")
	private int idQuestions;

	@Column(name="assess_type")
	private String assessType;

	@Override
	public String toString() {
		return "Question [idQuestions=" + idQuestions + ", assessType="
				+ assessType + ", fkFscid=" + fkFscid + ", fkId=" + fkId
				+ ", peerAssessAnswer=" + peerAssessAnswer + ", question="
				+ question + ", selfAssessAnswer=" + selfAssessAnswer
				+ ", supervisorAssessAnswer=" + supervisorAssessAnswer + "]";
	}

	@Column(name="fk_fscid")
	private int fkFscid;

	@Column(name="fk_id")
	private int fkId;

	@Column(name="peer_assess_answer")
	private String peerAssessAnswer;

	private String question;

	@Column(name="self_assess_answer")
	private String selfAssessAnswer;

	@Column(name="supervisor_assess_answer")
	private String supervisorAssessAnswer;

	public Question() {
	}

	public int getIdQuestions() {
		return this.idQuestions;
	}

	public void setIdQuestions(int idQuestions) {
		this.idQuestions = idQuestions;
	}

	public String getAssessType() {
		return this.assessType;
	}

	public void setAssessType(String assessType) {
		this.assessType = assessType;
	}

	public int getFkFscid() {
		return this.fkFscid;
	}

	public void setFkFscid(int fkFscid) {
		this.fkFscid = fkFscid;
	}

	public int getFkId() {
		return this.fkId;
	}

	public void setFkId(int fkId) {
		this.fkId = fkId;
	}

	public String getPeerAssessAnswer() {
		return this.peerAssessAnswer;
	}

	public void setPeerAssessAnswer(String peerAssessAnswer) {
		this.peerAssessAnswer = peerAssessAnswer;
	}

	public String getQuestion() {
		return this.question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getSelfAssessAnswer() {
		return this.selfAssessAnswer;
	}

	public void setSelfAssessAnswer(String selfAssessAnswer) {
		this.selfAssessAnswer = selfAssessAnswer;
	}

	public String getSupervisorAssessAnswer() {
		return this.supervisorAssessAnswer;
	}

	public void setSupervisorAssessAnswer(String supervisorAssessAnswer) {
		this.supervisorAssessAnswer = supervisorAssessAnswer;
	}

}