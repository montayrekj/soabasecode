package ph.com.alliance.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The persistent class for the user database table.
 * 
 */
//@Entity
//@Table(name="user")
public class Calc implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="idz")
	private String idz;
	
	private String op;
	private double num1;
	private double num2;
	
	
	
	@Override
    public String toString() {
      ///  return "User {" + "userID=" + uid + ", fname=" + fname + ", lname=" + lname + ", gender=" + gender + ", age=" + age + '}';
		return "asdasda";
    }
	public String getOp() {
		return op;
	}
	public void setOp(String op) {
		this.op = op;
	}
	public double getNum1() {
		return num1;
	}
	public void setNum1(double num1) {
		this.num1 = num1;
	}
	public double getNum2() {
		return num2;
	}
	public void setNum2(double num2) {
		this.num2 = num2;
	} 
}
