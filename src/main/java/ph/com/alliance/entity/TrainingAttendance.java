
package ph.com.alliance.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the training_attendance database table.
 * 
 */
@Entity
@Table(name="training_attendance")
@NamedQuery(name="TrainingAttendance.findAll", query="SELECT t FROM TrainingAttendance t")
public class TrainingAttendance implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="attendance_id")
	private int attendanceId;

	@Column(name="attendance_date")
	private String attendanceDate;

	@Column(name="participant_id")
	private int participantId;

	@Column(name="participant_status")
	private String participantStatus;

	@Column(name="training_id")
	private int trainingId;

	public TrainingAttendance() {
	}

	public int getAttendanceId() {
		return this.attendanceId;
	}

	public void setAttendanceId(int attendanceId) {
		this.attendanceId = attendanceId;
	}

	public String getAttendanceDate() {
		return this.attendanceDate;
	}

	public void setAttendanceDate(String attendanceDate) {
		this.attendanceDate = attendanceDate;
	}

	public int getParticipantId() {
		return this.participantId;
	}

	public void setParticipantId(int participantId) {
		this.participantId = participantId;
	}

	public String getParticipantStatus() {
		return this.participantStatus;
	}

	public void setParticipantStatus(String participantStatus) {
		this.participantStatus = participantStatus;
	}

	public int getTrainingId() {
		return this.trainingId;
	}

	public void setTrainingId(int trainingId) {
		this.trainingId = trainingId;
	}

}