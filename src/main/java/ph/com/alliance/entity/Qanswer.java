package ph.com.alliance.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the qanswers database table.
 * 
 */
@Entity
@Table(name="qanswers")
@NamedQuery(name="Qanswer.findAll", query="SELECT q FROM Qanswer q")
public class Qanswer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String answer;

	@Column(name="j_id")
	private int jId;

	@Column(name="p_id")
	private int pId;

	@Column(name="peer_answer")
	private String peerAnswer;

	@Column(name="q_id")
	private int qId;

	@Column(name="sp_answer")
	private String spAnswer;

	@Column(name="sp_id")
	private int spId;

	@Column(name="t_id")
	private int tId;

	@Column(name="u_id")
	private int uId;

	public Qanswer() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAnswer() {
		return this.answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public int getJId() {
		return this.jId;
	}

	public void setJId(int jId) {
		this.jId = jId;
	}

	public int getPId() {
		return this.pId;
	}

	public void setPId(int pId) {
		this.pId = pId;
	}

	public String getPeerAnswer() {
		return this.peerAnswer;
	}

	public void setPeerAnswer(String peerAnswer) {
		this.peerAnswer = peerAnswer;
	}

	public int getQId() {
		return this.qId;
	}

	public void setQId(int qId) {
		this.qId = qId;
	}

	public String getSpAnswer() {
		return this.spAnswer;
	}

	public void setSpAnswer(String spAnswer) {
		this.spAnswer = spAnswer;
	}

	public int getSpId() {
		return this.spId;
	}

	public void setSpId(int spId) {
		this.spId = spId;
	}

	public int getTId() {
		return this.tId;
	}

	public void setTId(int tId) {
		this.tId = tId;
	}

	public int getUId() {
		return this.uId;
	}

	@Override
	public String toString() {
		return "Qanswer [id=" + id + ", answer=" + answer + ", jId=" + jId
				+ ", pId=" + pId + ", peerAnswer=" + peerAnswer + ", qId="
				+ qId + ", spAnswer=" + spAnswer + ", spId=" + spId + ", tId="
				+ tId + ", uId=" + uId + "]";
	}

	public void setUId(int uId) {
		this.uId = uId;
	}

}