package ph.com.alliance.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the question_choices database table.
 * 
 */
@Entity
@Table(name="question_choices")
@NamedQuery(name="QuestionChoice.findAll", query="SELECT q FROM QuestionChoice q")
public class QuestionChoice implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="choice_id")
	private int choiceId;

	@Column(name="q_id")
	private int qId;

	public QuestionChoice() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getChoiceId() {
		return this.choiceId;
	}

	public void setChoiceId(int choiceId) {
		this.choiceId = choiceId;
	}

	public int getqId() {
		return qId;
	}

	public void setqId(int qId) {
		this.qId = qId;
	}

}