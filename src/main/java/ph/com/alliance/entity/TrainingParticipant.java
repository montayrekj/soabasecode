package ph.com.alliance.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the training_participants database table.
 * 
 */
@Entity
@Table(name="training_participants")
@NamedQuery(name="TrainingParticipant.findAll", query="SELECT t FROM TrainingParticipant t")
public class TrainingParticipant implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="participant_id")
	private int participantId;

	@Column(name="course_fdbackfrm")
	private String courseFdbackfrm;

	@Column(name="faci_fdbckfrm")
	private String faciFdbckfrm;

	@Column(name="skills_assessfrm")
	private String skillsAssessfrm;

	@Column(name="training_id")
	private int trainingId;

	private String trainingEffectiveness_frm;

	@Column(name="user_id")
	private int userId;

	public TrainingParticipant() {
	}

	public int getParticipantId() {
		return this.participantId;
	}

	public void setParticipantId(int participantId) {
		this.participantId = participantId;
	}

	public String getCourseFdbackfrm() {
		return this.courseFdbackfrm;
	}

	public void setCourseFdbackfrm(String courseFdbackfrm) {
		this.courseFdbackfrm = courseFdbackfrm;
	}

	public String getFaciFdbckfrm() {
		return this.faciFdbckfrm;
	}

	public void setFaciFdbckfrm(String faciFdbckfrm) {
		this.faciFdbckfrm = faciFdbckfrm;
	}

	public String getSkillsAssessfrm() {
		return this.skillsAssessfrm;
	}

	public void setSkillsAssessfrm(String skillsAssessfrm) {
		this.skillsAssessfrm = skillsAssessfrm;
	}

	public int getTrainingId() {
		return this.trainingId;
	}

	public void setTrainingId(int trainingId) {
		this.trainingId = trainingId;
	}

	public String getTrainingEffectiveness_frm() {
		return this.trainingEffectiveness_frm;
	}

	public void setTrainingEffectiveness_frm(String trainingEffectiveness_frm) {
		this.trainingEffectiveness_frm = trainingEffectiveness_frm;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "TrainingParticipant [participantId=" + participantId
				+ ", courseFdbackfrm=" + courseFdbackfrm + ", faciFdbckfrm="
				+ faciFdbckfrm + ", skillsAssessfrm=" + skillsAssessfrm
				+ ", trainingId=" + trainingId + ", trainingEffectiveness_frm="
				+ trainingEffectiveness_frm + ", userId=" + userId + "]";
	}

}