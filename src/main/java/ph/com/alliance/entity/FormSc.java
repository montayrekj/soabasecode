package ph.com.alliance.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the form_sc database table.
 * 
 */
@Entity
@Table(name="form_sc")
@NamedQuery(name="FormSc.findAll", query="SELECT f FROM FormSc f")
public class FormSc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String name;

	//bi-directional many-to-one association to Form
	@ManyToOne
	@JoinColumn(name="fk_fid")
	private Form form;

	//bi-directional many-to-one association to FormSc
	@ManyToOne
	@JoinColumn(name="fk_fscid")
	private FormSc formSc;

	//bi-directional many-to-one association to FormSc
	@OneToMany(mappedBy="formSc")
	private List<FormSc> formScs;

	public FormSc() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Form getForm() {
		return this.form;
	}

	public void setForm(Form form) {
		this.form = form;
	}

	public FormSc getFormSc() {
		return this.formSc;
	}

	public void setFormSc(FormSc formSc) {
		this.formSc = formSc;
	}

	public List<FormSc> getFormScs() {
		return this.formScs;
	}

	public void setFormScs(List<FormSc> formScs) {
		this.formScs = formScs;
	}

	public FormSc addFormSc(FormSc formSc) {
		getFormScs().add(formSc);
		formSc.setFormSc(this);

		return formSc;
	}

	public FormSc removeFormSc(FormSc formSc) {
		getFormScs().remove(formSc);
		formSc.setFormSc(null);

		return formSc;
	}

}