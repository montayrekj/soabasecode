package ph.com.alliance.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the effectiveness database table.
 * 
 */
@Entity
@NamedQuery(name="Effectiveness.findAll", query="SELECT e FROM Effectiveness e")
public class Effectiveness implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="question_ans")
	private String questionAns;

	@Column(name="question_name")
	private String questionName;

	@Column(name="question_type")
	private String questionType;

	public Effectiveness() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getQuestionAns() {
		return this.questionAns;
	}

	public void setQuestionAns(String questionAns) {
		this.questionAns = questionAns;
	}

	public String getQuestionName() {
		return this.questionName;
	}

	public void setQuestionName(String questionName) {
		this.questionName = questionName;
	}

	public String getQuestionType() {
		return this.questionType;
	}

	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}

	@Override
	public String toString() {
		return "Effectiveness [id=" + id + ", questionAns=" + questionAns
				+ ", questionName=" + questionName + ", questionType="
				+ questionType + "]";
	}

}