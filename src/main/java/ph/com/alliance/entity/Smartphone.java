//package ph.com.alliance.entity;
//
//import java.io.Serializable;
//import javax.persistence.*;
//
//
///**
// * The persistent class for the smartphones database table.
// * 
// */
//@Entity
//@Table(name="smartphones")
//@NamedQuery(name="Smartphone.findAll", query="SELECT s FROM Smartphone s")
//public class Smartphone implements Serializable {
//	private static final long serialVersionUID = 1L;
//
//	@Id
//	private int id;
//
//	private String brand;
//
//	private String color;
//
//	private String name;
//
//	private String OS_version;
//
//	private double price;
//
//	public Smartphone() {
//	}
//
//	public int getId() {
//		return this.id;
//	}
//
//	public void setId(int id) {
//		this.id = id;
//	}
//
//	public String getBrand() {
//		return this.brand;
//	}
//
//	public void setBrand(String brand) {
//		this.brand = brand;
//	}
//
//	public String getColor() {
//		return this.color;
//	}
//
//	public void setColor(String color) {
//		this.color = color;
//	}
//
//	public String getName() {
//		return this.name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public String getOS_version() {
//		return this.OS_version;
//	}
//
//	public void setOS_version(String OS_version) {
//		this.OS_version = OS_version;
//	}
//
//	public double getPrice() {
//		return this.price;
//	}
//
//	public void setPrice(double price) {
//		this.price = price;
//	}
//
//	@Override
//	public String toString() {
//		return "Phone ID: " + id + ", Brand: " + brand + ", Name: " + name + ", Color: " + color
//				+ ", OS_version: " + OS_version + ", Price: " + price;
//	}
//
//}