package ph.com.alliance.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the training database table.
 * 
 */
@Entity
@NamedQuery(name="Training.findAll", query="SELECT t FROM Training t")
public class Training implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="training_id")
	private int trainingId;

	@Column(name="annual_plan")
	private int annualPlan;


	@Column(name="date_end")
	private String dateEnd;

	@Column(name="date_start")
	private String dateStart;

	@Lob
	private String objectives;

	@Lob
	private String outline;

	private String skAssessFrm;

	private String tnaFrm;

	@Column(name="training_title")
	private String trainingTitle;

	public Training() {
	}

	public int getTrainingId() {
		return this.trainingId;
	}

	public void setTrainingId(int trainingId) {
		this.trainingId = trainingId;
	}

	public int getAnnualPlan() {
		return this.annualPlan;
	}

	public void setAnnualPlan(int annualPlan) {
		this.annualPlan = annualPlan;
	}

	public String getDateEnd() {
		return this.dateEnd;
	}

	public void setDateEnd(String dateEnd) {
		this.dateEnd = dateEnd;
	}

	public String getDateStart() {
		return this.dateStart;
	}

	public void setDateStart(String dateStart) {
		this.dateStart = dateStart;
	}

	public String getObjectives() {
		return this.objectives;
	}

	public void setObjectives(String objectives) {
		this.objectives = objectives;
	}

	public String getOutline() {
		return this.outline;
	}

	public void setOutline(String outline) {
		this.outline = outline;
	}

	public String getSkAssessFrm() {
		return this.skAssessFrm;
	}

	public void setSkAssessFrm(String skAssessFrm) {
		this.skAssessFrm = skAssessFrm;
	}

	public String getTnaFrm() {
		return this.tnaFrm;
	}

	public void setTnaFrm(String tnaFrm) {
		this.tnaFrm = tnaFrm;
	}

	public String getTrainingTitle() {
		return this.trainingTitle;
	}

	@Override
	public String toString() {
		return "Training [trainingId=" + trainingId + ", annualPlan="
				+ annualPlan + ", dateEnd=" + dateEnd + ", dateStart="
				+ dateStart + ", objectives=" + objectives + ", outline="
				+ outline + ", skAssessFrm=" + skAssessFrm + ", tnaFrm="
				+ tnaFrm + ", trainingTitle=" + trainingTitle + "]";
	}

	public void setTrainingTitle(String trainingTitle) {
		this.trainingTitle = trainingTitle;
	}

}