package ph.com.alliance.interceptors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class ModuleInterceptor extends HandlerInterceptorAdapter {

	public ModuleInterceptor() {
	}

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {

		HttpSession session = request.getSession();

		if (session.getAttribute("isLoggedIn") != null) {
			System.out.println("Im still logged in!");
			if (session.getAttribute("role").equals("employee")){
				if (request.getRequestURL().indexOf("admin") > 0){
					
					//redirect to not allowed page
					response.sendRedirect(request.getContextPath() + "/notallowed");
					return false;
				}
			}
			else if (session.getAttribute("role").equals("professional")){
				if (request.getRequestURL().indexOf("admin") > 0 ||
					request.getRequestURL().indexOf("supervisor") > 0){
					
					//redirect to not allowed page
					response.sendRedirect(request.getContextPath() + "/notallowed");
					return false;
				}
			}
			
		} else if (request.getRequestURL().indexOf("login") > 0) {
			System.out
					.println("THIS IS A REQUEST TO THE LOG IN JSP. Allow this request.");
		} else {
			//Redirect to log-in page.
			response.sendRedirect(request.getContextPath() + "/login");
			System.out.println("IM NOT LOGGED IN!");
			return false;
		}

		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
	}
}