package ph.com.alliance.controller.view;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ph.com.alliance.entity.Question;
import ph.com.alliance.entity.User;
import ph.com.alliance.service.QuestionService;


@Controller
@RequestMapping("/tnaemp")
public class TnaEmpController {
	
	@Autowired
	private QuestionService questionService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String loadTrainingsPage(HttpServletRequest request, ModelMap map) {
		List<Question> questionList = questionService.getQuestionsByForm(2);
		int ctr=0;
		User u = null;
		u = (User) request.getSession().getAttribute("user");
		String utype = u.getUserType();
		String temp;
		if(utype.equals("admin")){
			temp = "Supervisor";
		} else {
			temp = "Peer";
		}
		map.put("userType", temp);
		map.put("questions",questionList);
		map.put("counter", ctr);
		return "tnaemp";
	}
}
