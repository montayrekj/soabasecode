package ph.com.alliance.controller.view;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ph.com.alliance.entity.Training;
import ph.com.alliance.entity.TrainingParticipant;
import ph.com.alliance.entity.User;
import ph.com.alliance.service.DBTransactionTestService;
import ph.com.alliance.service.TrainingService;
import ph.com.alliance.service.UtilitiesService;

@Controller
@RequestMapping("/supervisor")
public class SupervisorController {

	@Autowired
	private TrainingService trainingService;
	
	@Autowired
	private DBTransactionTestService dbTransactionTestService;
	
	@Autowired
	private UtilitiesService utilitiesService;
	
	@RequestMapping(method=RequestMethod.GET)
	public String loadSupervisorPage(HttpServletRequest request, ModelMap map){
		List<Object[]> subordinateTrainingList = null;
		User supervisor = (User) request.getSession().getAttribute("user");
		int supervisorID = supervisor.getEmpId();
		
		subordinateTrainingList = trainingService.retrieveSubordinateTrainingList(supervisorID);
		if(subordinateTrainingList != null){
			System.out.println("dinullni");
			for(Object o[]:subordinateTrainingList){
//				for(int ctr = 0; ctr < 4; ctr++){
//					System.out.println(o[ctr]);
//				}
			}
		} else {
			System.out.println("nullni");
		}
		map.put("saReleased","disabled");
		map.put("stList", subordinateTrainingList);
		map.put("today", utilitiesService.getDateString(new Date()));
		
		return "supervisor";
	}
	
	@RequestMapping(value="/get-details", method=RequestMethod.POST)
	public String loadSupervisorDetails(HttpServletRequest request, ModelMap map){
		int participantID = Integer.parseInt(request.getParameter("prtID").toString()); 
		int trainingID = Integer.parseInt(request.getParameter("trID").toString());

		User participant = null;
		Training training = null;
		TrainingParticipant tp = null;
		
		participant = dbTransactionTestService.findEmployee(participantID);
		training = trainingService.retrieveTraining(trainingID);
		tp = dbTransactionTestService.getParticipant(participant.getUserId(),training.getTrainingId());
		
		if(tp.getSkillsAssessfrm().equals("released")){
			System.out.println("sudsk");
			System.out.println(tp.getSkillsAssessfrm());
			map.put("saReleased","enabled");
		} else {
			System.out.println("!sudsk");
			map.put("saReleased","enabled");
		}
		
		if(training != null && participant != null) {
			map.put("p", participant);
			map.put("t", training);
			map.put("dateStart", utilitiesService.convertDateFormat(training.getDateStart()));
			map.put("dateEnd", utilitiesService.convertDateFormat(training.getDateEnd()));
		}

		return "supervisor";
	}
}
