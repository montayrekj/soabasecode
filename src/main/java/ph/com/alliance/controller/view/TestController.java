//package ph.com.alliance.controller.view;
//
//import java.util.List;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.ModelMap;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//
//import ph.com.alliance.entity.Calc;
//import ph.com.alliance.entity.Smartphone;
//import ph.com.alliance.service.MyFunctionService;
//
//@Controller
//@RequestMapping("/testpage")
//public class TestController {
//
//	@Autowired
//	private MyFunctionService myFunctionService;   
//	
//    @RequestMapping(method = RequestMethod.GET)
//    public String viewTestPage(ModelMap map){
//    	List<Smartphone> myTableList = myFunctionService.getSmartPhones();
//    	map.put("myTable", myTableList);
//    	return "testpage";
//    }
//    
//    @RequestMapping(value = "/insert", method = RequestMethod.GET)
//    public String insertSmartPhone(HttpServletRequest request, ModelMap map, Smartphone phone){
//    	myFunctionService.insertSmartphone(phone);
//    	return this.viewTestPage(map);
//    }
//	
//    @RequestMapping(value = "/update", method = RequestMethod.GET)
//    public String updateSmartPhone(HttpServletRequest request, ModelMap map, Smartphone phone){
//    	System.out.println(phone.getOS_version());
//    	System.out.println(phone.getPrice());
//    	myFunctionService.updateSmartphone(phone);
//    	return this.viewTestPage(map);
//    }
//    
////	@RequestMapping(method=RequestMethod.GET)
////	public String computeOperation(HttpServletRequest request,  ModelMap map, Calc cal){
////		double num1=0,num2=0;
////		double res=0,res2=0;
////		String number1 = request.getParameter("num1");
////		String number2 = request.getParameter("num2");
////		String classDiv = "table col hide";
////
////		if(null != number1){
////			num1 = Double.parseDouble(request.getParameter("num1"));
////		}
////		
////		if(null != number2){
////			num2 = Double.parseDouble(request.getParameter("num2"));
////		}
////		String op = request.getParameter("op");
////	
////		//--------------------------------------------------------> Operations through request object
////		if(null != op){
////			if("+".equals(op)){
////				res=add(num1,num2);
////			}
////			else if("-".equals(op)){
////				res=subtract(num1,num2);
////			}
////			else if("*".equals(op)){
////				res=multiply(num1,num2);
////			}
////			else if("/".equals(op)){
////				res=divide(num1,num2);
////			}
////		}
////		
////		//-------------------------------------------------------> Operations through mapping
////		if(null != cal.getOp()){
////			if("+".equals(cal.getOp())){
////				classDiv = "table col show";
////				res2=add(cal.getNum1(),cal.getNum2());
////			}
////			else if("-".equals(cal.getOp())){
////				classDiv = "table col show";
////				res2=subtract(cal.getNum1(),cal.getNum2());
////			}
////			else if("*".equals(cal.getOp())){
////				classDiv = "table col show";
////				res2=multiply(cal.getNum1(),cal.getNum2());
////			}
////			else if("/".equals(cal.getOp())){
////				classDiv = "table col show";
////				res2=divide(cal.getNum1(),cal.getNum2());
////			}
////		}
////		
////		map.put("result", res);
////		map.put("result2", res2);
////		map.put("classDiv",classDiv);
////		
////		return "calculator";
////	}
////	
////	private double add(double num1, double num2){
////		return num1+num2;
////	}
////	
////	private double subtract(double num1, double num2){
////		return num1-num2;
////	}
////	
////	private double multiply(double num1, double num2){
////		return num1*num2;
////	}
////	
////	private double divide(double num1, double num2){
////		return num1/(num2*1.0f);
////	}
//	
//}
