package ph.com.alliance.controller.view;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ph.com.alliance.entity.AnnualPlan;
import ph.com.alliance.entity.Employee;
import ph.com.alliance.entity.Qanswer;
import ph.com.alliance.entity.Question;
import ph.com.alliance.entity.Training;
import ph.com.alliance.entity.TrainingParticipant;
import ph.com.alliance.entity.User;
import ph.com.alliance.service.AnnualPlanService;
import ph.com.alliance.service.EmployeeService;
import ph.com.alliance.service.QuestionService;
import ph.com.alliance.service.TrainingAttendanceService;
import ph.com.alliance.service.TrainingParticipantService;
import ph.com.alliance.service.TrainingService;
import ph.com.alliance.service.UserService;
import ph.com.alliance.service.UtilitiesService;
import ph.com.alliance.service.impl.DBTransactionTestServiceImpl;

@Controller
@RequestMapping("/admin")
public class AdministratorController {

	@Autowired
	private DBTransactionTestServiceImpl dbttsl;

	@Autowired
	private AnnualPlanService planService;

	@Autowired
	private TrainingService trainingService;

	@Autowired
	private TrainingParticipantService participantService;

	@Autowired
	private TrainingAttendanceService attendanceService;
	
	@Autowired
	private UtilitiesService utilitiesService;

	@Autowired
	private UserService userService;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private QuestionService questionService;
	
	@RequestMapping(value = "/training-plan", method = RequestMethod.GET)
	public String loadTrainingPlanPage(HttpServletRequest request, ModelMap map) {
		List<AnnualPlan> apl = null;
		String requestId = request.getParameter("planId");
		try{
			apl = planService.getAllAnnualPlan();
			if(null != requestId){
				int planId = Integer.parseInt(requestId);
				map.put("plan", planService.retrievePlanInfo(planId));
			}
		}catch(NumberFormatException e){
			System.out.println("Can't parse request parameter.");
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
		map.put("planList", apl);

		return "annualPlan";
	}

	@RequestMapping(value = "/trainingsched", method = RequestMethod.GET)
	public String loadTrainingSchedPage(HttpServletRequest request, ModelMap map) {
		String requestId = request.getParameter("plan_id");
		AnnualPlan ap = null;

		try {
			int planId = Integer.parseInt(requestId);
			ap = planService.retrievePlanInfo(planId);
		} catch (NullPointerException e) {
			System.out.println("Request parameter is null.");
		} catch (NumberFormatException e) {
			System.out.println("Request parameter is unparsable to int.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		map.put("planInfo", ap);

		return "trainingPlan";
	}

	@RequestMapping(value = "/trainingevents", method = RequestMethod.GET)
	public @ResponseBody List<Training> getAllTrainingEvents(
			HttpServletRequest request) {
		String requestId = request.getParameter("planId");
		try {
			int planId = Integer.parseInt(requestId);
			List<Training> tl = trainingService.getAllTrainingsPerPlan(planId);

			return tl;
		} catch (NullPointerException e) {
			System.out.println("Request parameter is null.");
		} catch (NumberFormatException e) {
			System.out.println("Request parameter is unparsable to int.");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@RequestMapping(value = "/newtraining", method = RequestMethod.POST)
	public @ResponseBody boolean createNewTraining(Training t) {
		boolean res = false;

		try {
			res = trainingService.createUpdateTraining(t);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return res;
	}

	@RequestMapping(value = "/updatetraining", method = RequestMethod.POST)
	public @ResponseBody Training updateTraining(HttpServletRequest request,
			Training t) {
		try {
			Training temp = trainingService.retrieveTraining(t.getTrainingId());
			t.setAnnualPlan(temp.getAnnualPlan());
			if (null == t.getObjectives()) {
				t.setObjectives(temp.getObjectives());
			}
			if (null == t.getOutline()) {
				t.setOutline(temp.getOutline());
			}
		} catch (NullPointerException e) {
			System.out.println("Null request parameter.");
		} catch (NumberFormatException e) {
			System.out.println("Can't parse request parameter.");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			trainingService.createUpdateTraining(t);
			attendanceService.deleteOnTrainingUpdate(trainingService
					.retrieveTraining(t.getTrainingId()));
		}

		return trainingService.retrieveTraining(t.getTrainingId());
	}

	@RequestMapping(value = "/generateTrainingId", method = RequestMethod.GET)
	public @ResponseBody int generateTrainingId() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date now = new Date();

		String date = df.format(now);
		int year = Integer.parseInt(date.substring(2, 4));
		int month = Integer.parseInt(date.substring(5, 7));
		int trainingCount = trainingService.countTrainings();
		int id = ((year * 100000000) + (month * 1000000) + (trainingCount + 1) * 100);

		return id;
	}

	@RequestMapping(value = "/createplan", method = RequestMethod.POST)
	public @ResponseBody boolean createAnnualPlan(AnnualPlan ap) {
		boolean res = false;

		try {
			res = planService.createAnnualPlan(ap);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return res;
	}

	@RequestMapping(value = "/getParticipantProfile", method = RequestMethod.GET)
	public @ResponseBody User getParticipantProfile(
			@RequestParam(value = "participantId") String participantId) {

		if (null != participantId) {
			try {
				int id = Integer.parseInt(participantId);
				TrainingParticipant tp = participantService
						.retrieveParticipant(id);
				if (null != tp) {
					return dbttsl.retrieveUser(tp.getUserId());
				}
			} catch (NumberFormatException e) {
				System.out.println("Unparsable request id.");
			}
		}

		return null;
	}
	
	@RequestMapping(value="/reports", method = RequestMethod.GET)
	public String loadReportsPage(HttpServletRequest request,ModelMap map){
		String requestId = request.getParameter("trainingId");
		
		if(null != requestId){
			try{
				int trainingId = Integer.parseInt(requestId);
				System.out.println(trainingId);
				map.put("t", trainingService.retrieveTraining(trainingId));
			}catch(NumberFormatException e){
				System.out.println("Can't parse request parameter.");
			}
		}
		map.put("today",utilitiesService.getDateString(new Date()));
		map.put("trainingList", trainingService.getAllTrainingsPerPlan(2017));
		return "reports";
	}
	
	@RequestMapping(value="/effectiveness", method = RequestMethod.GET)
	public String loadCourseReportsPage(HttpServletRequest request,ModelMap map){
		String requestId = request.getParameter("trainingId");
		
		if(null != requestId){
			try{
				int trainingId = Integer.parseInt(requestId);
				map.put("t", trainingService.retrieveTraining(trainingId));
				List<Qanswer> asl = questionService.getAllEffectivenessAnswer(17060500);
				for(Iterator<Qanswer> iteratorq = asl.iterator();iteratorq.hasNext();){
					Qanswer qa = iteratorq.next();
					if(qa.getQId()<58 || qa.getQId()>59){
						iteratorq.remove();
					}
				}
				List<Question> q = questionService.getQuestionsByForm(4);
				map.put("questions", q);
				if(q!=null){
					for(Question question:q){
						System.out.println(question);
					}
				}
				map.put("others", asl);
			}catch(NumberFormatException e){
				System.out.println("Can't parse request parameter.");
			}
		}
		map.put("today",utilitiesService.getDateString(new Date()));
		
		return "effectiveness";
	}
	
	@RequestMapping(value="/eff-feedback", method = RequestMethod.GET)
	public @ResponseBody List<Object> getEffectivenessFeedback(@RequestParam(value="trainingId") String requestId){
		
		if(null != requestId){
			try{
				int trainingId = Integer.parseInt(requestId);
				return questionService.getEffectivenessFeedback(trainingId);
			}catch(NumberFormatException e){
				System.out.println("Can't parse request parameter.");
			}
		}
		return null;
	}
	
	@RequestMapping(value="/eff-rating", method = RequestMethod.GET)
	public @ResponseBody List<Object> getEffectivenessRating(@RequestParam(value="trainingId") String requestId){
		
		if(null != requestId){
			try{
				int trainingId = Integer.parseInt(requestId);
				return questionService.getEffectivenessRating(trainingId);
			}catch(NumberFormatException e){
				System.out.println("Can't parse request parameter.");
			}
		}
		return null;
	}
	
	@RequestMapping(value="/eff-questions", method = RequestMethod.GET)
	public @ResponseBody List<Question> getEffectivenessQuestions(@RequestParam(value="formId") String frmId){
		if(frmId != null){
			try{
				return questionService.getQuestionsByForm(Integer.parseInt(frmId));
			} catch(Exception e){
			}
		}
		return null;
	}

	@RequestMapping(value="/participant-skills", method=RequestMethod.GET)
	public String loadParticpantSkillsPage(@RequestParam(value="trainingId") String requestId, ModelMap map, HttpServletRequest request){
		String requestParti = request.getParameter("userId");
		try {
			List<TrainingParticipant> tpl = participantService
					.getTrainingParticipants(Integer.parseInt(requestId));
			List<User> userList = new ArrayList<User>();
			for (TrainingParticipant tp : tpl) {
				User u = dbttsl.retrieveUser(tp.getUserId());
				userList.add(u);
			}
			if(null != requestParti){
				TrainingParticipant participant = dbttsl
						.getParticipant(Integer.parseInt(requestParti),
								Integer.parseInt(requestId));
				questionService.getPreAssesment(Integer.parseInt(requestParti));
				map.put("participant", participant);
			}
			map.put("userList", userList);
			map.put("trainingId",Integer.parseInt(requestId));
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return "participant-skills";
	}
	
	@RequestMapping(value="/getPreSkills", method=RequestMethod.GET)
	public @ResponseBody Object[] getPreSkills(@RequestParam(value="participantId") String requestId){
		if(null != requestId){
			try{
				return questionService.getPreAssesment(Integer.parseInt(requestId));
			}catch(Exception e){
				System.err.println(e.getMessage());
			}
		}
		return null;
	}
	
	@RequestMapping(value="/eff-questionNames", method = RequestMethod.GET)
	public @ResponseBody List<String> getEffectivenessQuestionNames(@RequestParam(value="formId") String frmId){
		List<Question> qList = questionService.getQuestionsByForm(Integer.parseInt(frmId));
		List<String> qNames = new ArrayList<String>();
		System.out.println("sudeffname");
		String temp = "$"+"{questions[0].getQuestion()}";
		int ctr = 0;
		if(frmId != null){
			try{
				if(qList != null){
					for(Question q:qList){
						if(q.getAssessType().equals("multiple")){
							qNames.add(q.getQuestion());
							ctr++;
						} else if (q.getAssessType().equals("multiple2")){
							qNames.add(q.getQuestion());
							ctr++;
						}
					}
				}
			} catch(Exception e){
				
			}
		}
		return qNames;
	}
	
	@RequestMapping(value="/getPostSkills", method=RequestMethod.GET)
	public @ResponseBody Object[] getPostSkills(@RequestParam(value="participantId") String requestId){
		if(null != requestId){
			try{
				return questionService.getPostAssesment(Integer.parseInt(requestId));
			}catch(Exception e){
				System.err.println(e.getMessage());
			}
		}
		return null;
	}

	@RequestMapping(value = "/systemusers", method = RequestMethod.GET)
	public String loadSystemUsersPage(ModelMap map) {
		List<User> userList = null;

		userList = userService.selectAllUsers();

		map.put("userList", userList);
		return "systemusers";
	}

	@RequestMapping(value = "/systemusers/create", method = RequestMethod.POST)
	public @ResponseBody boolean createUser(HttpServletRequest request,
			ModelMap map) {
		User u = new User();
		
		try {
			u.setUserId(userService.generateUserId());
			u.setEmpId(Integer.parseInt(request.getParameter("emp_id")));
			u.setUsername(request.getParameter("emp_id"));
			u.setUserPass("root");
			u.setUserType(request.getParameter("user_type"));
			u.setFirstName(request.getParameter("first_name"));
			u.setMiddleName(request.getParameter("middle_name"));
			u.setLastName(request.getParameter("last_name"));
			u.setAge(Integer.parseInt(request.getParameter("age")));
			u.setGender(request.getParameter("gender"));
			u.setAddress(request.getParameter("address"));
			u.setEmail(request.getParameter("email"));
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return userService.createUser(u);
	}

	@RequestMapping(value = "/systemusers/select", method = RequestMethod.POST)
	public @ResponseBody User selectUser(
			@RequestParam(value = "userId") int userId, ModelMap map) {
		User u = null;
		u = userService.selectUser(userId);
		return u;
	}

	@RequestMapping(value = "/systemusers/update", method = RequestMethod.POST)
	public @ResponseBody boolean updateUser(HttpServletRequest request,
			ModelMap map) {
		User u = new User();
		
		try {
			u.setUserId(Integer.parseInt(request.getParameter("user_id")));
			u.setUserType(request.getParameter("user_type"));
			u.setAddress(request.getParameter("address"));
			u.setEmail(request.getParameter("email"));
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return userService.updateUser(u);
	}

	@RequestMapping(value = "/systemusers/delete", method = RequestMethod.POST)
	public @ResponseBody boolean deleteUser(
			@RequestParam(value = "userId") int userId, ModelMap map) {
		boolean success = false;
		success = userService.deleteUser(userId);
		return success;
	}

	@RequestMapping(value = "/systemusers/generateID", method = RequestMethod.POST)
	public @ResponseBody int generateId(ModelMap map) {
		int id = userService.generateUserId();
		return id;
	}

	@RequestMapping(value = "/systemusers/selectEmployee", method = RequestMethod.POST)
	public @ResponseBody List<Employee> selectEmployee(
			@RequestParam(value = "lastName") String lastName, ModelMap map) {
		List<Employee> employeeList = null;
		employeeList = employeeService.selectEmployeeByLastName(lastName);
		return employeeList;
	}

}
