package ph.com.alliance.controller.view;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Example controller class that handles request for the application root.
 * 
 *
 */
@Controller
@RequestMapping("/")
public class IndexController {
	
    @RequestMapping(method=RequestMethod.GET)
    public void loadMainMenuIndex(HttpServletRequest request, HttpServletResponse response, ModelMap map){
    	if(request.getSession().getAttribute("isLoggedIn") != null)
			try {
				response.sendRedirect(request.getContextPath() + "/home");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		else
			try {
				response.sendRedirect(request.getContextPath() + "/login");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    }
    
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public void logout(HttpServletRequest request, HttpServletResponse response, ModelMap map)
    {
    	request.getSession().invalidate();
    	try {
			response.sendRedirect(request.getContextPath() + "/");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

}
