package ph.com.alliance.controller.view;

import java.time.LocalDate;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ph.com.alliance.entity.Training;
import ph.com.alliance.service.TrainingService;

@Controller
@RequestMapping("/calendar")
public class CalendarController {

	@Autowired
	private TrainingService trainingService;
	
	@RequestMapping(method=RequestMethod.GET)
	public String loadHomePage(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		map.put("year", LocalDate.now().getYear());
		
		return "Calendar";
	}
	
	@RequestMapping(value = "/trainingevents", method = RequestMethod.GET)
	public @ResponseBody List<Training> getAllTrainingEvents(
			HttpServletRequest request) {
		String requestId = request.getParameter("planId");
		try {
			int planId = Integer.parseInt(requestId);
			List<Training> tl = trainingService.getAllTrainingsPerPlan(planId);
			
			return tl;
		} catch (NullPointerException e) {
			System.out.println("Request parameter is null.");
		} catch (NumberFormatException e) {
			System.out.println("Request parameter is unparsable to int.");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	
}
