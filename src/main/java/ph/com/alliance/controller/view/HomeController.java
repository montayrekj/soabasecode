package ph.com.alliance.controller.view;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ph.com.alliance.entity.Notification;
import ph.com.alliance.entity.User;
import ph.com.alliance.service.NotificationsService;
import ph.com.alliance.service.impl.DBTransactionTestServiceImpl;

@Controller
@RequestMapping("/home")
public class HomeController {

	@Autowired
	private DBTransactionTestServiceImpl dbttsl;
	
	@Autowired
	private NotificationsService notifService;
	
	@RequestMapping(method=RequestMethod.GET)
	public String loadHomePage(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		User u = null;
		u = (User) request.getSession().getAttribute("user");
		String fullName=""+u.getFirstName()+" "+u.getMiddleName()+" "+u.getLastName();
		List<Notification> notifList = notifService.getNotificationsByUser(u.getUserId());
		
		map.put("username",u.getUsername());
		map.put("headername",fullName);
		map.put("headertype",u.getUserType());
		map.put("name",fullName);
		map.put("type",u.getUserType());
		map.put("idnum", u.getUserId());
		map.put("sex", u.getGender());
		// map.put("bday", "01-01-1996");
		map.put("address", u.getAddress());
		map.put("email", u.getEmail());
		
		if(notifList.size() == 0 || notifList == null){
			map.put("notifList", null);
		} else {
			map.put("notifList", notifList);
		}
		
		
		return "home";
	}
	
	@RequestMapping(value="/editUser", method=RequestMethod.POST)
	public String editUser(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		User u = null;
		u = (User) request.getSession().getAttribute("user");
		
		String username = request.getParameter("uname");
		String pass = request.getParameter("pass");
		String pword = request.getParameter("pword");
		String cpword = request.getParameter("cpword");
		
		if(pass.equals(u.getUserPass())){
			if(pword.equals(cpword)){
				u.setUsername(username);
				u.setUserPass(pword);
				dbttsl.updateUser(u);
			} else {
				return "password_!match";
			}
		} else {
			return "wrongpassword";
		}
		
		return this.loadHomePage(request, response, map);
	}
}
