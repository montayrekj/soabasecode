package ph.com.alliance.controller.view;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ph.com.alliance.entity.Form;
import ph.com.alliance.entity.Training;
import ph.com.alliance.entity.TrainingAttendance;
import ph.com.alliance.entity.TrainingFacilitator;
import ph.com.alliance.entity.TrainingParticipant;
import ph.com.alliance.entity.User;
import ph.com.alliance.service.FormService;
import ph.com.alliance.service.TrainingAttendanceService;
import ph.com.alliance.service.TrainingFacilitatorService;
import ph.com.alliance.service.TrainingParticipantService;
import ph.com.alliance.service.TrainingService;
import ph.com.alliance.service.UtilitiesService;
import ph.com.alliance.service.impl.DBTransactionTestServiceImpl;

@Controller
@RequestMapping("/trainings")
public class TrainingsController {

	@Autowired
	private DBTransactionTestServiceImpl dbttsl;

	@Autowired
	private TrainingService trainingService;

	@Autowired
	private TrainingAttendanceService attendanceService;

	@Autowired
	private TrainingFacilitatorService facilitatorService;

	@Autowired
	private TrainingParticipantService participantService;

	@Autowired
	private FormService formService;

	@Autowired
	private UtilitiesService utilitiesService;

	public LocalDate today = LocalDate.now();
	
	@RequestMapping(method = RequestMethod.GET)
	public String loadTrainingsPage(HttpServletRequest request, ModelMap map) {
		List<Training> userTrainingList = null;
		User participant = (User) request.getSession().getAttribute("user");
		userTrainingList = trainingService
				.retrieveTrainingsByParticipant(participant.getUserId());
		map.put("trainingList", userTrainingList);
		map.put("today", utilitiesService.getDateString(new Date()));

		map.put("saReleased", "disabled");
		map.put("cfReleased", "disabled");
		map.put("fdRleased", "disabled");

		return "trainings";
	}

	@RequestMapping(value = "/getAllFacilitators", method = RequestMethod.GET)
	public @ResponseBody List<User> getAllFacilitators(
			HttpServletRequest request) {

		String id = request.getParameter("training_id");
		try {
			List<TrainingFacilitator> tfl = facilitatorService
					.getTrainingFaciltators(Integer.parseInt(id.trim()));
			if (null != tfl) {
				List<User> ul = new ArrayList<User>();
				for (TrainingFacilitator tf : tfl) {
					User u = dbttsl.retrieveUser(tf.getUserId());
					ul.add(u);
				}
				return ul;
			}
		} catch (NullPointerException e) {
			System.out.println("Null request parameter.");
		} catch (NumberFormatException e) {
			System.out.println("Can't parse request parameter.");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@RequestMapping(value = "/getAllParticipants", method = RequestMethod.GET)
	public @ResponseBody List<User> getAllParticipants(
			HttpServletRequest request) {
		String id = request.getParameter("training_id");
		try {
			List<TrainingParticipant> tpl = participantService
					.getTrainingParticipants(Integer.parseInt(id.trim()));
			if (null != tpl) {
				List<User> ul = new ArrayList<User>();
				for (TrainingParticipant tp : tpl) {
					User u = dbttsl.retrieveUser(tp.getUserId());
					ul.add(u);
				}
				return ul;
			}
		} catch (NullPointerException e) {
			System.out.println("Request id is null");
		} catch (NumberFormatException e) {
			System.out.println("Unparsable string");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@RequestMapping(value = "/getAvailUsers", method = RequestMethod.GET)
	public @ResponseBody List<User> getAvailUsers(HttpServletRequest request) {
		String id = request.getParameter("training_id");
		if (null != id) {
			try {
				int training_id = Integer.parseInt(id.trim());
				Training t = trainingService.retrieveTraining(training_id);
				List<User> userList = dbttsl.selectAllUsers();
				if(null != t && userList.size() > 0){
					LocalDate tStart = LocalDate.parse(t.getDateStart());
					LocalDate tEnd = LocalDate.parse(t.getDateEnd());
					for(Iterator<User> iteratoru = userList.iterator(); iteratoru
							.hasNext();){
						User up = iteratoru.next();
						boolean flag = true;
						List<Training> tl = trainingService.retrieveTrainingsByParticipant(up.getUserId());
						tl.addAll(trainingService.retrieveTrainingsByFacilitator(up.getUserId()));
						for(Training ttl:tl){
							LocalDate ttlStart = LocalDate.parse(ttl.getDateStart());
							LocalDate ttlEnd = LocalDate.parse(ttl.getDateEnd());
							if(ttlEnd.compareTo(today) > 0){
								if(tStart.compareTo(ttlStart) >= 0 && tEnd.compareTo(ttlEnd) <=0){//new training is in between users joined training
									flag = false;
								}
								if(tEnd.compareTo(ttlStart)==0 || tStart.compareTo(ttlEnd)==0){//user has training at the end of joined training and vice versa
									flag = false;
								}
								if(tStart.compareTo(ttlStart) <=0 && tEnd.compareTo(ttlEnd) >=0 ){//training has covered user's joined trainng
									flag = false;
								}
							}
						}
						if(!flag){
							iteratoru.remove();
						}
					}
				}
				
				return userList;
			} catch (NumberFormatException e) {
				System.out.println("Unparsable string");
			}
		}
		return null;
	}

	@RequestMapping(value = "/viewTraining", method = RequestMethod.GET)
	public String loadTrainingView(HttpServletRequest request, ModelMap map) {
		String id = request.getParameter("training_id");
		try {
			int training_id = Integer.parseInt(id.trim());
			List<String> objectiveList = null;
			Training t = trainingService.retrieveTraining(training_id);
			if (null != t.getObjectives()) {
				objectiveList = new ArrayList<String>();
				String raw = t.getObjectives().trim();
				if (raw.length() > 0) {
					String objective = "";
					for (int x = 0; x < raw.length(); x++) {
						if (raw.charAt(x) == '|') {
							objectiveList.add(objective);
							objective = "";
						} else {
							objective += raw.charAt(x);
						}
					}
				}
			}
			map.put("training", t);
			map.put("objectives", objectiveList);
		} catch (NullPointerException e) {
			System.out.println("Request parameter is null.");
		} catch (NumberFormatException e) {
			System.out
					.println("Can't parse an integer from request parameter.");
		}
		return "viewTraining";
	}
	
	@RequestMapping(value = "/trainingInfo", method = RequestMethod.GET)
	public String loadTraningInfo(HttpServletRequest request, ModelMap map) {
		String id = request.getParameter("training_id");
		try {
			int training_id = Integer.parseInt(id.trim());
			List<String> objectiveList = null;
			List<Form> formList = formService.getAllForms();
			Training t = trainingService.retrieveTraining(training_id);
			if (null != t.getObjectives()) {
				objectiveList = new ArrayList<String>();
				String raw = t.getObjectives().trim();
				if (raw.length() > 0) {
					String objective = "";
					for (int x = 0; x < raw.length(); x++) {
						if (raw.charAt(x) == '|') {
							objectiveList.add(objective);
							objective = "";
						} else {
							objective += raw.charAt(x);
						}
					}
				}
			}
			map.put("today", utilitiesService.getDateString(new Date()));
			map.put("training", t);
			map.put("objectives", objectiveList);
			map.put("forms", formList);
			
		} catch (NullPointerException e) {
			System.out.println("Request parameter is null.");
		} catch (NumberFormatException e) {
			System.out
					.println("Can't parse an integer from request parameter.");
		}
		return "trainingInfo";
	}

	@RequestMapping(value = "/get-details", method = RequestMethod.POST)
	public String loadTrainingDetails(HttpServletRequest request, ModelMap map) {
		String trID = request.getParameter("trID").toString();
		User participant = (User) request.getSession().getAttribute("user");
		System.out.println("Training ID: " + trID);
		Training t = null;
		TrainingParticipant tp = null;
		t = trainingService.retrieveTraining(Integer.parseInt(trID));
		tp = dbttsl.getParticipant(participant.getUserId(),
				Integer.parseInt(trID));

		if (t != null) {
			System.out.println("Kit-an");
			map.put("training", t);
			map.put("dateStart",
					utilitiesService.convertDateFormat(t.getDateStart()));
			map.put("dateEnd",
					utilitiesService.convertDateFormat(t.getDateEnd()));
			if (tp != null) {
				if (tp.getSkillsAssessfrm().equals("released")) {
					map.put("saReleased", "enabled");
				} else {
					map.put("saReleased", "disabled");

				}

				if (tp.getCourseFdbackfrm().equals("released")) {
					map.put("cfReleased", "enabled");
				} else {
					map.put("cfReleased", "disabled");
				}
				
				if(tp.getFaciFdbckfrm().equals("released")){
					System.out.println("fdReleased");
					map.put("fdRleased", "enabled");
				} else {
					System.out.println("!fdReleased");
					map.put("fdRleased", "disabled");
				}
			}
		} else {
			System.out.println("Wala kit-an");
		}

		return "trainings";
	}
	
	@RequestMapping(value = "/forms", method = RequestMethod.POST)
	public void goToForms(HttpServletRequest request, HttpServletResponse response, ModelMap map,
			@RequestParam("tId") String trainingId,
			@RequestParam("ftype") String formType,
			@RequestParam("job") String job){
		
		System.out.println(trainingId + ":" + formType);
		String jobTemp[] = job.split(":");
		if(formType.equals("saf")){
			try {
				request.getSession().setAttribute("f_tId", trainingId);
				request.getSession().setAttribute("job",jobTemp[0]);
				if(jobTemp[0].equals("supervisor")){
					request.getSession().setAttribute("jobUid",jobTemp[1]);
				}
				response.sendRedirect(request.getContextPath() + "/assessment");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if(formType.equals("tef")){
			try{
				request.getSession().setAttribute("f_tId", trainingId);
				request.getSession().setAttribute("tUID", jobTemp[1]);

				response.sendRedirect(request.getContextPath() + "/tee");
			} catch(Exception e){
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if(formType.equals("tna")){
			try{
				request.getSession().setAttribute("f_tId", trainingId);
				
				response.sendRedirect(request.getContextPath() + "/tna");
			} catch(Exception e){
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if(formType.equals("fcfd")){
			try{
				request.getSession().setAttribute("f_tId", trainingId);
				
				response.sendRedirect(request.getContextPath() + "/facifdbck");
			} catch(Exception e){
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if(formType.equals("cff")){
			try{
				request.getSession().setAttribute("f_tId", trainingId);
				
				response.sendRedirect(request.getContextPath() + "/cff");
			} catch(Exception e){
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@RequestMapping(value = "/addFacilitator", method = RequestMethod.POST)
	public @ResponseBody boolean addFacilitator(
			@RequestParam(value = "faciId") String requestId,
			@RequestParam(value = "trainingId") String trainingId) {
		boolean success = false;
		Training tempT = null;
		
		if (null != trainingId && null != requestId) {
			try {
				int training = Integer.parseInt(trainingId);
				int faciId = Integer.parseInt(requestId);
				if (facilitatorService.getTrainingFaciltators(training).size() == 0) {
					TrainingFacilitator tf = new TrainingFacilitator();
					tf.setUserId(faciId);
					tf.setTrainingId(training);
					tf.setTnaForm("released");
					tf.setFaciFdbfrm("none");
					success = facilitatorService.addTrainingFacilitator(tf);
					System.out.println("Success: " + success);
					if(success){
						if(dbttsl.releaseForm2(tf)){
							tempT = trainingService.retrieveTraining(training);
							tempT.setTnaFrm("released");
							System.out.println(trainingService.createUpdateTraining(tempT));
						}
					}
				}
			} catch (NumberFormatException e) {
				System.out.println("Unparsable uid or trainingId.");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return success;
	}

	@RequestMapping(value = "/deleteFacilitator", method = RequestMethod.POST)
	public @ResponseBody boolean deleteFacilitator(
			@RequestParam(value = "faciId") String requestFaciId,
			@RequestParam(value = "trainingId") String requestId, ModelMap map) {
		try {
			int trainingId = Integer.parseInt(requestId);
			int faciId = Integer.parseInt(requestFaciId);
			TrainingFacilitator tf = new TrainingFacilitator();
			tf.setUserId(faciId);
			tf.setTrainingId(trainingId);
			
			return facilitatorService.deleteTrainingFacilitator(tf);
		} catch (NullPointerException e) {
			System.out.println("Request paramenter is null.");
		} catch (NumberFormatException e) {
			System.out.println("Can't parse request id");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	@RequestMapping(value = "/addParticipant", method = RequestMethod.POST)
	public @ResponseBody int addParticipant(
			@RequestParam(value = "uids[]") List<String> uidList,
			@RequestParam(value = "trainingId") String trainingId) {
		int success = 0;
		if (null != trainingId && null != uidList) {
			for (String uid : uidList) {
				try {
					TrainingParticipant tp = new TrainingParticipant();
					tp.setUserId(Integer.parseInt(uid));
					tp.setTrainingId(Integer.parseInt(trainingId));
					tp.setCourseFdbackfrm("none");
					tp.setSkillsAssessfrm("released");
					tp.setTrainingEffectiveness_frm("none");
					tp.setFaciFdbckfrm("none");
					
					success = participantService.addTrainingParticipant(tp) ? ++success
							: success;
					if(success > 0){
						dbttsl.releaseForm(tp);
					}
				} catch (NumberFormatException e) {
					System.out.println("Unparsable uid or trainingId.");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} 
		return success;
	}

	@RequestMapping(value = "/getParticipantAttendance", method = RequestMethod.POST)
	public @ResponseBody List<TrainingAttendance> getParticipantAttendance(
			@RequestParam(value = "partiIds[]") List<String> idList,
			@RequestParam(value = "trainingId") String requestId) {
		List<TrainingAttendance> retVal = new ArrayList<TrainingAttendance>();
		
		try{
			int trainingId = Integer.parseInt(requestId);
			for(String s:idList){
				TrainingParticipant tp = dbttsl.getParticipant(Integer.parseInt(s), trainingId);
				retVal.addAll(attendanceService.retrieveAttendanceByParticipant(tp.getParticipantId()));
			}
		}catch(NullPointerException e){
			System.out.println("Request parameter is null.");
		}catch(NumberFormatException e){
			System.out.println("Can't parse request parameter.");
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return retVal;
	}

	@RequestMapping(value = "/deleteParticipant", method = RequestMethod.POST)
	public @ResponseBody int deleteParticipant(
			@RequestParam(value = "partiIds[]") List<String> idList,
			@RequestParam(value = "trainingId") String requestId, ModelMap map) {
		int success = 0;

		try {
			int trainingId = Integer.parseInt(requestId);
			for (String id : idList) {
				TrainingParticipant tp = new TrainingParticipant();
				tp.setUserId(Integer.parseInt(id));
				tp.setTrainingId(trainingId);
				success = participantService.deleteTrainingParticipant(tp) ? ++success
						: success;
			}
		} catch (NullPointerException e) {
			System.out.println("Request paramenter is null.");
		} catch (NumberFormatException e) {
			System.out.println("Can't parse request id");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return success;
	}

	@RequestMapping(value = "/assignForm", method = RequestMethod.POST)
	public @ResponseBody String assignForm(ModelMap map,
			@RequestParam(value = "uids[]") List<String> uidList,
			@RequestParam(value = "trainingId") String trainingId,
			@RequestParam(value = "courseFdbackfrm") String cfForm,
			@RequestParam(value = "skillsAssessfrm") String skForm,
			@RequestParam(value = "trainingEffectiveness_frm") String teForm,
			@RequestParam(value = "faciFdbckfrm") String facifdForm) {
		try {

			if (null != uidList && null != trainingId) {
				for (String uid : uidList) {
					try {
						TrainingParticipant tp = new TrainingParticipant();
						
						tp.setCourseFdbackfrm(cfForm);
						tp.setSkillsAssessfrm(skForm);
						tp.setTrainingEffectiveness_frm(teForm);
						tp.setFaciFdbckfrm(facifdForm);
						
						tp.setUserId(Integer.parseInt(uid));
						tp.setTrainingId(Integer.parseInt(trainingId));
						
						dbttsl.updateTrainingParticipant(tp);
					} catch (Exception ex) {
						System.out.println(ex.getMessage());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "trainings";
	}
	
	@RequestMapping(value = "/assignForm2", method = RequestMethod.POST)
	public @ResponseBody String assignForm2(ModelMap map,
			@RequestParam(value = "uids[]") List<String> uidList,
			@RequestParam(value = "trainingId") String trainingId,
			@RequestParam(value = "faciFdbfrm") String faciFrm,
			@RequestParam(value = "tnaForm") String tnaFrm) {
		try {

			if (null != uidList && null != trainingId) {
				for (String uid : uidList) {
					try {
						TrainingFacilitator tf = new TrainingFacilitator();
						tf.setFaciFdbfrm(faciFrm);
						tf.setTnaForm(tnaFrm);
						tf.setUserId(Integer.parseInt(uid));
						tf.setTrainingId(Integer.parseInt(trainingId));

						dbttsl.updateTrainingFacilitator(tf);
					} catch (Exception ex) {
						System.out.println(ex.getMessage());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "trainings";
	}

	@RequestMapping(value = "/releaseForm", method = RequestMethod.POST)
	public @ResponseBody String generateForm(ModelMap map,
			TrainingParticipant tp) {
		try {
			dbttsl.releaseForm(tp);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "trainings";
	}
	
	@RequestMapping(value = "/releaseForm2", method = RequestMethod.POST)
	public @ResponseBody String generateForm(ModelMap map,
			TrainingFacilitator tf) {
		try {
			dbttsl.releaseForm2(tf);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "trainings";
	}

	@RequestMapping(value = "/populateAttendance", method = RequestMethod.POST)
	public @ResponseBody String populateAttendance(HttpServletRequest request,
			ModelMap map) {
		String requestId = request.getParameter("trainingId");
		try {
			int trainingId = Integer.parseInt(requestId.trim());
			List<TrainingParticipant> tpl = participantService
					.getTrainingParticipants(trainingId);
			Training t = trainingService.retrieveTraining(trainingId);
			final DateTimeFormatter formatter = DateTimeFormatter
					.ofPattern("yyyy-MM-dd");
			for (TrainingParticipant tp : tpl) {
				LocalDate start = LocalDate.parse(t.getDateStart(), formatter);
				LocalDate end = LocalDate.parse(t.getDateEnd(), formatter);
				for (; true != start.isAfter(end); start = start.plusDays(1)) {
					int id = ((start.getYear() - 2000) * 100000000)
							+ ((start.getMonthValue()) * 1000000)
							+ (start.getDayOfMonth() * 10000)
							+ (tp.getParticipantId() % 10000);
					TrainingAttendance ta = attendanceService
							.retrieveAttendanceById(id);
					if (null == ta) {
						ta = new TrainingAttendance();
						ta.setAttendanceId(id);
						ta.setAttendanceDate(start.toString());
						ta.setParticipantId(tp.getParticipantId());
						ta.setTrainingId(tp.getTrainingId());
					}
					attendanceService.insertAttendance(ta);
				}
			}
		} catch (NullPointerException e) {
			System.out.println("Request Parameter is null");
		} catch (NumberFormatException e) {
			System.out.println("Can't parse request parameter.");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@RequestMapping(value = "/getAttendance", method = RequestMethod.GET)
	public String getAttendance(HttpServletRequest request, ModelMap map) {
		String requestId = request.getParameter("trainingId");
		try {
			int trainingId = Integer.parseInt(requestId);

			map.put("attendanceList",
					attendanceService.getAttendance(trainingId));
			map.put("today", utilitiesService.getDateString(new Date()));
		} catch (NullPointerException e) {
			System.out.println("Request parameter is null.");
		} catch (NumberFormatException e) {
			System.out.println("Can't parse request parameter.");
		}

		return "attendance";
	}

	@RequestMapping(value = "/updateAttendance", method = RequestMethod.POST)
	public @ResponseBody boolean updateAttendance(TrainingAttendance ta) {
		try {
			attendanceService.updateAttendance(ta);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}
	
	@RequestMapping(value="/deleteTraining", method = RequestMethod.POST)
	public @ResponseBody boolean deleteTraining(@RequestParam(value="trainingId") String requestId){
		boolean retVal = false;
		
		try{
			retVal = trainingService.deleteTraining(Integer.parseInt(requestId));
		}catch(NullPointerException e){
			System.out.println("Request parameter is null.");
		}catch(NumberFormatException e){
			System.out.println("Can't parse request parameter");
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return retVal;
	}
	
	@RequestMapping(value="editForms", method = RequestMethod.POST)
	public @ResponseBody void editForms(HttpServletResponse response, HttpServletRequest request,
			@RequestParam(value="form_id") String fId){
		int id = Integer.parseInt(fId);
		System.out.println(fId);
		System.out.println(id);
		try{
			if(id == 1){
				
			} else if(id == 2){
				System.out.println("sudtnaedit");System.out.println(request.getContextPath() + "/tna/edit");
				response.sendRedirect(request.getContextPath() + "/tna/edit");
				
			} else if(id == 3){
				
			} else if(id == 4){
				response.sendRedirect(request.getContextPath() + "/tee/edit");
			} else if(id == 5){
				
			}
		} catch(Exception e){
			System.out.println("E: " + e.getMessage());
		}
	}
}
