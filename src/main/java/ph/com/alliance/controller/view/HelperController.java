package ph.com.alliance.controller.view;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ph.com.alliance.entity.Question;
import ph.com.alliance.entity.QuestionChoice;
import ph.com.alliance.service.QuestionChoiceService;
import ph.com.alliance.service.QuestionService;

@Controller
@RequestMapping("/help")
public class HelperController {
	@Autowired
	private QuestionService qService;
	
	@Autowired
	private QuestionChoiceService qcService;
	
	@RequestMapping(method = RequestMethod.GET)
	public void PropagateData() {
		List<Question> questionList = qService.getQuestionsByForm(1);
		List<Question> questionList2 = qService.getQuestionsByForm(2);
		List<Question> questionList3 = qService.getQuestionsByForm(3);
		List<Question> questionList4 = qService.getQuestionsByForm(4);
		//QuestionChoice qc = new QuestionChoice();
//		for(int i=0;i<questionList.size();i++){
//			if(questionList.get(i).getAssessType().equals("skills")){
//				qcService.setQuestionChoice(questionList.get(i).getIdQuestions(), 1);
//				qcService.setQuestionChoice(questionList.get(i).getIdQuestions(), 2);
//				qcService.setQuestionChoice(questionList.get(i).getIdQuestions(), 3);
//				qcService.setQuestionChoice(questionList.get(i).getIdQuestions(), 4);
//			}
//			else if(questionList.get(i).getAssessType().equals("years")){
//				qcService.setQuestionChoice(questionList.get(i).getIdQuestions(), 5);
//				qcService.setQuestionChoice(questionList.get(i).getIdQuestions(), 6);
//				qcService.setQuestionChoice(questionList.get(i).getIdQuestions(), 7);
//				qcService.setQuestionChoice(questionList.get(i).getIdQuestions(), 8);
//				qcService.setQuestionChoice(questionList.get(i).getIdQuestions(), 9);
//				qcService.setQuestionChoice(questionList.get(i).getIdQuestions(), 10);
//				qcService.setQuestionChoice(questionList.get(i).getIdQuestions(), 11);
//			}
//		}
		System.out.println(questionList4.size());
		for(int ctr=0;ctr<questionList4.size();ctr++){
			if(questionList4.get(ctr).getAssessType().equals("multiple")){
				qcService.setQuestionChoice(questionList4.get(ctr).getIdQuestions(), 14);
				qcService.setQuestionChoice(questionList4.get(ctr).getIdQuestions(), 15);
				qcService.setQuestionChoice(questionList4.get(ctr).getIdQuestions(), 16);
				qcService.setQuestionChoice(questionList4.get(ctr).getIdQuestions(), 17);
				qcService.setQuestionChoice(questionList4.get(ctr).getIdQuestions(), 18);
			}
			else if(questionList4.get(ctr).getAssessType().equals("multiple2")){
				qcService.setQuestionChoice(questionList4.get(ctr).getIdQuestions(), 19);
				qcService.setQuestionChoice(questionList4.get(ctr).getIdQuestions(), 20);
				qcService.setQuestionChoice(questionList4.get(ctr).getIdQuestions(), 21);
				qcService.setQuestionChoice(questionList4.get(ctr).getIdQuestions(), 22);
				qcService.setQuestionChoice(questionList4.get(ctr).getIdQuestions(), 23);
			}
		}
		
	}
}
