package ph.com.alliance.controller.view;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ph.com.alliance.entity.Choice;
import ph.com.alliance.entity.Question;
import ph.com.alliance.entity.QuestionChoice;
import ph.com.alliance.entity.User;
import ph.com.alliance.model.Answer;
import ph.com.alliance.service.ChoiceService;
import ph.com.alliance.service.QuestionChoiceService;
import ph.com.alliance.service.QuestionService;

@Controller
@RequestMapping("/cff")
public class CourseFeedbackFormController {

	@Autowired
	private QuestionService qService;

	@Autowired
	private QuestionChoiceService qcService;

	@Autowired
	private ChoiceService cService;

	@RequestMapping(method = RequestMethod.GET)
	public String loadTrainingsPageEmp(HttpServletRequest request, ModelMap map) {
		List<Question> myqList = null;
		List<QuestionChoice> myqcList = null;
		List<Choice> mycList = null;
		myqList = qService.getQuestionsByForm(3);
		myqcList = qcService.getQuestionChoices();
		mycList = cService.getChoices();
		map.put("questions", myqList);
		map.put("questionsChoice", myqcList);
		map.put("choice", mycList);
		return "cffemp";
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String loadTrainingsPage(HttpServletRequest request, ModelMap map) {
		List<Question> myqList = null;
		List<QuestionChoice> myqcList = null;
		List<Choice> mycList = null;
		myqList = qService.getQuestionsByForm(3);
		myqcList = qcService.getQuestionChoices();
		mycList = cService.getChoices();
		map.put("questions", myqList);
		map.put("questionsChoice", myqcList);
		map.put("choice", mycList);
		return "cff";
	}

	@RequestMapping(value="/addquestion",method = RequestMethod.POST)
	public String appendQuestion(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		List<Question> questionList3 = qService.getQuestionsByForm(3);
		String select = request.getParameter("answername");
		String m="",c="",q1="";
		String temp[] = null;
		int ctr=0;
		q1 = request.getParameter("questionname");
		boolean flag=false;
		Question q = new Question();
		q.setFkId(3);
		q.setFkFscid(0);
		q.setQuestion(q1);
		if(select.equals("multiple")){
			m = request.getParameter("mchoices");
			q.setAssessType("multiple");
			temp = m.split(",");
		}
		else if(select.equals("checkbox")){
			c = request.getParameter("cchoices");
			q.setAssessType("checkbox");
			temp = c.split(",");
		}
		else if(select.equals("essay")){
			q.setAssessType("essay");
		}
		flag=qService.addQuestion(q);
		if(flag){
			ctr = qService.getQuestionsByName(q1);
		}
		if(temp!=null){
			for(int i=0;i<temp.length;i++){
				cService.addChoice(temp[i]);
				qcService.setQuestionChoice(ctr, cService.getChoiceByName(temp[i]));
			}
		}
		return this.loadTrainingsPage(request, map);
	}
	
	@RequestMapping(value="/removequestion",method = RequestMethod.POST)
	public String removeQuestion(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		String[] qIdList = request.getParameterValues("qID");
		int id=0;
		if(qIdList != null){
			for(String idQ:qIdList){
				id=Integer.parseInt(idQ);
				qService.deleteQuestionById(id);
				qcService.deleteQuestionChoicesByQId(id);
			}
		}
		return this.loadTrainingsPage(request, map);
	}
	
	@RequestMapping(value="/submit", method = RequestMethod.POST)
	public void submitCFF(@RequestBody List<Answer> tanswer, HttpServletRequest request, HttpServletResponse response, ModelMap map){
		User u = null;
		u = (User) request.getSession().getAttribute("user");
		String tId = request.getSession().getAttribute("f_tId").toString();
		int uId = u.getUserId();
		System.out.println(tanswer);
		boolean flag = qService.addAnswers(tanswer, "employee", uId, Integer.parseInt(tId));
		System.out.println("Flag: " + flag);
		try {
			response.sendRedirect(request.getContextPath() + "/home");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
