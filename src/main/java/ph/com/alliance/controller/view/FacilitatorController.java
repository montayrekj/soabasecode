package ph.com.alliance.controller.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ph.com.alliance.entity.Training;
import ph.com.alliance.entity.TrainingFacilitator;
import ph.com.alliance.entity.User;
import ph.com.alliance.service.TrainingAttendanceService;
import ph.com.alliance.service.TrainingService;
import ph.com.alliance.service.UtilitiesService;
import ph.com.alliance.service.impl.DBTransactionTestServiceImpl;


@Controller
@RequestMapping("/facilitator")
public class FacilitatorController {

	@Autowired
	private TrainingService trainingService;

	@Autowired
	private UtilitiesService utilitiesService;
	
	@Autowired
	private DBTransactionTestServiceImpl dbttsl;
	
	@Autowired
	private TrainingAttendanceService attendanceService;

	@RequestMapping(method = RequestMethod.GET)
	public String loadFacilitatorPage(HttpServletRequest request, ModelMap map) {
		List<Training> facilTrainingList = null;
		User facilitator = (User) request.getSession().getAttribute("user");

		facilTrainingList = trainingService
				.retrieveTrainingsByFacilitator(facilitator.getUserId());
		map.put("trainingList", facilTrainingList);
		map.put("today", utilitiesService.getDateString(new Date()));
		map.put("outline", "disabled");
		map.put("tnaReleased", "disabled");
		return "facilitator";
	}

	@RequestMapping(value = "/get-details", method = RequestMethod.POST)
	public String loadTrainingDetails(HttpServletRequest request, ModelMap map) {
		String trID = request.getParameter("trID").toString();
		User facilitator = (User) request.getSession().getAttribute("user");
		System.out.println("Training ID: " + trID);
		TrainingFacilitator tf = null;
		Training t = null;
		t = trainingService.retrieveTraining(Integer.parseInt(trID));
		tf = dbttsl.getFacilitator(facilitator.getUserId(), Integer.parseInt(trID));
		List<String> objectiveList = null;

		if (t != null) {
			System.out.println("Kit-an");
			map.put("training", t);
			map.put("dateStart", utilitiesService.convertDateFormat(t.getDateStart()));
			map.put("dateEnd", utilitiesService.convertDateFormat(t.getDateEnd()));
			
			if (null != t.getObjectives()) {
				System.out.println("sudHere");
				objectiveList = new ArrayList<String>();
				String raw = t.getObjectives().trim();
				if (raw.length() > 0) {
					String objective = "";
					for (int x = 0; x < raw.length(); x++) {
						if (raw.charAt(x) == '|') {
							objectiveList.add(objective);
							objective = "";
						} else {
							objective += raw.charAt(x);
						}
					}
				}
			}
			for(String str:objectiveList){
				System.out.println(str);
			}
			map.put("objectives", objectiveList);
			if (tf != null) {
				map.put("outline", "enabled");

				if (tf.getTnaForm().equals("released")) {
					map.put("tnaReleased", "enabled");
				} else {
					map.put("tnaReleased", "disabled");
				}
			}
			
		} else {
			System.out.println("Wala kit-an");
		}
		
		return "facilitator";
	}
	
	@RequestMapping(value = "/updatetraining", method = RequestMethod.POST)
	public @ResponseBody Training updateTraining(HttpServletRequest request, ModelMap map,
			Training t) {
		List<String> objectiveList = null;
		Training updated = null;
		
		try {
			System.out.println(t.getTrainingId());
			Training temp = trainingService.retrieveTraining(t.getTrainingId());
			System.out.println(temp);
			System.out.println(t.getObjectives());
			System.out.println(t.getOutline());
			if(null == t.getObjectives()){
				t.setObjectives(temp.getObjectives());
			}
			if(null == t.getOutline()){
				t.setOutline(temp.getOutline());
			}
			t.setAnnualPlan(temp.getAnnualPlan());
			t.setDateEnd(temp.getDateEnd());
			t.setDateStart(temp.getDateStart());
			t.setTrainingTitle(temp.getTrainingTitle());
		} catch (NullPointerException e) {
			System.out.println("Null request parameter.");
		} catch (NumberFormatException e) {
			System.out.println("Can't parse request parameter.");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			trainingService.createUpdateTraining(t);
			attendanceService.deleteOnTrainingUpdate(trainingService.retrieveTraining(t.getTrainingId()));
			
		}
		
		try{
			updated = trainingService.retrieveTraining(t.getTrainingId());
			map.put("training",updated);
			if (null != updated.getObjectives()) {
				System.out.println("sudHere");
				objectiveList = new ArrayList<String>();
				String raw = updated.getObjectives().trim();
				if (raw.length() > 0) {
					String objective = "";
					for (int x = 0; x < raw.length(); x++) {
						if (raw.charAt(x) == '|') {
							objectiveList.add(objective);
							objective = "";
						} else {
							objective += raw.charAt(x);
						}
					}
				}
			}
			map.put("objectives",objectiveList);
		} catch(Exception e){
			
		}

		return updated;
	}
}
