package ph.com.alliance.controller.view;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ph.com.alliance.entity.User;
import ph.com.alliance.service.impl.DBTransactionTestServiceImpl;

@Controller
@RequestMapping("/login")
public class LoginController {

	@Autowired
	private DBTransactionTestServiceImpl dbttsl;
	
	@RequestMapping(method=RequestMethod.GET)
	public String login(ModelMap map, HttpServletRequest request){
		return "login";
	}
	
	@RequestMapping(value="/submit", method=RequestMethod.POST)
	public String loginSubmit(HttpServletRequest request, ModelMap map){
		String id = request.getParameter("login_id");
		String password = request.getParameter("login_password");
		System.out.println("ID: " + id + "; Password: "+ password);
		
		boolean success = false;
		//Pass values to service;
		User user = null; 
		
		if(dbttsl.loginUser(id, password)){
			System.out.println("naay user");
			user = dbttsl.findUser(id, password);
			if(user != null){
				success = true;
				System.out.println("User: " + user);
			}
		} else {
			System.out.println("way user");
			return "user_!found";
		}
		
		if(success){
			System.out.println("sud home");
			request.getSession().setAttribute("role", user.getUserType());
			request.getSession().setAttribute("isLoggedIn", true);
			request.getSession().setAttribute("user", user);
			
			return "home";
		}
		else 
			return "danger_alert";
	}
	
	
	
	
}
