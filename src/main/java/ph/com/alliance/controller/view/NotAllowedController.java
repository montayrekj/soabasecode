package ph.com.alliance.controller.view;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/notallowed")
public class NotAllowedController {

	@RequestMapping(method=RequestMethod.GET)
	public String loadNotAllowed(ModelMap map){
		return "notallowed";
	}
}
