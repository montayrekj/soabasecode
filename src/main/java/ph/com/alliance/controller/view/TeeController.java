package ph.com.alliance.controller.view;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ph.com.alliance.entity.Question;
import ph.com.alliance.entity.User;
import ph.com.alliance.model.Answer;
import ph.com.alliance.service.QuestionService;
import ph.com.alliance.service.TeeService;



@Controller
@RequestMapping("/tee")
public class TeeController {
	
	@Autowired
	private TeeService teeservice;
	
	@Autowired
	private QuestionService questionService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String loadTrainingsPageEmp(HttpServletRequest request, ModelMap map) {
		List<Question> myqList=null;
		myqList = questionService.getQuestionsByForm(4);
		
		map.put("questions",myqList);
		return "teeemp";
	}
	
	@RequestMapping(value="/edit",method = RequestMethod.GET)
	public String loadTrainingsPage(HttpServletRequest request, ModelMap map) {
		List<Question> myqList=null;
		myqList = questionService.getQuestionsByForm(4);
		
		map.put("questions",myqList);
		return "trainingEffectiveness";
	}
	
	@RequestMapping(value="/addquestion",method = RequestMethod.POST)
	public String appendQuestion(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		String temp = request.getParameter("questionname");
		String assess = request.getParameter("answername");
		boolean flag=false;
		Question q = new Question();
		q.setAssessType(assess);
		q.setFkId(4);
		q.setFkFscid(0);
		q.setQuestion(temp);
		flag=questionService.addQuestion(q);
		System.out.println(q);
		System.out.println(flag);
		return this.loadTrainingsPage(request, map);
	}
	@RequestMapping(value="/removequestion",method = RequestMethod.POST)
	public String removeQuestion(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		String[] qIdList = request.getParameterValues("qID");
		if(qIdList != null){
			for(String idQ:qIdList){
				questionService.deleteQuestionById(Integer.parseInt(idQ));
			}
		}
		return this.loadTrainingsPage(request, map);
	}
	@RequestMapping(value="/submit", method = RequestMethod.POST)
	@ResponseBody
	public void submitTEE(@RequestBody List<Answer> tanswer, HttpServletRequest request, HttpServletResponse response, ModelMap map){
		//User u = null;
		//u = (User) request.getSession().getAttribute("user");
		String tId = request.getSession().getAttribute("f_tId").toString();
		String uId = request.getSession().getAttribute("tUID").toString();
		 
		System.out.println(tanswer);
		boolean flag = questionService.addAnswers(tanswer, "employee", Integer.parseInt(uId), Integer.parseInt(tId));
		System.out.println("Flag: " + flag);
		try {
			response.sendRedirect(request.getContextPath() + "/home");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
