package ph.com.alliance.controller.view;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ph.com.alliance.entity.Question;
import ph.com.alliance.entity.User;
import ph.com.alliance.model.Answer;
import ph.com.alliance.service.QuestionService;

@Controller
@RequestMapping("/facifdbck")
public class FaciFeedbackController {

	@Autowired
	QuestionService questionService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String viewFacilitatorFeedbackForm(HttpServletRequest request, ModelMap map){
		List<Question> myqList=null;
		myqList = questionService.getQuestionsByForm(5);
		
		map.put("questions",myqList);
		return "faciFrm";
	}
	
	@RequestMapping(value="submit", method = RequestMethod.POST)
	public void submitForm(HttpServletRequest request, HttpServletResponse response, @RequestBody List<Answer> tanswer){
		User u = null;
		u = (User) request.getSession().getAttribute("user");
		String tId = request.getSession().getAttribute("f_tId").toString();
		int uId = u.getUserId();
		System.out.println(tanswer);
		boolean flag = questionService.addAnswers(tanswer, "employee", uId, Integer.parseInt(tId));
		System.out.println("Flag: " + flag);
		try {
			response.sendRedirect(request.getContextPath() + "/home");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
