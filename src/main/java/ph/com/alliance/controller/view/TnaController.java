package ph.com.alliance.controller.view;


import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ph.com.alliance.entity.Question;
import ph.com.alliance.entity.User;
import ph.com.alliance.model.Answer;
import ph.com.alliance.service.QuestionService;


@Controller
@RequestMapping("/tna")
public class TnaController {
	
	@Autowired
	private QuestionService questionService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String loadTrainingsPage(HttpServletRequest request, ModelMap map) {
		List<Question> questionList = questionService.getQuestionsByForm(2);
		int ctr=0;
		User u = null;
		u = (User) request.getSession().getAttribute("user");
		String utype = u.getUserType();
		String temp;
		if(utype.equals("admin")){
			temp = "Supervisor";
		} else {
			temp = "Peer";
		}
		map.put("userType", temp);
		map.put("questions",questionList);
		map.put("counter", ctr);
		return "tnaemp";
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String editTna(HttpServletRequest request, ModelMap map) {
		List<Question> questionList = questionService.getQuestionsByForm(2);
		int ctr=0;
		User u = null;
		u = (User) request.getSession().getAttribute("user");
		String utype = u.getUserType();
		String temp;
		if(utype.equals("admin")){
			temp = "Supervisor";
		} else {
			temp = "Peer";
		}
		map.put("userType", temp);
		map.put("questions",questionList);
		map.put("counter", ctr);
		return "tna";
	}
	
	@RequestMapping(value="/appendquestion",method = RequestMethod.POST)
	public String appendQuestion(HttpServletRequest request, HttpServletResponse response, ModelMap map){
		String temp = request.getParameter("div1");
		boolean flag=false;
		Question q = new Question();
		q.setAssessType("essay");
		q.setFkId(2);
		q.setFkFscid(0);
		q.setQuestion(temp);
		flag=questionService.addQuestion(q);
		System.out.println(q);
		System.out.println(flag);
		return this.loadTrainingsPage(request, map);
	}
	
	@RequestMapping(value="/removequestion",method = RequestMethod.POST)
	public @ResponseBody void removeQuestion(HttpServletRequest request, HttpServletResponse response, ModelMap map,
			@RequestParam(value = "data12") String id){
		System.out.println("ID: " + id);
		boolean flag=false;
		flag = questionService.deleteQuestionById(Integer.parseInt(id));
		System.out.println(flag);
		try {
			response.sendRedirect(request.getContextPath() + "/tna");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/submit", method = RequestMethod.POST)
	public void submitTna(@RequestBody List<Answer> tanswer, HttpServletRequest request, HttpServletResponse response, ModelMap map){
		User u = null;
		u = (User) request.getSession().getAttribute("user");
		String tId = request.getSession().getAttribute("f_tId").toString();
		int uId = u.getUserId();
		System.out.println(tanswer);
		boolean flag = questionService.addAnswers(tanswer, "employee", uId, Integer.parseInt(tId));
		System.out.println("Flag: " + flag);
		try {
			response.sendRedirect(request.getContextPath() + "/home");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
