package ph.com.alliance.controller.view;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ph.com.alliance.entity.Form;
import ph.com.alliance.entity.FormSc;
import ph.com.alliance.entity.Question;
import ph.com.alliance.entity.User;
import ph.com.alliance.model.Answer;
import ph.com.alliance.service.FormSCService;
import ph.com.alliance.service.QuestionService;

@Controller
@RequestMapping("/assessment")
public class AssessmentFormController {
	@Autowired
	private FormSCService formSCService;
	
	@Autowired
	private QuestionService questionService;
	
	public int ctr = 0;
	@RequestMapping(method = RequestMethod.GET)
	public String viewAssessment(HttpServletRequest request,ModelMap map){
		
		String flag="assessmentFormSelf";
		String peer="assessmentFormPeer";
		String supervisor="assessmentFormSupervisor";
		
		User u = null;
		u = (User) request.getSession().getAttribute("user");
		List<Form> myCategoryList = formSCService.getCategoryList();
		List<FormSc> mySubCategoryList = formSCService.getSubCategoryList();
		List<Question> questionList = questionService.getQuestions();
		
		String temp;
		/*if(utype.equals("admin")){
			temp = "Supervisor";
		} else if(utype.equals("employee")){
			temp = "Self";
		} else {
			temp = "Supervisor";
		}*/
		if(request.getSession().getAttribute("job").equals("supervisor")){
			temp = "Supervisor";
		} else {
			temp = "Self";
		}
		map.put("userType", temp);
		map.put("assessmentFormC", myCategoryList);
		map.put("assessmentFormSC", mySubCategoryList);
		map.put("questions",questionList);
		
		if(u.getUserType().equals("admin")){
			flag=supervisor;
		}
		
		return "assessmentForm";
	}
	
	@RequestMapping(value="/submit",method = RequestMethod.POST)
	@ResponseBody
	public void submitAssessment(@RequestBody List<Answer> answer, HttpServletRequest request, HttpServletResponse response, ModelMap map){
		User u = null;
		u = (User) request.getSession().getAttribute("user");
		String tId = request.getSession().getAttribute("f_tId").toString();
		String temp = request.getSession().getAttribute("job").toString();
		int uId;
		if(temp.equals("supervisor")){
			String uIdtemp = request.getSession().getAttribute("jobUid").toString();
			
			uId = Integer.parseInt(uIdtemp);
		} else {
			uId = u.getUserId();
		}
		System.out.println(answer);
		System.out.println(u.getUserType());
		
		boolean flag = questionService.addAnswers(answer, temp, uId, Integer.parseInt(tId));
	
		try {
			response.sendRedirect(request.getContextPath() + "/home");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
