package ph.com.alliance.dao;

import java.util.List;

import javax.persistence.EntityManager;

import ph.com.alliance.entity.Question;

public interface QuestionsDao {
	
	public boolean setAnswer(EntityManager em, Question q);
	
	public Question getQuestion(EntityManager em, int id);
	
	public boolean addQuestion(EntityManager em, Question q);
	
	public boolean deleteQuestionById(EntityManager em, int id);
	
	public List<Question> getQuestionsByForm(EntityManager em, int id);
	
	public List<Question> getQuestions(EntityManager em);
	
	public int getQuestionsByName(EntityManager em , String name);
}
