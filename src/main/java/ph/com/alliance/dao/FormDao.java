package ph.com.alliance.dao;

import java.util.List;

import javax.persistence.EntityManager;

import ph.com.alliance.entity.Form;

public interface FormDao {
	public List<Form> getAllForms(EntityManager em);
	
	public Form getFormById(EntityManager em, int id);
}
