package ph.com.alliance.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import ph.com.alliance.entity.Employee;

@Repository("employeeDao")
public interface EmployeeDao {

	public List<Employee> selectEmployeesByLastName(EntityManager em, String lastName);
	
}
