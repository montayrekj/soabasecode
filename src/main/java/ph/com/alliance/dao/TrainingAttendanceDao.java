package ph.com.alliance.dao;

import java.util.List;

import javax.persistence.EntityManager;

import ph.com.alliance.entity.Training;
import ph.com.alliance.entity.TrainingAttendance;

public interface TrainingAttendanceDao {
	
	/*
	 * @param pEM
	 * @param ta
	 */
	public void insertUpdateParticipantAttendance(EntityManager pEM, TrainingAttendance ta);
	
	/*
	 * @param pEM
	 * @param trainingId
	 */
	public List<TrainingAttendance> getAttendanceByTraining(EntityManager pEM, int trainingId);

	/*
	 * @param pEM
	 * @param ta
	 */
	public void updateAttendance(EntityManager pEM, TrainingAttendance ta);
	
	/*
	 * @param pEM
	 * @param ta
	 * 
	 */
	public int deleteOnTrainingUpdate(EntityManager pEM, Training t);

	/*
	 *@param pEM
	 *@param attendanceId 
	 */
	public TrainingAttendance retrieveAttendanceById(EntityManager pEM, int attendanceId);

	/*
	 * @param pEM
	 * @param partiId
	 */
	public List<TrainingAttendance> retrieveAttendanceByParticipant(EntityManager pEM, int partiId);
}
