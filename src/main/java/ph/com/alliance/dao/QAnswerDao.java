package ph.com.alliance.dao;

import java.util.List;

import javax.persistence.EntityManager;

import ph.com.alliance.entity.Qanswer;
import ph.com.alliance.entity.Question;
import ph.com.alliance.entity.TrainingFacilitator;
import ph.com.alliance.entity.TrainingParticipant;

public interface QAnswerDao {
	public Qanswer getQanswer(EntityManager em, int qId, int uId, int tId);
	
	public boolean setAnswer(EntityManager em, Qanswer qa);
	
	public boolean	propagateQuestions(EntityManager em, List<Question> questionList, List<TrainingParticipant> tpList);
	
	public boolean	propagateQuestionsByFacilitator(EntityManager em, List<Question> questionList, List<TrainingFacilitator> tfList);
	
	public List<Object> getEffectivenessFeedback(EntityManager pEM, int trainingId);
	
	public List<Object> getEffectivenessRating(EntityManager pEM, int trainingId);
	
	public Object getEffectivenessFeedbackByQuestion(EntityManager pEM, int trainingId, int qid);
	
	public Object getEffectivenessRatingByQuestion(EntityManager pEM, int trainingId, int qid);
	
	public List<Qanswer> getAllEffectivessAnswer(EntityManager pEM, int trainingId);
	
	public List<Object[]> getPreGenericAssesment(EntityManager pEM, int partiId);
	
	public List<Object[]> getPreTechnicalAssesment(EntityManager pEM, int partiId);
	
	public List<Object[]> getPreSEAssesment(EntityManager pEM, int partiId);
	
	public List<Object[]> getPostGenericAssesment(EntityManager pEM, int partiId);
	
	public List<Object[]> getPostTechnicalAssesment(EntityManager pEM, int partiId);
	
	public List<Object[]> getPostSEAssesment(EntityManager pEM, int partiId);
}
