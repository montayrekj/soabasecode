package ph.com.alliance.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import ph.com.alliance.dao.ChoiceDao;
import ph.com.alliance.entity.Choice;
import ph.com.alliance.entity.QuestionChoice;
@Repository("choiceDao")
public class ChoiceDaoImpl implements ChoiceDao{

	@Override
	public void addChoice(EntityManager em, String name) {
		Choice c = new Choice();
		c.setName(name);
		try{
			em.persist(c);
		} catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	@Override
	public int getChoiceByName(EntityManager em, String name) {
		int id=0;
		try {
			Choice c = null;
			Query cQuery = em
					.createQuery("FROM Choice c WHERE name = :name");
			cQuery.setParameter("name", name);
			c = (Choice) cQuery.getSingleResult();
			id = c.getId();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return id;
	}

	@Override
	public List<Choice> getChoices(EntityManager em) {
		List<Choice> c = null;
		Query cQuery = em
				.createQuery("FROM Choice c");
		c = cQuery.getResultList();
		return c;
	}

	@Override
	public void deleteChoicesByQId(EntityManager em, int qid) {
		try {
			List<QuestionChoice> qc=null;
			int cid=0;
			Query qcQuery = em
					.createQuery("FROM QuestionChoice qc WHERE qc.qId = :id");
			qcQuery.setParameter("id", qid);
			qc = qcQuery.getResultList();
			System.out.println(qc);
			for(int i=0;i<qc.size();i++){
				cid=qc.get(i).getChoiceId();
				System.out.println(cid);
				Query cQuery = em
						.createQuery("DELETE FROM Choice c WHERE c.id = :id");
				cQuery.setParameter("id", cid);
				cQuery.executeUpdate();
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
