package ph.com.alliance.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import ph.com.alliance.dao.TrainingFacilitatorDao;
import ph.com.alliance.entity.TrainingFacilitator;
import ph.com.alliance.entity.TrainingParticipant;

@Repository("trainingFacilitatorDao")
public class TrainingFacilitatorDaoImpl implements TrainingFacilitatorDao {

	@Override
	public List<TrainingFacilitator> getAllFacilitatorPerTraining(EntityManager pEM, int trainingId) {

		try {
			Query q = pEM.createQuery("FROM TrainingFacilitator tf WHERE tf.trainingId = :trainingId");
			q.setParameter("trainingId", trainingId);
			return q.getResultList();
		} catch (Exception e){
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public void addTrainingFacilitator(EntityManager pEM, TrainingFacilitator tf) {
		try {
			pEM.persist(tf);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteTrainingFacilitator(EntityManager pEM, TrainingFacilitator tf) {
		try {
			Query qFind = pEM.createQuery("FROM TrainingFacilitator tf WHERE tf.userId = :userId AND tf.trainingId = :trainingId");
			qFind.setParameter("userId", tf.getUserId());
			qFind.setParameter("trainingId", tf.getTrainingId());

			TrainingFacilitator result = (TrainingFacilitator) qFind.getSingleResult();

			Query qDel = pEM.createQuery("DELETE FROM TrainingFacilitator tf WHERE tf.facilitatorId = :tfId");
			qDel.setParameter("tfId", result.getFacilitatorId());
			qDel.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean updateTrainingFacilitator(EntityManager pEM,
			TrainingFacilitator tf) {
		// TODO Auto-generated method stub
		try {
			Query query = pEM
					.createQuery("FROM TrainingFacilitator tf WHERE userId = :uId AND trainingId = :tId");
			query.setParameter("uId", tf.getUserId());
			query.setParameter("tId", tf.getTrainingId());
			System.out.println(tf);
			TrainingFacilitator tf2 = (TrainingFacilitator) query
					.getSingleResult();
			System.out.println("TF2: " + tf2);
			tf.setFacilitatorId(tf2.getFacilitatorId());
			System.out.println("temp");
			if (!tf2.getFaciFdbfrm().equals("none")) {
				tf.setFaciFdbfrm(tf2.getFaciFdbfrm());
			} else if (!tf2.getTnaForm().equals("none")) {
				tf.setTnaForm(tf2.getTnaForm());
			}
			System.out.println(tf);
			pEM.merge(tf);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<TrainingFacilitator> releaseForm(EntityManager em,
			TrainingFacilitator tf) {
		// TODO Auto-generated method stub
		List<TrainingFacilitator> tfList = null;
		Boolean flag = false;
		try {
			Query query = em
					.createQuery("FROM TrainingFacilitator tf WHERE trainingId = :tId");
			query.setParameter("tId", tf.getTrainingId());
			System.out.println(tf);

			tfList = query.getResultList();
			if (tfList != null) {
				System.out.println("not null");
				if (tf.getFaciFdbfrm().equals("released")) {
					for (TrainingFacilitator itemz : tfList) {
						System.out.println(itemz);
						itemz.setFaciFdbfrm("released");
						em.merge(itemz);
					}
					flag = true;
				} else if (tf.getTnaForm().equals("released")) {
					for (TrainingFacilitator itemz : tfList) {
						System.out.println(itemz);
						itemz.setTnaForm("released");
						em.merge(itemz);
					}
					flag = true;
				}

			} else {
				System.out.println("null");
			}

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}

		if (flag == true) {
			return tfList;
		} else {
			return null;
		}
	}

	@Override
	public TrainingFacilitator getFacilitator(EntityManager em, int id,
			int training_id) {
		// TODO Auto-generated method stub
		try {
			Query q = em
					.createQuery("FROM TrainingFacilitator tp WHERE tp.trainingId = :training_id AND tp.userId = :uId");
			q.setParameter("training_id", training_id);
			q.setParameter("uId", id);
			return (TrainingFacilitator) q.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
