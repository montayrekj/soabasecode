package ph.com.alliance.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import ph.com.alliance.dao.TrainingParticipantDao;
import ph.com.alliance.entity.TrainingParticipant;

@Repository("trainingParticipantDao")
public class TrainingParticipantDaoImpl implements TrainingParticipantDao {

	@Override
	public List<TrainingParticipant> getAllParticipantPerTraining(
			EntityManager pEM, int trainingId) {

		try {
			Query q = pEM
					.createQuery("FROM TrainingParticipant tp WHERE tp.trainingId = :trainingId");
			q.setParameter("trainingId", trainingId);
			return q.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public void addTrainingParticipant(EntityManager pEM, TrainingParticipant tp) {
		try {
			pEM.persist(tp);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteTrainingParticipant(EntityManager pEM,
			TrainingParticipant tp) {
		try {
			Query qFind = pEM
					.createQuery("FROM TrainingParticipant tp WHERE tp.userId = :userId AND tp.trainingId =:trainingId");
			qFind.setParameter("userId", tp.getUserId());
			qFind.setParameter("trainingId", tp.getTrainingId());

			TrainingParticipant result = (TrainingParticipant) qFind
					.getSingleResult();

			Query qDel = pEM
					.createQuery("DELETE FROM TrainingParticipant tp WHERE tp.participantId = :tpId");
			qDel.setParameter("tpId", result.getParticipantId());
			qDel.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean updateTrainingParticipant(EntityManager em,
			TrainingParticipant tp) {
		// TODO Auto-generated method stub
		try {
			Query query = em
					.createQuery("FROM TrainingParticipant tp WHERE userId = :uId AND trainingId = :tId");
			query.setParameter("uId", tp.getUserId());
			query.setParameter("tId", tp.getTrainingId());
			System.out.println(tp);
			TrainingParticipant tp2 = (TrainingParticipant) query
					.getSingleResult();
			System.out.println("TP2: " + tp2);
			tp.setParticipantId(tp2.getParticipantId());
			System.out.println("temp");
			if (!tp2.getSkillsAssessfrm().equals("none")) {
				tp.setSkillsAssessfrm(tp2.getSkillsAssessfrm());
			} else if (!tp2.getCourseFdbackfrm().equals("none")) {
				tp.setCourseFdbackfrm(tp2.getCourseFdbackfrm());
			} else if(!tp2.getFaciFdbckfrm().equals("none")){
				tp.setFaciFdbckfrm(tp2.getFaciFdbckfrm());
			}
			System.out.println(tp);
			em.merge(tp);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<TrainingParticipant> releaseForm(EntityManager em,
			TrainingParticipant tp) {
		// TODO Auto-generated method stub
		List<TrainingParticipant> tpList = null;
		Boolean flag = false;
		try {
			Query query = em
					.createQuery("FROM TrainingParticipant tp WHERE trainingId = :tId");
			query.setParameter("tId", tp.getTrainingId());
			System.out.println(tp);

			tpList = query.getResultList();
			if (tpList != null) {
				System.out.println("not null");
				if (tp.getSkillsAssessfrm().equals("released")) {
					for (TrainingParticipant itemz : tpList) {
						System.out.println(itemz);
						itemz.setSkillsAssessfrm("released");
						em.merge(itemz);
					}
					flag = true;
				} else if (tp.getCourseFdbackfrm().equals("released")) {
					for (TrainingParticipant itemz : tpList) {
						System.out.println(itemz);
						itemz.setCourseFdbackfrm("released");
						em.merge(itemz);
					}
					flag = true;
				} else if (tp.getTrainingEffectiveness_frm().equals("released")) {
					for (TrainingParticipant itemz : tpList) {
						System.out.println(itemz);
						itemz.setTrainingEffectiveness_frm("released");
						em.merge(itemz);
					}
					flag = true;
				} else if (tp.getFaciFdbckfrm().equals("released")) {
					for (TrainingParticipant itemz : tpList) {
						System.out.println(itemz);
						itemz.setFaciFdbckfrm("released");
						em.merge(itemz);
					}
					flag = true;
				}

			} else {
				System.out.println("null");
			}

		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}

		if (flag == true) {
			return tpList;
		} else {
			return null;
		}
	}

	@Override
	public TrainingParticipant getParticipant(EntityManager em, int id,
			int training_id) {
		// TODO Auto-generated method stub
		try {
			Query q = em
					.createQuery("FROM TrainingParticipant tp WHERE training_id = :training_id AND userId = :uId");
			q.setParameter("training_id", training_id);
			q.setParameter("uId", id);
			return (TrainingParticipant) q.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public TrainingParticipant retrieveParticipant(EntityManager pEM,
			int participantId) {
		try {
			return pEM.find(TrainingParticipant.class, participantId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
