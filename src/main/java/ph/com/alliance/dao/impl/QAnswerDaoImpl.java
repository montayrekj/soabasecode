package ph.com.alliance.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import ph.com.alliance.dao.QAnswerDao;
import ph.com.alliance.entity.Qanswer;
import ph.com.alliance.entity.Question;
import ph.com.alliance.entity.TrainingFacilitator;
import ph.com.alliance.entity.TrainingParticipant;
import ph.com.alliance.entity.User;

@Repository("qAnswerDao")
public class QAnswerDaoImpl implements QAnswerDao{

	@Override
	public Qanswer getQanswer(EntityManager em, int qId, int uId, int tId) {
		// TODO Auto-generated method stub
		Qanswer q = null;
		
		Query questionQuery = em.createQuery("FROM Qanswer q WHERE q.qId = :id AND q.uId = :uId AND q.tId = :tId");
		questionQuery.setParameter("id", qId);
		questionQuery.setParameter("uId", uId);
		questionQuery.setParameter("tId", tId);
		q = (Qanswer) questionQuery.getSingleResult();
		
		return q;
	}

	@Override
	public boolean setAnswer(EntityManager em, Qanswer qa) {
		// TODO Auto-generated method stub
		boolean success = false;
		
		try{
			em.merge(qa);
			success = true;
		} catch(Exception e){
			System.out.println(e.getMessage());
			success = false;
		}
		return success;
	}

	@Override
	public boolean propagateQuestions(EntityManager em,
			List<Question> questionList, List<TrainingParticipant> tpList) {
		// TODO Auto-generated method stub
		boolean success = false;
		User tempU = null;
		try{
			if(tpList != null){
				for(TrainingParticipant tp:tpList){
					//Get its supervisor
					try{
					 Query spQuery = em.createQuery("SELECT sup FROM User sup, Employee e, User u "
				                + "WHERE sup.empId = e.employee.empId "
				                + "AND e.empId = u.empId "
				                + "AND u.userId = :uId");
					
						spQuery.setParameter("uId", tp.getUserId());
						tempU = (User) spQuery.getSingleResult();
						System.out.println(tempU);
					} catch(NoResultException nre){
						tempU = new User();
						tempU.setUserId(0);
					} catch(Exception e){
						success = false;
						System.out.println(e.getMessage());
					}
					if(questionList != null){
						for(Question q:questionList){
							String temp = q.getIdQuestions() + tp.getUserId() + "";
							Qanswer qa = new Qanswer();
							//qa.setId(Integer.parseInt(temp));
							qa.setQId(q.getIdQuestions());
							qa.setUId(tp.getUserId());
							qa.setTId(tp.getTrainingId());
							qa.setJId(tp.getParticipantId());
							qa.setSpId(tempU.getUserId());
							System.out.println(qa);
							em.persist(qa);
						}
					}
				}
			}
			success = true;
		} catch (Exception e){
			success = false;
			System.out.println(e.getMessage());
		}
		return success;
	}

	@Override
	public boolean propagateQuestionsByFacilitator(EntityManager em,
			List<Question> questionList, List<TrainingFacilitator> tfList) {
		// TODO Auto-generated method stub
		boolean success = false;
		User tempU = null;
		try{
			if(tfList != null){
				for(TrainingFacilitator tf:tfList){
					//Get its supervisor
					try{
					 Query spQuery = em.createQuery("SELECT sup FROM User sup, Employee e, User u "
				                + "WHERE sup.empId = e.employee.empId "
				                + "AND e.empId = u.empId "
				                + "AND u.userId = :uId");
					
						spQuery.setParameter("uId", tf.getUserId());
						tempU = (User) spQuery.getSingleResult();
						System.out.println(tempU);
					} catch(NoResultException nre){
						tempU = new User();
						tempU.setUserId(0);
					} catch(Exception e){
						success = false;
						System.out.println(e.getMessage());
					}
					if(questionList != null){
						for(Question q:questionList){
							String temp = q.getIdQuestions() + tf.getUserId() + "";
							Qanswer qa = new Qanswer();
							//qa.setId(Integer.parseInt(temp));
							qa.setQId(q.getIdQuestions());
							qa.setUId(tf.getUserId());
							qa.setTId(tf.getTrainingId());
							qa.setJId(tf.getFacilitatorId());
							qa.setSpId(tempU.getUserId());
							System.out.println(qa);
							em.persist(qa);
						}
					}
				}
			}
			success = true;
		} catch (Exception e){
			success = false;
			System.out.println(e.getMessage());
		}
		return success;
	}
	
	public List<Object> getEffectivenessFeedback(EntityManager pEM, int trainingId) {
		
		try{
			
			Query q = pEM.createQuery("SELECT q.qId, q.answer, SUM(CASE WHEN q.answer ='Strongly Agree' THEN 1 ELSE 0 END) as StronglyAgree,"
									+ "SUM(CASE WHEN q.answer = 'Agree' THEN 1 ELSE 0 END) as Agree,"
									+ "SUM(CASE WHEN q.answer = 'Neutral' THEN 1 ELSE 0 END) as Neutral,"
									+ "SUM(CASE WHEN q.answer = 'Disagree' THEN 1 ELSE 0 END) as Disagree,"
									+ "SUM(CASE WHEN q.answer = 'Strongly Disagree' THEN 1 ELSE 0 END) as StronglyDisagree"
									+ " FROM Qanswer q" 
									+ " WHERE q.tId=:trainingId AND q.qId >= 52 AND q.qId <=55"
									+ " GROUP BY q.qId");
			q.setParameter("trainingId", trainingId);
			return q.getResultList();
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Object> getEffectivenessRating(EntityManager pEM, int trainingId) {
		
		try{
			Query q = pEM.createQuery("SELECT q.qId, q.answer, "
									+ "SUM(CASE WHEN q.answer='EE' THEN 1 ELSE 0 END)as EE,"
									+ "SUM(CASE WHEN q.answer='SS' THEN 1 ELSE 0 END)as SS," 
									+ "SUM(CASE WHEN q.answer='ME' THEN 1 ELSE 0 END)as ME,"
									+ "SUM(CASE WHEN q.answer='DN' THEN 1 ELSE 0 END)as DN,"
									+ "SUM(CASE WHEN q.answer='NI' THEN 1 ELSE 0 END)as NI"
									+ " FROM Qanswer q"
									+ " WHERE q.tId =:trainingId"
									+ " AND q.qId >=56 AND q.qId <=57"
									+ " GROUP BY q.qId");
			q.setParameter("trainingId", trainingId);
			return q.getResultList();
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	
	@Override
	public List<Qanswer> getAllEffectivessAnswer(EntityManager pEM,
			int trainingId) {
		try{
			Query q = pEM.createQuery("FROM Qanswer q WHERE q.tId =:trainingId ORDER BY q.qId asc");
			q.setParameter("trainingId", trainingId);
			return q.getResultList();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public Object getEffectivenessFeedbackByQuestion(EntityManager pEM,
			int trainingId, int qid) {
		// TODO Auto-generated method stub
		try{
			Query q = pEM.createQuery("SELECT q.qId, q.answer, "
									+ "SUM(CASE WHEN q.answer ='Strongly Agree' THEN 1 ELSE 0 END) as StronglyAgree,"
									+ "SUM(CASE WHEN q.answer = 'Agree' THEN 1 ELSE 0 END) as Agree,"
									+ "SUM(CASE WHEN q.answer = 'Neutral' THEN 1 ELSE 0 END) as Neutral,"
									+ "SUM(CASE WHEN q.answer = 'Disagree' THEN 1 ELSE 0 END) as Disagree,"
									+ "SUM(CASE WHEN q.answer = 'Strongly Disagree' THEN 1 ELSE 0 END) as StronglyDisagree"
									+ " FROM Qanswer q" 
									+ " WHERE q.tId=:trainingId AND q.qId = :qId"
									+ " GROUP BY q.qId");
			q.setParameter("trainingId", trainingId);
			q.setParameter("qId", qid);
			return q.getSingleResult();
		} catch(NoResultException ex){
			return null;
		} catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public List<Object[]> getPreGenericAssesment(EntityManager pEM, int partiId) {
		
		try{
			Query q = pEM.createQuery("SELECT q.answer, AVG(CASE q.answer "
					+ "WHEN 'low' THEN 4 "
					+ "WHEN 'beginner' THEN 6 "
					+ "WHEN 'mid/practicing' THEN 8 "
					+ "WHEN 'high performer' THEN 10 ELSE 0 END)as average"
					+ " FROM Qanswer q "
					+ "WHERE q.jId =:partiId AND ((q.qId >= 21 AND q.qId <=35) OR q.qId = 47) ");
			q.setParameter("partiId", partiId);
			return q.getResultList();
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Object getEffectivenessRatingByQuestion(EntityManager pEM,
			int trainingId, int qid) {
		// TODO Auto-generated method stub
		try{
			Query q = pEM.createQuery("SELECT q.qId, q.answer, "
									+ "SUM(CASE WHEN q.answer='EE' THEN 1 ELSE 0 END)as EE,"
									+ "SUM(CASE WHEN q.answer='SS' THEN 1 ELSE 0 END)as SS," 
									+ "SUM(CASE WHEN q.answer='ME' THEN 1 ELSE 0 END)as ME,"
									+ "SUM(CASE WHEN q.answer='DN' THEN 1 ELSE 0 END)as DN,"
									+ "SUM(CASE WHEN q.answer='NI' THEN 1 ELSE 0 END)as NI"
									+ " FROM Qanswer q"
									+ " WHERE q.tId =:trainingId"
									+ " AND q.qId = :qId"
									+ " GROUP BY q.qId");
			q.setParameter("trainingId", trainingId);
			q.setParameter("qId", qid);
			return q.getSingleResult();
		} catch(NoResultException ex){
			return null;
		} catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public List<Object[]> getPreTechnicalAssesment(EntityManager pEM, int partiId) {
		try{
			Query q = pEM.createQuery("SELECT q.answer, AVG(CASE q.answer "
					+ "WHEN 'low' THEN 4 "
					+ "WHEN 'beginner' THEN 6 "
					+ "WHEN 'mid/practicing' THEN 8 "
					+ "WHEN 'high performer' THEN 10 ELSE 0 END)as average"
					+ " FROM Qanswer q "
					+ "WHERE q.qId >= 1 AND q.qId <=7 AND q.jId =:partiId");
			q.setParameter("partiId", partiId);
			return q.getResultList();
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Object[]> getPreSEAssesment(EntityManager pEM, int partiId) {
		try{
			Query q = pEM.createQuery("SELECT q.answer, AVG(CASE q.answer "
					+ "WHEN 'low' THEN 4 "
					+ "WHEN 'beginner' THEN 6 "
					+ "WHEN 'mid/practicing' THEN 8 "
					+ "WHEN 'high performer' THEN 10 ELSE 0 END)as average"
					+ " FROM Qanswer q "
					+ "WHERE q.jId =:partiId AND (q.qId = 36 OR q.qId = 37)");
			q.setParameter("partiId", partiId);
			return q.getResultList();
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public List<Object[]> getPostGenericAssesment(EntityManager pEM, int partiId) {
		
		try{
			Query q = pEM.createQuery("SELECT q.spAnswer, AVG(CASE q.spAnswer "
					+ "WHEN 'low' THEN 4 "
					+ "WHEN 'beginner' THEN 6 "
					+ "WHEN 'mid/practicing' THEN 8 "
					+ "WHEN 'high performer' THEN 10 ELSE 0 END)as average"
					+ " FROM Qanswer q "
					+ "WHERE q.jId =:partiId AND ((q.qId >= 21 AND q.qId <=35) OR q.qId = 47) ");
			q.setParameter("partiId", partiId);
			return q.getResultList();
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Object[]> getPostTechnicalAssesment(EntityManager pEM, int partiId) {
		try{
			Query q = pEM.createQuery("SELECT q.spAnswer, AVG(CASE q.spAnswer "
					+ "WHEN 'low' THEN 4 "
					+ "WHEN 'beginner' THEN 6 "
					+ "WHEN 'mid/practicing' THEN 8 "
					+ "WHEN 'high performer' THEN 10 ELSE 0 END)as average"
					+ " FROM Qanswer q "
					+ "WHERE q.qId >= 1 AND q.qId <=7 AND q.jId =:partiId");
			q.setParameter("partiId", partiId);
			return q.getResultList();
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Object[]> getPostSEAssesment(EntityManager pEM, int partiId) {
		try{
			Query q = pEM.createQuery("SELECT q.spAnswer, AVG(CASE q.answer "
					+ "WHEN 'low' THEN 4 "
					+ "WHEN 'beginner' THEN 6 "
					+ "WHEN 'mid/practicing' THEN 8 "
					+ "WHEN 'high performer' THEN 10 ELSE 0 END)as average"
					+ " FROM Qanswer q "
					+ "WHERE q.jId =:partiId AND (q.qId = 36 OR q.qId = 37)");
			q.setParameter("partiId", partiId);
			return q.getResultList();
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
}
