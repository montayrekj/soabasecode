package ph.com.alliance.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import ph.com.alliance.dao.FormDao;
import ph.com.alliance.entity.Form;

@Repository("formDao")
public class FormDaoImpl implements FormDao{

	@Override
	public List<Form> getAllForms(EntityManager em) {
		// TODO Auto-generated method stub
		List<Form> myList = null;
		Query query = em.createQuery("FROM Form f");
		myList = query.getResultList();
		return myList;
	}

	@Override
	public Form getFormById(EntityManager em, int id) {
		// TODO Auto-generated method stub
		Form f = null;
		
		try{
			Query query = em.createQuery("FROM Form f WHERE f.id = :fId");
			query.setParameter("fId", id);
			f = (Form) query.getSingleResult();
		} catch(Exception e){
			System.out.println(e.getMessage());
		}
		
		return f;
	}

}
