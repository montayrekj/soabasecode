package ph.com.alliance.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Repository;

import ph.com.alliance.dao.AnnualPlanDao;
import ph.com.alliance.entity.AnnualPlan;

@Repository("annualPlanDao")
public class AnnualPlanDaoImpl implements AnnualPlanDao{

	@Override
	public List<AnnualPlan> getAllAnnualPlan(EntityManager pEM) {

		try{
			Query q = pEM.createQuery("FROM AnnualPlan ap");
			return q.getResultList();
		}catch(EntityNotFoundException e){
			System.err.println(e.getMessage());
		} catch(Exception e){
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public AnnualPlan retrievePlanInfo(EntityManager pEM, int planId){

		try{
			return pEM.find(AnnualPlan.class, planId);
		}catch(EntityNotFoundException e){
			System.err.println(e.getMessage());
		}

		return null;
	}

	@Override
	public void createAnnualPlan(EntityManager pEM, AnnualPlan ap){

		try{
			pEM.persist(ap);
		}catch(PersistenceException e){
			System.err.println(e.getMessage());
		}catch(ConstraintViolationException e){
			System.err.println(e.getMessage());
		}
	}
}
