package ph.com.alliance.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import ph.com.alliance.dao.TeeDao;
import ph.com.alliance.entity.Effectiveness;
@Repository("teeDao")
public class TeeDaoImpl implements TeeDao{

	@Override
	public List<Effectiveness> getAllQuestion(EntityManager em) {
		// TODO Auto-generated method stub
		List<Effectiveness> myQlist = null;
		Query qQuery = em.createQuery("FROM Effectiveness e");
		myQlist = qQuery.getResultList();
		System.out.println(myQlist.size());
		return myQlist;
	}

	@Override
	public boolean setAnswer(EntityManager em, Effectiveness e) {
		// TODO Auto-generated method stub
		boolean success = false;
		try{
			System.out.println(e);
			em.merge(e);
			em.flush();
			success = true;
		} catch(Exception ex){
			System.out.println(ex.getMessage());
			success = false;
		}
		return success;
	}

	@Override
	public Effectiveness getEffectiveness(EntityManager em, int id) {
		// TODO Auto-generated method stub
		Effectiveness e = null;
		Query myQuery = em.createQuery("FROM Effectiveness e WHERE e.id = :id");
		myQuery.setParameter("id", id);
		e = (Effectiveness) myQuery.getSingleResult();
		return e;
	}

}
