package ph.com.alliance.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import ph.com.alliance.dao.FormSCDao;
import ph.com.alliance.entity.Form;
import ph.com.alliance.entity.FormSc;

@Repository("formSCDao")
public class FormSCDaoImpl implements FormSCDao {
	/*
	@Override
	public List<FormSc> getSubCategoryList(EntityManager em, int categoryId) {
		List<FormSc> mySCList = null;
		Query query = em
				.createQuery("FROM FormSc f WHERE fk_fid = :categoryID");
		query.setParameter("categoryID", categoryId);
		mySCList = query.getResultList();
		System.out.println(mySCList);
		return mySCList;
	}
	
	@Override
	public List<FormSc> getSuperSubCategoryList(EntityManager em,
			int subCategoryId) {
		// TODO Auto-generated method stub
		List<FormSc> myList = null;
		Query query = em
				.createQuery("FROM FormSc f WHERE fk_fscid = :subCategoryID");
		query.setParameter("subCategoryID", subCategoryId);
		myList = query.getResultList();
		return myList;
	}
*/
	@Override
	public List<Form> getAllCategory(EntityManager em) {
		// TODO Auto-generated method stub
		List<Form> myList = null;
		Query query = em.createQuery("FROM Form f");
		myList = query.getResultList();
		return myList;
	}

	@Override
	public List<FormSc> getAllSubCategoryList(EntityManager em) {
		List<FormSc> myList = null;
		Query query = em.createQuery("FROM FormSc f");
		myList = query.getResultList();
		return myList;
	}

	@Override
	public List<FormSc> getAllSuperSubCategoryList(EntityManager em) {
		List<FormSc> myList = null;
		Query query = em.createQuery("FROM FormSc f");
		myList = query.getResultList();
		return myList;
	}

}
