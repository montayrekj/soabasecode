package ph.com.alliance.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import ph.com.alliance.dao.QuestionChoiceDao;
import ph.com.alliance.entity.QuestionChoice;

@Repository("questionChoiceDao")
public class QuestionChoiceDaoImpl implements QuestionChoiceDao {

	@Override
	public void setQuestionChoice(EntityManager em,int qid, int cid) {
		QuestionChoice qc = new QuestionChoice();
		qc.setqId(qid);
		qc.setChoiceId(cid);
		try {
			em.persist(qc);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@Override
	public List<QuestionChoice> getQuestionChoices(EntityManager em) {
		List<QuestionChoice> qc = null;
		Query qcQuery = em
				.createQuery("FROM QuestionChoice qc");
		qc = qcQuery.getResultList();
		return qc;
	}

	@Override
	public void deleteQuestionChoicesByQId(EntityManager em, int qid) {
		try {
			Query questionQuery = em
					.createQuery("DELETE FROM QuestionChoice qc WHERE qc.qId = :id");
			questionQuery.setParameter("id", qid);
			questionQuery.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
	}

}
