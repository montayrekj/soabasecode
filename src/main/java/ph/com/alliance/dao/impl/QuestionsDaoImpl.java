package ph.com.alliance.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import ph.com.alliance.dao.QuestionsDao;
import ph.com.alliance.entity.Question;
import ph.com.alliance.model.Answer;

@Repository("questionsDao")
public class QuestionsDaoImpl implements QuestionsDao {

	@Override
	public List<Question> getQuestions(EntityManager em) {
		// TODO Auto-generated method stub
		System.out.println("sud diri!");
		List<Question> question = null;
		try {
			Query questionQuery = em.createQuery("FROM Question q");
			question = questionQuery.getResultList();
			for (Question quest : question) {
				System.out.println(quest);
			}
		} catch (Exception e) {
			System.out.println("Message: " + e.getMessage());
		}

		return question;
	}

	@Override
	public List<Question> getQuestionsByForm(EntityManager em, int id) {
		List<Question> question = null;
		Query questionQuery = em
				.createQuery("FROM Question q WHERE fkId = :fId");
		questionQuery.setParameter("fId", id);
		question = questionQuery.getResultList();
		return question;
	}

	@Override
	public boolean setAnswer(EntityManager em, Question q) {
		// TODO Auto-generated method stub
		boolean success = false;

		try {
			em.merge(q);
			success = true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			success = false;
		}
		return success;
	}

	@Override
	public Question getQuestion(EntityManager em, int id) {
		// TODO Auto-generated method stub
		Question q = null;

		Query questionQuery = em
				.createQuery("FROM Question q WHERE q.idQuestions = :id");
		questionQuery.setParameter("id", id);
		q = (Question) questionQuery.getSingleResult();

		return q;

	}

	@Override
	public boolean addQuestion(EntityManager em, Question q) {
		boolean success = false;
		try {
			em.persist(q);
			System.out.println(q);
			success = true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			success = false;
		}
		return success;
	}

	@Override
	public boolean deleteQuestionById(EntityManager em, int id) {
		boolean success = false;

		try {
			Query questionQuery = em
					.createQuery("DELETE FROM Question q WHERE q.idQuestions = :id");
			questionQuery.setParameter("id", id);
			questionQuery.executeUpdate();
			success = true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			success = false;
		}
		return success;
	}

	@Override
	public int getQuestionsByName(EntityManager em, String name) {
		int id=0;
		try {
			Question question = null;
			Query questionQuery = em
					.createQuery("FROM Question q WHERE question = :name");
			questionQuery.setParameter("name", name);
			question = (Question) questionQuery.getSingleResult();
			id = question.getIdQuestions();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return id;
	}

}
