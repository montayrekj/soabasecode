package ph.com.alliance.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import ph.com.alliance.dao.TrainingDao;
import ph.com.alliance.entity.Training;

@Repository("trainingDao")
public class TrainingDaoImpl implements TrainingDao {


	@Override
	public long countAllTrainings(EntityManager pEM) {
		try {
			Query query = pEM.createQuery("SELECT COUNT(*) FROM Training t");
			return (long) query.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return 0;

	}

	@Override
	public void createUpdateTraining(EntityManager pEM, Training t) {
		try {
			pEM.merge(t);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Training> getAllTrainingsPerPlan(EntityManager pEM, int planId) {
		List<Training> t = null;
		try {
			Query q = pEM
					.createQuery("FROM Training t WHERE t.annualPlan = :planId");
			q.setParameter("planId", planId);
			t = q.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return t;
	}

	@Override
	public Training retriveTraining(EntityManager pEM, int id) {
		try {
			return pEM.find(Training.class, id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public int countPendingEvents(EntityManager pEM, int planId, String today) {
		try{
			StringBuffer b = new StringBuffer();
			b.append("SELECT COUNT(*) FROM Training t WHERE t.annualPlan = :planId ");
			b.append("AND t.dateStart > :date");
			Query q = pEM.createQuery(b.toString());
			q.setParameter("planId", planId);
			q.setParameter("date", today);
			return (int)((long) q.getSingleResult());
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int countOngoingEvents(EntityManager pEM, int planId, String today) {
		try{
			StringBuffer b = new StringBuffer();
			b.append("SELECT COUNT(*) FROM Training t WHERE t.annualPlan = :planId ");
			b.append("AND t.dateStart <= :date AND t.dateEnd >= :date");
			Query q = pEM.createQuery(b.toString());
			q.setParameter("planId", planId);
			q.setParameter("date", today);
			
			return (int)((long) q.getSingleResult());
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int countFinishedEvents(EntityManager pEM, int planId, String today) {
		try{
			StringBuffer b = new StringBuffer();
			b.append("SELECT COUNT(*) FROM Training t WHERE t.annualPlan = :planId ");
			b.append("AND t.dateEnd < :date");
			Query q = pEM.createQuery(b.toString());
			q.setParameter("planId", planId);
			q.setParameter("date", today);
			return (int)((long) q.getSingleResult());
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public List<Training> retrieveTrainingsByParticipant(EntityManager em,
			int id) {
		// TODO Auto-generated method stub
        List<Training> t = null;

        Query q = em
                .createQuery("Select t FROM Training t, TrainingParticipant tp, User u, AnnualPlan ap "
                        + "WHERE t.trainingId = tp.trainingId "
                        + "AND ap.id = t.annualPlan "
                        + "AND tp.userId = u.userId "
                        + "AND u.userId = :userId");
        q.setParameter("userId", id);
        try {
            t = q.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return t;

	}

	@Override
	public List<Training> retrieveTrainingsByFacilitator(EntityManager em,
			int facilID) {
		// TODO Auto-generated method stub
        List<Training> tr = null;
        Query q = em
                .createQuery("SELECT t FROM Training t, TrainingFacilitator tf, User u "
                        + "WHERE t.trainingId = tf.trainingId "
                        + "AND tf.userId = u.userId "
                        + "AND u.userId = :facilID");
        q.setParameter("facilID", facilID);
        try {
            tr = q.getResultList();
            //System.out.print(tr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tr;

	}

	@Override
	public List<Object[]> retrieveSubordinateTrainingList(EntityManager em,
			int supID) {
		// TODO Auto-generated method stub
        List<Object[]> stList = null;
        Query q = em.createQuery("SELECT t, u, tp FROM User sup, Employee e, User u, TrainingParticipant tp, Training t "
                + "WHERE sup.empId = e.employee.empId "
                + "AND e.empId = u.empId "
                + "AND u.userId = tp.userId "
                + "AND tp.trainingId = t.trainingId "
                + "AND sup.userId = :supID");
        q.setParameter("supID", supID);
        try {
            stList = q.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stList;

	}
	@Override
	public void deleteTraining(EntityManager pEM, int trainingId) {
		try{
			pEM.remove(pEM.find(Training.class, trainingId));
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
