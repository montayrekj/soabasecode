package ph.com.alliance.dao.impl;

import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TransactionRequiredException;

import org.springframework.stereotype.Repository;

import ph.com.alliance.dao.UserDao;
import ph.com.alliance.entity.User;

/**
 * Sample data access object implementation using Java Persistence API.
 * 
 *
 */
@Repository("userDao")
public class UserDaoImpl implements UserDao {
	
	
	/**
	 * TO DO:
	 * 1. Create Own Exception Class (ie. MyException class)
	 * 2. Let dao handle all hibernate/sql related exceptions and throw MyException to service layer 
	 * 		so that service layer will only handle MyException should there be errors
	 * 3. Every dao function should throw MyException
	 * 4. Should dao handle NullPointerExceptions?
	 */

	
	/*
	 * (non-Javadoc)
	 * @see ph.com.alliance.dao.UserDao#createUser(javax.persistence.EntityManager, ph.com.alliance.entity.User)
	 */
	@Override
	public boolean createUser(EntityManager pEM, User pUser) {	
		boolean success = true;
				
		try {
			
			pEM.persist(pUser);
			
		} catch (EntityExistsException ee) {
			// instantiate MyException class here and set custom error codes common to all
			// ie. throw new MyException(<ERROR CODE HERE>, <ERROR MESSAGE HERE>)
			ee.getMessage();
			success = false;
		} catch (IllegalArgumentException iae) {
			// instantiate MyException class here and set custom error codes common to all
			// ie. throw new MyException(<ERROR CODE HERE>, <ERROR MESSAGE HERE>)
			iae.getMessage();
			success = false;
		} catch (TransactionRequiredException trxe) {
			// instantiate MyException class here and set custom error codes common to all
			// ie. throw new MyException(<ERROR CODE HERE>, <ERROR MESSAGE HERE>)
			trxe.getMessage();
			success = false;
		}
		
		return success;
	}

	/*
	 * (non-Javadoc)
	 * @see ph.com.alliance.dao.UserDao#updateUser(javax.persistence.EntityManager, ph.com.alliance.entity.User)
	 */
	@Override
	public User updateUser(EntityManager pEM, User pUser) {
		User user = null;
		
		try {
			user = pEM.merge(pUser);
		} catch (IllegalArgumentException iae) {
			iae.getMessage();
		} catch (TransactionRequiredException trxe) {
			trxe.getMessage();
		}
		
		return user;
	}

	/*
	 * (non-Javadoc)
	 * @see ph.com.alliance.dao.UserDao#deleteUser(javax.persistence.EntityManager, ph.com.alliance.entity.User)
	 */
	@Override
	public boolean deleteUser(EntityManager pEM, int uID) {
		boolean success = true;
		
		try{
			pEM.remove(pEM.find(User.class, uID));
		}catch(Exception e){
			e.printStackTrace();
			success = false;
		}
		
		return success;
	}

	/*
	 * (non-Javadoc)
	 * @see ph.com.alliance.dao.UserDao#selectUser(javax.persistence.EntityManager, java.lang.String)
	 */
	@Override
	public User selectUser(EntityManager pEM, int pUid) {
		User user = null;
				
		try {
			
			user = pEM.find(User.class, pUid);
						
		} catch (IllegalArgumentException iae) {
			iae.getMessage();
		}
			
		return user;
	}

	/*
	 * (non-Javadoc)
	 * @see ph.com.alliance.dao.UserDao#selectUsers(javax.persistence.EntityManager, java.lang.String)
	 */
	@Override
	public List<User> selectUsers(EntityManager pEM, String pKey) {

		return null;
	}
	

	/*
	 * (non-Javadoc)
	 * @see ph.com.alliance.dao.UserDao#selectAllUsers(javax.persistence.EntityManager)
	 */
	@Override
	public List<User> selectAllUsers(EntityManager pEM) {
		List<User> list = null;
		try{
			Query q = pEM.createQuery("FROM User u");
			list = q.getResultList();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return list;
	}

	@Override
	public User findUser(EntityManager pEM, String username, String password) {
		// TODO Auto-generated method stub
		Query userQuery = pEM.createQuery("FROM User u WHERE u.username like :username AND u.userPass like :pass");
		userQuery.setParameter("username", username);
		userQuery.setParameter("pass", password);
		User u = null;
		try{
			u = (User) userQuery.getSingleResult();
		} catch(Exception e){
			//e.printStackTrace();
		}
		
		return u;
	}
	
	@Override
	public User findUsername(EntityManager pEM, String username) {
		// TODO Auto-generated method stub
		Query userQuery = pEM.createQuery("FROM User u WHERE u.username like :username");
		userQuery.setParameter("username", username);
		User u = null;
		try{
			u = (User) userQuery.getSingleResult();
		} catch(Exception e){
			//e.printStackTrace();
		}
		
		return u;
	}

	@Override
	public User retrieveUserInfo(EntityManager pEM, int id) {
		try{
			return pEM.find(User.class, id);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public User findEmployee(EntityManager em, int userID) {
		User u = null;
		try {
			Query q = em.createQuery("FROM User u WHERE u.userId = :userID");
			q.setParameter("userID", userID);
			u = (User) q.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return u;
	}

	@Override
	public int generateUserId(EntityManager em) {
		try {
			Query q = em.createQuery("SELECT (MAX(u.userId) + 1) FROM User u");
			return (int) q.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return -999;
		}
	}

}
