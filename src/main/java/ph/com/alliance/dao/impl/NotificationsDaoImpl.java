package ph.com.alliance.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import ph.com.alliance.dao.NotificationsDao;
import ph.com.alliance.entity.Notification;
import ph.com.alliance.entity.Training;
import ph.com.alliance.entity.TrainingFacilitator;
import ph.com.alliance.entity.TrainingParticipant;

@Repository("notificationsDao")
public class NotificationsDaoImpl implements NotificationsDao{

	@Override
	public boolean insertNotificationsByParticipant(EntityManager em,
			List<TrainingParticipant> list, String name) {
		// TODO Auto-generated method stub
		boolean success = false;
		
		try{
			if(list != null){
				for(TrainingParticipant tp:list){
					Query query = em.createQuery("FROM Training t WHERE t.trainingId = :tId");
					query.setParameter("tId", tp.getTrainingId());
					Training t = (Training) query.getSingleResult();
					String message = "The " + name + " of " + t.getTrainingTitle() + " has been released. Please answer the form!";
					Notification n = new Notification();
					n.setMessage(message);
					n.setTId(t.getTrainingId());
					n.setUId(tp.getUserId());
					System.out.println(n);
					
					em.persist(n);
				}
			}
		} catch(Exception e){
			System.out.println(e.getMessage());
		}
		
		return success;
	}
	
	public boolean insertNotificationsByFacilitator(EntityManager em,
			List<TrainingFacilitator> list, String name) {
		// TODO Auto-generated method stub
		boolean success = false;
		
		try{
			if(list != null){
				for(TrainingFacilitator tp:list){
					Query query = em.createQuery("FROM Training t WHERE t.trainingId = :tId");
					query.setParameter("tId", tp.getTrainingId());
					Training t = (Training) query.getSingleResult();
					String message = "The " + name + " of " + t.getTrainingTitle() + " has been released. Please answer the form!";
					Notification n = new Notification();
					n.setMessage(message);
					n.setTId(t.getTrainingId());
					n.setUId(tp.getUserId());
					System.out.println(n);
					
					em.persist(n);
				}
			}
		} catch(Exception e){
			System.out.println(e.getMessage());
		}
		
		return success;
	}


	@Override
	public List<Notification> getNotificationsByUser(EntityManager em, int id) {
		// TODO Auto-generated method stub
		List<Notification> notifList = null;
		
		try{
			Query query = em.createQuery("FROM Notification n WHERE n.uId = :uId");
			query.setParameter("uId", id);
			notifList = query.getResultList();
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		
		
		return notifList;
	}

}
