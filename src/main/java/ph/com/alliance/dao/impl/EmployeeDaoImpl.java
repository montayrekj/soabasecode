package ph.com.alliance.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import ph.com.alliance.dao.EmployeeDao;
import ph.com.alliance.entity.Employee;

@Repository("employeeDao")
public class EmployeeDaoImpl implements EmployeeDao {

	@Override
	public List<Employee> selectEmployeesByLastName(EntityManager em,
			String lastName) {
		List<Employee> employeeList = null;
		try {
			Query q = em
					.createQuery("SELECT e.empId, e.firstName, e.middleName, e.lastName, e.age, e.gender"
							+ " FROM Employee e WHERE e.lastName like :lastName" 
							+ " AND e.empId NOT IN (SELECT u.empId FROM User u)");
			q.setParameter("lastName", "%"+lastName+"%");
			employeeList = q.getResultList();
			System.out.println("Entries: "+employeeList.size());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return employeeList;
	}

}
