package ph.com.alliance.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import ph.com.alliance.dao.TrainingAttendanceDao;
import ph.com.alliance.entity.Training;
import ph.com.alliance.entity.TrainingAttendance;

@Repository("trainingAttendanceDao")
public class TrainingAttendanceDaoImpl implements TrainingAttendanceDao {

	@Override
	public void insertUpdateParticipantAttendance(EntityManager pEM,
			TrainingAttendance ta) {
		try {
			pEM.merge(ta);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<TrainingAttendance> getAttendanceByTraining(
			EntityManager pEM, int trainingId) {
		try {
			Query q = pEM
					.createQuery("FROM TrainingAttendance ta WHERE ta.trainingId =:trainingId ORDER BY ta.participantId , ta.attendanceDate");
			q.setParameter("trainingId", trainingId);
			return q.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void updateAttendance(EntityManager pEM, TrainingAttendance ta) {
		try {
			Query q = pEM
					.createQuery("UPDATE TrainingAttendance ta SET ta.participantStatus =:status WHERE ta.attendanceId=:attId");
			q.setParameter("status", ta.getParticipantStatus());
			q.setParameter("attId", ta.getAttendanceId());
			q.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public int deleteOnTrainingUpdate(EntityManager pEM, Training t) {
		try {
			Query q = pEM.createQuery("DELETE FROM TrainingAttendance ta "
					+ "WHERE ta.trainingId = :trainingId "
					+ "AND ta.attendanceDate <:start "
					+ "OR ta.attendanceDate >:end");
			q.setParameter("trainingId", t.getTrainingId());
			q.setParameter("start", t.getDateStart());
			q.setParameter("end", t.getDateEnd());
			return q.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public TrainingAttendance retrieveAttendanceById(EntityManager pEM,
			int attendanceId) {
		
		try{
			return pEM.find(TrainingAttendance.class, attendanceId);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<TrainingAttendance> retrieveAttendanceByParticipant(
			EntityManager pEM, int partiId) {
		
		try{
			Query q = pEM.createQuery("FROM TrainingAttendance ta WHERE ta.participantId = :partiId");
			q.setParameter("partiId", partiId);
			
			return q.getResultList();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return null;
	}

}
