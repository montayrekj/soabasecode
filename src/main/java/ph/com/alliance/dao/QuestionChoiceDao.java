package ph.com.alliance.dao;

import java.util.List;

import javax.persistence.EntityManager;

import ph.com.alliance.entity.QuestionChoice;


public interface QuestionChoiceDao {
	public void setQuestionChoice(EntityManager em,int qid,int cid);
	public List<QuestionChoice> getQuestionChoices(EntityManager em);
	public void deleteQuestionChoicesByQId(EntityManager em,int qid);
}
