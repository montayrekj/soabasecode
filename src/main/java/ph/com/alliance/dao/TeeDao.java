package ph.com.alliance.dao;

import java.util.List;

import javax.persistence.EntityManager;

import ph.com.alliance.entity.Effectiveness;

public interface TeeDao {
	public List<Effectiveness> getAllQuestion(EntityManager em);
	
	public Effectiveness getEffectiveness(EntityManager em, int id);
	
	public boolean setAnswer(EntityManager em, Effectiveness e);
}
