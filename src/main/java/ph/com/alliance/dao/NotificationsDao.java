package ph.com.alliance.dao;

import java.util.List;

import javax.persistence.EntityManager;

import ph.com.alliance.entity.Notification;
import ph.com.alliance.entity.TrainingFacilitator;
import ph.com.alliance.entity.TrainingParticipant;

public interface NotificationsDao {
	public boolean insertNotificationsByParticipant(EntityManager em, List<TrainingParticipant> List, String name);
	public boolean insertNotificationsByFacilitator(EntityManager em, List<TrainingFacilitator> List, String name);
	
	
	public List<Notification> getNotificationsByUser(EntityManager em, int id);

}
