package ph.com.alliance.dao;

import java.util.List;

import javax.persistence.EntityManager;

import ph.com.alliance.entity.User;

/**
 * 
 * 
 */
public interface UserDao {
	/**
	 * 
	 * @param puser
	 * @return
	 */
	public boolean createUser(EntityManager pEM, User pUser);
	
	/**
	 * 
	 * @param p_em
	 * @param pUser
	 * @return
	 */
	public User updateUser(EntityManager pEM, User pUser);
	
	/**
	 * 
	 * @param p_em
	 * @param pUser
	 * @return
	 */
	public boolean deleteUser(EntityManager pEM, int uID);
	
	/**
	 * 
	 * @param p_em
	 * @param pUser
	 * @return
	 */
	public User selectUser(EntityManager pEM, int pUid);
	
	/**
	 * 
	 * @param p_em
	 * @param pKey
	 * @return
	 */
	public List<User> selectUsers(EntityManager pEM, String pKey);
	
	/**
	 * 
	 * @param pEM
	 * @return
	 */
	public List<User> selectAllUsers(EntityManager pEM);
	
	/**
	 * 
	 * @param pEM
	 * @return
	 */
	public User retrieveUserInfo(EntityManager pEM, int id);
	
	
	public User findUser(EntityManager pEM,String username, String password);
	//public Facilitator upgradeUser(EntityManager pEM, String id);

	User findUsername(EntityManager pEM, String username);

	public User findEmployee(EntityManager em, int empID);
	
	public int generateUserId(EntityManager em); 
}
