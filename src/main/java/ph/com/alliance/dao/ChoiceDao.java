package ph.com.alliance.dao;

import java.util.List;

import javax.persistence.EntityManager;

import ph.com.alliance.entity.Choice;

public interface ChoiceDao {
	public void addChoice(EntityManager em,String name);
	public int getChoiceByName(EntityManager em,String name);
	public List<Choice> getChoices(EntityManager em);
	public void deleteChoicesByQId(EntityManager em, int qid);
}
