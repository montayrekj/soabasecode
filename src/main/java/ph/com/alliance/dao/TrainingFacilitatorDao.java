package ph.com.alliance.dao;

import java.util.List;

import javax.persistence.EntityManager;

import ph.com.alliance.entity.TrainingFacilitator;
import ph.com.alliance.entity.TrainingParticipant;

public interface TrainingFacilitatorDao {

	public List<TrainingFacilitator> releaseForm(EntityManager em, TrainingFacilitator tf);
	
	/**
	 * 
	 * @param trainingId
	 * @param pEM
	 * @return
	 */
	public List<TrainingFacilitator> getAllFacilitatorPerTraining(EntityManager pEM, int trainingId);

	/**
	 * 
	 * @param tf
	 * @param pEM
	 *
	 */
	public void addTrainingFacilitator(EntityManager pEM, TrainingFacilitator tf);

	/**
	 * 
	 * @param tf
	 * @param pEM
	 *
	 */
	public void deleteTrainingFacilitator(EntityManager pEM, TrainingFacilitator tf);
	
	/**
	 * 
	 * @param tf
	 * @param pEM
	 *
	 */
	public boolean updateTrainingFacilitator(EntityManager pEM, TrainingFacilitator tf);
	
	public TrainingFacilitator getFacilitator(EntityManager em, int id, int training_id);
}
