package ph.com.alliance.dao;

import java.util.List;

import javax.persistence.EntityManager;

import ph.com.alliance.entity.Training;
import ph.com.alliance.entity.User;

public interface TrainingDao {
	
	public List<Training> retrieveTrainingsByParticipant(EntityManager em, int uId);

	public List<Training> retrieveTrainingsByFacilitator(EntityManager em,
			int facilID);
	
	public List<Object[]> retrieveSubordinateTrainingList(EntityManager em, int supID);
	
	/*
	 * @param pEM
	 * @param trainingId
	 * @return
	 */
	public Training retriveTraining(EntityManager pEM, int trainingId);
	
	/*
	 * @param pEM
	 * @param planId
	 * @return
	 */
	public List<Training> getAllTrainingsPerPlan(EntityManager pEM, int planId);
	
	/*
	 * @param pEM
	 * @return
	 */
	public long countAllTrainings(EntityManager pEM);
	
	/*
	 * @param pEM
	 * @param t
	 */
	public void createUpdateTraining(EntityManager pEM, Training t);
	
	/*
	 * @param pEM
	 * @param planId
	 * @param today
	 * @param t
	 */
	public int countPendingEvents(EntityManager pEM, int planId, String today);
	
	/*
	 * @param pEM
	 * @param planId
	 * @param today
	 * @param t
	 */
	public int countOngoingEvents(EntityManager pEM, int planId, String today);
	
	/*
	 * @param pEM
	 * @param planId
	 * @param today
	 * @param t
	 */
	public int countFinishedEvents(EntityManager pEM, int planId, String today);
	
	/*
	 * @param pEM
	 * @param trainingId
	 */
	public void deleteTraining(EntityManager pEM, int trainingId);

	
}
