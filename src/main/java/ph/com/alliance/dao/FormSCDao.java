package ph.com.alliance.dao;

import java.util.List;

import javax.persistence.EntityManager;

import ph.com.alliance.entity.Form;
import ph.com.alliance.entity.FormSc;

public interface FormSCDao { 
	public List<Form> getAllCategory(EntityManager em);
	public List<FormSc> getAllSubCategoryList(EntityManager em);
	public List<FormSc> getAllSuperSubCategoryList(EntityManager em);
}
