package ph.com.alliance.dao;

import java.util.List;

import javax.persistence.EntityManager;

import ph.com.alliance.entity.AnnualPlan;

public interface AnnualPlanDao {
	
	/**
	 * 
	 * 
	 * @param pEM
	 * @return
	 */
	public List<AnnualPlan> getAllAnnualPlan(EntityManager pEM);

	/**
	 * 
	 * @param planId
	 * @param pEM
	 * @return
	 */
	public AnnualPlan retrievePlanInfo(EntityManager pEM, int planId);

	/**
	 * 
	 * @param ap
	 * @param pEM
	 * @return
	 */
	public void createAnnualPlan(EntityManager pEM, AnnualPlan ap);
}
