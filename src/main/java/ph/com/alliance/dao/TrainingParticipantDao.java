package ph.com.alliance.dao;

import java.util.List;

import javax.persistence.EntityManager;

import ph.com.alliance.entity.TrainingParticipant;

public interface TrainingParticipantDao {


	public TrainingParticipant getParticipant(EntityManager em, int id, int training_id);

	public boolean updateTrainingParticipant(EntityManager em, TrainingParticipant tp);
	public List<TrainingParticipant> releaseForm(EntityManager em, TrainingParticipant tp);


	/**
	 * 
	 * @param trainingId
	 * @param pEM
	 * @return
	 */
	public List<TrainingParticipant> getAllParticipantPerTraining(EntityManager pEM, int trainingId);

	/**
	 * 
	 * @param tp
	 * @param pEM
	 * 
	 */
	public void addTrainingParticipant(EntityManager pEM, TrainingParticipant tp);

	/**
	 * 
	 * @param tp
	 * @param pEM
	 * 
	 */

	public void deleteTrainingParticipant(EntityManager em, TrainingParticipant tp);
	
	/*
	 * 
	 * 
	 */
	public TrainingParticipant retrieveParticipant(EntityManager pEM, int participantId);
}
