package ph.com.alliance.service;

import java.util.List;

import ph.com.alliance.entity.TrainingParticipant;

public interface TrainingParticipantService {
	
	/**
	 * 
	 * @param trainingId
	 * @return
	 */
	public List<TrainingParticipant> getTrainingParticipants(int trainingId);
	
	/**
	 * 
	 * @param tp
	 * @return
	 */
	public boolean addTrainingParticipant(TrainingParticipant tp);
	
	/**
	 * 
	 * @param tp
	 * @return
	 */
	public boolean deleteTrainingParticipant(TrainingParticipant tp);
	
	/*
	 * 
	 * @param participantId
	 * @return
	 */
	public TrainingParticipant retrieveParticipant(int participantId);
}
