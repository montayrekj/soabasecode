package ph.com.alliance.service;

import java.util.List;

import ph.com.alliance.entity.Qanswer;
import ph.com.alliance.entity.Question;
import ph.com.alliance.model.Answer;

public interface QuestionService {
	public List<Question> getQuestions();
	public boolean addAnswers(List<Answer> answer, String userType, int uId, int tId);
	public boolean deleteQuestionById(int id);
	public boolean addQuestion(Question q);
	public List<Question> getQuestionsByForm(int id);
	public int getQuestionsByName(String name);

	
	public List<Object> getEffectivenessFeedback(int trainingId);
	
	public List<Object> getEffectivenessRating(int trainingId);
	
	public List<Qanswer> getAllEffectivenessAnswer(int trainingId);
	
	public Object[] getPreAssesment(int partiId);
	
	public Object[] getPostAssesment(int partiId);
}
