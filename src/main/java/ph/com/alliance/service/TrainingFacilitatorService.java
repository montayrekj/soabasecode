package ph.com.alliance.service;

import java.util.List;

import ph.com.alliance.entity.TrainingFacilitator;

public interface TrainingFacilitatorService {
	
	/**
	 * 
	 * @param trainingId
	 * @return
	 */
	public List<TrainingFacilitator> getTrainingFaciltators(int trainingId);
	
	/**
	 * 
	 * @param tf
	 * @return
	 */
	public boolean addTrainingFacilitator(TrainingFacilitator tf);
	
	/**
	 * 
	 * @param tf
	 * @return
	 */
	public boolean deleteTrainingFacilitator(TrainingFacilitator tf);
}
