package ph.com.alliance.service;

import java.util.List;

import ph.com.alliance.entity.AnnualPlan;

public interface AnnualPlanService {
	
	/**
	 * 
	 * @return
	 */
	public List<AnnualPlan> getAllAnnualPlan();

	/**
	 * @param planId
	 * @return
	 */
	public AnnualPlan retrievePlanInfo(int planId);

	/**
	 * @param ap
	 * @return
	 */
	public boolean createAnnualPlan(AnnualPlan ap);
}
