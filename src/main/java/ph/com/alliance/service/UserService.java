package ph.com.alliance.service;

import java.util.List;

import ph.com.alliance.entity.User;

public interface UserService {
	
	public boolean createUser(User u);
	
	public boolean updateUser(User u);
	
	public List<User> selectAllUsers();
	
	public boolean deleteUser(int uID);
	
	public User selectUser(int uID);
	
	public int generateUserId();
}
