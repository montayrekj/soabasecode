package ph.com.alliance.service;

import java.util.List;

import ph.com.alliance.entity.Notification;

public interface NotificationsService {
	public List<Notification> getNotificationsByUser(int id);
}
