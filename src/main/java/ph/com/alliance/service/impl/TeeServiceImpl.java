package ph.com.alliance.service.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import ph.com.alliance.dao.TeeDao;
import ph.com.alliance.entity.Effectiveness;
import ph.com.alliance.entity.Question;
import ph.com.alliance.model.Answer;
import ph.com.alliance.model.Teeanswer;
import ph.com.alliance.service.TeeService;
@Service("teeService")
public class TeeServiceImpl implements TeeService{

	@Autowired
	private TeeDao teedao;
	
	@Autowired
	private JpaTransactionManager transactionManager;
	
	@Override
	public List<Effectiveness> getAllQuestion() {
			EntityManager em = transactionManager.getEntityManagerFactory()
				.createEntityManager();
	
		List<Effectiveness> myTableList = null;
		try {
			em.getTransaction().begin();
			myTableList = teedao.getAllQuestion(em);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
	
		return myTableList;
	}

	@Override
	public boolean setAnswer(List<Teeanswer> answer) {
		// TODO Auto-generated method stub
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		try {
			for(Teeanswer item:answer){
				if(!item.getValue().equals("") && !item.getName().equals("")){
    				String temp = item.getName();
    				//System.out.println(temp);
    				Effectiveness eff = null;
    				eff = teedao.getEffectiveness(em, Integer.parseInt(temp.trim()));
    				eff.setId(Integer.parseInt(temp.trim()));
    				eff.setQuestionAns(item.getValue());
    				result = teedao.setAnswer(em, eff);
				}
    		}
			
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
	}

}
