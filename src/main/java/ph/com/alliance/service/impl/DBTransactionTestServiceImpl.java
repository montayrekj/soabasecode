package ph.com.alliance.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import ph.com.alliance.dao.AnnualPlanDao;
import ph.com.alliance.dao.FormDao;
import ph.com.alliance.dao.NotificationsDao;
import ph.com.alliance.dao.QAnswerDao;
import ph.com.alliance.dao.QuestionsDao;
import ph.com.alliance.dao.TrainingDao;
import ph.com.alliance.dao.TrainingFacilitatorDao;
import ph.com.alliance.dao.TrainingParticipantDao;
import ph.com.alliance.dao.UserDao;
import ph.com.alliance.entity.AnnualPlan;
import ph.com.alliance.entity.Form;
import ph.com.alliance.entity.Question;
import ph.com.alliance.entity.TrainingFacilitator;
import ph.com.alliance.entity.TrainingParticipant;
import ph.com.alliance.entity.User;
import ph.com.alliance.service.DBTransactionTestService;

/**
 * Example service implementation that hadles database transaction. Database
 * transaction starts in this layer of the application, and it also ends here.
 *
 */
@Service("dBTransactionService")
public class DBTransactionTestServiceImpl implements DBTransactionTestService {

	@Autowired
	private UserDao userDao;

	@Autowired
	private TrainingDao trainingDao;
	
	@Autowired
	private QuestionsDao questionsDao;

	@Autowired
	private QAnswerDao qAnswerDao;
	
	@Autowired
	private TrainingFacilitatorDao facilitatorDao;

	@Autowired
	private TrainingParticipantDao participantDao;
	
	@Autowired
	private AnnualPlanDao planDao;
	
	@Autowired
	private NotificationsDao notifDao;

	@Autowired
	private JpaTransactionManager transactionManager;
	
	@Autowired
	private FormDao formDao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.alliance.service.DBTransactionTestService#createUser(ph.com.alliance
	 * .entity.User)
	 */
	@Override
	public boolean createUser(User pUser) {
		EntityManager em = transactionManager.getEntityManagerFactory()
				.createEntityManager();
		boolean result = false;

		em.getTransaction().begin();
		try {
			result = userDao.createUser(em, pUser);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.alliance.service.DBTransactionTestService#updateUser(ph.com.alliance
	 * .entity.User)
	 */
	@Override
	public User updateUser(User pUser) {
		EntityManager em = transactionManager.getEntityManagerFactory()
				.createEntityManager();
		User result = null;

		em.getTransaction().begin();

		try {
			result = userDao.updateUser(em, pUser);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.getMessage();
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.alliance.service.DBTransactionTestService#deleteUser(ph.com.alliance
	 * .entity.User)
	 */
	@Override
	public void deleteUser(User pUser) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.alliance.service.DBTransactionTestService#selectUser(ph.com.alliance
	 * .entity.User)
	 */
	@Override
	public User retrieveUser(int uId) {
		EntityManager pEM = transactionManager.getEntityManagerFactory()
				.createEntityManager();
		try{
			return userDao.retrieveUserInfo(pEM, uId);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(pEM.isOpen())
				pEM.close();
		}
		
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.alliance.service.DBTransactionTestService#selectUsers(java.lang
	 * .String)
	 */
	@Override
	public List<User> selectUsers(String pKey) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.alliance.service.DBTransactionTestService#createUserAndProduct
	 * (ph.com.alliance.entity.User, ph.com.alliance.entity.Product, boolean)
	 */
	/*@Override
	public boolean createUserAndProduct(User pUser, Product pProd,
			boolean pRollbackFlag) {
		EntityManager em = transactionManager.getEntityManagerFactory()
				.createEntityManager();
		boolean result = false;

		em.getTransaction().begin();
		try {
			userDao.createUser(em, pUser);
			// productDao.createProduct(em, pProd);

			if (pRollbackFlag) {
				throw new Exception(
						"FORCED TO THROW EXCEPTION TO ROLLBACK THIS TRANSACTION!");
			} else {
				em.getTransaction().commit();
			}

		} catch (Exception e) {
			e.getMessage();

			if (em.getTransaction().isActive()) {
				System.out
						.println("ROLLBACK FOR createUserAndProduct function.");
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}

		return result;
	}*/

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ph.com.alliance.service.DBTransactionTestService#createUserAndUpdateProduct
	 * (ph.com.alliance.entity.User, ph.com.alliance.entity.Product, boolean)
	 */
	/*@Override
	public boolean createUserAndUpdateProduct(User pUser, Product pProd,
			boolean pRollbackFlag) {
		EntityManager em = transactionManager.getEntityManagerFactory()
				.createEntityManager();
		boolean result = false;

		em.getTransaction().begin();
		try {
			userDao.createUser(em, pUser);
			// productDao.updateProduct(em, pProd);

			if (pRollbackFlag) {
				throw new Exception(
						"FORCED TO THROW EXCEPTION TO ROLLBACK THIS TRANSACTION!");
			} else {
				em.getTransaction().commit();
			}

		} catch (Exception e) {
			e.getMessage();

			if (em.getTransaction().isActive()) {
				System.out
						.println("ROLLBACK FOR createUserAndUpdateProduct function.");
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}

		return result;
	}*/

	/*
	 * (non-Javadoc)
	 * 
	 * @see ph.com.alliance.service.DBTransactionTestService#selectAllUsers()
	 */
	@Override
	public List<User> selectAllUsers() {
		EntityManager em = transactionManager.getEntityManagerFactory()
				.createEntityManager();
		List<User> userList = null;

		try {
			userList = userDao.selectAllUsers(em);
		} catch (Exception e) {
			System.out.print(e.getMessage());
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}

		return userList;
	}

	@Override
	public boolean loginUser(String username, String password) {
		// TODO Auto-generated method stub
		boolean flag = false;
		EntityManager em = transactionManager.getEntityManagerFactory()
				.createEntityManager();
		if (userDao.findUsername(em, username) != null) {
			flag = true;
		}
		return flag;
	}

	@Override
	public User findUser(String username, String password) {
		// TODO Auto-generated method stub
		boolean flag = false;
		EntityManager em = transactionManager.getEntityManagerFactory()
				.createEntityManager();
		return userDao.findUser(em, username, password);
	}

	@Override
	public boolean upgradeUser(String id) {
		// TODO Auto-generated method stub

		boolean success = false;

		/*
		 * EntityManager em = transactionManager.getEntityManagerFactory()
		 * .createEntityManager(); Facilitator f = null; f =
		 * userDao.upgradeUser(em, id); if (f != null) { try {
		 * em.getTransaction().begin(); success = facilitatorDao.addUser(em, f);
		 * em.getTransaction().commit(); } catch (Exception e) {
		 * System.out.println(e.getMessage());
		 * 
		 * if (em.getTransaction().isActive()) {
		 * System.out.println("ROLLBACK FOR upgradeUser function.");
		 * em.getTransaction().rollback(); } } finally { if (em.isOpen()) {
		 * em.close(); } } }
		 */
		return success;
	}
	
	@Override
	public User findEmployee(int userID){
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();

		User u = null;
		try {
			u = userDao.findEmployee(em, userID);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		return u;
	}
	
	@Override
	public boolean updateTrainingParticipant(TrainingParticipant tp) {
		// TODO Auto-generated method stub
		EntityManager em = transactionManager.getEntityManagerFactory()
				.createEntityManager();
		em.getTransaction().begin();
		try {
			participantDao.updateTrainingParticipant(em, tp);
			em.getTransaction().commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
		} finally {
			if (em.isOpen())
				em.close();
		}

		return false;
	}

	@Override
	public boolean releaseForm(TrainingParticipant tp) {
		// TODO Auto-generated method stub
		int frmId = -1;
		List<Question> questionsList = null;
		List<TrainingParticipant> tpList = null;
		EntityManager em = transactionManager.getEntityManagerFactory()
				.createEntityManager();
		boolean propagateSuccess = false;
		
		em.getTransaction().begin();
		try {
			//Get the form questions
			if(tp.getSkillsAssessfrm().equals("released")){
				frmId = 1;
				questionsList = questionsDao.getQuestionsByForm(em, frmId);
			} else if(tp.getCourseFdbackfrm().equals("released")){
				frmId = 3;
				questionsList = questionsDao.getQuestionsByForm(em, frmId);
			} else if(tp.getTrainingEffectiveness_frm().equals("released")){
				frmId = 4;
				questionsList = questionsDao.getQuestionsByForm(em, frmId);
			} else if(tp.getFaciFdbckfrm().equals("released")){
				frmId = 5;
				questionsList = questionsDao.getQuestionsByForm(em, frmId);
			}
			
			//Do something so that the user can now answer the form
			tpList = participantDao.releaseForm(em, tp);
			
			//Propagate the questions of the form in the answer table, so that it will be ready for answer
			propagateSuccess = qAnswerDao.propagateQuestions(em, questionsList, tpList);
			
			//If the questions propagation is successful? Send notifications to the user! yayyyy!
			if(propagateSuccess){
				Form f = formDao.getFormById(em, frmId);
				if(f != null){
					String form = f.getName();
					notifDao.insertNotificationsByParticipant(em, tpList, form);
				}
						
				em.getTransaction().commit();
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
		} finally {
			if (em.isOpen())
				em.close();
		}

		return false;
	}

	@Override
	public TrainingParticipant getParticipant(int id, int training_id) {
		// TODO Auto-generated method stub
		EntityManager em = transactionManager.getEntityManagerFactory()
				.createEntityManager();
		try{
			return participantDao.getParticipant(em, id, training_id);
		} catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean updateTrainingFacilitator(TrainingFacilitator tf) {
		// TODO Auto-generated method stub
		EntityManager em = transactionManager.getEntityManagerFactory()
				.createEntityManager();
		em.getTransaction().begin();
		try {
			facilitatorDao.updateTrainingFacilitator(em, tf);
			em.getTransaction().commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
		} finally {
			if (em.isOpen())
				em.close();
		}

		return false;
	}

	@Override
	public boolean releaseForm2(TrainingFacilitator tf) {
		// TODO Auto-generated method stub
		int frmId = -1;
		List<Question> questionsList = null;
		List<TrainingFacilitator> tfList = null;
		EntityManager em = transactionManager.getEntityManagerFactory()
				.createEntityManager();
		boolean propagateSuccess = false;
		
		em.getTransaction().begin();
		try {
			//Get the form questions
			if(tf.getFaciFdbfrm().equals("released")){
				frmId = 4;
				questionsList = questionsDao.getQuestionsByForm(em, frmId);
			} else if(tf.getTnaForm().equals("released")){
				frmId = 2;
				questionsList = questionsDao.getQuestionsByForm(em, frmId);
			}
			
			//Do something so that the user can now answer the form
			tfList = facilitatorDao.releaseForm(em, tf);
			
			//Propagate the questions of the form in the answer table, so that it will be ready for answer
			propagateSuccess = qAnswerDao.propagateQuestionsByFacilitator(em, questionsList, tfList);
			
			//If the questions propagation is successful? Send notifications to the user! yayyyy!
			if(propagateSuccess){
				Form f = formDao.getFormById(em, frmId);
				if(f != null){
					String form = f.getName();
					notifDao.insertNotificationsByFacilitator(em, tfList, form);
				}
						
				em.getTransaction().commit();
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
		} finally {
			if (em.isOpen())
				em.close();
		}

		return false;
	}

	@Override
	public TrainingFacilitator getFacilitator(int id, int training_id) {
		// TODO Auto-generated method stub
		EntityManager em = transactionManager.getEntityManagerFactory()
				.createEntityManager();
		try{
			return facilitatorDao.getFacilitator(em, id, training_id);
		} catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
}
