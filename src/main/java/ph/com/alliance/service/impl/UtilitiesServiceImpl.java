package ph.com.alliance.service.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Service;

import ph.com.alliance.service.UtilitiesService;

@Service("utilitiesService")
public class UtilitiesServiceImpl implements UtilitiesService {

	@Override
	public String getDateString(Date date) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return df.format(date);
	}

	@Override
	public String convertDateFormat(String dateString) {
		Date date = null;
		try {
			date = new SimpleDateFormat("yyyy-MM-dd").parse(dateString);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DateFormat df = new SimpleDateFormat("MMMMM dd, yyyy");
		return df.format(date);
	}

}
