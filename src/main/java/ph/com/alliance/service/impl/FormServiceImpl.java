package ph.com.alliance.service.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import ph.com.alliance.dao.FormDao;
import ph.com.alliance.entity.Form;
import ph.com.alliance.entity.Question;
import ph.com.alliance.service.FormService;

@Service("formService")
public class FormServiceImpl implements FormService{

	@Autowired
	private JpaTransactionManager transactionManager;
	
	@Autowired
	private FormDao formdao;
	
	@Override
	public List<Form> getAllForms() {
		// TODO Auto-generated method stub
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<Form> formList = null;
		
		try {
			formList = formdao.getAllForms(em);
		} catch (Exception e) {
			System.out.print(e.getMessage());
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return formList;	
	}

}
