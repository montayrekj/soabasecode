package ph.com.alliance.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.RollbackException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import ph.com.alliance.dao.TrainingFacilitatorDao;
import ph.com.alliance.entity.TrainingFacilitator;
import ph.com.alliance.service.TrainingFacilitatorService;

@Service("trainingFacilitatorService")
public class TrainingFacilitatorServiceImpl implements
		TrainingFacilitatorService {

	@Autowired
	private JpaTransactionManager transactionManager;
	
	@Autowired
	private TrainingFacilitatorDao facilitatorDao;
	
	@Override
	public List<TrainingFacilitator> getTrainingFaciltators(int trainingId) {
		EntityManager pEM = transactionManager.getEntityManagerFactory()
				.createEntityManager();
		try {
			return facilitatorDao.getAllFacilitatorPerTraining(pEM, trainingId);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (pEM.isOpen())
				pEM.close();
		}

		return null;
	}

	@Override
	public boolean addTrainingFacilitator(TrainingFacilitator tf) {
		EntityManager pEM = transactionManager.getEntityManagerFactory()
				.createEntityManager();
		pEM.getTransaction().begin();
		try {
			facilitatorDao.addTrainingFacilitator(pEM, tf);
			pEM.getTransaction().commit();
			return true;
		} catch (RollbackException e) {
			System.err.println(e.getMessage());
			if (pEM.getTransaction().isActive())
				pEM.getTransaction().rollback();
		} finally {
			if (pEM.isOpen())
				pEM.close();
		}

		return false;
	}

	@Override
	public boolean deleteTrainingFacilitator(TrainingFacilitator tf) {
		EntityManager em = transactionManager.getEntityManagerFactory()
				.createEntityManager();
		em.getTransaction().begin();
		try {
			facilitatorDao.deleteTrainingFacilitator(em, tf);
			em.getTransaction().commit();
			return true;
		} catch (RollbackException e) {
			System.err.println(e.getMessage());
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
		} finally {
			if (em.isOpen())
				em.close();
		}
		
		return false;
	}

}
