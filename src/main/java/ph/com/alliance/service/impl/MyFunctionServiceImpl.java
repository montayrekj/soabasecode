//package ph.com.alliance.service.impl;
//
//import java.util.List;
//
//import javax.persistence.EntityManager;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.orm.jpa.JpaTransactionManager;
//import org.springframework.stereotype.Service;
//
//import ph.com.alliance.dao.MyTableDao;
//import ph.com.alliance.entity.Smartphone;
//import ph.com.alliance.service.MyFunctionService;
//
//@Service("myFunctionService")
//public class MyFunctionServiceImpl implements MyFunctionService {
//
//	@Autowired
//	private MyTableDao myTableDao;
//
//	@Autowired
//	private JpaTransactionManager transactionManager;
//
//	/*
//	*
//	*
//	*
//	* EJ Gwapo
//	*
//	*
//	*/
//
//
////	@Override
////	public List<Mytable> getMyTables() {
////		// TODO Auto-generated method stub
////		EntityManager em = transactionManager.getEntityManagerFactory()
////				.createEntityManager();
////
////		List<Mytable> myTableList = null;
////		try {
////			em.getTransaction().begin();
////			myTableList = myTableDao.getMyTables(em);
////			em.getTransaction().commit();
////		} catch (Exception e) {
////			e.printStackTrace();
////			if (em.getTransaction().isActive()) {
////				em.getTransaction().rollback();
////			}
////		} finally {
////			if (em.isOpen()) {
////				em.close();
////			}
////		}
////
////		return myTableList;
////	}
//
//	@Override
//	public void deleteSmartphone(Smartphone phone, int id) {
//		// TODO Auto-generated method stub
//		EntityManager em = transactionManager.getEntityManagerFactory()
//				.createEntityManager();
//
//		try {
//			em.getTransaction().begin();
//			myTableDao.deleteSmartPhone(em, phone, id);
//			em.getTransaction().commit();
//		} catch (Exception e) {
//			e.printStackTrace();
//			if (em.getTransaction().isActive()) {
//				em.getTransaction().rollback();
//			}
//		} finally {
//			if (em.isOpen()) {
//				em.close();
//			}
//		}
//	}
//
//	@Override
//	public List<Smartphone> getSmartPhones() {
//		// TODO Auto-generated method stub
//		EntityManager em = transactionManager.getEntityManagerFactory()
//				.createEntityManager();
//
//		List<Smartphone> myTableList = null;
//		try {
//			em.getTransaction().begin();
//			myTableList = myTableDao.getSmartphones(em);
//			em.getTransaction().commit();
//		} catch (Exception e) {
//			e.printStackTrace();
//			if (em.getTransaction().isActive()) {
//				em.getTransaction().rollback();
//			}
//		} finally {
//			if (em.isOpen()) {
//				em.close();
//			}
//		}
//
//		return myTableList;
//	}
//
//	@Override
//	public void insertSmartphone(Smartphone phone) {
//		// TODO Auto-generated method stub
//		EntityManager em = transactionManager.getEntityManagerFactory()
//				.createEntityManager();
//
//		try {
//			em.getTransaction().begin();
//			myTableDao.insertSmartPhone(em, phone);
//			em.getTransaction().commit();
//		} catch (Exception e) {
//			e.printStackTrace();
//			if (em.getTransaction().isActive()) {
//				em.getTransaction().rollback();
//			}
//		} finally {
//			if (em.isOpen()) {
//				em.close();
//			}
//		}
//	}
//
//	@Override
//	public void updateSmartphone(Smartphone phone) {
//		// TODO Auto-generated method stub
//		EntityManager em = transactionManager.getEntityManagerFactory()
//				.createEntityManager();
//
//		try {
//			em.getTransaction().begin();
//			myTableDao.updateSmartPhone(em, phone);
//			em.getTransaction().commit();
//		} catch (Exception e) {
//			e.printStackTrace();
//			if (em.getTransaction().isActive()) {
//				em.getTransaction().rollback();
//			}
//		} finally {
//			if (em.isOpen()) {
//				em.close();
//			}
//		}
//	}
//
//}
