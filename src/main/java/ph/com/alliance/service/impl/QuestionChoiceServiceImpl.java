package ph.com.alliance.service.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import ph.com.alliance.dao.ChoiceDao;
import ph.com.alliance.dao.QuestionChoiceDao;
import ph.com.alliance.entity.QuestionChoice;
import ph.com.alliance.service.QuestionChoiceService;

@Service("questionChoiceService")
public class QuestionChoiceServiceImpl implements QuestionChoiceService{
	
	@Autowired
	private QuestionChoiceDao qcDao;
	
	@Autowired
	private ChoiceDao cDao;
	
	@Autowired
	private JpaTransactionManager transactionManager;
	
	@Override
	public void setQuestionChoice(int qid, int cid) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		em.getTransaction().begin();
		try {
			qcDao.setQuestionChoice(em, qid, cid);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
	}

	@Override
	public List<QuestionChoice> getQuestionChoices() {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<QuestionChoice> qc = null;
		
		try{
			qc = qcDao.getQuestionChoices(em);
		} catch(Exception e){
			System.out.println(e.getMessage());
		}
		
		return qc;
	}

	@Override
	public void deleteQuestionChoicesByQId(int qid) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		em.getTransaction().begin();
		try {
			cDao.deleteChoicesByQId(em, qid);
			qcDao.deleteQuestionChoicesByQId(em, qid);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
	}

}
