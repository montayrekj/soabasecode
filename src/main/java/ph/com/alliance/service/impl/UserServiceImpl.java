package ph.com.alliance.service.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import ph.com.alliance.dao.UserDao;
import ph.com.alliance.entity.User;
import ph.com.alliance.service.UserService;

@Service("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	private JpaTransactionManager transactionManager;

	@Autowired
	private UserDao userDao;

	@Override
	public boolean createUser(User u) {
		EntityManager pEM = transactionManager.getEntityManagerFactory()
				.createEntityManager();

		boolean success = false;
		pEM.getTransaction().begin();
		try {
			success = userDao.createUser(pEM, u);
			pEM.getTransaction().commit();
		} catch (Exception e) {
			if (pEM.getTransaction().isActive())
				pEM.getTransaction().rollback();
			success = false;
		} finally {
			if (pEM.isOpen())
				pEM.close();
		}

		return success;
	}

	@Override
	public boolean updateUser(User u) {
		EntityManager pEM = transactionManager.getEntityManagerFactory()
				.createEntityManager();
		User oldUser = this.selectUser(u.getUserId());
		
		oldUser.setUserType(u.getUserType());
		oldUser.setAddress(u.getAddress());
		oldUser.setEmail(u.getEmail());
		
		User updatedUser = null;
		pEM.getTransaction().begin();
		try {
			updatedUser = userDao.updateUser(pEM, oldUser);
			pEM.getTransaction().commit();
		} catch (Exception e) {
			if (pEM.getTransaction().isActive())
				pEM.getTransaction().rollback();
		} finally {
			if (pEM.isOpen())
				pEM.close();
		}

		return (updatedUser != null);
	}

	@Override
	public List<User> selectAllUsers() {
		EntityManager pEM = transactionManager.getEntityManagerFactory()
				.createEntityManager();

		List<User> userList = null;
		try {
			userList = userDao.selectAllUsers(pEM);
		} catch (Exception e) {
			System.out.print(e.getMessage());
		} finally {
			if (pEM.isOpen()) {
				pEM.close();
			}
		}

		return userList;
	}

	@Override
	public boolean deleteUser(int uID) {
		EntityManager pEM = transactionManager.getEntityManagerFactory()
				.createEntityManager();
		boolean success = false;

		pEM.getTransaction().begin();
		try {
			success = userDao.deleteUser(pEM, uID);
			pEM.getTransaction().commit();
			return true;
		} catch (Exception e) {
			if (pEM.getTransaction().isActive())
				pEM.getTransaction().rollback();
			e.printStackTrace();
			success = false;
		} finally {
			if (pEM.isOpen())
				pEM.close();
		}

		return success;
	}

	@Override
	public User selectUser(int uID) {
		EntityManager pEM = transactionManager.getEntityManagerFactory()
				.createEntityManager();

		User u = null;
		try {
			u = userDao.selectUser(pEM, uID);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (pEM.isOpen())
				pEM.close();
		}

		return u;
	}

	@Override
	public int generateUserId() {
		EntityManager pEM = transactionManager.getEntityManagerFactory()
				.createEntityManager();
		
		int id = -999;
		try {
			id = userDao.generateUserId(pEM);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (pEM.isOpen())
				pEM.close();
		}
		
		return id;
	}

}
