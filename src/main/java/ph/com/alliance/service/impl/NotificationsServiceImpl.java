package ph.com.alliance.service.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import ph.com.alliance.dao.NotificationsDao;
import ph.com.alliance.entity.Notification;
import ph.com.alliance.service.NotificationsService;

@Service("notificationsService")
public class NotificationsServiceImpl implements NotificationsService{

	@Autowired
	private NotificationsDao notifDao;

	@Autowired
	private JpaTransactionManager transactionManager;
	
	@Override
	public List<Notification> getNotificationsByUser(int id) {
		// TODO Auto-generated method stub
		EntityManager em = transactionManager.getEntityManagerFactory()
				.createEntityManager();
		List<Notification> notifList = null;
		
		em.getTransaction().begin();
		try {
			notifList = notifDao.getNotificationsByUser(em, id);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return notifList;
	}

}
