package ph.com.alliance.service.impl;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.RollbackException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import ph.com.alliance.dao.AnnualPlanDao;
import ph.com.alliance.dao.TrainingDao;
import ph.com.alliance.entity.AnnualPlan;
import ph.com.alliance.service.AnnualPlanService;

@Service("annualPlanService")
public class AnnualPlanServiceImpl implements AnnualPlanService {

	@Autowired
	private JpaTransactionManager transactionManager;
	
	@Autowired
	private AnnualPlanDao planDao;
	
	@Autowired
	private TrainingDao trainingDao;
	
	@Override
	public List<AnnualPlan> getAllAnnualPlan(){
		EntityManager pEM = transactionManager.getEntityManagerFactory().createEntityManager();

		try{
			List<AnnualPlan> apl = planDao.getAllAnnualPlan(pEM);
			LocalDate localDate = LocalDate.now();
			String year = Integer.toString(localDate.getYear());
			String month = Integer.toString(localDate.getMonthValue());
			String day = Integer.toString(localDate.getDayOfMonth());
			String today = year + "-" + month + "-" + day;
			for(AnnualPlan ap:apl){
				ap.setPendingEvents(trainingDao.countPendingEvents(pEM, ap.getId(), today));
				ap.setOngoingEvents(trainingDao.countOngoingEvents(pEM, ap.getId(), today));
				ap.setFinishedEvents(trainingDao.countFinishedEvents(pEM, ap.getId(), today));
			}
			
			return apl;
		} catch(Exception e){
			e.printStackTrace();
		} finally{
			if(pEM.isOpen())
				pEM.close();
		}

		return null;
	}

	@Override
	public AnnualPlan retrievePlanInfo(int planId){
		EntityManager pEM = transactionManager.getEntityManagerFactory().createEntityManager();

		try{
			SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			String today = sm.format(date).toString();
			AnnualPlan ap = planDao.retrievePlanInfo(pEM, planId);
			ap.setPendingEvents(trainingDao.countPendingEvents(pEM, planId, today.toString()));
			ap.setOngoingEvents(trainingDao.countOngoingEvents(pEM, planId, today.toString()));
			ap.setFinishedEvents(trainingDao.countFinishedEvents(pEM, planId, today.toString()));
			return ap;
		} catch(Exception e){
			e.printStackTrace();
		} finally{
			if(pEM.isOpen())
				pEM.close();
		}

		return null;
	}

	@Override
	public boolean createAnnualPlan(AnnualPlan ap){
		EntityManager pEM = transactionManager.getEntityManagerFactory().createEntityManager();

		pEM.getTransaction().begin();
		try{
			planDao.createAnnualPlan(pEM, ap);
			pEM.getTransaction().commit();
			return true;
		} catch(RollbackException e){
			System.err.println(e.getMessage());
			if(pEM.getTransaction().isActive())
				pEM.getTransaction().rollback();
		} finally{
			if(pEM.isOpen())
				pEM.close();
		}

		return false;
	}
}
