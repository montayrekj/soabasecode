package ph.com.alliance.service.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import ph.com.alliance.dao.FormSCDao;
import ph.com.alliance.entity.Form;
import ph.com.alliance.entity.FormSc;
import ph.com.alliance.service.FormSCService;
@Service("formSCService")
public class FormSCServiceImpl implements FormSCService{

	@Autowired
	private FormSCDao formSCDao;
	
	@Autowired
	private JpaTransactionManager transactionManager;
	/*
	@Override
	public List<FormSc> getSubCategoryList(int categoryId) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<FormSc> myList = null;
		
		try {
			myList = formSCDao.getSubCategoryList(em, categoryId);
		} catch (Exception e) {
			System.out.print(e.getMessage());
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return myList;
	}

	@Override
	public List<FormSc> getSuperSubCategoryList(int subCategoryId) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<FormSc> myList = null;
		
		try {
			myList = formSCDao.getSuperSubCategoryList(em, subCategoryId);
		} catch (Exception e) {
			System.out.print(e.getMessage());
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return myList;
	}
*/
	@Override
	public List<Form> getCategoryList() {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<Form> myList = null;
		
		try {
			myList = formSCDao.getAllCategory(em);
		} catch (Exception e) {
			System.out.print(e.getMessage());
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return myList;
	}

	@Override
	public List<FormSc> getSubCategoryList() {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<FormSc> myList = null;
		
		try {
			myList = formSCDao.getAllSubCategoryList(em);
		} catch (Exception e) {
			System.out.print(e.getMessage());
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return myList;
	}

	@Override
	public List<FormSc> getSuperSubCategoryList() {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<FormSc> myList = null;
		
		try {
			myList = formSCDao.getAllSuperSubCategoryList(em);
		} catch (Exception e) {
			System.out.print(e.getMessage());
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return myList;
	}

}
