package ph.com.alliance.service.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import ph.com.alliance.dao.TrainingAttendanceDao;
import ph.com.alliance.entity.Training;
import ph.com.alliance.entity.TrainingAttendance;
import ph.com.alliance.service.TrainingAttendanceService;

@Service("trainingAttendanceService")
public class TrainingAttendanceServiceImpl implements TrainingAttendanceService{

	@Autowired
	private JpaTransactionManager transactionManager;
	
	@Autowired
	private TrainingAttendanceDao attendanceDao;
	
	@Override
	public void insertAttendance(TrainingAttendance ta) {
		EntityManager pEM = transactionManager.getEntityManagerFactory().createEntityManager();
		
		pEM.getTransaction().begin();
		try{
			attendanceDao.insertUpdateParticipantAttendance(pEM, ta);
			pEM.getTransaction().commit();
		}catch(Exception e){
			if(pEM.getTransaction().isActive())
				pEM.getTransaction().rollback();
			e.printStackTrace();
		}finally{
			if(pEM.isOpen())
				pEM.close();
		}
	}

	@Override
	public List<TrainingAttendance> getAttendance(int trainingId) {
		EntityManager pEM = transactionManager.getEntityManagerFactory().createEntityManager();
		
		try{
			return attendanceDao.getAttendanceByTraining(pEM, trainingId);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(pEM.isOpen())
				pEM.close();
		}
		
		return null;
	}

	@Override
	public void updateAttendance(TrainingAttendance ta) {
		EntityManager pEM = transactionManager.getEntityManagerFactory().createEntityManager();
		
		pEM.getTransaction().begin();
		try{
			attendanceDao.updateAttendance(pEM, ta);
			pEM.getTransaction().commit();
		}catch(Exception e){
			if(pEM.getTransaction().isActive())
				pEM.getTransaction().rollback();
			e.printStackTrace();
		}finally{
			if(pEM.isOpen())
				pEM.close();
		}
		
	}

	@Override
	public int deleteOnTrainingUpdate(Training t) {
		EntityManager pEM = transactionManager.getEntityManagerFactory().createEntityManager();
		int res = 0;
		
		pEM.getTransaction().begin();
		try{
			res = attendanceDao.deleteOnTrainingUpdate(pEM, t);
			pEM.getTransaction().commit();
		}catch(Exception e){
			if(pEM.getTransaction().isActive())
				pEM.getTransaction().rollback();
			e.printStackTrace();
		}finally{
			if(pEM.isOpen())
				pEM.close();
		}
		
		return res;
	}

	@Override
	public TrainingAttendance retrieveAttendanceById(int attendanceId) {
		EntityManager pEM = transactionManager.getEntityManagerFactory().createEntityManager();
		
		try{
			return attendanceDao.retrieveAttendanceById(pEM, attendanceId);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(pEM.isOpen())
				pEM.close();
		}
		
		return null;
	}

	@Override
	public List<TrainingAttendance> retrieveAttendanceByParticipant(int partiId) {
		EntityManager pEM = transactionManager.getEntityManagerFactory().createEntityManager();
		
		try{
			return attendanceDao.retrieveAttendanceByParticipant(pEM, partiId);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(pEM.isOpen())
				pEM.close();
		}
		
		return null;
	}

}
