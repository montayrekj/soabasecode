package ph.com.alliance.service.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import ph.com.alliance.dao.EmployeeDao;
import ph.com.alliance.entity.Employee;
import ph.com.alliance.service.EmployeeService;

@Service("employeeService")
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private JpaTransactionManager transactionManager;
	
	@Autowired
	private EmployeeDao employeeDao;
	
	@Override
	public List<Employee> selectEmployeeByLastName(String lastName) {
		EntityManager pEM = transactionManager.getEntityManagerFactory().createEntityManager();
		System.out.println("Last Name: " + lastName);
		List<Employee> employeeList = null;
		try {
			employeeList = employeeDao.selectEmployeesByLastName(pEM, lastName);
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if (pEM.isOpen())
				pEM.close();
		}
		
		return employeeList;
	}

}
