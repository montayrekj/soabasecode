package ph.com.alliance.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.RollbackException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import ph.com.alliance.dao.TrainingParticipantDao;
import ph.com.alliance.entity.TrainingParticipant;
import ph.com.alliance.service.TrainingParticipantService;

@Service("trainingParticipantService")
public class TrainingParticipantServiceImpl implements
		TrainingParticipantService {

	@Autowired
	private JpaTransactionManager transactionManager;
	
	@Autowired
	private TrainingParticipantDao participantDao;
	
	@Override
	public List<TrainingParticipant> getTrainingParticipants(int trainingId) {
		EntityManager pEM = transactionManager.getEntityManagerFactory()
				.createEntityManager();
		try {
			return participantDao.getAllParticipantPerTraining(pEM, trainingId);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (pEM.isOpen())
				pEM.close();
		}

		return null;
	}

	@Override
	public boolean addTrainingParticipant(TrainingParticipant tp) {
		EntityManager pEM = transactionManager.getEntityManagerFactory()
				.createEntityManager();
		pEM.getTransaction().begin();
		try {
			participantDao.addTrainingParticipant(pEM, tp);
			pEM.getTransaction().commit();
			return true;
		} catch (RollbackException e) {
			System.err.println(e.getMessage());
			if (pEM.getTransaction().isActive())
				pEM.getTransaction().rollback();
		} finally {
			if (pEM.isOpen())
				pEM.close();
		}

		return false;
	}

	@Override
	public boolean deleteTrainingParticipant(TrainingParticipant tp) {
		EntityManager em = transactionManager.getEntityManagerFactory()
				.createEntityManager();
		em.getTransaction().begin();
		try {
			participantDao.deleteTrainingParticipant(em, tp);
			em.getTransaction().commit();
			return true;
		} catch (RollbackException e) {
			System.err.println(e.getMessage());
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
		} finally {
			if (em.isOpen())
				em.close();
		}
		
		return false;
	}

	@Override
	public TrainingParticipant retrieveParticipant(int participantId) {
		EntityManager pEM = transactionManager.getEntityManagerFactory().createEntityManager();
		
		try{
			return participantDao.retrieveParticipant(pEM, participantId);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(pEM.isOpen())
				pEM.close();
		}
		return null;
	}

}
