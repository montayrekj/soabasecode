package ph.com.alliance.service.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import ph.com.alliance.dao.ChoiceDao;
import ph.com.alliance.entity.Choice;
import ph.com.alliance.entity.QuestionChoice;
import ph.com.alliance.service.ChoiceService;
@Service("choiceService")
public class ChoiceServiceImpl implements ChoiceService{

	@Autowired
	private ChoiceDao cDao;
	
	@Autowired
	private JpaTransactionManager transactionManager;
	
	@Override
	public void addChoice(String name) {
		// TODO Auto-generated method stub
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		em.getTransaction().begin();
		try {
			cDao.addChoice(em, name);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
	}

	@Override
	public int getChoiceByName(String name) {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		int id=0;
		
		try{
			id = cDao.getChoiceByName(em, name);
		} catch(Exception e){
			System.out.println(e.getMessage());
		}
		
		return id;
	}

	@Override
	public List<Choice> getChoices() {
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<Choice> c = null;
		
		try{
			c = cDao.getChoices(em);
		} catch(Exception e){
			System.out.println(e.getMessage());
		}
		
		return c;
	}

}
