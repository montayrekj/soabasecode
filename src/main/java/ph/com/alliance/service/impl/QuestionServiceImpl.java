package ph.com.alliance.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import ph.com.alliance.dao.QAnswerDao;
import ph.com.alliance.dao.QuestionsDao;
import ph.com.alliance.dao.TrainingParticipantDao;
import ph.com.alliance.entity.Qanswer;
import ph.com.alliance.entity.Question;
import ph.com.alliance.model.Answer;
import ph.com.alliance.service.QuestionService;

@Service("questionService")
public class QuestionServiceImpl implements QuestionService{

	@Autowired
	private QuestionsDao questionDao;
	
	@Autowired
	private QAnswerDao qAnswerDao;
	
	@Autowired
	private TrainingParticipantDao tpDao;
	
	@Autowired
	private JpaTransactionManager transactionManager;

	private Question q;
	
	@Override
	public List<Question> getQuestions() {
		// TODO Auto-generated method stub
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<Question> questionList = null;
		
		try {
			questionList = questionDao.getQuestions(em);
		} catch (Exception e) {
			System.out.print(e.getMessage());
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return questionList;
	}

	@Override
	public boolean addAnswers(List<Answer> answer, String userType, int uId, int tId) {
		// TODO Auto-generated method stub
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		boolean result = false;
		
		em.getTransaction().begin();
		try {
			for(Answer item:answer){
				if(!item.getValue().equals("") && !item.getName().equals("")){
	    			if(userType.equals("supervisor")){
	    				String temp = item.getName();
	    				System.out.println(temp);
	    				Qanswer qa = null;
	    				
	    				
	    				qa = qAnswerDao.getQanswer(em, Integer.parseInt(temp.trim()), uId, tId);
	    				qa.setQId(Integer.parseInt(temp.trim()));
	    				qa.setSpAnswer(item.getValue());
	    				System.out.println("QA: " + qa);
	    				
	    				result = qAnswerDao.setAnswer(em, qa);
	    			} else if(userType.equals("employee")){
	    				String temp = item.getName();
	    				System.out.println(temp);
	    				Qanswer qa = null;
	    				
	    				qa = qAnswerDao.getQanswer(em, Integer.parseInt(temp.trim()), uId, tId);
	    				qa.setQId(Integer.parseInt(temp.trim()));
	    				qa.setAnswer(item.getValue());
	    				
	    				
	    				result = qAnswerDao.setAnswer(em, qa);
	    			} else {
	    				String temp = item.getName();
	    				System.out.println(temp);
	    				Qanswer qa = null;
	    				
	    				qa = qAnswerDao.getQanswer(em, Integer.parseInt(temp.trim()), uId, tId);
	    				qa.setQId(Integer.parseInt(temp.trim()));
	    				qa.setAnswer(item.getValue());
	    				
	    				result = qAnswerDao.setAnswer(em, qa);
	    			}
				}
    		}
			
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		
		return result;
		
	}

	@Override
	public List<Question> getQuestionsByForm(int id) {
		// TODO Auto-generated method stub
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		List<Question> questions = null;
		
		try{
			questions = questionDao.getQuestionsByForm(em, id);
		} catch(Exception e){
			System.out.println(e.getMessage());
		}
		
		return questions;
	}
	

	@Override
	public boolean addQuestion(Question q) {
		// TODO Auto-generated method stub
		boolean success=false;
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		em.getTransaction().begin();
		try {
			success = questionDao.addQuestion(em, q);
			System.out.println(q);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			success = false;
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		return success;
	}

	@Override
	public boolean deleteQuestionById(int id) {
		boolean success=false;
		EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
		em.getTransaction().begin();
		try {
			success = questionDao.deleteQuestionById(em, id);
			System.out.println(q);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			success = false;
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		return success;
	}

	@Override
    public int getQuestionsByName(String name) {
        EntityManager em = transactionManager.getEntityManagerFactory().createEntityManager();
        int id=0;
        
        try{
            id = questionDao.getQuestionsByName(em, name);
        } catch(Exception e){
            System.out.println(e.getMessage());
        }
        
        return id;
    }

	@Override
	public List<Object> getEffectivenessFeedback(int trainingId) {
		EntityManager pEM = transactionManager.getEntityManagerFactory().createEntityManager();
		List<Question> qList = null;
		List<Object> oList = new ArrayList<Object>();
		try{
			qList = questionDao.getQuestionsByForm(pEM, 4);
			if(qList != null){
				for(Question q:qList){
					if(q.getAssessType().equals("multiple")){
						System.out.println(q.getIdQuestions());
						Object o = qAnswerDao.getEffectivenessFeedbackByQuestion(pEM, trainingId, q.getIdQuestions());
						System.out.println(o);
						oList.add(o);
					}
				}
			}
			return oList;
			//return qAnswerDao.getEffectivenessFeedback(pEM, trainingId);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(pEM.isOpen())
				pEM.close();
		}
		
		return null;
	}

	@Override
	public List<Object> getEffectivenessRating(int trainingId) {
		EntityManager pEM = transactionManager.getEntityManagerFactory().createEntityManager();
		List<Question> qList = null;
		List<Object> oList = new ArrayList<Object>();
		try{
			qList = questionDao.getQuestionsByForm(pEM, 4);
			if(qList != null){
				for(Question q:qList){
					if(q.getAssessType().equals("multiple2")){
						System.out.println(q.getIdQuestions());
						Object o = qAnswerDao.getEffectivenessRatingByQuestion(pEM, trainingId, q.getIdQuestions());
						System.out.println(o);
						oList.add(o);
					}
				}
			}
			return oList;
			//return qAnswerDao.getEffectivenessRating(pEM, trainingId);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(pEM.isOpen())
				pEM.close();
		}
		return null;
	}

	@Override
	public List<Qanswer> getAllEffectivenessAnswer(int trainingId) {
		EntityManager pEM = transactionManager.getEntityManagerFactory().createEntityManager();
		
		try{
			return qAnswerDao.getAllEffectivessAnswer(pEM, trainingId);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(pEM.isOpen())
				pEM.close();
		}
		return null;
	}

	@Override
	public Object[] getPreAssesment(int partiId) {
		EntityManager pEM = transactionManager.getEntityManagerFactory().createEntityManager();
		try{
			Object generic = qAnswerDao.getPreGenericAssesment(pEM, partiId).get(0)[1];
			Object technical = qAnswerDao.getPreTechnicalAssesment(pEM, partiId).get(0)[1];
			Object se = qAnswerDao.getPreSEAssesment(pEM, partiId).get(0)[1];
			Object[] ret = new Object[3];
			ret[0] = null != generic?generic:0;
			ret[1] = null != technical?technical:0;
			ret[2] = null != se?se:0;
			return ret;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(pEM.isOpen())
				pEM.close();
		}
		return null;
	}

	@Override
	public Object[] getPostAssesment(int partiId) {
		EntityManager pEM = transactionManager.getEntityManagerFactory().createEntityManager();
		try{
			Object generic = qAnswerDao.getPostGenericAssesment(pEM, partiId).get(0)[1];
			Object technical = qAnswerDao.getPostTechnicalAssesment(pEM, partiId).get(0)[1];
			Object se = qAnswerDao.getPostSEAssesment(pEM, partiId).get(0)[1];
			Object[] ret = new Object[3];
			ret[0] = null != generic?generic:0;
			ret[1] = null != technical?technical:0;
			ret[2] = null != se?se:0;
			return ret;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(pEM.isOpen())
				pEM.close();
		}
		return null;
	}
	
}
