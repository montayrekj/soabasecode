package ph.com.alliance.service.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;

import ph.com.alliance.dao.TrainingDao;
import ph.com.alliance.entity.Training;
import ph.com.alliance.service.TrainingService;

@Service("trainingService")
public class TrainingServiceImpl implements TrainingService {

	@Autowired
	private TrainingDao trainingDao;

	@Autowired
	private JpaTransactionManager transactionManager;

	@Override
	public List<Training> retrieveTrainingsByParticipant(int id) {
		EntityManager em = transactionManager.getEntityManagerFactory()
				.createEntityManager();
		List<Training> userTrainingList = null;
		try {
			userTrainingList = trainingDao.retrieveTrainingsByParticipant(em,
					id);
		} catch (Exception e) {
			System.out.print(e.getMessage());
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		return userTrainingList;
	}

	@Override
	public List<Training> retrieveTrainingsByFacilitator(int facilID) {
		EntityManager em = transactionManager.getEntityManagerFactory()
				.createEntityManager();
		List<Training> facilTrainingList = null;
		try {
			facilTrainingList = trainingDao.retrieveTrainingsByFacilitator(em,facilID);
		} catch (Exception e) {
			System.out.print(e.getMessage());
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		return facilTrainingList;
	}

	@Override
	public Training retrieveTraining(int trainingID) {
		EntityManager em = transactionManager.getEntityManagerFactory()
				.createEntityManager();
		Training tr = null;
		try {
			tr = trainingDao.retriveTraining(em, trainingID);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		return tr;
	}

	@Override
	public List<Object[]> retrieveSubordinateTrainingList(int supervisorID) {
		EntityManager em = transactionManager.getEntityManagerFactory()
				.createEntityManager();
		List<Object[]> stList = null;
		try {
			stList = trainingDao.retrieveSubordinateTrainingList(em, supervisorID);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (em.isOpen()) {
				em.close();
			}
		}
		return stList;
	}

	@Override
	public List<Training> getAllTrainingsPerPlan(int planId) {
		EntityManager pEM = transactionManager.getEntityManagerFactory()
				.createEntityManager();

		try {
			return trainingDao.getAllTrainingsPerPlan(pEM, planId);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (pEM.isOpen())
				pEM.close();
		}

		return null;
	}

	@Override
	public boolean createUpdateTraining(Training t) {
		EntityManager pEM = transactionManager.getEntityManagerFactory()
				.createEntityManager();

		pEM.getTransaction().begin();
		try {
			trainingDao.createUpdateTraining(pEM, t);
			pEM.getTransaction().commit();
			return true;
		} catch (Exception e) {
			if (pEM.getTransaction().isActive())
				pEM.getTransaction().rollback();
		} finally {
			if (pEM.isOpen())
				pEM.close();
		}

		return false;
	}

	@Override
	public int countTrainings() {
		EntityManager pEM = transactionManager.getEntityManagerFactory()
				.createEntityManager();

		try {
			return (int) trainingDao.countAllTrainings(pEM);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (pEM.isOpen())
				pEM.close();
		}

		return 0;
	}

	@Override
	public boolean deleteTraining(int trainingId) {
		EntityManager pEM = transactionManager.getEntityManagerFactory().createEntityManager();
		
		pEM.getTransaction().begin();
		try{
			trainingDao.deleteTraining(pEM, trainingId);
			pEM.getTransaction().commit();
			return true;
		}catch(Exception e){
			if(pEM.getTransaction().isActive())
				pEM.getTransaction().rollback();
			e.printStackTrace();
		}finally{
			if(pEM.isOpen())
				pEM.close();
		}
		
		return false;
	}

}
