package ph.com.alliance.service;

import java.util.List;

import ph.com.alliance.entity.Form;
import ph.com.alliance.entity.FormSc;

public interface FormSCService {
	public List<FormSc> getSubCategoryList();
	public List<FormSc> getSuperSubCategoryList();
	public List<Form> getCategoryList();
}
