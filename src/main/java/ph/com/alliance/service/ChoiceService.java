package ph.com.alliance.service;

import java.util.List;

import ph.com.alliance.entity.Choice;


public interface ChoiceService {
	public void addChoice(String name);
	public int getChoiceByName(String name);
	public List<Choice> getChoices();
}
