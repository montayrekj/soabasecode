package ph.com.alliance.service;

import java.util.List;

import ph.com.alliance.entity.Training;

public interface TrainingService {
	public List<Training> retrieveTrainingsByParticipant(int id);
	public Training retrieveTraining(int trainingID);
	public List<Training> retrieveTrainingsByFacilitator(int userId);
	public List<Object[]> retrieveSubordinateTrainingList(int supervisorID);
	
	/**
	 * 
	 * @return
	 */
	public List<Training> getAllTrainingsPerPlan(int planId);

	/**
	 * @param t
	 * @return
	 */
	public boolean createUpdateTraining(Training t);

	/**
	 * 
	 * @return
	 */
	public int countTrainings();
	
	/**
	 * 
	 * @return
	 */
	public boolean deleteTraining(int trainingId);
		
}
