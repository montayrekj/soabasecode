package ph.com.alliance.service;

import java.util.List;

import ph.com.alliance.entity.Training;
import ph.com.alliance.entity.TrainingAttendance;

public interface TrainingAttendanceService {
	
	/*
	 * @param trainingId
	 * 
	 */
	public List<TrainingAttendance> getAttendance(int trainingId);
	
	/*
	 * @param ta
	 * 
	 */
	public void insertAttendance(TrainingAttendance ta);
	
	/*
	 * @param ta
	 * 
	 */
	public void updateAttendance(TrainingAttendance ta);
	
	/*
	 * @param t
	 */
	public int deleteOnTrainingUpdate(Training t);
	
	/*
	 * @param attendanceId
	 */
	public TrainingAttendance retrieveAttendanceById(int attendanceId);
	
	/*
	 * @param partiId
	 */
	public List<TrainingAttendance> retrieveAttendanceByParticipant(int partiId);
}
