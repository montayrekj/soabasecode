package ph.com.alliance.service;

import java.util.List;

import ph.com.alliance.entity.Employee;

public interface EmployeeService {
	
	public List<Employee> selectEmployeeByLastName(String lastName);

}
