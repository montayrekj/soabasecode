package ph.com.alliance.service;

import java.util.Date;

public interface UtilitiesService {
	public String getDateString(Date date);
	public String convertDateFormat(String dateString);
}
