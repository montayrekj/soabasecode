
package ph.com.alliance.service;

import java.util.List;

import ph.com.alliance.entity.TrainingFacilitator;
import ph.com.alliance.entity.TrainingParticipant;
import ph.com.alliance.entity.User;

/**
 * 
 * 
 */
public interface DBTransactionTestService {
	/**
	 * 
	 * @return
	 */
	public boolean createUser(User pUser);
	
	/**
	 * 
	 */
	public User updateUser(User pUser);
	
	/**
	 * 
	 */
	public void deleteUser(User pUser);
	
	/**
	 * 
	 * @return
	 */
	public User retrieveUser(int uId);
	
	/**
	 * 
	 * @return
	 */
	public List<User> selectUsers(String pKey);
	
	
	/**
	 * 
	 * @return
	 */
	public List<User> selectAllUsers();
	
	/**
	 * 
	 * @return
	 */
	public boolean updateTrainingParticipant(TrainingParticipant tp);
	
	/**
	 * 
	 * @return
	 */
	public boolean updateTrainingFacilitator(TrainingFacilitator tf);
	
	/**
	 * 
	 * @return
	 */
	public boolean releaseForm(TrainingParticipant tp);
	
	/**
	 * 
	 * @return
	 */
	public boolean releaseForm2(TrainingFacilitator tf);
	
	/**
	 * 
	 * @return
	 */
	public TrainingParticipant getParticipant(int id, int training_id);
	
	/**
	 * 
	 * @return
	 */
	public TrainingFacilitator getFacilitator(int id, int training_id);

	/*-------------------- MULTI TABLE TRANSASCTION TESTS -----------------------*/
	//public boolean createUserAndProduct(User pUser, Product pProd, boolean pRollbackFlag);
	//public boolean createUserAndUpdateProduct(User pUser, Product pProd, boolean pRollbackFlag);
	
	
	public User findUser(String username, String password);
	public boolean loginUser(String username, String password);
	public boolean upgradeUser(String id);
	public User findEmployee(int userID);
}
