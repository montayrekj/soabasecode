package ph.com.alliance.service;

import java.util.List;

import ph.com.alliance.entity.Effectiveness;
import ph.com.alliance.model.Teeanswer;

public interface TeeService {
	public List<Effectiveness> getAllQuestion();
	
	public boolean setAnswer(List<Teeanswer> answer);
}
