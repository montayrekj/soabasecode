package ph.com.alliance.service;

import java.util.List;

import ph.com.alliance.entity.QuestionChoice;


public interface QuestionChoiceService {
	public void setQuestionChoice(int qid,int cid);
	public List<QuestionChoice> getQuestionChoices();
	public void deleteQuestionChoicesByQId(int qid);
}
