package ph.com.alliance.service;

import java.util.List;

import ph.com.alliance.entity.Form;

public interface FormService {
	public List<Form> getAllForms();
}
